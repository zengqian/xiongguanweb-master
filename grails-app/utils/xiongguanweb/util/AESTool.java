package xiongguanweb.util;


import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class AESTool {
    private static final String KEY_AES = "AES";

    public static String encrypt(String src, String key) throws Exception {
        if (key == null || key.length() != 16) {
            throw new Exception("key不满足条件");
        }
        byte[] raw = key.getBytes();
        SecretKeySpec skeySpec = new SecretKeySpec(raw, KEY_AES);
        Cipher cipher = Cipher.getInstance(KEY_AES);
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        byte[] encrypted = cipher.doFinal(src.getBytes());
        return byte2hex(encrypted);
    }

    public static String encryptBase64(String src, String key) throws Exception {
        if (key == null || key.length() != 16) {
            throw new Exception("key不满足条件");
        }
        byte[] raw = key.getBytes();
        SecretKeySpec skeySpec = new SecretKeySpec(raw, KEY_AES);
        Cipher cipher = Cipher.getInstance(KEY_AES);
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
        byte[] encrypted = cipher.doFinal(src.getBytes());
        return Base64.encode(encrypted);
    }

    public static String decrypt(String src, String key) throws Exception {
        if (key == null || key.length() != 16) {
            throw new Exception("key不满足条件");
        }
        byte[] raw = key.getBytes();
        SecretKeySpec skeySpec = new SecretKeySpec(raw, KEY_AES);
        Cipher cipher = Cipher.getInstance(KEY_AES);
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] encrypted1 = hex2byte(src);
        byte[] original = cipher.doFinal(encrypted1);
        String originalString = new String(original);
        return originalString;
    }

    public static String decryptBase64(String src, String key) throws Exception {
        if (key == null || key.length() != 16) {
            throw new Exception("key不满足条件");
        }
        byte[] raw = key.getBytes();
        SecretKeySpec skeySpec = new SecretKeySpec(raw, KEY_AES);
        Cipher cipher = Cipher.getInstance(KEY_AES);
        cipher.init(Cipher.DECRYPT_MODE, skeySpec);
        byte[] encrypted1 = Base64.decode(src);
        byte[] original = cipher.doFinal(encrypted1);
        String originalString = new String(original,"UTF-8");
        return originalString;
    }

    public static byte[] hex2byte(String strhex) {
        if (strhex == null) {
            return null;
        }
        int l = strhex.length();
        if (l % 2 == 1) {
            return null;
        }
        byte[] b = new byte[l / 2];
        for (int i = 0; i != l / 2; i++) {
            b[i] = (byte) Integer.parseInt(strhex.substring(i * 2, i * 2 + 2),
                    16);
        }
        return b;
    }

    public static String byte2hex(byte[] b) {
        String hs = "";
        String stmp = "";
        for (int n = 0; n < b.length; n++) {
            stmp = (Integer.toHexString(b[n] & 0XFF));
            if (stmp.length() == 1) {
                hs = hs + "0" + stmp;
            } else {
                hs = hs + stmp;
            }
        }
        return hs.toUpperCase();
    }

    public static void main(String[] args) throws Exception {
//        String string="eGTK7wMvkCdS/ZUKg98r8Z4N+4vMGa8OffqaROkCNQ5UVL4lM9cW57Eu3CxZresdu2k4s/HuCFXB8ddDcWveDvyQOageC+7/EBPYTG3MV72tfhIXcelfBLet/r9Q+SDYgdQtNaovO+XmfTt7NNysofjx+KoDcdSI5wHFfVQwjN2Nr9icTmLHht0lrDvFel5zpBMGmD1nY8csWbNosHMuk3/xduI7NFGS83giqEGEXZDWro+tJ+24ayoRgJn41Y1Mz2fRV+fBiGsLUtpLPyrsYpfB26cFgFO3t+c3vRH9xCc=";
//        String key = "f15831e69d484a41";
//        String s = AESTool.decryptBase64(string, key);
        String s ="uckKECdw7GF+OVSJ9AF+Cgl1YT6VYESFQWbTb2I3udK213U/+g6GBC2PGWy2mAU0rPXe0v68oJutotzd8w5s9+Zd/yZ6CYyAERm59pBCP0a3SmJf81Dixxw4slkBC1Ubqul6YZifij1NLQZa2pZdL+ZUbwHfU5YBO++dvm3XvI5Qv9B4I8RlF5KSuKW4xoXjvIJ2eRZ4dz07osZqSmqbnZmJSv3KYpLJWrttrvp2ycnNarcS7Fvzd5wiZm574HfgK3MqkT307wDPsneNB5iQ0ZSTj0A3W7/JTplc9ROirUP/F+2mtzVgznbTy5wr+P9zOTA/0Gq+q9/qh3MNED0DQZiFmlrIADYHheyh1KpfG8uEdK2q4xO2wVn1NkM9sHudeMv5dIMCWLkeT0bQQmUlIWdQ8VVibkY0vB1cVT/vB7LS7F1bU/ABTRnoY82A1TPcN7n3NjHp9kPSTbUBeFW56Q==";
        String des = decryptBase64(s, "f15831e69d484a41");
        System.out.println(System.getProperty("file.encoding"));
        System.out.println(new String(des.getBytes("utf-8"),"GBK"));
    }
}
