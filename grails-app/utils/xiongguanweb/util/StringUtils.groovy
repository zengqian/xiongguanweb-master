package xiongguanweb.util

import java.text.DecimalFormat
import java.text.NumberFormat

class StringUtils {
    //处理手机号码，[4-7] 转为*
    static String replaceMobil(String mobil) {
        return mobil.substring(0, mobil.length() - (mobil.substring(3)).length()) + "****" + mobil.substring(7)
    }

    //处理身份证号码，[4-14] 转为*
    static String replaceIdCard(String idcard) {
        return idcard.substring(0, idcard.length() - (idcard.substring(3)).length()) + "***********" + idcard.substring(14)
    }

    //处理姓名，[4-7] 转为*
    static String replaceName(String name) {
        return name.substring(0, name.length() - (name.substring(0)).length()) + "*" + name.substring(1)
    }

    //金额格式化 0.00 | 191.00  | 9,112.00
    static String moneyFormat(String num) {
        NumberFormat nf = new DecimalFormat("#,##0.00")
        return nf.format(new BigDecimal(num).divide(new BigDecimal(100).setScale(2)))
    }


    public static void main(String[] args) {
//        println(replaceMobil('15757176876'))
        println(moneyFormat("0"))
    }
}
