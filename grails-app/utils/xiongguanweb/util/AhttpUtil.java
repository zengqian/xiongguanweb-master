package xiongguanweb.util;

import com.alibaba.fastjson.JSON;
import xiongguanweb.http.RequestParams;
import xiongguanweb.http.request.Phone_info;
import xiongguanweb.http.request.Request;
import xiongguanweb.http.request.RequestHeader;
import xiongguanweb.util.DateUtils;
import xiongguanweb.util.RSATool;

import java.util.Date;

public final class AhttpUtil {

    //获取请求参数JSON串
    public static String getRequestJson(RequestParams requestParams) {
        if (requestParams == null) {
            return null;
        }
        //设置接口代码
        requestParams.putHead("interCode", "1001001");

        Phone_info phone_info = new Phone_info();
        phone_info.setLatitude("0");
        phone_info.setLongitude("0");
        phone_info.setBrand("web");
        phone_info.setType("web");
        phone_info.setOs_version("web");

        RequestHeader requestHeader = new RequestHeader();
        requestHeader.setInterCode("1001001");
        requestHeader.setAes_token(requestParams.getBody().get("aes_token"));
        requestHeader.setTrdate(DateUtils.formatDateTime(new Date()));
        requestHeader.setSeq("1");
        requestHeader.setTrcode(requestParams.getHeads().get("trcode")); //获取交易代码
        requestHeader.setTrtype(requestParams.getBody().get("trtype") == null ? "01" : requestParams.getBody().get("trtype")); //默认 01【数据传输接口】
        requestHeader.setSource("REG-01"); //REG-03 --android
        requestHeader.setPhone_info(phone_info);
        requestHeader.setSign(RSATool.signInfo(requestParams, requestHeader));

        Request request = new Request(requestHeader, requestParams.getBody());
        return JSON.toJSONString(request);
    }

}
