package xiongguanweb.util

class PropKit {

    private static Properties properties

    private static synchronized init(String fileName) {
        properties = new Properties()
        properties.load(PropKit.class.getClassLoader().getResourceAsStream(fileName))
    }

    static Properties use(String fileName) {
        init(fileName)
        return properties
    }

//    public static void main(String[] args) {
//        String s = PropKit.use("syscomfig.properties").getProperty("BASE_URL")
//        print(s)
//
//    }
}
