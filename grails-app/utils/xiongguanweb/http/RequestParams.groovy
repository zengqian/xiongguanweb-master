package xiongguanweb.http;

/**
 * @author YCJ
 * @创建时间 2017/11/24 10:31
 * @说明 http 请求参数封装
 */
class RequestParams {
    //下面三个常量用于网络请求，填写请求参数时区分参数在HTTP请求位置

    static final int REQ_BODY = 0; //参数位于body

    static final int REQ_HEAD = 1; //参数位于head


    private Map<String, String> body;
    private Map<String, String> heads;


    RequestParams() {
        body = new HashMap<>();
        heads = new HashMap<>();
    }

    /**
     * 设置请求参数
     *
     * @param type 1 or 2
     * @param key 参数名
     * @param value 参数值
     */
    void put(int type, String key, String value) {
        if (type != REQ_BODY || type != REQ_HEAD) {
            throw new RuntimeException("不存的类型,type=" + type);
        }
        if (type == REQ_HEAD) {
            heads.put(key, value);
        } else {
            body.put(key, value);
        }
    }

    /**
     * 设置请求体参数
     *
     * @param key
     * @param value
     */
    void putBody(String key, String value) {
        if (value != null && value.length() > 0 && !value.equals("null")) {
            body.put(key, value);
        }
    }

    /**
     * 设置请求头参数
     *
     * @param key
     * @param value
     */
    void putHead(String key, String value) {
        if (value != null && value.length() > 0 && !value.equals("null")) {
            heads.put(key, value);
        }
    }


    Map<String, String> getBody() {
        return body;
    }

    Map<String, String> getHeads() {
        return heads;
    }
}

