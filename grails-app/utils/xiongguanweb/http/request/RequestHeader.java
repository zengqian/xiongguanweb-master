package xiongguanweb.http.request;


import xiongguanweb.base.BaseBean;

public class RequestHeader extends BaseBean {
    private String interCode;

    private String sign;

    private String trdate;

    private String seq;

    private String trcode;

    private String trtype;

    private String source;

    private String aes_token;

    private Phone_info phone_info;

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getTrdate() {
        return trdate;
    }

    public void setTrdate(String trdate) {
        this.trdate = trdate;
    }

    public String getAes_token() {
        return aes_token;
    }

    public void setAes_token(String aes_token) {
        this.aes_token = aes_token;
    }

    public String getTrcode() {
        return trcode;
    }

    public void setTrcode(String trcode) {
        this.trcode = trcode;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getInterCode() {
        return interCode;
    }

    public void setInterCode(String interCode) {
        this.interCode = interCode;
    }

    public String getTrtype() {
        return trtype;
    }

    public void setTrtype(String trtype) {
        this.trtype = trtype;
    }

    public Phone_info getPhone_info() {
        return phone_info;
    }

    public void setPhone_info(Phone_info phone_info) {
        this.phone_info = phone_info;
    }
}
