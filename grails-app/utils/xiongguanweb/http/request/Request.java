package xiongguanweb.http.request;


import xiongguanweb.base.BaseBean;

import java.util.Map;

public class Request extends BaseBean {
    private RequestHeader header;
    private Map data;

    public Request() {
    }

    public Request(RequestHeader header, Map data) {
        this.header = header;
        this.data = data;
    }

    public RequestHeader getHeader() {
        return header;
    }

    public void setHeader(RequestHeader header) {
        this.header = header;
    }

    public Map getData() {
        return data;
    }

    public void setData(Map data) {
        this.data = data;
    }
}
