package xiongguanweb.http.response;


import xiongguanweb.base.BaseBean;

public class ResponseHeader extends BaseBean {
    private String errorcode;
    private String errormsg;
    private String seq;
    private String succflag;
    private String trcode;
    private String trdate;

    public String getErrorcode() {
        return errorcode;
    }

    public void setErrorcode(String errorcode) {
        this.errorcode = errorcode;
    }

    public String getErrormsg() {
        return errormsg;
    }

    public void setErrormsg(String errormsg) {
        this.errormsg = errormsg;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getSuccflag() {
        return succflag;
    }

    public void setSuccflag(String succflag) {
        this.succflag = succflag;
    }

    public String getTrcode() {
        return trcode;
    }

    public void setTrcode(String trcode) {
        this.trcode = trcode;
    }

    public String getTrdate() {
        return trdate;
    }

    public void setTrdate(String trdate) {
        this.trdate = trdate;
    }
}
