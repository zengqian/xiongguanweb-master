package xiongguanweb.http.response;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import xiongguanweb.base.BaseBean;

public class Response extends BaseBean {

    private ResponseHeader header;
    private String data;

    public ResponseHeader getHeader() {
        return header;
    }

    public void setHeader(ResponseHeader header) {
        this.header = header;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
