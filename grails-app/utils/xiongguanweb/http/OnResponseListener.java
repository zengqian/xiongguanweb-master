package xiongguanweb.http;

import javax.servlet.http.HttpSession;

/**
 * @author YCJ
 * @version 1.0
 * @Date 2018/4/12 14:24
 * @Des http  响应回调接口
 */
public interface OnResponseListener {

    void onSuccess(int errorcode, String errormsg, String data);

    void onFailure(int errorcode, String errormsg);

    void onTokenExpired(int errorcode, HttpSession sion);
}
