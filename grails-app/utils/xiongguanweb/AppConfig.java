package xiongguanweb;

public final class AppConfig {

    //session 存储用户token信息 key值表
    public static final String TOKEN = "app_token";

    public static final String APP_USER_INFO = "app_user_info";

    public static final String APP_REAL_INFO = "app_real_info";

    public static final String GAS_INFO = "gas_info";

    public static final String VERIFY_CODE = "verify_code";

    public static final String ISREALNAME = "1";


    //爱加密 true--不开启  false--开启
    public static boolean dev = false;

    public static final String PRIVATE_KEY = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAKr1Ch8QEQY3gt2wID5I3x46BjNY9inkLNWvgzIBCubiokpIbMJnw/Z7vSkCRkQuojuCMUaAcKp9lmRdiQfQ0wH0dCzSS0vptz4K29OAfRKWtYlRxMsQ1vp85LIZ5T8ikr2/Hdp+qsmn8LUVDjZ32BuA3mV3LX2EuzThmsvzjDerAgMBAAECgYBMAsVsSEV5gScDkHLVnqrtkpj9LZnNyHES1YW8ru/89ll4M7l/uolLx3q0iBlb6Wwij2F/Wk49z7cViu0Jd5TJdvRVSYa0p9z4esQLYBFOjB8owEMmu+cOxUlecGoo9yPea2welO9+Pdeu5csBvbgv4HgVc1O32tS0/5H8WgeaKQJBAO/Yucb4F5y1UO4nrussigtbzg87BeGdHqDvbLbm16RvM/lAjUX4y1FoHnm9lh9hyfYDwK8koTTziiW/Bm+l2kUCQQC2eJGlMaRmA6dpFHuy9aaoPV+CbVBqjmDDS8i2wg2xmMjDIJ/tbfCM+BHK3x9BESzE+GjaH1dNsV7yp1TroWEvAkEAizroPVRvgIzQui9XHzXvvMBCUh0VQO8QhKP4HATNUassJJkQaXoPrOYgKtOffgAqWSG8jO9alX56x4aomc9RcQJAAuPZ+rHUlsEMD8ABTY5Se2utbZVppqZmpfE3Vp2wNdXjqCOGJO6O1sy/C2WJSzCIcv40OvAwI8zhYhqAX9MIhQJBALEDwe0Tfe7v5ZO8S+1KmdawlG8XjKAYAIPsz28OT7yHBA5hwmfFraaNM0aFW7neUCAHBHMnwxXVMUdGXPZCeaM=";
    public static final String PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCq9QofEBEGN4LdsCA+SN8eOgYzWPYp5CzVr4MyAQrm4qJKSGzCZ8P2e70pAkZELqI7gjFGgHCqfZZkXYkH0NMB9HQs0ktL6bcMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCq9QofEBEGN4LdsCA+SN8eOgYzWPYp5CzVr4MyAQrm4qJKSGzCZ8P2e70pAkZELqI7gjFGgHCqfZZkXYkH0NMB9HQs0ktL6bc+CtvTgH0SlrWJUcTLENb6fOSyGeU/IpK9vx3afqrJp/C1FQ42d9gbgN5ldy19hLs04ZrL84w3qwIDAQAB";


}
