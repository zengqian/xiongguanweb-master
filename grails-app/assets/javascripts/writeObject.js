﻿var WIN_32_ACTIVEX_VERSION = 2003009005;					//Windows系统下32位控件版本号
var WIN_64_ACTIVEX_VERSION = 2003009005;					//Windows系统下64位控件版本号
var WIN_PLUGIN_VERSION = 2003009005;						//Windows系统下插件版本号
var MAC_PLUGIN_VERSION = 2003009005;						//Mac系统下插件版本号
var WIN_SETUP_PATH = "login/dowControl";			//Windows系统下安装程序下载路径
var MAC_SETUP_PATH = "setup/PowerEnterLZBANK.zip";			//Mac系统下安装程序下载路径
var LocalObjVersion="";
var isInistall = false;

//公钥
var pk = "8E468CB6E0B100C39B440BF80A26154E3B897C54C992AC7CE615CAAFE0ADE5E6F52AE8ACD94BFAF50108009B31145DC573388A341477E17FFFCA5F26F80E156D9727B39D5E9B7308AF6E1EF871E9507EE35F4180CB4DBE7892D7C28D9A8075E4BA573BF5EEEA3E5B9846D115422490F0C93BC13D72EFAB8256870EF6F49D98B9";

//安全输入控件
var PassCtrlClsid = "clsid:32BC59DA-1470-479B-9A02-4063169C0FBB";
var EditCtrlClsid = "clsid:6B492E46-D57F-42D3-B720-72C88DCE492B";
var UtilCtrlClsid = "clsid:28552737-D381-459E-AE04-44CCE7D521DD";
var CtlName = "POWERENTERLZBANK.PowerUtilityXLZBANKCtrl.1";

//安全输入插件
var MIME = "application/x-vnd-csii-powerenter-lzbank";
var PowerEnterPluginDescription = "PowerEnter Plug-in for LZBANK";

//控件默认属性
function powerConfig(args) {
	var defaults = { 
		"width":150,
		"height":22,
		"maxLength":18,
		"minLength":6,
		"maskChar":"*",
		"backColor":"#FFFFFF",
		"textColor":"#000000",
		"caption":"兰州银行直销银行",
		"borderColor":"#ffffff",
		"accepts":"[:graph:]+",
		"softKeyboard":"false",
		"name": "unti"
	};
	for (var p in args)
		if (args[p] != null) defaults[p] = args[p];
	return defaults;
}

function writePluginObject(oid, clsid, cfg) {
	document.write('<object id="' + oid + '" type="' + clsid
		+ '" width="' + cfg.width + '" height="' + cfg.height
		+ '" style="width:' + cfg.width + 'px;height:' + cfg.height + 'px">');
	for (var name in cfg)
		document.write('<param name="' + name + '" value="' + cfg[name] + '">');
	document.write('</object>');
    // document.write('<input id="' + oid + '" class="btn_div form_input" placeholder="请输入密码"/>');
};

function writeObject(oid, clsid, cfg) {	
	document.write('<object id="' + oid + '" classid="' + clsid
			+ '" width="' + cfg.width + '" height="' + cfg.height  + '">');
	for (var name in cfg)
		document.write('<param name="' + name + '" value="' + cfg[name] + '">');
	document.write('</object>');
	// document.write('<input id="' + oid + '" class="btn_div form_input" placeholder="请输入密码"/>');
};

function writeEditObject(oid, cfg) {
	if (!oid || typeof(oid) != "string") {
		alert("writePassObj Failed: oid are required!");
	} else {
		setPEXSetupUrl(oid);
		if(isInistall)
		{
			if (isIE())
			{
				writeObject(oid, EditCtrlClsid, powerConfig(cfg));
			}
			else
			{
				writePluginObject(oid, MIME, powerConfig(cfg));
			}
		}
	}
};

function writePassObject(oid, cfg) {
	if (!oid || typeof(oid) != "string") {
		alert("writePassObj Failed: oid are required!");
	} else {
		setPEXSetupUrl(oid);
		if(isInistall)
		{
			if (isIE())
			{
				writeObject(oid, PassCtrlClsid, powerConfig(cfg));
			}
			else
			{
				writePluginObject(oid, MIME, powerConfig(cfg));
			}
		}
	}
};

function writeUtilObject(oid, cfg) {
	if (!oid || typeof(oid) != "string") {
		alert("writePassObj Failed: oid are required!");
	} else {
		if (isIE())
		{
			writeObject(oid, UtilCtrlClsid, powerConfig(cfg));
		}
		else
		{
			writePluginObject(oid, MIME, powerConfig(cfg));
		}
	}
};

function getPassInput(id, ts, spanId, massage) 
{
    try
    {
		var powerobj = document.getElementById(id);	
		powerobj.setTimestamp(ts);
		powerobj.publicKeyModulus(pk);
		var nresult = powerobj.verify();
		if(nresult < 0)
		{
			var error;
			if(nresult == -1)
			{
				error = "内容不能为空";
			}
			else if(nresult == -2)
			{
				error = "输入长度不足";
			}
			else if(nresult == -3)
			{
				error = "输入内容不合规";
			}
			else
			{
				error = powerobj.lastError(); 
			}
            // layer.msg(massage +error)
			PEGetElement(spanId).innerHTML = massage +error;
			return null;
		}
		
		value = powerobj.getValue();
		if(value=="")
		{
            layer.msg(massage+powerobj.lastError())
			// PEGetElement(spanId).innerHTML= massage+powerobj.lastError();
			return null;
		}
		else
		{
			return value;
		}
	}
	catch(e)
	{
		console.log(e)
        layer.msg("控件尚未安装，请下载并安装控件！:"+e)
		// PEGetElement(spanId).innerHTML= "控件尚未安装，请下载并安装控件！:"+e;
	}
	return null;
}

function getEditInput(id, ts, spanId,massage) 
{
    try 
    {
		var powerobj = document.getElementById(id);
		alert(powerobj)
		powerobj.setTimestamp(ts);
		powerobj.publicKeyModulus(pk);
		var nresult = powerobj.verify();
		if(nresult < 0)
		{			
			var error;
			if(nresult == -1)
			{
				error = "内容不能为空";
			}
			else if(nresult == -2)
			{
				error = "输入长度不足";
			}
			else if(nresult == -3)
			{
				error = "输入内容不合规";
			}
			else
			{
				error = powerobj.lastError(); 
			}
            layer.msg(massage +error)
			// PEGetElement(spanId).innerHTML = massage +error;
			return null;
		}	
				
		value = powerobj.getValue();
		if(value=="")
		{
            layer.msg(massage+powerobj.lastError())
			// PEGetElement(spanId).innerHTML= massage+powerobj.lastError();
			return null;
		}
		else
		{
			return value;
		}
	}
	catch(e)
	{
        layer.msg("控件尚未安装，请下载并安装控件!")
		// PEGetElement(spanId).innerHTML= "控件尚未安装，请下载并安装控件!";
	}
	return null;
}

function getMFMInput(id, ts, spanId,massage) 
{
    try 
    {
		var powerobj = document.getElementById(id);	
		powerobj.setTimestamp(ts);
		powerobj.publicKeyModulus(pk);
		value = powerobj.getMFM();
		if(value=="")
		{
            layer.msg(massage + powerobj.lastError())
			// PEGetElement(spanId).innerHTML= massage + powerobj.lastError();
			return null;
		}
		else
		{
			return value;
		}
	}
	catch(e)
	{
        layer.msg(massage + e.message)
		// PEGetElement(spanId).innerHTML= massage + e.message;
	}
	return null;
}

function PEGetElement(id)
{
	return  window.document.getElementById(id);
}

function showDownloadPage()
{
	$.layer({
        type: 0,
        title: "控件下载",
        shade: [0.3, '#000'],
        area: ['320px', '200px'],
        border: [0, 0.3, '#000'],
        dialog: {
			msg: '请您下载安装文件并完成安装。',
			type: 7,
			btns: 2,                    
	        btn: ['完成安装','重新下载'],
	        yes: function(){
        		location.reload();
	        }, no: function(){
	        	window.location = getDownLoadPath();
	        	showDownloadPage();
	        }
	    },
		close: function(index){
	    	location.reload();
		}
    });
}

function setPEXSetupUrl(oid)
{
	var DownloadPath = getDownLoadPath();
	var ObjVersion = getObjVersion();
	if(isRegisteredPowerEnter()==false){
		if((navigator.platform == "Win32") || 
		   (navigator.platform == "Windows") || 
		   (navigator.platform == "Win64") ||
		   (navigator.platform == "Mac68K") || 
		   (navigator.platform == "MacPPC") || 
		   (navigator.platform == "Macintosh") || 
		   (navigator.platform == "MacIntel")){
			document.write('<a href="'+DownloadPath+'" onclick="showDownloadPage()"  class="download_install">点击此处下载控件</a>');
		}else{
			document.write('<a href="#" class="download_install">暂不支持此系统</a>');
		}
		isInistall = false;
	}else{
		var LocalObjVersion = getLocalObjVersion();
		if(LocalObjVersion < ObjVersion){
			document.write('<a href="'+DownloadPath+'" class="download_install">点击此处更新控件</a>');
			isInistall = false;
		} else {
			isInistall = true;
		}
	}
}

function isRegisteredPowerEnter(){
	try{
		if (isIE()){
			new ActiveXObject(CtlName);
		}else{
			var powerEnterPlugin = navigator.plugins[PowerEnterPluginDescription];
			if(powerEnterPlugin == null)
				return false;
		}
	}catch(e){
		return false;   
	}
	return true;
}

function getDownLoadPath()
{
	if((navigator.platform == "Win32") || (navigator.platform == "Windows") || (navigator.platform == "Win64"))
		return WIN_SETUP_PATH;				//Windows
	else if((navigator.platform == "Mac68K") || (navigator.platform == "MacPPC") || (navigator.platform == "Macintosh") || (navigator.platform == "MacIntel"))
		return MAC_SETUP_PATH;		    //MAC

	return WIN_SETUP_PATH; 
}

function getObjVersion()
{
    if((navigator.platform == "Win64" || navigator.cpuClass == "x64")){
        if (isIE())
            return WIN_64_ACTIVEX_VERSION;         // Windows系统下64位控件版本
        else
            return WIN_PLUGIN_VERSION;             // Windows系统下插件版本
     }else if((navigator.platform == "Win32") || (navigator.platform == "Windows")){
        if (isIE())
            return WIN_32_ACTIVEX_VERSION;         // Windows系统下32位控件版本
        else
            return WIN_PLUGIN_VERSION;             // Windows系统下插件版本
     }else if((navigator.platform == "Mac68K") || (navigator.platform == "MacPPC") || (navigator.platform == "Macintosh") || (navigator.platform == "MacIntel")){
     	return MAC_PLUGIN_VERSION;		  		  // Mac系统下插件版本
     }
     return "";
}

// writeUtilObject("versionObj",{"width":1,"height":1});

function getLocalObjVersion()
{
	if(LocalObjVersion == "")
	{
		try{
			LocalObjVersion = PEGetElement("versionObj").getVersion();
		}catch(e){
			LocalObjVersion = getObjVersion();
		}
	}
	return LocalObjVersion;
}

function isIE()
{
	if (navigator.appName == 'Microsoft Internet Explorer' || navigator.userAgent.indexOf("Trident")>0)
		return true;
	else
		return false;
}

