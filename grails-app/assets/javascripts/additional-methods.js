$(document).ready(function () {
    //不包含中文
    jQuery.validator.addMethod("notCN", function (value, element) {
        return this.optional(element) || /^[^\u4e00-\u9fa5]+$/.test(value);
    }, "不能包含中文");

    // 字符验证
    jQuery.validator.addMethod("stringCheck", function (value, element) {
        return this.optional(element) || /^[\u0391-\uFFE5\w]+$/.test(value);
    }, "只能包括中文字、英文字母、数字");

    // 小数验证
    jQuery.validator.addMethod("dotNumber", function (value, element) {
        return this.optional(element) || /(^[1-9](\d+)?(\.\d{1,2})?$)|(^\d\.\d{1,2}$)/.test(value);
    }, "只能输入大于0数字且小数最多两位");

    // 数字验证(^[1-9](\d+)?(\.\d{1,2})?$)|(^\d\.\d{1,2}$)
    jQuery.validator.addMethod("Number", function (value, element) {
        return this.optional(element) || /^[0-9]*$/.test(value);
    }, "只能输入数字");

    // 中文验证
    jQuery.validator.addMethod("chinese", function (value, element) {
        return this.optional(element) || /[\u4E00-\u9FA5\uF900-\uFA2D]/.test(value);
    }, "请输入中文");


    // 中文字两个字节
    jQuery.validator.addMethod("byteRangeLength", function (value, element, param) {
        var length = value.length;
        for (var i = 0; i < value.length; i++) {
            if (value.charCodeAt(i) > 127) {
                length++;
            }
        }
        return this.optional(element) || (length >= param[0] && length <= param[1]);
    }, "请确保输入的值在3-15个字节之间(一个中文字算2个字节)");


    // 身份证号码验证
    jQuery.validator.addMethod("isIdCardNo", function (value, element) {
        var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
        return this.optional(element) || reg.test(value);
    }, "请正确输入您的身份证号码");

    // 手机号码验证
    jQuery.validator.addMethod("isMobile", function (value, element) {
        var length = value.length;
        var mobile = /^1[3456789]\d{9}$/;
        return this.optional(element) || (length == 11 && mobile.test(value));
    }, "请正确填写您的手机号码");


    // 电话号码验证
    jQuery.validator.addMethod("isTel", function (value, element) {
        var tel = /^\d{3,4}-?\d{7,9}$/;    //电话号码格式010-12345678
        return this.optional(element) || (tel.test(value));
    }, "请正确填写您的电话号码");

    // 联系电话(手机/电话皆可)验证
    jQuery.validator.addMethod("isPhone", function (value, element) {
        var length = value.length;
        var mobile = /^(((13[0-9]{1})|(15[0-9]{1}))+\d{8})$/;
        var tel = /^\d{3,4}-?\d{7,9}$/;
        return this.optional(element) || (tel.test(value) || mobile.test(value));
    }, "请正确填写您的联系电话");


    // 邮政编码验证
    jQuery.validator.addMethod("isZipCode", function (value, element) {
        var tel = /^[0-9]{6}$/;
        return this.optional(element) || (tel.test(value));
    }, "请正确填写您的");

    // 密码串验证
    jQuery.validator.addMethod("stringCheckPassword", function (value, element) {
        var reg = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,20}$/;
        return this.optional(element) || (reg.test(value));
    }, "必须包含字母、数字且长度不小于6位");

    // 密码串验证
    jQuery.validator.addMethod("isEmail", function (value, element) {
        var reg = /^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z0-9]{2,6}$/;
        return this.optional(element) || (reg.test(value));
    }, "请正确填写您的邮箱地址");

    // 传真
    jQuery.validator.addMethod("isFax", function (value, element) {
        var reg = /^(\d{3,4}-)?\d{7,8}$/;
        return this.optional(element) || (reg.test(value));
    }, "请正确填写您的传真");

    //银行卡号
    jQuery.validator.addMethod("isBankCard", function (value, element) {
        var pattern = /^([1-9]{1})(\d{14}|\d{18})$/;
        return this.optional(element) || (pattern.test(value));
    }, "请正确填写您的银行卡号");

    //checkbox
    jQuery.validator.addMethod("isCheckk", function (value, element) {
        var flag;
        if($(".checkbox_div").hasClass("active")){
            $(".checkbox_dailog").addClass("ac")
            flag = true
        }else {
            console.log($(".checkbox_dailog").hasClass("ac"))
            if ($(".checkbox_dailog").hasClass("ac")) {
                $(".checkbox_dailog").toggleClass("ac")
                flag = false
            }
        }
       // $(".checkbox_div").addClass("active")
        return flag;
    }, "");


});