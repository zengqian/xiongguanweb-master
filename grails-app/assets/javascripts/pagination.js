var backfun = function () {

};
var total;
$(document).ready(function () {


    /* 分页js */
    $(".number").on('click', '.addnumber', function () {
        var page = Math.ceil(total / 10);
        if (page != 0) {
            var currentpage = $(".addnumber").prev().text()
            $("#page").val(currentpage)
            backfun()
            $.firstinit(total, currentpage, backfun)
        }
    });


    $(".number").on('click', '.reducenumber', function () {
        var page = Math.ceil(total / 10);
        if (page != 0) {
            var currentpage = $(".reducenumber").next().text()
            $("#page").val(currentpage)
            backfun()
            $.firstinit(total, currentpage, backfun)
        }
    });
    $(".number").on('click', '.butt', function () {
        var page = Math.ceil(total / 10);
        if (page != 0) {
            if ($("button.paginactive")) {

                $("button.paginactive").toggleClass("paginactive")
            }
            $(this).addClass("paginactive")
            $("#page").val(getPage())
            backfun()
            $.firstinit(total, getPage(), backfun)
        }
    })
    $(".prev_page").click(function () {
        var page = Math.ceil(total / 10);
        if (page != 0) {
            var currentpage = $("button.paginactive").prev().text()
            if (currentpage >= 1) {
                $("#page").val(currentpage)
                backfun()
                $.firstinit(total, currentpage, backfun)
            }
        }
    })
    $(".next_page").click(function () {
        var page = Math.ceil(total / 10);
        if (page != 0) {
            var currentpage = $("button.paginactive").next().text()
            var page = Math.ceil(total / 10);
            if (currentpage && parseInt(currentpage) <= page) {
                $("#page").val(currentpage)
                backfun()
                $.firstinit(total, currentpage, backfun)
            }
        }
    })
    $(".last_page").click(function () {
        var page = Math.ceil(total / 10);
        if (page != 0) {
            $("#page").val(page)
            backfun()
            $.firstinit(total, page, backfun)
        }
    })

});

function getPage() {

    return $(".paginactive").val()
}

$.extend({
    "firstinit": function (total1, current, callback) {
        total = total1
        // callback();
        if (!current) {
            current = 1
        }
        $(".number").empty()
        var page = Math.ceil(total / 10);
        var cur = parseInt(current)
        if (cur > 5) {
            $(".number").append('<button class="butt" value="1">1</button>')
            if (cur - 2 > 3 && cur < page || cur === page) {
                $(".number").append('<button class="reducenumber">...</button>')
            }
            if (cur <= page - 1) {
                $(".number").append('<button class="butt" value="' + parseInt(cur - 1) + '">' + parseInt(cur - 1) + '</button>')
                $(".number").append('<button class="butt paginactive" value="' + cur + '">' + cur + '</button>')
                $(".number").append('<button class="butt" value="' + parseInt(cur + 1) + '">' + parseInt(cur + 1) + '</button>')
            }
            if (cur + 2 < page - 1) {
                $(".number").append('<button class="addnumber">...</button>')
            }
            if (page - cur === 3) {
                $(".number").append('<button class="butt" value="' + parseInt(cur + 2) + '">' + parseInt(cur + 2) + '</button>')

            }
            if (page - cur === 0) {
                $(".number").append('<button class="butt" value="' + parseInt(cur - 2) + '">' + parseInt(cur - 2) + '</button>')
                $(".number").append('<button class="butt" value="' + parseInt(cur - 1) + '">' + parseInt(cur - 1) + '</button>')
            }
            if (cur !== page && parseInt(cur + 1) < page) {
                $(".number").append('<button class="butt" value="' + page + '">' + page + '</button>')
            }
            if (cur === page) {
                $(".number").append('<button class="butt paginactive" value="' + page + '">' + page + '</button>')

            }
        } else {
            if (page > 5) {

                for (var i = 1; i <= 5; i++) {
                    if (i === cur) {
                        $(".number").append('<button class="butt paginactive" value="' + i + '">' + i + '</button>')
                    } else {
                        $(".number").append('<button class="butt" value="' + i + '">' + i + '</button>')
                    }
                }
                if (cur === 5) {
                    $(".number").append('<button class="butt" value="' + 6 + '">' + 6 + '</button>')
                }
                if (cur + 2 <= page - 1) {
                    $(".number").append('<button class="addnumber">...</button>')
                } else {
                    $(".number").append('<button class="butt" value="' + parseInt(page - 1) + '">' + parseInt(page - 1) + '</button>')
                }
                $(".number").append('<button class="butt" value="' + page + '">' + page + '</button>')
            } else {
                if (page != 0) {
                    for (var i = 1; i <= page; i++) {
                        if (i === cur) {
                            $(".number").append('<button class="butt paginactive" value="' + i + '">' + i + '</button>')
                        } else {
                            $(".number").append('<button class="butt" value="' + i + '">' + i + '</button>')
                        }
                    }

                }
            }

        }

        backfun = callback;
    }
});
