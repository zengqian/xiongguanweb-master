//公共服务效果(已去除)
//上一张
$(".left_prev").on("click", function () {
    var left = $(".pic_ul").position({of: "#pic_div"}).left;
    console.log(left)
    if (left >= -726 && left < 0) {
        $(".public_service ul").offset({"left": left + 242 + $("#pic_div").offset().left})
    } else {
        $(".public_service ul").offset({"left": left - 726 + $("#pic_div").offset().left})
    }
})
//下一张
$(".right_next").on("click", function () {
    var left = $(".pic_ul").position({of: "#pic_div"}).left;
    console.log(left)
    if (left >= -488) {
        $(".public_service ul").offset({"left": left - 242 + $("#pic_div").offset().left})
    }
    else {
        $(".public_service ul").offset({"left": left + 726 + $("#pic_div").offset().left})
    }
})

/**
 * 合作商家效果
 */
$(".buiness_div li a").hover(function () {
    //获取当前光标距离ul顶端的距离
    var top = $(this).position({of: ".buiness_div ul"}).top - 5;
    //为色块设置偏移
    $(".ul_div").offset({top: top + $(".buiness_div ul").offset().top});
    //计算右边div应该怎么处理，偏移多少
    var set = Math.floor((top) / 50)
    $(".buiness_banner_div>div").offset({top: $(".buiness_div ul").offset().top - set * 202})
    //去除上一个的active，为当前对象设置active样式
    $("li.active").removeClass("active")
    $(this).parent("li").toggleClass("active")
})
/**
 * 首页banner图左点击右点击
 */
$(".left").on('click', function () {
    //获取当前显示的图片
    var _this_active = $("a.active")
    //获取当前的下一张并且不为空
    if (_this_active.prev("a").attr("class") != undefined) {
        $("a.active").removeClass("active")
        _this_active.prev("a").addClass("active")
    }
});
$(".right").on('click', function () {
    var _this_active = $("a.active")
    //获取当前的上一张并且不为空
    if (_this_active.next("a").attr("class") != undefined) {
        $("a.active").removeClass("active")
        _this_active.next("a").addClass("active")
    }
});

/**
 * 表单focus弹框显示
 */
$(".form_input").focusin(function () {
    $(this).next(".form_mess").toggle()
});
$(".form_input").focusout(function () {
    $(this).next(".form_mess").toggle()
})

$(".li").on('click', function () {
    var old_active = $(this).parent().siblings("li")
    old_active.find("span").removeClass("jiantou_active")
    old_active.find("ul").removeClass("ul_active")

    // $("ul.ul_active").removeClass("ul_active")
    $(this).find("label").toggleClass("jiantou_active")
    $(this).find("span").toggleClass("jiantou_active")
    $(this).next("ul").toggleClass("ul_active")
})

/**
 * 下拉渐显
 */
// $(window).scroll(function(){
//     var _this = $(this).scrollTop();
//     console.log(_this)
// console.log($(".animate_active1").offset().top+"top1")
// console.log($(".animate_active2").offset().top+"top2")
// console.log($(".animate_active3").offset().top+"top3")
// if(_this > $(".animate_active1").offset().top-680){
//     $(".animate_active1").addClass("animate_fade")
// }
// if(_this > $(".animate_active2").offset().top-680){
//     $(".animate_active2").addClass("animate_fade")
// }
// if(_this > $(".animate_active3").offset().top-680){
//     $(".animate_active3").addClass("animate_fade")
// }
// if(_this > $(".animate_active").offset().top-680){
//     $(".animate_active").addClass("animate_fade")
// }
// if(_this > $(".animate_active4").offset().top-680){
//     $(".animate_active4").addClass("animate_fade")
// }

// })
/**
 * 首页导航条效果
 */
$(window).scroll(function () {
    var _this = $(window).scrollTop();
    if (_this !== 0) {
        $(".index_header").addClass("active")
        $(".index_ul li a").addClass("active")
        $(".logo").attr("src","/assets/index/logo.png")
    }else {
        $(".index_header").removeClass("active")
        $(".index_ul li a").removeClass("active")
        $(".logo").attr("src","/assets/index/white_logo.png")
    }
})

/**
 * 手机充值选框效果
 */
$(".recharge_phone_div").on('click',function () {
    $(".recharge_phone_div.checked").removeClass("checked")
    $(this).toggleClass("checked")
    $(".recharge_amount").val($(this).text().replace("元",""))
})
$(".recharge_amount").focusin(function(){
    $(".recharge_phone_div.checked").removeClass("checked")
});

/**
 * 石油卡充值选框效果
 */
$(".oil_add .oil_radio").on('click',function () {
    $(".oil_radio.checked").removeClass("checked")
    $(this).toggleClass("checked")

})

/**
 * 手机充值下拉框
 */
$(".select_div").on("click",function () {
    $(".select_ul").toggleClass("active")
})
$(".unicom_option_radio").on("click",function () {
    $(".select_ul").toggleClass("active")
    $(".select_div").html($(".unicom_option_radio[type='radio']:checked").parent().html())
    $(".select_ul li.active").removeClass("active")
    $(this).parent().toggleClass("active")
})

$(".checkbox_div").on('click',function () {
   $(".checkbox_div").toggleClass("active")
});

/**
 * 获取短信验证码
 */
    // --------------------------------------------------发送短信验证码start-------------------------------------------------------------------
    //当前秒数
var times
var interval
$(".get_tell_code").click(function () {
    var mobileNo = $("input[name='phone']").val();
    var interfaceCode = $("input[name='interfaceCode']").val();
    if($("input[name=certno]").length>0){
        var certno = $("input[name=certno]").val();
        var username = $("input[name=username]").val();
        if (regexpPhone(mobileNo)&& regexpCertno(certno) && username) {
            $.post("/msg/sendPhoneMsg", {mobile: mobileNo,interfaceCode: interfaceCode}, function (json) {
                console.log(json)
                if (json.code == 0) {
                    layer.msg("短信已发送，注意查收！")
                    //60秒后重发
                    times = 60;
                    interval = self.setInterval("refreshBtnText()", 1000);
                    $(".get_tell_code").css("pointer-events", "none");
                } else {
                    layer.msg(json.errormsg)
                }
            });
        }else {
            layer.msg("请确认是否正确填写了电话号码,身份证号码和姓名")
        }
    }else {
        if (regexpPhone(mobileNo)) {
            $.post("/msg/sendPhoneMsg", {mobile: mobileNo,interfaceCode: interfaceCode}, function (json) {
                console.log(json)
                if (json.code == 0) {
                    layer.msg("短信已发送，注意查收！")
                    //60秒后重发
                    times = 60;
                    interval = self.setInterval("refreshBtnText()", 1000);
                    $(".get_tell_code").css("pointer-events", "none");
                } else {
                    layer.msg(json.errormsg)
                }
            });
        }else {
            layer.msg("请确认是否正确填写了电话号码")
        }

    }
});

function refreshBtnText() {
    times = times - 1;
    if (times <= 0) {
        window.clearInterval(interval);
        $(".get_tell_code").html("获取短信验证码")
        $(".get_tell_code").css("pointer-events", "auto");
    } else {
        $(".get_tell_code").html(times + "秒后重发")
    }
}

function regexpPhone(value) {
    var length = value.length;
    var mobile = /^1[3456789]\d{9}$/;
    return (length == 11 && mobile.test(value));
}

function regexpCertno(value) {
    var length = value.length;
    var mobile = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
    return (mobile.test(value));
}

//--------------------------------------------------发送短信验证码end-------------------------------------------------------------------



//---------------------------------------------------图片验证码刷新------------------------------------------------------------------------------
$("#verifyCodeImg").click(function () {
    $("#verifyCodeImg").attr("src", "/user/verifyCode?time=" + new Date().getTime());
})

$(".hidden_money").click(function () {
    $(".money_div").toggle()
});

// $(".update_logo").mouseenter(function () {
//     // $(".update_logo").addClass("active")
//     //添加修改文字
//     $(".body_top_img form").remove()
//     $(".body_top_img").append("<form id='upload_form' action='/user/upload' enctype = 'multipart/form-data' method='post'> " +
//         "<img src='../assets/life/38.png' class='update_logo_txt' onmouseleave='updatelogoactive()' onclick='updatelogoclick()'/>" +
//         "<input type='file' name='upload_logo' title='修改头像' onchange='upload()' style=' position: absolute;\n" +
//         "    top: 0;\n" +
//         "    left: 0;\n" +
//         "    width: 100%;\n" +
//         "    height: 100%;\n" +
//         "    opacity: 0;'/></form>")
//     // $(".body_top_img").append("<form id='upload_form'><input type='file' name='upload_logo' title='修改头像'/></form>")
// });

function updatelogoactive() {
    // $(".update_logo").removeClass("active")
    $("img").remove(".update_logo_txt")
}

$(".rechare_radio").on("click", function () {
    $(".rechare_radio").parent().siblings("input").attr("disabled",true);
    $(this).parent().siblings("input").attr("disabled",false);
})

// var winWindth = window.innerWidth;
// $(".a").offset({left: -winWindth})

/**
 * 待开发功能弹框
 */
function toast(a) {
    if(a){
    layer.msg(a)
    }else {
    layer.msg("应用正在对接中，敬请期待")
    }
}
// $(".banner_a").animate('',500,banner)

