$().ready(function () {
    //判断当前登录状态，显示不同head-right
    $.post("/user/isLogin", function (json) {
        if (json.isLogin) {
            $("#login_true").toggle();
            $("#login_false").toggle();
            if (json.logo != null && json.logo != "") {
                $(".banner_logo>img").attr("src", json.logo)
            }
        }
    });
});

function isReal(url) {
    //判断是否实名，进行不同操作跳转
//     $.post("/user/isReal", function (json) {
//         if (json.code == 0) {
//             window.location.href = "http://localhost:8899" + url
//         } else {
//             layer.msg(json.msg)
//         }
//     });
//     $.ajax({
//         type: "POST",      //data 传送数据类型。post 传递
//         dataType: 'json',  // 返回数据的数据类型json
//         url: "/user/isReal",  // yii 控制器/方法
//         error: function () {
//
//             // window.location.href = "http://60.164.190.154:8180/login"
//             window.location.href = "http://localhost:8080/login"
//         }, success: function (json) {
//             if (json.code == 0) {
//                 window.location.href = "http://localhost:8080" + url
//                 // window.location.href = "http://60.164.190.154:8180" + url
//             } else {
//                 layer.confirm('当前操作需要进行实名认证', {
//                     btn: ['立即前往', '以后再说'] //按钮
//                 }, function (index) {
//                     layer.close(index);
//                     // window.location.href = "http://localhost:8080"
//                 }, function () {
//                 });
//                 //layer.msg(json.msg)
//             }
//         }
//     })
//     window.location.href = "http://192.168.2.201:8091" + url
    window.location.href = url
}

function loginout() {
    layer.confirm('当前操作将退出登录', {
        btn: ['确认', '取消'] //按钮
    }, function () {
        window.location.href = "/login/loginout"
        // window.location.href = "http://192.168.2.201:8091/login/loginout"
        // window.location.href = "http://60.164.190.154:8180/login/loginout"
    }, function () {
    });
}
