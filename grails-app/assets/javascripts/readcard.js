//读取广汇燃气重庆山城卡信息
function Read_GHRQ_CQSCCard() {
    console.log("读卡start")
    WebCtl.Read_GHRQ_CQSCCard();
    if (WebCtl.Status == 0) {
        $("input[name=cardno]").val(WebCtl.GHRQ_CQSCCardNo)
        $("input[name=bugdate]").val(WebCtl.GHRQ_CQSCBuyDate)
        $("input[name=gasvalue]").val(WebCtl.GHRQ_CQSCGas)
        $("input[name=bugtimes]").val(WebCtl.GHRQ_CQSCBuyTimes)
        $(".current_gasvalue").text(WebCtl.GHRQ_CQSCGas)
    } else {
        GetErrMessage(WebCtl.Status);
    }
}

//广汇燃气重庆山城卡写卡
function Write_GHRQ_CQSCOrders(token, cardno, buytimes, gasvalue, orderid) {
    var dtCur = new Date();
    var yearCur = dtCur.getFullYear();
    var monCur = dtCur.getMonth() + 1;
    var dayCur = dtCur.getDate();
    var hCur = dtCur.getHours();
    var mCur = dtCur.getMinutes();
    var sCur = dtCur.getSeconds();
    var timeCur = yearCur + "-" + (monCur < 10 ? "0" + monCur : monCur) + "-"
        + (dayCur < 10 ? "0" + dayCur : dayCur) + " " + (hCur < 10 ? "0" + hCur : hCur)
        + ":" + (mCur < 10 ? "0" + mCur : mCur) + ":" + (sCur < 10 ? "0" + sCur : sCur);
    var timestr = timeCur.substring(0, 4) + timeCur.substring(5, 7) + timeCur.substring(8, 10);
    WebCtl.Write_GHRQ_CQSCOrders(token, "56001", "0", cardno, timestr, Number(buytimes)+1,
        Number(gasvalue), 5, 0, 10, 99999);
    var flag = "1"
    if (WebCtl.Status == 0) {
        flag = "0"
        $(".recharging").toggle()
        $(".recharge_finish").toggle()
    } else {
        GetErrMessage(WebCtl.Status);
        $(".recharging").toggle()
        $(".recharge_fail").toggle()
    }
    $.post('/user/huiNengGasWriteResult', {"token": token, "orderId": orderid, "flag": flag,"cardNo": cardno},
        function (json) {
            Read_GHRQ_CQSCCard()
            if (json.errorcode == 0) {
                if(flag == "1"){
                    layer.msg("退款成功")
                }
            }else {
                layer.msg("退款已受理，请等待退款...")
            }
        })
}

function Read_GHRQ_DLXCard() {
    WebCtl.Read_GHRQ_DLXCard();
    if (WebCtl.Status == 0) {
        $("input[name=cardno]").val(WebCtl.GHRQ_DLXCardNo)
        $("input[name=gasvalue]").val(WebCtl.GHRQ_DLXGas)
        $("input[name=CustomerId]").val(WebCtl.GHRQ_DLXCustomerId)
        $("input[name=bugtimes]").val(WebCtl.GHRQ_DLXBuyTimes)
        $(".current_gasvalue").text(WebCtl.GHRQ_DLXGas)
    } else {
        GetErrMessage(WebCtl.Status);
    }
}

function Write_GHRQ_DLXOrders(token, cardno, buytimes, gasvalue, orderid,CustomerId) {
    WebCtl.Write_GHRQ_DLXOrders(token, "56001", "0",CustomerId, cardno, Number(buytimes)+1,
        Number(gasvalue));
    var flag = "1"
    if (WebCtl.Status == 0) {
        flag = "0"
        $(".recharging").toggle()
        $(".recharge_finish").toggle()
    } else {
        GetErrMessage(WebCtl.Status);
        $(".recharging").toggle()
        $(".recharge_fail").toggle()
    }
    $.post('/user/huiNengGasWriteResult', {"token": token, "orderId": orderid, "flag": flag,"cardNo": cardno},
        function (json) {
            Read_GHRQ_DLXCard()
            if (json.errorcode == 0) {
                if(flag == "1"){
                    layer.msg("退款成功")
                }
            }else {
                layer.msg("退款已受理，请等待退款...")
            }
        })
}
function Read_GHRQ_NJYHCard() {
    WebCtl.Read_GHRQ_NJYHCard();
    if (WebCtl.Status == 0) {
        $("input[name=cardno]").val(WebCtl.GHRQ_NJYHCardNo)
        $("input[name=gasvalue]").val(WebCtl.GHRQ_NJYHGas)
        $("input[name=operateId]").val(WebCtl.GHRQ_NJYHOperateId)
        $("input[name=salepointId]").val(WebCtl.GHRQ_NJYHSalepointId)
        $("input[name=bugtimes]").val(WebCtl.GHRQ_NJYHBuyTimes)
        $(".current_gasvalue").text(WebCtl.GHRQ_NJYHGas)
        $("input[name=bugdate]").val(WebCtl.GHRQ_NJYHBuyDate)
    } else {
        GetErrMessage(WebCtl.Status);
    }
}

function Write_GHRQ_NJYHOrders(token, cardno, gasvalue, orderid,operateId,salepointId) {
    WebCtl.Write_GHRQ_NJYHOrders(token, "56001", "0",operateId, salepointId, cardno, Number(gasvalue));
    var flag = "1"
    if (WebCtl.Status == 0) {
        flag = "0"
        $(".recharging").toggle()
        $(".recharge_finish").toggle()
    } else {
        GetErrMessage(WebCtl.Status);
        $(".recharging").toggle()
        $(".recharge_fail").toggle()
    }
    $.post('/user/huiNengGasWriteResult', {"token": token, "orderId": orderid, "flag": flag,"cardNo": cardno},
        function (json) {
            Read_GHRQ_NJYHCard()
            if (json.errorcode == 0) {
                if(flag == "1"){
                    layer.msg("退款成功")
                }
            }else {
                layer.msg("退款已受理，请等待退款...")
            }
        })
}

function GetErrMessage(errno) {
    WebCtl.GetErrMessage(errno);
    layer.msg(WebCtl.Outdata)
}