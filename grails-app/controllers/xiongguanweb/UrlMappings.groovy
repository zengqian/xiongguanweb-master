package xiongguanweb

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/login/login")
        "500"(view:'/error')
        "404"(view:'/notFound')

        "/login"(view: "/login/login")
        "/businesslogin"(view: "/login/business_login")

        "/forget1"(view: "/user/forget_01")

        "/register"(view: "/user/register")

        "/gas_index"(view: "/user/gas_index")
        "/gas_detail"(view: "/user/gas_detail")
        "/gas_add"(view: "/user/gas_add")
        "/gas_query"(view: "/user/gas_query")
        "/gas_query_result"(view: "/user/gas_query_result")
        "/gas_first"(view: "/user/gas_first")
        "/gas_card_recharge_finish"(view: "/user/gas_card_recharge_finish")
        "/gas_card_recharge_info"(view: "/user/gas_card_recharge_info")
        "/gas_card_recharge_working"(view: "/user/gas_card_recharge_working")
        "/gas_readcard"(view: "/user/gas_readcard")
        "/gas_select"(view: "/user/gas_select")
        "/gas_needknow"(view: "/user/gas_needknow")

        "/water_index"(view: "/user/water_index")
        "/water_detail"(view: "/user/water_detail")
        "/water_add"(view: "/user/water_add")
        "/water_query"(view: "/user/water_query")
        "/water_query_result"(view: "/user/water_query_result")

        "/electric_index"(view: "/user/electric_index")
        "/electric_detail"(view: "/user/electric_detail")
        "/electric_add"(view: "/user/electric_add")
        "/electric_query"(view: "/user/electric_query")
        "/electric_query_result"(view: "/user/electric_query_result")

        "/warm_index"(view: "/user/warm_index")
        "/warm_detail"(view: "/user/warm_detail")
        "/warm_add"(view: "/user/warm_add")
        "/warm_query"(view: "/user/warm_query")
        "/warm_query_result"(view: "/user/warm_query_result")

        "/fine_query"(view: "/user/fine_query")
        "/fine_detail"(view: "/user/fine_detail")

        "/oil_add"(view: "/user/oil_add")
        "/oil_question"(view: "/user/oil_question")
        "/oil_recharge"(view: "/user/oil_recharge")
        "/oil_recharge_explain"(view: "/user/oil_recharge_explain")
        "/oil_recharge_record"(view: "/user/oil_recharge_record")


        "/reservation_conveninenceCard"(view: "/user/reservation_conveninenceCard")
        "/reservation_conveninenceCardRecord"(view: "/user/reservation_conveninenceCardRecord")
        "/queryBill"(view: "/user/queryBill")

        "/baihe_integral"(view: "/user/baihe_integral")
        "/customs_letter"(view: "/user/customs_letter")

        "/about_library"(view: "/user/library/about_library")
        "/arrearage_info"(view: "/user/library/arrearage_info")
        "/library_card_info"(view: "/user/library/library_card_info")
        "/card_activate"(view: "/user/library/card_activate")
        "/deposit"(view: "/user/library/deposit")
        "/mylibrary_card"(view: "/user/library/mylibrary_card")
        "/need_know"(view: "/user/library/need_know")
        "/open_time"(view: "/user/library/open_time")
        "/xiongguan_library"(view: "/user/library/xiongguan_library")

        "/myaccount_info"(view: "/user/soical_security/myaccount_info")
        "/mydeposit"(view: "/user/soical_security/mydeposit")
        "/myextract"(view: "/user/soical_security/myextract")
        "/myloan"(view: "/user/soical_security/myloan")
        "/myloan_detail"(view: "/user/soical_security/myloan_detail")
        "/soical_security_index"(view: "/user/soical_security/soical_security_index")

        "/personal_center"(view: "/user/personal_center")
        "/recharge_phone"(view: "/user/recharge_phone")
        "/torealname"(view: "/user/torealname")
        "/realname_info"(view: "/user/realname_info")
        "/realname_checkcode"(view: "/user/realname_checkcode")
        "/realname_paypass"(view: "/user/realname_paypass")

        "/about_convenience_card"(view: "/user/about_convenience_card")
        "/about_convenience_newsinfo"(view: "/user/about_convenience_newsinfo")
        "/convenience_card_application"(view: "/user/convenience_card_application")
        "/convenience_goverment"(view: "/user/convenience_goverment")
        "/service_guide"(view: "/user/service_guide")
        "/serviceguide_info"(view: "/user/serviceguide_info")

        "/about_museum"(view: "/user/about_museum")

        "/dow"(view: "/dowload")
        "/gzh"(view: "/gonzhonhao")
    }
}
