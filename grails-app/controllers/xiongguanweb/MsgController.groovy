package xiongguanweb

import grails.converters.JSON
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class MsgController {
      private static Logger logger = LoggerFactory.getLogger(MsgController.class)
    def msgService
    //发送短息验证码
    def sendPhoneMsg() {
        def data=[:]
        def type = "${params.interfaceCode}"
        def mobile = "${params.mobile}"
        def resDate = msgService.getMess(type,mobile, getSession())
        switch (resDate.code) {
            case 401:
                redirect(uri: "/login/login")
                break
            case 0:
                data.code = resDate.code
                render data as JSON
                break
            default:
                data.code = resDate.code
                data.errormsg = resDate.errormsg
                render data as JSON
                break
        }
    }



    /**
     * 忘记密码验证手机验证码
     */
    def checkPhoneCode () {
        def data=[:]
        def interfaceCode = "${params.interfaceCode}"
        def mobile = "${params.phone}"
        def authCode = "${params.tellCode}"
      def resDate = msgService.checkMessCode(interfaceCode,mobile,authCode,getSession())
        switch (resDate.code) {
            case 401:
                redirect(uri: "/login/login")
                break
            case 0:
                data.code = resDate.code
                render data as JSON
                break
            default:
                data.code = resDate.code
                data.errormsg = resDate.errormsg
                render data as JSON
                break
        }
    }/**
     * 注册验证手机验证码
     */
    def checkRegisterPhoneCode () {
        def data=[:]
        def interfaceCode = "${params.interfaceCode}"
        def mobile = "${params.phone}"
        def authCode = "${params.tellCode}"
      def resData = msgService.checkMessCode(interfaceCode,mobile,authCode,getSession())
        switch (resData.code) {
            case 401:
                redirect(uri: "/login/login")
                break
            case 0:
                render resData as JSON
                break
            default:
                render resData as JSON
                break
        }
    }
}
