package xiongguanweb

import grails.converters.JSON
import org.grails.web.json.JSONObject
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import xiongguanweb.VerifyCodeUtils
import xiongguanweb.http.HttpClient
import xiongguanweb.http.OnResponseListener
import xiongguanweb.http.RequestParams
import xiongguanweb.util.Base64
import xiongguanweb.webserviceclient.*

import javax.imageio.ImageIO
import javax.servlet.http.HttpSession
import java.awt.image.BufferedImage
import java.text.SimpleDateFormat

class UserController {
    private static Logger logger = LoggerFactory.getLogger(UserController.class)
    def userService
    /**
     * 获取图片验证码
     */
    def verifyCode() {
        response.setHeader("Expires", "-1")
        response.setHeader("Cache-Control", "no-cache")
        response.setHeader("Pragma", "-1")

        //生成随机验证码吗，长度为4
        String verifyCode = VerifyCodeUtils.generateVerifyCode(4)
        println verifyCode
        //保存验证码session
        session.setAttribute(AppConfig.VERIFY_CODE, verifyCode)
        //输出到网络流
        VerifyCodeUtils.outputImage(200, 80, response.getOutputStream(), verifyCode)
    }

    /**
     * 验证图片验证码，并销毁
     */

//    @RequestMapping(method = RequestMethod.POST)
    def checkVerifyCode() {
        def verifyCode = "${params.verifyCode}"
        String sessionCode = session.getAttribute(AppConfig.VERIFY_CODE)
        session.removeAttribute(AppConfig.VERIFY_CODE)
        if (verifyCode.equalsIgnoreCase(sessionCode)) {
            render true
        } else {
            render false
        }
    }

    /**
     * 一户通用户注册
     */
    def userRegister() {
        logger.info("register start")
        def data = [:]
        def systemuuid = UUID.randomUUID().toString().replaceAll("-", "")
        logger.info("systemuuid===" + systemuuid)
        def username = "${params.username}"
        def pwd = "${params.password1}"
        def mobile = "${params.phone}"
        CcServiceInObj ccServiceInObj = new CcServiceInObj()
        JSONObject jsonObject = new JSONObject()
        jsonObject.put("userid", mobile)
        jsonObject.put("systemuuid", systemuuid)
        jsonObject.put("systemname", "xgbmk")
        jsonObject.put("password", pwd)
        jsonObject.put("passwordConfir", pwd)
        jsonObject.put("mobile", mobile)
        jsonObject.put("username", username)
        ccServiceInObj.setJsonObj(jsonObject.toString())
        ccServiceInObj.setSystemName("xgbmk")
        CcServiceOutObj out = null
        CCWebSrv service = new CCWebSrv()
        ICCWebSrv serviceImp = service.getCCWebSrvPort()
        logger.info("json==" + jsonObject)
        try {
            out = serviceImp.mainBind(ccServiceInObj)
            logger.info("code==" + out.getErrorCode())
            String jsonobj = out.getJsonObj()
            logger.info("jsonobj==" + jsonobj)
            if (!jsonobj.equals("false")) {
                logger.info("jsonobj==" + jsonobj)
                JSONObject jsonObject1 = com.alibaba.fastjson.JSONObject.parseObject(jsonobj)
                logger.info("jsonObject1=====" + jsonObject1)
                data.data = jsonObject1
            }
            data.code = out.getErrorCode()
        } catch (Exception_Exception e) {
            e.printStackTrace()
        }
        render data as JSON

    }

    /**
     * 用户注册保存信息
     */
    def userRegisterSaveInfo() {
        logger.info("userRegisterSaveInfo...")
        def username = "${params.loginName ?: ''}"
        def uuid = "${params.uuid ?: ''}"
        def pwd = "${params.pwd ?: ''}"
        RequestParams params = new RequestParams()
        params.putBody("loginName", username)
        params.putBody("pwd", pwd)
        params.putBody("uuid", uuid)
        params.putHead("trcode", "1050")
        def resData = [:]
        HttpClient.me().post(getSession(), params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                logger.info("errorcode==" + errorcode)
                resData.errorcode = errorcode
                render resData as JSON
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
                render resData as JSON
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                redirect(uri: "/login")
            }
        })

    }

    //获取用户信息
    def userInfo() {
        log.info("查询用户信息")
        def resData = userService.userInfo(getSession())
        def data = [:]
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                session.setAttribute(AppConfig.APP_USER_INFO, resData.userinfo)
                data.errorcode = resData.errorcode
                data.Authorization = (session.getAttribute(AppConfig.TOKEN))
                data.imgUrl = resData.userinfo.imageUrl
                data.userName = resData.userinfo.userName
                data.isRealname = resData.userinfo.isRealname
                if (data.isRealname == AppConfig.ISREALNAME) {
                    data.isrealname = resData.realnameinfo
                    session.setAttribute(AppConfig.APP_REAL_INFO, resData.realnameinfo)
                }
                render(view: "personal_center", model: [data: resData])
                break
            default:
                render(view: "personal_center", model: [data: resData])
                break
        }
    }

    def getRealInfo() {
        log.info("查询用户信息")
        def resData = userService.userInfo(getSession())
        def data = [:]
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                session.setAttribute(AppConfig.APP_USER_INFO, resData.userinfo)
                data.errorcode = resData.errorcode
                data.isRealname = resData.userinfo.isRealname
                if (data.isRealname == AppConfig.ISREALNAME) {
                    data.isrealname = resData.realnameinfo
                    session.setAttribute(AppConfig.APP_REAL_INFO, resData.realnameinfo)
                }
                render(view: "realname_info", model: [data: resData])
                break
            default:
                render(view: "realname_info", model: [data: resData])
                break
        }

    }

    def lostRealInfo() {
        def cardno = "${params.cardNo}"
        log.info("实名信息解绑")
        def resData = userService.lostRealInfo(cardno, getSession())
        def data = [:]
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                render resData as JSON
                break
            default:
                render resData as JSON
                break
        }
    }

    def upload() {
        def logo = request.getFile('upload_logo')
        def filePath = session.getServletContext().getRealPath("assets/${new Date().format("yyyyMMdd")}/")
        File f = new File(filePath)
        if (!f.exists()) {
            f.mkdirs()
        }


        def logoName = logo.getOriginalFilename()
        def fileSuffix = logoName.substring(logoName.lastIndexOf("."))
        def fileName = "${new Date().format("yyyyMMddHHmmss")}${fileSuffix}"
        def file = new File("${filePath}${fileName}")

        logo.transferTo(file)

        def resDatas = userService.updateLogo(file, getSession())
//        log.info("查询用户信息")
//        def resData = userService.userInfo(getSession())
//        def data = [:]
//        switch (resData.errorcode) {
//            case 401:
//                redirect(uri: "/login")
//                break
//            case 0:
//                session.setAttribute(AppConfig.APP_USER_INFO, resData.userinfo)
//                data.errorcode = resData.errorcode
//                data.Authorization = (session.getAttribute(AppConfig.TOKEN))
//                data.imgUrl = resData.userinfo.imageUrl
//                data.userName = resData.userinfo.userName
//                data.isRealname = resData.userinfo.isRealname
//                if (data.isRealname == AppConfig.ISREALNAME) {
//                    data.isrealname = resData.realnameinfo
//                    session.setAttribute(AppConfig.APP_REAL_INFO, resData.realnameinfo)
//                }
//                render(view: "personal_center", model: [data: resData])
//                break
//            default:
//                render(view: "personal_center", model: [data: resData])
//                break
//        }
        redirect(action: "userInfo")
    }

    /**
     * 判断是否登录
     */
    def isLogin() {
        def data = [:]
        def uuid = session.getAttribute("username")
        def userInfo = session.getAttribute(AppConfig.APP_USER_INFO)
        logger.info("userinfo==" + userInfo)
        if (uuid != null && userInfo != null) {
            data.isLogin = true
            data.logo = userInfo.imageUrl
        } else {
            data.isLogin = false
        }
        render data as JSON
    }

    /**是否实名 （需要实名的业务需先调用该接口再跳转）
     *
     * @return
     */
    def isReal() {
        def data = [:]
        def resData = userService.userInfo(getSession())
        if (resData.userinfo.isRealname == "1") {
            data << [code: 0]
        } else {
            data << [code: 1, msg: "未实名，是否先进行实名。"]
        }
        render data as JSON
    }

    /**
     * 重置登录密码
     */
    def resetLoginpass() {
        logger.info("重置密码start")
        def mobile = "${params.mobile ?: ''}"
        def password = "${params.password ?: ''}"
        def passwordConfirm = "${params.passwordConfirm ?: ''}"
        def data = [:]
        CcServiceInObj ccServiceInObj = new CcServiceInObj()
        JSONObject jsonObject = new JSONObject()
        jsonObject.put("LZBId", mobile)
        jsonObject.put("pwd", password)
        jsonObject.put("confirmPwd", passwordConfirm)
        ccServiceInObj.setSystemName("xgbmk")
        ccServiceInObj.setJsonObj(jsonObject.toString())
        CcServiceOutObj out = null
        CCWebSrv service = new CCWebSrv()
        ICCWebSrv serviceImp = service.getCCWebSrvPort()
        logger.info("重置密码sencond")
        try {
            logger.info("kao:======" + ccServiceInObj.toString())
            out = serviceImp.resetLZBIdPW(ccServiceInObj)
            logger.info("重置密码last" + out.getErrorCode())
            logger.info("重置密码last" + out.getJsonObj())
            data.code = out.getErrorCode()
        } catch (Exception_Exception e) {
            e.printStackTrace()
        }
        render data as JSON
    }

    /**
     * 获取水费户头
     */
    def getWater() {
        log.info("查询水费户头")
        def resData = userService.getAccountInfo(getSession(), "31")
        def data = [:]
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                logger.info("水投用户：" + resData.data)
                render(view: "water_index", model: [data: resData.data])
                break
            default:
                logger.info("error:" + resData)
//                data.errorcode = resData.errorcode
                render(view: "water_index", model: [data: resData.data])
                break
        }
    }

    /** 添加水费户头
     *
     * @return
     */
    def addWater() {
        logger.info("添加水费户头")
        def watercom = userService.getRechargeCom("31", getSession())
        render(view: "water_add", model: [data: watercom.data])
    }
    /**
     * 水费户头详情
     */
    def queryWater() {
        logger.info("水费户头详情")
        def userNo = "${params.userNo ?: ''}"
        def branchGname = "${params.branchGname ?: ''}"
        def branchGuid = "${params.branchGuid ?: ''}"
        def name = "${params.name ?: ''}"
        def address = "${params.address ?: ''}"
        def odertype = "${params.odertype ?: ''}"
        def certNo = session.getAttribute(AppConfig.APP_REAL_INFO).certNo
//        def resData = userService.queryWaterBalance(userNo, getSession())
        def data = [:]
        switch (0) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                data = [userNo: userNo, certNo: certNo, branchGname: branchGname, address: address, name: name, branchGuid: branchGuid, odertype: odertype]
                render(view: "water_detail", model: [data: data])
                break
            default:
                render(view: "water_detail", model: [data: data])
                break
        }
    }

    /**
     * 水费下单
     * @return
     */
    def waterRecharge() {
        def userno = "${params.userno ?: ''}"
        def branchguid = "${params.branchguid ?: ''}"
        def ordertype = "${params.odertype ?: ''}"
        def payment = "${params.payment ?: ''}"
        def username = session.getAttribute(AppConfig.APP_USER_INFO).phoneNo
        def resData = userService.getWaterOrder(username, getSession(), ordertype, payment, userno, branchguid)
        switch (resData.code) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                def orderno = resData.data.orderId
                logger.info("orderno===" + orderno)
                resData.code = 0
                resData.orderno = orderno
                render resData as JSON
                break
            default:
                render resData as JSON
                break
        }
    }
    /** 绑定水投账户(添加)
     *
     * @return
     */
    def bindWater() {
        logger.info("绑定水投账户(添加)")
        def alisName = "${params.alisName ?: ''}"
        def name = "${params.name ?: ''}"
        def userNp = "${params.userNp ?: ''}"
        def branchguid = "${params.bankAccNo ?: ''}"//
        def branchgname = "${params.branchgname ?: ''}"
        def address = "测试"
        def dealType = "31"//55 燃气 31 水费
        def resData = userService.bindWaterAccount(alisName, name, userNp, branchguid, branchgname, address, dealType, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                render resData as JSON
                break
            default:
                render resData as JSON
                break
        }

    }
    /**
     * 忘记户号查询缴费机构列表
     * @return
     */
    def forgetWaterAccount() {
        logger.info("忘记户号缴费机构列表")
        def watercom = userService.getRechargeCom("31", getSession())
        render(view: "water_query", model: [data: watercom.data])
    }
    /**
     * 忘记户号查询
     * @return
     */
    def forgetWaterQuery() {
        logger.info("忘记户号查询")
        def branchguid = "${params.selbranch ?: ''}"//100310000000001
        def branchgname = "${params.branchgname ?: ''}"
        def bankAccNo = "${params.bankAccNo ?: ''}"//F4EC6FBD-8DFA-4B3E-BB03-99D3D7991DA7
        def certNo = "${params.certNo ?: ''}"
        def name = "${params.name ?: ''}"
        def resData = userService.queryWaterAccount(bankAccNo, certNo, name, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                resData.code = 0
                resData.branchgname = branchgname
                resData.bankAccNo = bankAccNo
                resData.certNo = certNo
                render(view: "water_query_result", model: [data: resData])
                break
            default:
                render(view: "water_query_result", model: [data: resData])
                break
        }
    }
    /** (忘记户号)绑定水投账户
     *
     * @return
     */
    def bindWaterAccount() {
        logger.info("绑定水投账户")
        def alisName = "测试"
        def name = "${params.username ?: ''}"
        def userNp = "${params.usercode ?: ''}"
        def branchguid = "${params.bankAccNo ?: ''}"
        def branchgname = "${params.branchgname ?: ''}"
        def address = "${params.addr ?: ''}"
        def dealType = "31"//55 燃气 31 水费
        def resData = userService.bindWaterAccount(alisName, name, userNp, branchguid, branchgname, address, dealType, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                render resData as JSON
                //render(view: "water_query_result", model: [data: resData])
                break
            default:
                render resData as JSON
                break
        }
    }
    /** 删除水投账户
     *
     * @return
     */
    def delWater() {
        logger.info("删除水投账户")
        def name = "${params.name ?: ''}"
        def userNp = "${params.userNp ?: ''}"
        def dealType = "31"//55 燃气 31 水费
        def resData = userService.unBindAccount(name, userNp, dealType, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                render resData as JSON
                break
            default:
                render resData as JSON
                break
        }
    }
    /**
     * 获取燃气户头
     */
    def getGas() {
        log.info("获取燃气户头")
        def resData = userService.getAccountInfo(getSession(), "55")
        def data = [:]
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                logger.info("燃气用户：" + resData.data)
                render(view: "gas_index", model: [data: resData.data])
                break
            default:
                logger.info("error:" + resData)
//                data.errorcode = resData.errorcode
                render(view: "gas_index", model: [data: resData.data])
                break
        }
    }
    /**
     * 燃气户头详情
     */
    def queryGas() {
        logger.info("燃气户头详情")
        //address alisName branchGname branchGuid feesNo name
        def odertype = "${params.odertype ?: ''}"//
        def address = "${params.address ?: ''}"
        def branchGname = "${params.branchGname ?: ''}"
        def name = "${params.name ?: ''}"
        def userNo = "${params.feesNo ?: ''}"
        def branchGuid = "${params.branchGuid ?: ''}"
        //查询余额接口
        def resData = userService.queryGasBalance(branchGuid, userNo, getSession())
        def data = [:]
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                //branchGname  userNo name certNo  address accountbalance
                data = [branchGuid: branchGuid, odertype: odertype, branchGname: branchGname, userNo: userNo, name: name, certNo: resData.data.icNumber, address: address, accountbalance: resData.data.accountbalance]
                render(view: "gas_detail", model: [data: data])
                break
            default:
                render(view: "gas_detail", model: [data: data])
                break
        }
    }
    /** 添加燃气户头(返回机构列表)
     *
     * @return
     */
    def addGas() {
        logger.info("添加燃气户头")
        def gasrcom = userService.getRechargeCom("55", getSession())
        render(view: "gas_add", model: [data: gasrcom.data])
    }
    /** 绑定燃气账户(添加)
     *
     * @return
     */
    def bindGas() {
        logger.info("绑定燃气账户(添加)")
        def alisName = "${params.alisName ?: ''}"
        def name = "${params.name ?: ''}"
        def userNp = "${params.userNp ?: ''}"
        def branchguid = "${params.branchguid ?: ''}"//
        def branchgname = "${params.branchgname ?: ''}"
        def address = ""
        def dealType = "55"//55 燃气 31 水费
        def resData = userService.bindGasAccount(alisName, name, userNp, branchguid, branchgname, address, dealType, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                render resData as JSON
                break
            default:
                render resData as JSON
                break
        }
    }
    /**
     * 忘记户号查询缴费机构列表
     * @return
     */
    def forgetGas() {
        logger.info("忘记户号缴费机构列表")
        def gasrcom = userService.getRechargeCom("55", getSession())
        render(view: "gas_query", model: [data: gasrcom.data])
    }
    /**
     * （燃气）忘记户号查询
     * @return
     */
    def forgetGasQuery() {
        logger.info("忘记户号查询（燃气）")
        def branchgname = "${params.branchgname ?: ''}"
        def branchguid = "${params.branchguid ?: ''}"//F4EC6FBD-8DFA-4B3E-BB03-99D3D7991DA7
        def certNo = "${params.certNo ?: ''}"
        def name = "${params.name ?: ''}"
        def resData = userService.queryGasAccount(branchguid, certNo, name, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                resData.code = 0
                resData.branchgname = branchgname
                resData.branchguid = branchguid
                render(view: "gas_query_result", model: [data: resData])
                break
            default:
                render(view: "gas_query_result", model: [data: resData])
                break
        }

    }
    /** 忘记户号查询绑定(燃气)
     *
     * @return
     */
    def bindGasAccount() {
        logger.info("忘记户号查询绑定(燃气)")
        def alisName = "测试"
        def name = "${params.name ?: ''}"
        def userNp = "${params.userNp ?: ''}"
        def branchguid = "${params.branchguid ?: ''}"//
        def branchgname = "${params.branchgname ?: ''}"
        def address = "${params.address ?: ''}"
        def dealType = "55"//55 燃气 31 水费
        def resData = userService.bindGasAccount(alisName, name, userNp, branchguid, branchgname, address, dealType, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                render resData as JSON
                break
            default:
                render resData as JSON
                break
        }
    }
    /** 删除燃气账户
     *
     * @return
     */
    def delGas() {
        logger.info("删除燃气账户")
        def name = "${params.name ?: ''}"
        def userNp = "${params.userNp ?: ''}"
        def dealType = "55"//55 燃气 31 水费
        def resData = userService.unBindAccount(name, userNp, dealType, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                render resData as JSON
                break
            default:
                render resData as JSON
                break
        }
    }

    /**
     * 燃气下单
     * @return
     */
    def gasRecharge() {
        def userno = "${params.userno}"
        def branchguid = "${params.branchguid}"
        def ordertype = "${params.odertype}"
        def payment = "${params.payment}"
        def username = session.getAttribute(AppConfig.APP_USER_INFO).phoneNo//手机号即用户名
        def resData = userService.getGasOrder(username, getSession(), ordertype, payment, userno, branchguid)
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                def orderno = resData.data.orderId//百合易付订单号
                resData.code = 0
                resData.orderno = orderno
                render resData as JSON
                break
            default:
                render resData as JSON
                break
        }
    }
    /** 话费充值
     * @return
     */
    def phoneRecharge() {
        logger.info("话费充值")
        def flag = "${params.mobile_company ?: ''}"//1移动2联通3 电信
        def mobile = "${params.phone ?: ''}"//手机号
        def payment = "${params.amount ?: ''}"//充值金额
        def resData = userService.getPhoneOrder(flag, mobile, payment, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                def orderno = resData.data.orderId//百合易付订单号
                logger.info("orderno===" + orderno)
                resData.orderno = orderno
                render resData as JSON
                break
            default:
                render resData as JSON
                break
        }
    }
    /** 违章罚单查询
     * @return
     */
    def queryFine() {
        logger.info("违章罚单查询")
        def fineNo = "${params.fineNo ?: ''}"
        def resData = userService.getFineInfo(fineNo, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                logger.info("data==" + resData.toString())
                render resData as JSON//fine_detail
                break
            default:
                render resData as JSON
                break
        }
    }
    /** 返回罚单详情页面
     * @return
     */
    def toFineInfo() {
        logger.info("返回罚单详情页面")
        def PaySnglNo = "${params.PaySnglNo ?: ''}"//罚单号
        def RltnpNm = "${params.RltnpNm ?: ''}"
        def PltNo = "${params.PltNo ?: ''}"
        def TotAmt = "${params.TotAmt ?: ''}"
        def PylAmt = "${params.PylAmt ?: ''}"
        def LtPymtAmt = "${params.LtPymtAmt ?: ''}"
        def PaySnglTyp = "${params.PaySnglTyp ?: ''}"
        def DrvsLcnsNo = "${params.DrvsLcnsNo ?: ''}"
        def FileNo = "${params.FileNo ?: ''}"
        def AtmbLicTp = "${params.AtmbLicTp ?: ''}"
        def DealYr = "${params.DealYr ?: ''}"
        def DealTm = "${params.DealTm ?: ''}"
        def ExecUnitNm = "${params.ExecUnitNm ?: ''}"
        def ExecInstNm = "${params.ExecInstNm ?: ''}"
        def PyeInstCd = "${params.PyeInstCd ?: ''}"
        def data = [PaySnglNo : PaySnglNo, RltnpNm: RltnpNm, PltNo: PltNo, TotAmt: TotAmt, PylAmt: PylAmt, LtPymtAmt: LtPymtAmt,
                    PaySnglTyp: PaySnglTyp, DrvsLcnsNo: DrvsLcnsNo, FileNo: FileNo, AtmbLicTp: AtmbLicTp, DealYr: DealYr,
                    DealTm    : DealTm, ExecUnitNm: ExecUnitNm, ExecInstNm: ExecInstNm, PyeInstCd: PyeInstCd]
        render(view: "fine_detail", model: [data: data])
    }
    /**
     * 违章罚款缴费
     */
    def fineRecharge() {
        logger.info("违章罚款缴费")
        def PaySnglNo = "${params.PaySnglNo ?: ''}"//罚单号
        def RltnpNm = "${params.RltnpNm ?: ''}"
        def PltNo = "${params.PltNo ?: ''}"
        def TotAmt = "${params.TotAmt ?: ''}"
        def PylAmt = "${params.PylAmt ?: ''}"
        def LtPymtAmt = "${params.LtPymtAmt ?: ''}"
        def PaySnglTyp = "${params.PaySnglTyp ?: ''}"
        def DrvsLcnsNo = "${params.DrvsLcnsNo ?: ''}"
        def FileNo = "${params.FileNo ?: ''}"
        def AtmbLicTp = "${params.AtmbLicTp ?: ''}"
        def DealYr = "${params.DealYr ?: ''}"
        def DealTm = "${params.DealTm ?: ''}"
        def ExecUnitNm = "${params.ExecUnitNm ?: ''}"
        def ExecInstNm = "${params.ExecInstNm ?: ''}"
        def PyeInstCd = "${params.PyeInstCd ?: ''}"
        def body = [PaySnglNo : PaySnglNo, RltnpNm: RltnpNm, PltNo: PltNo, TotAmt: TotAmt, PylAmt: PylAmt, LtPymtAmt: LtPymtAmt,
                    PaySnglTyp: PaySnglTyp, DrvsLcnsNo: DrvsLcnsNo, FileNo: FileNo, AtmbLicTp: AtmbLicTp, DealYr: DealYr,
                    DealTm    : DealTm, ExecUnitNm: ExecUnitNm, ExecInstNm: ExecInstNm, PyeInstCd: PyeInstCd]
        def resData = userService.getFineOrder(body, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                def orderno = resData.data.paymentThirdId
                logger.info("orderno===" + orderno)
                resData.code = 0
                def order = "function=PayUniform&transaction_id=" + orderno
                resData.method = Base64.encode(order.getBytes()).replace("=", "")
                resData.orderno = orderno
                render resData as JSON
                break
            default:
                render resData as JSON
                break
        }
    }


    /** 获取汇能燃气机构列表
     *
     * @return
     */
    def getHuiNengGasList() {
        RequestParams params = new RequestParams()
        params.putBody("orderType", "56001")
        params.putHead("trcode", "5603")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(getSession(), params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                logger.info("data==" + data)
                List<Object> list = com.alibaba.fastjson.JSON.parseArray(data)
                logger.info("list==" + list)
                resData.errorcode = errorcode
                resData.data = list
                render(view: "gas_select", model: [data: resData])
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
                render(view: "gas_select", model: [data: resData])
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                redirect(uri: "/login")
            }
        })
    }

    /** 根据选取的卡类型跳转读卡页面
     *
     * @return
     */
    def toGasRead() {
        def data = [:]
        def company = "${params.company ?: ''}"
        def cardType = "${params.cardType ?: ''}"
        data.company = company
        data.cardType = cardType
        logger.info("卡类型：" + cardType)
        render(view: "gas_readcard", model: [data: data])
    }

    /** 查询卡信息
     *
     * @return
     */
    def queryGasInfo() {
        logger.info("queryGasInfo...")
        def gasAmount = "${params.gasamount ?: ''}"
        def CustomerId = "${params.CustomerId ?: ''}"
        def cardNo = "${params.cardno ?: ''}"
        def gasNum = "${params.gasvalue ?: ''}"
        def bugtimes = "${params.bugtimes ?: ''}"
        def operateId = "${params.operateId ?: ''}"
        def salepointId = "${params.salepointId ?: ''}"
        RequestParams params = new RequestParams()
        params.putBody("buyGasQuantity", gasAmount)
        params.putBody("gusCardNo", cardNo)
        params.putBody("cardGasNum", gasNum)
        params.putHead("trcode", "5601")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(getSession(), params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                logger.info("data==" + data)
                def jsonobject = com.alibaba.fastjson.JSON.parseObject(data)
                logger.info("jsonobject==" + jsonobject)
                resData.errorcode = errorcode
                resData.data = jsonobject
                jsonobject += ["buyGasQuantity": gasAmount, "bugtimes": bugtimes, cardnos: cardNo, CustomerId: CustomerId, operateId: operateId, salepointId: salepointId]
                logger.info("jsonobject==" + jsonobject)
                session.setAttribute(AppConfig.GAS_INFO, jsonobject)
                render resData as JSON
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
                render resData as JSON
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                redirect(uri: "/login")
            }
        })
    }

    /**汇能燃气缴费
     *
     */
    def gasorder() {
        def buyGasQuantity = "${params.gasamount ?: ''}"
        def cardType = "${params.cardType ?: ''}"
        println cardType
        def branchguid = userService.getRechargeCom("56001", getSession()).data[0].bankAccNo
        def resData = userService.gasrecharge(getSession(), buyGasQuantity, branchguid, cardType)
        switch (resData.code) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                def orderno = resData.data.orderId
                logger.info("orderno===" + orderno)
                resData.code = 0
//                def order = "function=PayUniform&transaction_id=" + orderno
//                resData.method = Base64.encode(order.getBytes()).replace("=", "")
                resData.orderno = orderno
                //为写卡操作存放orderid
                session.setAttribute("orderid", resData.data.orderId)
                render resData as JSON
                break
            default:
                render resData as JSON
                break
        }
    }

    /**
     * 获取汇能写卡token
     */
    def getGasToken() {
        def gasinfo = session.getAttribute(AppConfig.GAS_INFO)
        def orderid = session.getAttribute("orderid")
//        def orderid = "2018120509513500000005690"
        RequestParams params = new RequestParams()
        params.putBody("target", "56001")
        params.putBody("orderId", orderid ?: '')
        params.putBody("cardNo", gasinfo.cardnos)
        params.putHead("trcode", "5604")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(getSession(), params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                logger.info("data==" + data)
                def jsonobject = com.alibaba.fastjson.JSON.parseObject(data)
                logger.info("jsonobject==" + jsonobject)
                resData.errorcode = errorcode
                resData.token = jsonobject.token
                resData.cardno = gasinfo.cardnos
                resData.CustomerId = gasinfo.CustomerId
                resData.orderid = orderid
                resData.cardType = jsonobject.cardType
                resData.buytimes = gasinfo.bugtimes
                resData.gasvalue = gasinfo.buyGasQuantity
                resData.salepointId = gasinfo.salepointId
                resData.operateId = gasinfo.operateId
                render resData as JSON
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
                render resData as JSON
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                redirect(uri: "/login")
            }
        })
    }

    /**
     * 汇能燃气写卡结果返回
     */
    def huiNengGasWriteResult() {
        logger.info("huineng fnahui")
        def token = "${params.token ?: ''}"
        def orderId = "${params.orderId ?: ''}"
        def flag = "${params.flag ?: ''}"
        def cardno = "${params.cardNo ?: ''}"
        RequestParams params = new RequestParams()
        params.putBody("token", token)
        params.putBody("orderId", orderId)
        params.putBody("cardNo", cardno)
//        params.putBody("orderId", "2018120509513500000005690")
        params.putBody("flag", flag)
        params.putHead("trcode", "5606")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(getSession(), params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                logger.info("data==" + data)
                def jsonobject = com.alibaba.fastjson.JSON.parseObject(data)
                resData.errorcode = errorcode
                resData.data = jsonobject
                render resData as JSON
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
                render resData as JSON
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                redirect(uri: "/login")
            }
        })

    }
    /**
     *  判断图书馆应用有无激活以及有无缴纳信息
     * @return
     */
    def judgeLibrary() {
        def certNo = session.getAttribute(AppConfig.APP_REAL_INFO).certNo
        def userType = "001";
        def resData = userService.queryMyLibCard(userType, certNo, getSession())
        println(resData.isActive)
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            default:
                render resData as JSON
                break
        }
    }
    /**
     * 获取图书馆用户信息
     * @return
     */
    def getLibrary() {
        logger.info("获取图书馆用户信息")
        def certNo = session.getAttribute(AppConfig.APP_REAL_INFO).certNo
        def userType = "001";
        def resData = userService.queryMyLibCard(userType, certNo, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                if (resData.user) {
                    resData.order_id = resData.user.order_id
                }
                render(view: "/user/library/xiongguan_library", model: [data: resData])
                break
            default:
                render(view: "/user/library/xiongguan_library", model: [data: resData])
                break
        }
    }
    /**
     * 获取图书馆借阅证
     * @return
     */
    def queryMyLibCard() {
        logger.info("获取图书馆借阅证")
        def certNo = session.getAttribute(AppConfig.APP_REAL_INFO).certNo
        def userType = "001";
        def resData = userService.queryMyLibCard(userType, certNo, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                if (resData.isActive == 0) {//应用未激活跳转至激活页面
                    redirect(uri: "/card_activate")
                } else {
                    if (resData.user) {
                        resData.order_id = resData.user.order_id
                        //转换支付时间
                        String payment_time = resData.user.payment_time
                        if (payment_time.equals("0")) {
                            resData.user.payment_time = "";
                        } else {
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                            resData.user.payment_time = sdf.format(new Date(Long.parseLong(payment_time)))
                        }
                        //转换用户等级userLevel
                        switch (resData.user.userLevel) {
                            case "001":
                                resData.user.userLevel = "临时用户";
                                break
                            case "002":
                                resData.user.userLevel = "便民卡用户";
                                break
                            case "003":
                                resData.user.userLevel = "普通用户";
                                break
                            case "004":
                                resData.user.userLevel = "高级用户";
                                break
                            case "005":
                                resData.user.userLevel = "较高级用户";
                                break
                            default:
                                resData.user.userLevel = "其他"
                                break
                        }
                        //转换支付方式payment_type
                        switch (resData.user.payment_type) {
                            case "0":
                                resData.user.payment_type = ""
                                break
                            case "0002":
                                resData.user.payment_type = "支付宝"
                                break
                            case "0003":
                                resData.user.payment_type = "财付通"
                                break
                            case "0004":
                                resData.user.payment_type = "银联"
                                break
                            case "0005":
                                resData.user.payment_type = "综合积分"
                                break
                            case "0002":
                                resData.user.payment_type = "支付宝"
                                break
                            case "0006":
                                resData.user.payment_type = "手机快付"
                                break
                            case "0007":
                                resData.user.payment_type = "中金支付"
                                break
                            case "0008":
                                resData.user.payment_type = "面值卡"
                                break
                            case "0009":
                                resData.user.payment_type = "百合易付"
                                break
                            case "0010":
                                resData.user.payment_type = "微信支付"
                                break
                            case "0011":
                                resData.user.payment_type = "电子钱包闪付"
                                break
                            case "0012":
                                resData.user.payment_type = "超级网银"
                                break
                            case "0013":
                                resData.user.payment_type = "银联无跳转"
                                break
                            case "0014":
                                resData.user.payment_type = "通联支付"
                                break
                            case "0015":
                                resData.user.payment_type = "银商支付"
                                break
                            case "0016":
                                resData.user.payment_type = "积分联盟"
                                break
                            default:
                                resData.user.payment_type = "其他"
                                break
                        }


                    }
                    render(view: "/user/library/mylibrary_card", model: [data: resData])
                }
                break
            default:
                render(view: "/user/library/mylibrary_card", model: [data: resData])
                break
        }

    }
    /**
     * 图书馆应用激活
     * @return
     */
    def activeLibraryCard() {
        logger.info("图书馆用户激活")
        def certNo = session.getAttribute(AppConfig.APP_REAL_INFO).certNo
        def userType = "001";
        def resData = userService.activeLibraryCard(userType, certNo, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                render resData as JSON
                break
            default:
                render resData as JSON
                break
        }
    }
    /**
     * 图书馆借阅信息
     * @return
     */
    def querylibReadInfo() {
        logger.info("图书馆借阅信息")
        def index = "${params.page ?: '1'}"//页码，默认第一页
        def pageSize = "${params.pageSize ?: '10'}"//默认每页显示数量10
        def certNo = session.getAttribute(AppConfig.APP_REAL_INFO).certNo
        def userType = "001";
        def judedata = userService.queryMyLibCard(userType, certNo, getSession())
        if (judedata.isActive == 0) {//未激活直接跳转至激活页面
            redirect(uri: "/card_activate")
        } else {
            def resData = userService.querylibReadInfo(userType, certNo, index, pageSize, getSession())
            switch (resData.errorcode) {
                case 401:
                    redirect(uri: "/login")
                    break
                case 0:
                    resData.page = index
                    render(view: "/user/library/library_card_info", model: [data: resData])
                    break
                default:
                    render(view: "/user/library/library_card_info", model: [data: resData])
                    break
            }
        }

    }
    /**
     * 图书馆押金退还
     * @return
     */
    def refundLibrary() {
        logger.info("图书馆押金退还")
        def oldOrderId = "${params.orderId ?: ''}"//原商户订单号
        def amt = "${params.amt ?: ''}"
        def certNo = session.getAttribute(AppConfig.APP_REAL_INFO).certNo
        def userType = "001"
        def dealType = "005"
        def resData = userService.depositRefunded(oldOrderId, amt, dealType, userType, certNo, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                def orderno = resData.data.paymentThirdId
                logger.info("orderno===" + orderno)
                resData.code = 0
                def order = "function=PayUniform&transaction_id=" + orderno
                resData.method = Base64.encode(order.getBytes()).replace("=", "")
                resData.orderno = orderno
                render resData as JSON
                break
            default:
                render resData as JSON
                break
        }
    }
    /**
     * 图书馆缴纳押金
     * @return
     */
    def payLibrary() {
        logger.info("图书馆缴纳押金")
        def username = session.getAttribute(AppConfig.APP_USER_INFO).phoneNo
        def certNo = session.getAttribute(AppConfig.APP_REAL_INFO).certNo
        def resData = userService.payLibrary(username, certNo, "001", "001", "100", getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                def orderno = resData.data.paymentThirdId
                logger.info("orderno===" + orderno)
                resData.code = 0
                def order = "function=PayUniform&transaction_id=" + orderno
                resData.method = Base64.encode(order.getBytes()).replace("=", "")
                resData.orderno = orderno
                render resData as JSON
                break
            default:
                render resData as JSON
                break
        }
    }
    /**
     * 查询图书馆欠费信息
     * @return
     */
    def arrearageLibrary() {
        logger.info("查询图书馆欠费信息")
        def certNo = session.getAttribute(AppConfig.APP_REAL_INFO).certNo
        def resData = userService.arrearageInfo(certNo, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                render(view: "/user/library/arrearage_info", model: [data: resData.data])
                break
            default:
                render(view: "/user/library/arrearage_info", model: [data: resData.data])
                break
        }
    }
    /**
     * 图书馆欠费缴纳
     * @return
     */
    def payArrearage() {
        logger.info("图书馆欠费缴纳")
        def username = session.getAttribute(AppConfig.APP_USER_INFO).phoneNo
        def certNo = session.getAttribute(AppConfig.APP_REAL_INFO).certNo
        def userType = "001"
        def dealType = "${params.type ?: ''}"
        def amt = "${params.amt ?: ''}"
        def resData = userService.payLibrary(username, certNo, userType, dealType, amt, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                def orderno = resData.data.paymentThirdId
                logger.info("orderno===" + orderno)
                resData.code = 0
                def order = "function=PayUniform&transaction_id=" + orderno
                resData.method = Base64.encode(order.getBytes()).replace("=", "")
                resData.orderno = orderno
                render resData as JSON
                break
            default:
                render resData as JSON
                break
        }
    }
    /**
     * 雄关信用查询
     * @return
     */
    def queryCredit() {
        logger.info("雄关信用查询")
        def name = session.getAttribute(AppConfig.APP_USER_INFO).userName
        def certNo = session.getAttribute(AppConfig.APP_REAL_INFO).certNo
        def resData = userService.queryCreditInfo(name, certNo, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                String crLvl = resData.data.service.BODY.CrLvl //信用余额，可贷款金额，用户星级
                println("crLvl===" + crLvl)
                String[] str = crLvl.split(",")
                resData.score = str[0]
                resData.amt = str[1]
                int le = Integer.parseInt(str[2]) //需要字典转换
                if (le >= 350 && le < 550) {
                    resData.level = "信用较差"
                } else if (le >= 550 && le < 600) {
                    resData.level = "信用中等"
                } else if (le >= 600 && le < 650) {
                    resData.level = "信用良好"
                } else if (le >= 650 && le < 700) {
                    resData.level = "信用优秀"
                } else if (le >= 700 && le < 950) {
                    resData.level = "信用极好"
                } else {
                    resData.level = "信用数据有误"
                }
                render(view: "customs_letter", model: [data: resData])
                break
            default:
                render(view: "customs_letter", model: [data: resData])
                break
        }
    }

    /**
     * 百合积分查询
     * @return
     */
    def queryScore() {
        logger.info("百合积分查询")
        def bankCardNo = session.getAttribute(AppConfig.APP_REAL_INFO).cardNo
        def resData = userService.queryScoreInfo(bankCardNo, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                resData.score = resData.data.service.BODY.IntgrAcctList.IntgrAcctMssgArray.IntgrBal //积分
                render(view: "baihe_integral", model: [data: resData])
                break
            default:
                render(view: "baihe_integral", model: [data: resData])
                break
        }
    }
    /**
     * 公积金查询
     * @return
     */
    def queryFund() {
        logger.info("公积金查询")
        def name = session.getAttribute(AppConfig.APP_USER_INFO).userName
        def certNo = session.getAttribute(AppConfig.APP_REAL_INFO).certNo
        def resData = userService.queryFundInfo(name, certNo, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                render(view: "/user/soical_security/soical_security_index", model: [data: resData.data])
                break
            default:
                println "data=========" + resData
                render(view: "/user/soical_security/soical_security_index", model: [data: resData])
                break
        }
    }
    /**
     * 公积金账户信息
     * @return
     */
    def queryFundAcc() {
        logger.info("公积金账户信息")
        def name = session.getAttribute(AppConfig.APP_USER_INFO).userName
        def certNo = session.getAttribute(AppConfig.APP_REAL_INFO).certNo
        def resData = userService.queryFundInfo(name, certNo, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                render(view: "/user/soical_security/myaccount_info", model: [data: resData.data])
                break
            default:
                render(view: "/user/soical_security/myaccount_info", model: [data: resData.data])
                break
        }
    }
    /**
     * 公积金提取信息
     * @return
     */
    def queryFundExtract() {
        logger.info("公积金提取信息")
        def name = session.getAttribute(AppConfig.APP_USER_INFO).userName
        def certNo = session.getAttribute(AppConfig.APP_REAL_INFO).certNo
        def resData = userService.queryFundInfo(name, certNo, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                render(view: "/user/soical_security/myextract", model: [data: resData.data])
                break
            default:
                render(view: "/user/soical_security/myextract", model: [data: resData.data])
                break
        }
    }
    /**
     * 公积金缴存信息
     * @return
     */
    def queryFundDeposit() {
        logger.info("公积金缴存信息")
        def name = session.getAttribute(AppConfig.APP_USER_INFO).userName
        def certNo = session.getAttribute(AppConfig.APP_REAL_INFO).certNo
        def resData = userService.queryFundInfo(name, certNo, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                render(view: "/user/soical_security/mydeposit", model: [data: resData.data])
                break
            default:
                render(view: "/user/soical_security/mydeposit", model: [data: resData.data])
                break
        }
    }
    /**
     * 公积金贷款信息
     * @return
     */
    def queryFundLoan() {
        logger.info("公积金贷款信息")
        def name = session.getAttribute(AppConfig.APP_USER_INFO).userName
        def certNo = session.getAttribute(AppConfig.APP_REAL_INFO).certNo
        def resData = userService.queryFundInfo(name, certNo, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                render(view: "/user/soical_security/myloan", model: [data: resData.data])
                break
            default:
                render(view: "/user/soical_security/myloan", model: [data: resData.data])
                break
        }
    }
    /**
     * 公积金贷款详细信息
     * @return
     */
    def queryFundLoanDetail() {
        logger.info("公积金贷款详细信息")
        def PvdFndAcctNo2 = "${params.PvdFndAcctNo2 ?: ''}"
        def LoanNo = "${params.LoanNo ?: ''}"
        def PvdFndLoanAmt = "${params.PvdFndLoanAmt ?: ''}"
        def PvdFndLoanTerm = "${params.PvdFndLoanTerm ?: ''}"
        def PvdFndLoanRpyMth = "${params.PvdFndLoanRpyMth ?: ''}"
        def PvdFndLoanMoRpyAmt = "${params.PvdFndLoanMoRpyAmt ?: ''}"
        def PvdFndLoanStrtDt = "${params.PvdFndLoanStrtDt ?: ''}"
        def PvdFndLoanExpDt = "${params.PvdFndLoanExpDt ?: ''}"
        def PvdFndLoanClsDt = "${params.PvdFndLoanClsDt ?: ''}"
        def PvdFndLoanRpyBal = "${params.PvdFndLoanRpyBal ?: ''}"
        def ComLndrNm = "${params.ComLndrNm ?: ''}"
        def ComLndrIdCrdNo = "${params.ComLndrIdCrdNo ?: ''}"
        def ComLndEstbPvdFndFlg = "${params.ComLndEstbPvdFndFlg ?: ''}"
        def CtcAdr1 = "${params.CtcAdr1 ?: ''}"
        def LoanSt = "${params.LoanSt ?: ''}"
        def PrdRpySt = "${params.PrdRpySt ?: ''}"
        def OdueAmt = "${params.OdueAmt ?: ''}"
        def OdueTms = "${params.OdueTms ?: ''}"
        def MxCtnsOdueTms = "${params.MxCtnsOdueTms ?: ''}"
        def resData = [PvdFndAcctNo2   : PvdFndAcctNo2, LoanNo: LoanNo, PvdFndLoanAmt: PvdFndLoanAmt, PvdFndLoanTerm: PvdFndLoanTerm,
                       PvdFndLoanRpyMth: PvdFndLoanRpyMth, PvdFndLoanMoRpyAmt: PvdFndLoanMoRpyAmt, PvdFndLoanStrtDt: PvdFndLoanStrtDt, PvdFndLoanExpDt: PvdFndLoanExpDt, PvdFndLoanClsDt: PvdFndLoanClsDt,
                       PvdFndLoanRpyBal: PvdFndLoanRpyBal, ComLndrNm: ComLndrNm, ComLndrIdCrdNo: ComLndrIdCrdNo, ComLndEstbPvdFndFlg: ComLndEstbPvdFndFlg, CtcAdr1: CtcAdr1, LoanSt: LoanSt, PrdRpySt: PrdRpySt, OdueAmt: OdueAmt,
                       OdueTms         : OdueTms, MxCtnsOdueTms: MxCtnsOdueTms
        ]
        render(view: "/user/soical_security/myloan_detail", model: [data: resData])
    }
    /**
     * 网点列表查询
     * @return
     */
    def queryCardSiteList() {
        logger.info("网点列表查询")
        def resData = userService.querySiteList(getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                render(view: "reservation_conveninenceCard", model: [data: resData.data])
                break
            default:
                render(view: "reservation_conveninenceCard", model: [data: resData.data])
                break
        }
    }
    /**
     * 获取短信验证码
     * @return
     */
    def getAuthCode() {
        logger.info("获取短信验证码")
        def mobile = "${params.mobile ?: ""}"
        def resData = userService.getAuthCode(mobile, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                resData.code = resData.data.code
                render resData as JSON
                break
            default:
                println(resData)
                render resData as JSON
                break
        }
    }
    /**
     * 便民卡预约
     * @return
     */
    def reservationCard() {
        logger.info("便民卡预约")
        def name = "${params.username ?: ""}"
        def certNo = "${params.certno ?: ""}"
        def siteId = "${params.branchid ?: ""}"
        def mobile = "${params.phone ?: ""}"
        String authCode = "${params.code ?: ""}"
        def resData = [:]
        resData = userService.reservationCard(name, certNo, siteId, mobile, authCode, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                render resData as JSON
                break
            default:
                render resData as JSON
                break
        }

    }
    /**
     * 便民卡预约记录
     * @return
     */
    def reservationRecord() {
        logger.info("便民卡预约记录")
        def resData = userService.reservationRecord(getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                render(view: "reservation_conveninenceCardRecord", model: [data: resData.data])
                break
            default:
                render(view: "reservation_conveninenceCardRecord", model: [data: resData.data])
                break
        }
    }
    /**
     * 缴费账单查询
     * @return
     */
    def queryBill() {
        logger.info("缴费账单查询")
        println("页码==================" + "${params.page}")
        def pageNumber = "${params.page ?: '1'}"//页码，默认第一页
        def pageSize = "${params.pageSize ?: '10'}"//默认每页显示数量10
        String[] types = request.getParameterValues("orderType")
        String[] source = request.getParameterValues("orderSource")
        StringBuilder sbType = new StringBuilder()
        if (types != null && types.size() > 0) {
            for (int i = 0; i < types.size(); i++) {//多个查询类型用|隔开
                if (i == 0) {
                    sbType.append(types[i])
                } else {
                    sbType.append("|")
                    sbType.append(types[i])
                }
            }
        }

        StringBuilder sbSource = new StringBuilder()
        if (source != null && source.size() > 0) {
            for (int i = 0; i < source.size(); i++) {//多个查询类型用|隔开
                if (i == 0) {
                    sbSource.append(source[i])
                } else {
                    sbSource.append("|")
                    sbSource.append(source[i])
                }
            }
        }

        def resData = userService.queryBillInfo(sbType.toString(), sbSource.toString(), pageNumber, pageSize, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                resData.page = pageNumber //页码
                resData.total = resData.data.totalRow //页码
                render resData as JSON
                //render(view: "queryBill", model: [data: resData])
                break
            default:
                render resData as JSON
                break
        }
    }
    /**
     * 用户订单查询(全部)
     * @return
     */
    def queryOrderInfo() {
        logger.info("用户订单查询(全部)")
        def status = ""//01待支付 02处理中 05交易完成 06订单取消
        def pageNumber = "${params.page ?: '1'}"//页码,默认传第一页
        def pageSize = "10"//每页显示数量默认十条
        def resData = userService.queryOrderInfo(pageNumber, pageSize, status, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                resData.page = pageNumber
                render resData as JSON
                break
            default:
                render resData as JSON
                //render(view: "reservation_conveninenceCardRecord", model: [data: resData.data])
                break
        }
    }
    /**
     * 查询已取消订单
     * @return
     */
    def queryCancelOrder() {
        logger.info("查询已取消订单")
        def status = "06"//01待支付 02处理中 05交易完成 06订单取消
        def pageNumber = "${params.page ?: '1'}"//页码,默认传第一页
        def pageSize = "10"//每页显示数量默认十条
        def resData = userService.queryOrderInfo(pageNumber, pageSize, status, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                resData.page = pageNumber
                render resData as JSON
                break
            default:
                render resData as JSON
                //render(view: "reservation_conveninenceCardRecord", model: [data: resData.data])
                break
        }
    }
    /**
     * 查询未付款订单
     * @return
     */
    def queryUnPayOrder() {
        logger.info("查询未付款订单")
        def status = "01"//01待支付 02处理中 05交易完成 06订单取消
        def pageNumber = "${params.page ?: '1'}"//页码,默认传第一页
        def pageSize = "10"//每页显示数量默认十条
        def resData = userService.queryOrderInfo(pageNumber, pageSize, status, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                resData.page = pageNumber
                render resData as JSON
                break
            default:
                render resData as JSON
                //render(view: "reservation_conveninenceCardRecord", model: [data: resData.data])
                break
        }
    }
    /**
     * 查询交易完成订单
     * @return
     */
    def queryPayOrder() {
        logger.info("查询交易完成订单")
        def status = "05"//01待支付 02处理中 05交易完成 06订单取消
        def pageNumber = "${params.page ?: '1'}"//页码,默认传第一页
        def pageSize = "10"//每页显示数量默认十条
        def resData = userService.queryOrderInfo(pageNumber, pageSize, status, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                resData.page = pageNumber
                render resData as JSON
                break
            default:
                render resData as JSON
                //render(view: "reservation_conveninenceCardRecord", model: [data: resData.data])
                break
        }
    }
    /**
     *  获取加油卡绑定信息
     * @return
     */
    def queryOils() {
        logger.info("获取加油卡绑定信息")
        def resData = userService.getBindOils(getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                render(view: "oil_recharge", model: [data: resData.data])
                break
            default:
                render(view: "oil_recharge", model: [data: resData.data])
                break
        }
    }
    /**
     *  添加加油卡
     * @return
     */
    def addOil() {
        logger.info("添加加油卡")
        def cardType = "${params.oilRadio ?: ''}"
        println("cardType=====" + cardType)
        def cardNo = "${params.cardNo ?: ''}"
        def resData = userService.bindOil(cardType, cardNo, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                render resData as JSON
                break
            default:
                render resData as JSON
                break
        }
    }
    /**
     * 加油卡充值
     * @return
     */
    def oilRecharge() {
        def cardNo = "${params.cardNo ?: ''}"
        cardNo = cardNo.trim()
        println(cardNo);
        def payment = "${params.paymoney ?: ''}"
        def username = session.getAttribute(AppConfig.APP_USER_INFO).phoneNo
        println("payment======" + payment)
        def resData = userService.getOilOrder(cardNo, username, payment, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                def orderno = resData.data.paymentThirdId
                logger.info("orderno===" + orderno)
                resData.code = 0
                def order = "function=PayUniform&transaction_id=" + orderno
                resData.method = Base64.encode(order.getBytes()).replace("=", "")
                resData.orderno = orderno
                render resData as JSON
                break
            default:
                render resData as JSON
                break
        }
    }
    /**
     *  查找石油充值记录(只有充值成功的才能查出来)
     * @return
     */
    def findOilsRecords() {
        logger.info("查找石油充值记录")
        def pageNumber = "${params.page ?: '1'}"//页码，默认第一页
        def pageSize = "${params.pageSize ?: '10'}"//默认每页显示数量10
        def ordertype = "41001|41002";//中化石油卡充值,中石化41001 ; 中石油41002
        String ordersource = "REG-04|REG-03|REG-02|REG-01|REG-99" //订单来源
        def resData = userService.queryBillInfo(ordertype, ordersource, pageNumber, pageSize, getSession())
        switch (resData.errorcode) {
            case 401:
                redirect(uri: "/login")
                break
            case 0:
                resData.page = pageNumber //页码
                resData.total = resData.data.totalRow //总记录数
                render resData as JSON
                break
            default:
                render resData as JSON
                break
        }
    }
}
