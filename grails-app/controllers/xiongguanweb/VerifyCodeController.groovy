package xiongguanweb

import xiongguanweb.util.VerifyCodeUtils

//图片验证码处理
class VerifyCodeController {

    /**
     * 获取图片验证码
     */
    def create() {
        response.setHeader("Expires", "-1")
        response.setHeader("Cache-Control", "no-cache")
        response.setHeader("Pragma", "-1")

        //生成随机验证码吗，长度为4
        String verifyCode = VerifyCodeUtils.generateVerifyCode(5)
        //保存验证码session
        session.setAttribute(AppConfig.VERIFY_CODE, verifyCode)
        //输出到网络流
        VerifyCodeUtils.outputImage(200, 80, response.getOutputStream(), verifyCode)
    }

    /**
     * 验证图片验证码，并销毁session存储的验证码
     */
    def check() {
        def verifyCode = "${params.verifyCode}"
        String sessionCode = session.getAttribute(AppConfig.VERIFY_CODE)
        session.removeAttribute(AppConfig.VERIFY_CODE)
        if (verifyCode.equalsIgnoreCase(sessionCode)) {
            render true
        } else {
            render false
        }
    }


}

