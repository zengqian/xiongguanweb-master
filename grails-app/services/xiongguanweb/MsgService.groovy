package xiongguanweb

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import xiongguanweb.http.HttpClient
import xiongguanweb.http.OnResponseListener
import xiongguanweb.http.RequestParams

import javax.servlet.http.HttpSession

class MsgService {
    private static Logger logger = LoggerFactory.getLogger(MsgService.class)

    def getMess(String type, String mobile, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("interfaceCode", type)
        params.putBody("mobile", mobile)
        params.putBody("deviceId", "web")
        params.putHead("trcode", "2001")

        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                resData.code = errorcode

            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.code = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.code = 401
            }
        })

        return resData

    }

    def checkMessCode(String interfaceCode, String mobile, String authCode, HttpSession session) {
        def resData = [:]
        RequestParams params = new RequestParams()
        params.putBody("interfaceCode", interfaceCode)
        params.putBody("mobile", mobile)
        params.putBody("authCode", authCode)
        params.putBody("deviceId", "web")
        params.putHead("trcode", "2007")
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                resData.code = errorcode
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.code = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.code = 401
            }
        })
        return resData
    }
}
