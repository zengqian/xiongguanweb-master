package xiongguanweb

import com.alibaba.fastjson.JSON
import grails.transaction.Transactional
import groovy.json.JsonOutput
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import xiongguanweb.http.HttpClient
import xiongguanweb.http.OnResponseListener
import xiongguanweb.http.RequestParams
import xiongguanweb.util.AESTool

import javax.servlet.http.HttpSession

@Transactional
class UserService {
    private static Logger logger = LoggerFactory.getLogger(UserService.class)

    @Value('${userService.baseurl}')
    protected String BASEURL
    @Value('${userService.deposit}')
    protected String DEPOSIT   //图书馆押金
    /**
     * 登录获取用户信息
     */
    def userInfo(HttpSession session) {
        RequestParams params = new RequestParams()
        params.putHead("trcode", "1005")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                resData.errorcode = errorcode
                def jsonObject = JSON.parseObject(data)
//                def encodeType = System.getProperty("file.encoding")
                resData.userinfo = JSON.parseObject(AESTool.decryptBase64(jsonObject.getString("userInfo"), "f15831e69d484a41"))
                logger.info("用户信息==${resData.userinfo}")
                if (jsonObject.getString("realnameInfo")) {
                    String s = AESTool.decryptBase64(jsonObject.getString("realnameInfo"), "f15831e69d484a41")
//                    if(!"UTF-8".equals(encodeType)){
//                        s = new String(s.getBytes("GBK"),"utf-8")
//                    }
                    resData.realnameinfo = JSON.parseObject(s)
                    logger.info("用户信息==${resData.realnameinfo}")
                }
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg

            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401

            }
        })

        return resData
    }

    def lostRealInfo(String cardNo, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("cardNo", cardNo)
        params.putHead("trcode", "1043")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                resData.errorcode = errorcode
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401

            }
        })
        return resData

    }

    /**
     * 获取优讯keyid
     */
    def getKeyId(HttpSession session) {
        RequestParams params = new RequestParams()
        params.putHead("trcode", "1032")
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                resData.errorcode = errorcode
                def jsonObject = JSON.parseObject(data)
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401

            }
        })

        return resData

    }

    /**
     * 获取用户绑定户号信息
     */
    def getAccountInfo(HttpSession session, String dealtype) {
        RequestParams params = new RequestParams()
        params.putBody("dealType", dealtype)
        params.putHead("trcode", "1021")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                resData.errorcode = errorcode
                List<Object> list = JSON.parseArray(data)
                logger.info("json==" + data)
                resData.data = list
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg

            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401

            }
        })

        return resData

    }

    /**
     * 水费缴费
     */
    def getWaterOrder(String username, HttpSession session, String ordertype, String payment, String userno, String branchid) {
        RequestParams params = new RequestParams()
        params.putBody("orderType", ordertype)
        params.putBody("orderDesc", "水费缴费")
        params.putBody("userName", username)
        params.putBody("userNo", userno)
        params.putBody("payment", payment)
        params.putBody("notifyUrl", "")
        params.putBody("transChannel", "pc")
        params.putBody("backUrl", BASEURL + "/user/getWater")
        params.putBody("showUrl", "")
        params.putBody("branchguid", branchid)
        params.putHead("trcode", "3104")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                resData.code = errorcode
                def jsonObject = JSON.parseObject(data)
                resData.data = jsonObject
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.code = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.code = 401
            }
        })

        return resData
    }

    /**   获取缴费机构列表
     *
     * @param type 缴费类型
     * @return
     */
    def getRechargeCom(String type, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("type", type)
        params.putHead("trcode", "7002")
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                logger.info("data==" + data)
                List<Object> jsonObject = JSON.parseArray(data)
                logger.info("list==" + jsonObject)
                resData.data = jsonObject
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }

    /**  查询水投账户余额
     *
     * @param fesno 户号
     * @param session
     * @return
     */
    def queryWaterBalance(String fesno, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("userNo", fesno)
        params.putHead("trcode", "3103")
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                resData.errorcode = errorcode
                def jsonObject = JSON.parseObject(data)
                resData.data = jsonObject
                logger.info("json==" + jsonObject)
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
    /**
     * 绑定水费户号
     * @param alisName 别名
     * @param name 姓名
     * @param userNp 户号
     * @param branchguid 机构号
     * @param branchgname 机构名称
     * @param address 地址
     * @param dealType 业务类别
     * @param session
     * @return
     */
    def bindWaterAccount(String alisName, String name, String userNp, String branchguid, String branchgname, String address, String dealType, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("alisName", alisName)
        params.putBody("name", name)
        params.putBody("userNp", userNp)
        params.putBody("branchguid", branchguid)
        params.putBody("branchgname", branchgname)
        params.putBody("address", address)
        params.putBody("dealType", dealType)
        params.putHead("trcode", "3102")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                resData.errorcode = errorcode
                def jsonObject = JSON.parseObject(data)
                resData.data = jsonObject
                logger.info("json==" + jsonObject)
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
    /**
     * 查询水投户号
     * @param branchguid
     * @param certNo
     * @param name
     * @param session
     * @return
     */
    def queryWaterAccount(String branchguid, String certNo, String name, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("certNo", certNo)
        params.putBody("name", name)
        params.putBody("branchguid", branchguid)
        params.putHead("trcode", "3101")
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                logger.info("data==" + data)
                def jsonObject = JSON.parseObject(data)
                List<Object> users = JSON.parseArray(jsonObject.get("users").toString())
                logger.info("list==" + users)
                resData.errorcode = errorcode
                resData.users = users
                resData.total = jsonObject.get("total").toString()
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
    /**
     * 查询燃气账户余额
     * @param branchguid 机构代码
     * @param fesno 户号
     * @param session
     * @return
     */
    def queryGasBalance(String branchguid, String fesno, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("userNo", fesno)
        params.putBody("branchguid", branchguid)
        params.putHead("trcode", "5503")
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                resData.errorcode = errorcode
                def jsonObject = JSON.parseObject(data)
                resData.data = jsonObject
                logger.info("json==" + jsonObject)
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
    /**
     * 燃气缴费
     */
    def getGasOrder(String username, HttpSession session, String ordertype, String payment, String userno, String branchid) {
        RequestParams params = new RequestParams()
        params.putBody("orderType", ordertype)
        params.putBody("orderDesc", "昆仑燃气缴费")
        params.putBody("userName", username)
        params.putBody("userNo", userno)
        params.putBody("payment", payment)
        params.putBody("notifyUrl", "")
        params.putBody("transChannel", "pc")
        params.putBody("backUrl", BASEURL + "/user/getGas")
        params.putBody("showUrl", "")
        params.putBody("branchguid", branchid)
        params.putHead("trcode", "5504")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                resData.errorcode = errorcode
                def jsonObject = JSON.parseObject(data)
                resData.data = jsonObject
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
    /**
     * 绑定燃气户号
     * @param alisName 别名
     * @param name 姓名
     * @param userNp 户号
     * @param branchguid 机构号
     * @param branchgname 机构名称
     * @param address 地址
     * @param dealType 业务类别
     * @param session
     * @return
     */
    def bindGasAccount(String alisName, String name, String userNp, String branchguid, String branchgname, String address, String dealType, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("alisName", alisName)
        params.putBody("name", name)
        params.putBody("userNp", userNp)
        params.putBody("branchguid", branchguid)
        params.putBody("branchgname", branchgname)
        params.putBody("address", address)
        params.putBody("dealType", dealType)
        params.putHead("trcode", "5502")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                resData.errorcode = errorcode
                def jsonObject = JSON.parseObject(data)
                resData.data = jsonObject
                logger.info("json==" + jsonObject)
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
    /**
     * 查询燃气户号
     * @param branchguid
     * @param certNo
     * @param name
     * @param session
     * @return
     */
    def queryGasAccount(String branchguid, String certNo, String name, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("branchguid", branchguid)
        params.putBody("icNumber", certNo)
        params.putBody("userName", name)
        params.putHead("trcode", "5501")
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                logger.info("data==" + data)
                def jsonObject = JSON.parseObject(data)
                List<Object> users = JSON.parseArray(jsonObject.get("userlist").toString())
                logger.info("list==" + users)
                resData.errorcode = errorcode
                resData.users = users
                resData.total = users.size()
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
    /**
     * 解绑户号
     * @param name 户主姓名
     * @param userNp 户号
     * @param dealType
     * @param session
     * @return
     */
    def unBindAccount(String name, String userNp, String dealType, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("name", name)
        params.putBody("userNp", userNp)
        params.putBody("dealType", dealType)
        params.putHead("trcode", "1049")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                logger.info("data==" + data)
                def jsonObject = JSON.parseObject(data)
                resData.errorcode = errorcode
                resData.data = jsonObject
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
    /**
     * 运营商缴费
     * @param flag 1 移动 2 联通 3 电信
     * @param mobile 手机号
     * @param payment 充值面额
     * @param session
     */
    def getPhoneOrder(String flag, String mobile, String payment, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("flag", flag)
        params.putBody("mobile", mobile)
        params.putBody("payment", payment)
        params.putBody("backUrl", BASEURL + "/recharge_phone")//回调地址
        params.putBody("transChannel", "pc")
        params.putHead("trcode", "2005")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                logger.info("data==" + data)
                def jsonObject = JSON.parseObject(data)
                resData.errorcode = errorcode
                resData.data = jsonObject
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
    /**
     * 交通罚没查询
     * @param fineNo 罚单号
     * @param session
     * @return
     */
    def getFineInfo(String fineNo, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("PaySnglNo", fineNo)
        params.putHead("trcode", "2003")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                logger.info("data==" + data)
                def jsonObject = JSON.parseObject(data)
                resData.errorcode = errorcode
                resData.data = jsonObject.service.BODY
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
    /**
     * 统一下单(交通罚没缴费)
     * @param body 交通罚没查询返回的业务参数
     * @param session
     * @return
     */
    def getFineOrder(Map body, HttpSession session) {
        String bodyJsonStr = JsonOutput.toJson(body)
        //println(bodyJsonStr)
        RequestParams params = new RequestParams()
        params.putBody("orderType", "10001")
        params.putBody("orderDes", "交通罚没")//改正
        params.putBody("payment", body.get("TotAmt"))//缴费金额
        params.putBody("paymentFlag", "3")
        params.putBody("transChannel", "pc")//pc端
        params.putBody("paymentOutService", "百合易付")
        params.putBody("paymentOutTk", "LZ_BANK_BHYF")
        params.putBody("paymentType", "99")
        params.putBody("paymentBankType", "")
        params.putBody("paymentBankNo", "")
        params.putBody("paymentThirdId", "")
        params.putBody("notifyUrl", "")
        params.putBody("backUrl", BASEURL + "/recharge_phone")//支付成功返回页面
        params.putBody("showUrl", "")//
        params.putBody("body", bodyJsonStr)
        params.putHead("trcode", "2004")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                logger.info("data==" + data)
                def jsonObject = JSON.parseObject(data)
                resData.errorcode = errorcode
                resData.data = jsonObject
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }

    /** 汇能燃气下单
     *
     * @param session
     * @param gasvalue
     * @param branchguid
     * @param gasinfo
     * @return
     */
    def gasrecharge(HttpSession session, String gasvalue, String branchguid, String cardtype) {
        def gasinfo = session.getAttribute(AppConfig.GAS_INFO)
        RequestParams params = new RequestParams()
        params.putBody("CardNo", gasinfo.CardNo ?: '')
        params.putBody("UserNo", gasinfo.UserNo ?: '')
        params.putBody("Address", "12")
        params.putBody("RgltVrbl", gasinfo.RgltVrbl ?: '')
        params.putBody("ChagGasNum", gasinfo.ChagGasNum ?: '')
        params.putBody("TotBal", gasinfo.TotBal ?: '')
        params.putBody("AcctBal", gasinfo.AcctBal ?: '')
        params.putBody("PrefBal", gasinfo.PrefBal ?: '')
        params.putBody("UsrNm", "12")
        params.putBody("AssAmt", "12")
        params.putBody("PrefAmt", gasinfo.PrefAmt ?: '')
        params.putBody("PayAmt", gasinfo.PayAmt ?: '')
        params.putBody("OrgCd", "12")
        params.putBody("MaxPrchGasNum", "12")
        params.putBody("TotPrchGasTms", "12")
        params.putBody("TotPrchGasNum", gasinfo.TotPrchGasNum ?: '')
        params.putBody("TotPrchGasAmt", gasinfo.TotPrchGasAmt ?: '')
        params.putBody("LastPrchGasNum", gasinfo.LastPrchGasNum ?: '')
        params.putBody("PrvsPrchGasNum", gasinfo.PrvsPrchGasNum ?: '')
        params.putBody("BuyGasQuantity", gasvalue)
        params.putBody("cardType", cardtype)
        params.putBody("transChannel", "pc")
        params.putBody("branchguid", branchguid)
        params.putBody("backUrl", BASEURL + "/gas_card_recharge_working")
        params.putHead("trcode", "5602")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                logger.info("data==" + data)
                def jsonObject = JSON.parseObject(data)
                resData.code = errorcode
                resData.data = jsonObject
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.code = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.code = 401
            }
        })
        return resData
    }
    /**
     * 图书馆用户信息查询
     * @param userType 图书用户类型 001 身份证 003 便民卡
     * @param userNo 身份证或便民卡卡号
     * @param session
     * @return
     */
    def queryMyLibCard(String userType, String userNo, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("userType", userType)
        params.putBody("userNo", userNo)
        params.putHead("trcode", "2107")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                resData.errorcode = errorcode
                logger.info("data==" + data)
                def jsonObject = JSON.parseObject(data)
                def user = JSON.parseObject(jsonObject.get("data").toString())
                String isActive = jsonObject.getString("isActive")
                String hasPayment = jsonObject.getString("hasPayment")
                resData.user = user
                resData.isActive = isActive.equals("0") ? 0 : 1//0未激活1已激活
                resData.hasPayment = (hasPayment == null || hasPayment.equals("0")) ? 0 : 1//0未缴费1已缴费
                if (hasPayment != null && hasPayment.equals("1")) {
                    resData.amt = DEPOSIT//押金默认100
                } else {
                    resData.amt = 0
                }
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
    /**
     *  激活图书馆应用
     * @param userType
     * @param userNo
     * @param session
     * @return
     */
    def activeLibraryCard(String userType, String userNo, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("userType", userType)
        params.putBody("userNo", userNo)
        params.putHead("trcode", "2101")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                logger.info("data==" + data)
                resData.errorcode = errorcode
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
    /**
     * 借阅信息
     * @param userType
     * @param userNo
     * @param session
     * @return
     */
    def querylibReadInfo(String userType, String userNo, String index, String pageSize, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("userType", userType)
        params.putBody("userNo", userNo)
        params.putBody("index", index)
        params.putBody("pageSize", pageSize)
        params.putHead("trcode", "2102")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                logger.info("data==" + data)
                def jsonObject = JSON.parseObject(data)
                def jsonObject2 = JSON.parseObject(jsonObject.get("data").toString())
                List<Object> reads = JSON.parseArray(jsonObject2.get("data").toString())
                resData.start = jsonObject2.getString("start")
                resData.size = jsonObject2.getString("size")
                resData.total = jsonObject2.getString("total")
                resData.reads = reads
                resData.errorcode = errorcode
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
    /**
     * 图书馆押金退还
     * @param oldOrderId
     * @param amt
     * @param dealType
     * @param userType
     * @param userNo
     * @param session
     * @return
     */
    def depositRefunded(String oldOrderId, String amt, String dealType, String userType, String userNo, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("backUrl", BASEURL + "/user/getLibrary")
        params.putBody("oldOrderId", oldOrderId)
        params.putBody("amt", amt)
        params.putBody("dealType", dealType)
        params.putBody("userType", userType)
        params.putBody("userNo", userNo)
        params.putHead("trcode", "2106")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                resData.errorcode = errorcode
                def jsonObject = JSON.parseObject(data)
                resData.data = jsonObject
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
    /**
     * 图书馆缴纳
     * @param userName 用户名
     * @param certNo 身份证号码
     * @param payment 缴纳金额
     * @param session
     * @return
     */
    def payLibrary(String userName, String certNo, String userType, String dealType, String payment, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("userName", userName)
        params.putBody("userNo", certNo)
        params.putBody("userType", userType)
        params.putBody("dealType", dealType)
        params.putBody("payment", payment)
        params.putBody("transChannel", "pc")
        params.putBody("notifyUrl", "")
        params.putBody("backUrl", BASEURL + "/user/getLibrary")
        params.putBody("showUrl", "")
        params.putBody("branchguid", "")
        params.putHead("trcode", "2104")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                resData.errorcode = errorcode
                def jsonObject = JSON.parseObject(data)
                resData.data = jsonObject
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
    /**
     * 查询图书馆欠费信息
     * @param certNo
     * @param session
     * @return
     */
    def arrearageInfo(String certNo, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("userNo", certNo)
        params.putBody("userType", "001")
        params.putHead("trcode", "2103")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                resData.errorcode = errorcode
                def jsonObject = JSON.parseObject(data)
                def info = JSON.parseArray(jsonObject.get("data").toString())
                if (info.size() > 0) {
                    for (int i = 0; i < info.size(); i++) {
                        def job = info.getJSONObject(i);
                        def amountYuan = job.getInteger("amount") / 100
                        job.put("amt", amountYuan)
                    }
                    println(info.toString())
                    resData.data = info
                }
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
    /**
     * 综合信用查询
     * @param name
     * @param certNo
     * @param session
     * @return
     */
    def queryCreditInfo(String name, String certNo, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("name", name)
        params.putBody("certNo", certNo)
        params.putHead("trcode", "1014")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                logger.info("data==" + data)
                resData.errorcode = errorcode
                def jsonObject = JSON.parseObject(data)
                resData.data = jsonObject
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
    /**
     * 综合积分查询
     * @param name
     * @param certNo
     * @param session
     * @return
     */
    def queryScoreInfo(String cardNo, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("bankCardNo", cardNo)
        params.putHead("trcode", "1013")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                logger.info("data==" + data)
                resData.errorcode = errorcode
                def jsonObject = JSON.parseObject(data)
                resData.data = jsonObject
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
    /**
     * 公积金查询
     * @param name
     * @param certNo
     * @param session
     * @return
     */
    def queryFundInfo(String name, String certNo, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("name", name)
        params.putBody("certNo", certNo)
        params.putHead("trcode", "9915")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                resData.errorcode = errorcode
                logger.info("data==" + data)
                def jsonObject = JSON.parseObject(data)
                resData.data = jsonObject
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
    /**
     * 便民卡预约
     * @param name
     * @param certNo
     * @param siteId
     * @param mobile
     * @param authCode
     * @param session
     * @return
     */
    def reservationCard(name, certNo, siteId, mobile, authCode, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("userName", name)
        params.putBody("certNo", certNo)
        params.putBody("siteId", siteId)
        params.putBody("mobile", mobile)
        params.putBody("authCode", authCode)
        params.putHead("trcode", "2044")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                resData.errorcode = errorcode
                logger.info("data==" + data)
                def jsonObject = JSON.parseObject(data)
                resData.data = jsonObject
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
    /**
     * 便民卡网点列表查询
     * @return
     */
    def querySiteList(HttpSession session) {
        RequestParams params = new RequestParams()
        params.putHead("trcode", "2043")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                resData.errorcode = errorcode
                logger.info("data==" + data)
                def jsonObject = JSON.parseArray(data)
                resData.data = jsonObject
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
    /**
     * 便民卡预约记录
     * @return
     */
    def reservationRecord(HttpSession session) {
        RequestParams params = new RequestParams()
        params.putHead("trcode", "2045")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                resData.errorcode = errorcode
                logger.info("data==" + data)
                def records = JSON.parseArray(data)
                resData.data = records
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }

    /**
     * 短信验证码查询
     * @return
     */
    def getAuthCode(String mobile, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("interfaceCode", "2044")
        params.putBody("mobile", mobile)
        params.putHead("trcode", "2001")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                resData.errorcode = errorcode
                logger.info("data==" + data)
                def jsonObject = JSON.parseObject(data)
                resData.data = jsonObject
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
    /**
     *  用户账单查询
     * @param orderType 账单类型
     * @param orderSourse 账单来源
     * @param pageNumber
     * @param pageSize
     * @param session
     * @return
     */
    def queryBillInfo(String orderType, String orderSource, pageNumber, pageSize, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("pageNumber", pageNumber)
        params.putBody("pageSize", pageSize)
        params.putBody("orderType", orderType)
        params.putBody("orderSource", orderSource)
        params.putHead("trcode", "2008")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                logger.info("data==" + data)
                def jsonObject = JSON.parseObject(data)
                resData.errorcode = errorcode
                resData.data = jsonObject
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
    /**
     * 用户订单查询
     * @param pageNumber
     * @param pageSize
     * @param status
     * @param session
     * @return
     */
    def queryOrderInfo(String pageNumber, String pageSize, String status, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("pageNumber", pageNumber)
        params.putBody("pageSize", pageSize)
        params.putBody("status", status)
        params.putHead("trcode", "2006")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                logger.info("data==" + data)
                def jsonObject = JSON.parseObject(data)
                resData.errorcode = errorcode
                resData.data = jsonObject
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
    /**
     * 加油卡绑定信息
     * @param cardType
     * @param cardNo
     * @param session
     * @return
     */
    def getBindOils(HttpSession session) {
        RequestParams params = new RequestParams()
        params.putHead("trcode", "2113")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                logger.info("data==" + data)
                def jsonArray = JSON.parseArray(data)
                resData.errorcode = errorcode
                resData.data = jsonArray
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
    /**
     * 加油卡绑定
     * @param cardType 中石化41001 ; 中石油41002
     * @param cardNo
     * @param session
     * @return
     */
    def bindOil(String cardType, String cardNo, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("cardType", cardType)
        params.putBody("cardNo", cardNo)
        params.putHead("trcode", "2112")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                logger.info("data==" + data)
                def jsonObject = JSON.parseObject(data)
                resData.errorcode = errorcode
                resData.data = jsonObject
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
    /**
     * 燃气缴费
     */
    def getOilOrder(String cardNo, String userName, String payment, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("cardNo", cardNo)
        params.putBody("userName", userName)
        params.putBody("payment", payment)
        params.putBody("notifyUrl", "")
        params.putBody("transChannel", "pc")
        params.putBody("backUrl", BASEURL + "/user/queryOils")
        params.putBody("showUrl", "")
        params.putHead("trcode", "2111")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                resData.errorcode = errorcode
                def jsonObject = JSON.parseObject(data)
                resData.data = jsonObject
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }


    //修改头像 第一步
    def updateLogo(File uploadfile, HttpSession session) {
        RequestParams params = new RequestParams()
        params.putHead("trcode", "1008")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))

        def resData = [:]

        HttpClient.me().uploadFileWithParams("http://192.168.2.232:8879/file/uploadFile", params, "fileName", uploadfile, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                def jsonObject = JSON.parseObject(data)
                logger.info("上传第一步："+jsonObject)

                def datas = uploadLogo(jsonObject.imageUrl,session)
                logger.info("上传第二步："+datas.errorcode)
                resData.errorcode = datas.errorcode
                resData.errormsg = "success"
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })

        return resData
    }


    //修改头像第二步
    def uploadLogo (String logoUrl,HttpSession session) {
        RequestParams params = new RequestParams()
        params.putBody("imageUrl", logoUrl)
        params.putHead("trcode", "1007")
        params.putHead("Jwt", session.getAttribute(AppConfig.TOKEN))
        def resData = [:]
        HttpClient.me().post(session, params, new OnResponseListener() {
            @Override
            void onSuccess(int errorcode, String errormsg, String data) {
                resData.errorcode = errorcode
                def jsonObject = JSON.parseObject(data)
                logger.info("上传第二步："+jsonObject)
                resData.data = jsonObject
            }

            @Override
            void onFailure(int errorcode, String errormsg) {
                resData.errorcode = errorcode
                resData.errormsg = errormsg
            }

            @Override
            void onTokenExpired(int errorcode, HttpSession sion) {
                resData.errorcode = 401
            }
        })
        return resData
    }
}


