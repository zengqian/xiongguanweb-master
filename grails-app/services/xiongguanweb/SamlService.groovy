package xiongguanweb

import com.isprint.am.saml.AuthnResponse
import com.isprint.am.saml.sp.SP
import com.isprint.am.saml.sp.SPConfiguration
import grails.transaction.Transactional
import org.slf4j.Logger
import org.slf4j.LoggerFactory

@Transactional
class SamlService {
    private static Logger logger = LoggerFactory.getLogger(SamlService.class)

    /**
     * 解析SAMLResponse
     * @param samlres
     * @return
     */
    def Map getSamlData(String samlres){
         logger.info("samlres"+samlres)
        Map<String, String> samlData = new HashMap()
        SPConfiguration conf = null
        SP sp
        try {
            conf = SPConfiguration.newInstance("d:\\Sam\\saml-sp.properties")
            sp = SP.newInstance(conf)
            logger.info("加载SP配置文件成功!")
            String sSamlResponse = samlres
            if(null == sSamlResponse){
                logger.info("samlres为空")
                return null
            }
            AuthnResponse samlResponse = sp.parseAuthnResponse(sSamlResponse)

            samlData = samlResponse.getAttributes()
        } catch (com.isprint.am.saml.SAMLException e) {
            e.printStackTrace()
        }
        logger.info("samldata==="+samlData)
        return samlData
    }
}
