
package xiongguanweb.webserviceclient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>modifyLZBId complex type的 Java 类。
 * 
 * <p>以下模式片段指定包含在此类中的预期内容。
 * 
 * <pre>
 * &lt;complexType name="modifyLZBId"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CCServiceInObj" type="{http://webservices.ccservice.isprint.com/}ccServiceInObj" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "modifyLZBId", propOrder = {
    "ccServiceInObj"
})
public class ModifyLZBId {

    @XmlElement(name = "CCServiceInObj")
    protected CcServiceInObj ccServiceInObj;

    /**
     * 获取ccServiceInObj属性的值。
     * 
     * @return
     *     possible object is
     *     {@link CcServiceInObj }
     *     
     */
    public CcServiceInObj getCCServiceInObj() {
        return ccServiceInObj;
    }

    /**
     * 设置ccServiceInObj属性的值。
     * 
     * @param value
     *     allowed object is
     *     {@link CcServiceInObj }
     *     
     */
    public void setCCServiceInObj(CcServiceInObj value) {
        this.ccServiceInObj = value;
    }

}
