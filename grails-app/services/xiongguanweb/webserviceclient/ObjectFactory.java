
package xiongguanweb.webserviceclient;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the xiongguanweb.webserviceclient package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ECompanyRegist_QNAME = new QName("http://webservices.ccservice.isprint.com/", "eCompanyRegist");
    private final static QName _MobileValidateSMSOTP_QNAME = new QName("http://webservices.ccservice.isprint.com/", "mobileValidateSMSOTP");
    private final static QName _GetLZBIdBySecondIdResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getLZBIdBySecondIdResponse");
    private final static QName _EUserRegist_QNAME = new QName("http://webservices.ccservice.isprint.com/", "eUserRegist");
    private final static QName _ValidateSMSOTP_QNAME = new QName("http://webservices.ccservice.isprint.com/", "validateSMSOTP");
    private final static QName _MobileMainBind_QNAME = new QName("http://webservices.ccservice.isprint.com/", "mobileMainBind");
    private final static QName _MberRealNameAuthResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "mberRealNameAuthResponse");
    private final static QName _GetLZBIdBySecondId_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getLZBIdBySecondId");
    private final static QName _RegisterLZBId_QNAME = new QName("http://webservices.ccservice.isprint.com/", "registerLZBId");
    private final static QName _QueryCustInfos_QNAME = new QName("http://webservices.ccservice.isprint.com/", "queryCustInfos");
    private final static QName _GetLzbOtherByUserIdResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getLzbOtherByUserIdResponse");
    private final static QName _ValidateImageResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "validateImageResponse");
    private final static QName _UnbindLZBIdResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "unbindLZBIdResponse");
    private final static QName _ChangePhoneRequestResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "changePhoneRequestResponse");
    private final static QName _ValidateInfoResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "validateInfoResponse");
    private final static QName _GetAnyDataPublicMethod_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getAnyDataPublicMethod");
    private final static QName _ValidateMethodResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "validateMethodResponse");
    private final static QName _SendEmailResetPasswordResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "sendEmailResetPasswordResponse");
    private final static QName _GetLZBIdAssistant_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getLZBIdAssistant");
    private final static QName _CheckCerNumResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "checkCerNumResponse");
    private final static QName _GetccLogResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getccLogResponse");
    private final static QName _GetOrSetSamlResponseData_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getOrSetSamlResponseData");
    private final static QName _SendEmailResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "sendEmailResponse");
    private final static QName _VerifyEmailResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "verifyEmailResponse");
    private final static QName _EresetLZBIdPWResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "eresetLZBIdPWResponse");
    private final static QName _AccessUserProfileResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "accessUserProfileResponse");
    private final static QName _FreezeResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "freezeResponse");
    private final static QName _LocalValidateSMSOTPResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "localValidateSMSOTPResponse");
    private final static QName _ChangeLZBIdPW_QNAME = new QName("http://webservices.ccservice.isprint.com/", "changeLZBIdPW");
    private final static QName _EchangeLZBIdPW_QNAME = new QName("http://webservices.ccservice.isprint.com/", "echangeLZBIdPW");
    private final static QName _EloginResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "eloginResponse");
    private final static QName _GetLZBIdAndBindInfo_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getLZBIdAndBindInfo");
    private final static QName _MobileMainBindResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "mobileMainBindResponse");
    private final static QName _ReflashSessionResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "reflashSessionResponse");
    private final static QName _EMobileLogin_QNAME = new QName("http://webservices.ccservice.isprint.com/", "eMobileLogin");
    private final static QName _GetLzbOtherByUserId_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getLzbOtherByUserId");
    private final static QName _LocalValidateSMSOTP_QNAME = new QName("http://webservices.ccservice.isprint.com/", "localValidateSMSOTP");
    private final static QName _ModifyEmailByPhoneForUnactiveResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "modifyEmailByPhoneForUnactiveResponse");
    private final static QName _EUserUpdateResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "eUserUpdateResponse");
    private final static QName _ModifyEmailByPhoneForUnactive_QNAME = new QName("http://webservices.ccservice.isprint.com/", "modifyEmailByPhoneForUnactive");
    private final static QName _VerifyEmail_QNAME = new QName("http://webservices.ccservice.isprint.com/", "verifyEmail");
    private final static QName _GetElzbid_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getElzbid");
    private final static QName _ModifyLZBId_QNAME = new QName("http://webservices.ccservice.isprint.com/", "modifyLZBId");
    private final static QName _UpdateEmailByToken_QNAME = new QName("http://webservices.ccservice.isprint.com/", "updateEmailByToken");
    private final static QName _EnrollFaceResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "enrollFaceResponse");
    private final static QName _EnrollFace_QNAME = new QName("http://webservices.ccservice.isprint.com/", "enrollFace");
    private final static QName _MobileValidateSMSOTPResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "mobileValidateSMSOTPResponse");
    private final static QName _ValidateImage_QNAME = new QName("http://webservices.ccservice.isprint.com/", "validateImage");
    private final static QName _GetElzbidResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getElzbidResponse");
    private final static QName _LoginResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "loginResponse");
    private final static QName _GetElzbidForCCWebResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getElzbidForCCWebResponse");
    private final static QName _MobileLoginResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "mobileLoginResponse");
    private final static QName _SendSMSOTP_QNAME = new QName("http://webservices.ccservice.isprint.com/", "sendSMSOTP");
    private final static QName _GetUserLoginResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getUserLoginResponse");
    private final static QName _GetUserLogin_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getUserLogin");
    private final static QName _EMobileLoginResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "eMobileLoginResponse");
    private final static QName _Logout_QNAME = new QName("http://webservices.ccservice.isprint.com/", "logout");
    private final static QName _BindLZBId_QNAME = new QName("http://webservices.ccservice.isprint.com/", "bindLZBId");
    private final static QName _MoveAssistantUser_QNAME = new QName("http://webservices.ccservice.isprint.com/", "moveAssistantUser");
    private final static QName _CheckCerNum_QNAME = new QName("http://webservices.ccservice.isprint.com/", "checkCerNum");
    private final static QName _EsendSMSOTPResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "esendSMSOTPResponse");
    private final static QName _BindLZBIdResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "bindLZBIdResponse");
    private final static QName _ValidateCardInfoResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "validateCardInfoResponse");
    private final static QName _ChangePhoneRequest_QNAME = new QName("http://webservices.ccservice.isprint.com/", "changePhoneRequest");
    private final static QName _EvalidateSMSOTP_QNAME = new QName("http://webservices.ccservice.isprint.com/", "evalidateSMSOTP");
    private final static QName _MoveAssistantUserResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "moveAssistantUserResponse");
    private final static QName _GetAllSystemInfo_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getAllSystemInfo");
    private final static QName _Login_QNAME = new QName("http://webservices.ccservice.isprint.com/", "login");
    private final static QName _VerifyHttpAndBackUserInfoResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "verifyHttpAndBackUserInfoResponse");
    private final static QName _ValidateInfo_QNAME = new QName("http://webservices.ccservice.isprint.com/", "validateInfo");
    private final static QName _GetElzbidForCCWeb_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getElzbidForCCWeb");
    private final static QName _ValidateSMSOTPResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "validateSMSOTPResponse");
    private final static QName _GetLZBIdResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getLZBIdResponse");
    private final static QName _LogoutResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "logoutResponse");
    private final static QName _GetChangePhoneRequest_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getChangePhoneRequest");
    private final static QName _GenerateQRCodeResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "generateQRCodeResponse");
    private final static QName _SendSMSOTPResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "sendSMSOTPResponse");
    private final static QName _MainBindResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "mainBindResponse");
    private final static QName _EUserRegistResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "eUserRegistResponse");
    private final static QName _EresetLZBIdPW_QNAME = new QName("http://webservices.ccservice.isprint.com/", "eresetLZBIdPW");
    private final static QName _EsendSMSOTP_QNAME = new QName("http://webservices.ccservice.isprint.com/", "esendSMSOTP");
    private final static QName _GetLZBIdAssistantResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getLZBIdAssistantResponse");
    private final static QName _GetReservedMobilesResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getReservedMobilesResponse");
    private final static QName _InvokeResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "invokeResponse");
    private final static QName _QueryUnbindRequestByYHTUUID_QNAME = new QName("http://webservices.ccservice.isprint.com/", "queryUnbindRequestByYHTUUID");
    private final static QName _VerifyHttpAndBackUserInfo_QNAME = new QName("http://webservices.ccservice.isprint.com/", "verifyHttpAndBackUserInfo");
    private final static QName _SendEmail_QNAME = new QName("http://webservices.ccservice.isprint.com/", "sendEmail");
    private final static QName _ResetLZBIdPW_QNAME = new QName("http://webservices.ccservice.isprint.com/", "resetLZBIdPW");
    private final static QName _OpenCustCleaRes_QNAME = new QName("http://webservices.ccservice.isprint.com/", "openCustCleaRes");
    private final static QName _MainBind_QNAME = new QName("http://webservices.ccservice.isprint.com/", "mainBind");
    private final static QName _UnbindLZBId_QNAME = new QName("http://webservices.ccservice.isprint.com/", "unbindLZBId");
    private final static QName _QueryCustInfosResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "queryCustInfosResponse");
    private final static QName _ModifyLZBIdResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "modifyLZBIdResponse");
    private final static QName _GetSessionTokenByQrcode_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getSessionTokenByQrcode");
    private final static QName _EnterpriseUpdateResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "enterpriseUpdateResponse");
    private final static QName _GetccLog_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getccLog");
    private final static QName _GetAllSystemInfoResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getAllSystemInfoResponse");
    private final static QName _ECompanyRegistResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "eCompanyRegistResponse");
    private final static QName _GetSystemSSOURLBySystemnameResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getSystemSSOURLBySystemnameResponse");
    private final static QName _Elogin_QNAME = new QName("http://webservices.ccservice.isprint.com/", "elogin");
    private final static QName _GetLZBId_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getLZBId");
    private final static QName _EUserUpdate_QNAME = new QName("http://webservices.ccservice.isprint.com/", "eUserUpdate");
    private final static QName _EnterpriseUpdate_QNAME = new QName("http://webservices.ccservice.isprint.com/", "enterpriseUpdate");
    private final static QName _Openepay_QNAME = new QName("http://webservices.ccservice.isprint.com/", "openepay");
    private final static QName _GetReservedMobiles_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getReservedMobiles");
    private final static QName _SendEmailResetPassword_QNAME = new QName("http://webservices.ccservice.isprint.com/", "sendEmailResetPassword");
    private final static QName _GetChangePhoneRequestResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getChangePhoneRequestResponse");
    private final static QName _GetOrSetSamlResponseDataResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getOrSetSamlResponseDataResponse");
    private final static QName _ReflashSession_QNAME = new QName("http://webservices.ccservice.isprint.com/", "reflashSession");
    private final static QName _MaintainCustFee_QNAME = new QName("http://webservices.ccservice.isprint.com/", "maintainCustFee");
    private final static QName _EvalidateSMSOTPResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "evalidateSMSOTPResponse");
    private final static QName _MberRealNameAuth_QNAME = new QName("http://webservices.ccservice.isprint.com/", "mberRealNameAuth");
    private final static QName _QueryUnbindRequestByYHTUUIDResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "queryUnbindRequestByYHTUUIDResponse");
    private final static QName _MaintainCustFeeResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "maintainCustFeeResponse");
    private final static QName _ResetLZBIdPWResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "resetLZBIdPWResponse");
    private final static QName _GenerateQRCode_QNAME = new QName("http://webservices.ccservice.isprint.com/", "generateQRCode");
    private final static QName _GetLZBIdAndBindInfoResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getLZBIdAndBindInfoResponse");
    private final static QName _OpenepayResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "openepayResponse");
    private final static QName _ValidateMethod_QNAME = new QName("http://webservices.ccservice.isprint.com/", "validateMethod");
    private final static QName _GetSystemSSOURLBySystemname_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getSystemSSOURLBySystemname");
    private final static QName _GetURLDateResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getURLDateResponse");
    private final static QName _Freeze_QNAME = new QName("http://webservices.ccservice.isprint.com/", "freeze");
    private final static QName _GetURLDate_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getURLDate");
    private final static QName _Invoke_QNAME = new QName("http://webservices.ccservice.isprint.com/", "invoke");
    private final static QName _UpdateEmailByTokenResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "updateEmailByTokenResponse");
    private final static QName _EchangeLZBIdPWResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "echangeLZBIdPWResponse");
    private final static QName _RegisterLZBIdResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "registerLZBIdResponse");
    private final static QName _ValidateCardInfo_QNAME = new QName("http://webservices.ccservice.isprint.com/", "validateCardInfo");
    private final static QName _GetAnyDataPublicMethodResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getAnyDataPublicMethodResponse");
    private final static QName _AccessUserProfile_QNAME = new QName("http://webservices.ccservice.isprint.com/", "accessUserProfile");
    private final static QName _OpenCustCleaResResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "openCustCleaResResponse");
    private final static QName _GetSessionTokenByQrcodeResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "getSessionTokenByQrcodeResponse");
    private final static QName _MobileLogin_QNAME = new QName("http://webservices.ccservice.isprint.com/", "mobileLogin");
    private final static QName _ChangeLZBIdPWResponse_QNAME = new QName("http://webservices.ccservice.isprint.com/", "changeLZBIdPWResponse");
    private final static QName _Exception_QNAME = new QName("http://webservices.ccservice.isprint.com/", "Exception");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: xiongguanweb.webserviceclient
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CcServiceOutObj }
     * 
     */
    public CcServiceOutObj createCcServiceOutObj() {
        return new CcServiceOutObj();
    }

    /**
     * Create an instance of {@link CcServiceOutObj.Suuid }
     * 
     */
    public CcServiceOutObj.Suuid createCcServiceOutObjSuuid() {
        return new CcServiceOutObj.Suuid();
    }

    /**
     * Create an instance of {@link ECompanyRegist }
     * 
     */
    public ECompanyRegist createECompanyRegist() {
        return new ECompanyRegist();
    }

    /**
     * Create an instance of {@link MobileValidateSMSOTP }
     * 
     */
    public MobileValidateSMSOTP createMobileValidateSMSOTP() {
        return new MobileValidateSMSOTP();
    }

    /**
     * Create an instance of {@link GetLZBIdBySecondIdResponse }
     * 
     */
    public GetLZBIdBySecondIdResponse createGetLZBIdBySecondIdResponse() {
        return new GetLZBIdBySecondIdResponse();
    }

    /**
     * Create an instance of {@link EUserRegist }
     * 
     */
    public EUserRegist createEUserRegist() {
        return new EUserRegist();
    }

    /**
     * Create an instance of {@link ValidateSMSOTP }
     * 
     */
    public ValidateSMSOTP createValidateSMSOTP() {
        return new ValidateSMSOTP();
    }

    /**
     * Create an instance of {@link MobileMainBind }
     * 
     */
    public MobileMainBind createMobileMainBind() {
        return new MobileMainBind();
    }

    /**
     * Create an instance of {@link MberRealNameAuthResponse }
     * 
     */
    public MberRealNameAuthResponse createMberRealNameAuthResponse() {
        return new MberRealNameAuthResponse();
    }

    /**
     * Create an instance of {@link GetLZBIdBySecondId }
     * 
     */
    public GetLZBIdBySecondId createGetLZBIdBySecondId() {
        return new GetLZBIdBySecondId();
    }

    /**
     * Create an instance of {@link RegisterLZBId }
     * 
     */
    public RegisterLZBId createRegisterLZBId() {
        return new RegisterLZBId();
    }

    /**
     * Create an instance of {@link QueryCustInfos }
     * 
     */
    public QueryCustInfos createQueryCustInfos() {
        return new QueryCustInfos();
    }

    /**
     * Create an instance of {@link GetLzbOtherByUserIdResponse }
     * 
     */
    public GetLzbOtherByUserIdResponse createGetLzbOtherByUserIdResponse() {
        return new GetLzbOtherByUserIdResponse();
    }

    /**
     * Create an instance of {@link ValidateImageResponse }
     * 
     */
    public ValidateImageResponse createValidateImageResponse() {
        return new ValidateImageResponse();
    }

    /**
     * Create an instance of {@link UnbindLZBIdResponse }
     * 
     */
    public UnbindLZBIdResponse createUnbindLZBIdResponse() {
        return new UnbindLZBIdResponse();
    }

    /**
     * Create an instance of {@link ChangePhoneRequestResponse }
     * 
     */
    public ChangePhoneRequestResponse createChangePhoneRequestResponse() {
        return new ChangePhoneRequestResponse();
    }

    /**
     * Create an instance of {@link ValidateInfoResponse }
     * 
     */
    public ValidateInfoResponse createValidateInfoResponse() {
        return new ValidateInfoResponse();
    }

    /**
     * Create an instance of {@link GetAnyDataPublicMethod }
     * 
     */
    public GetAnyDataPublicMethod createGetAnyDataPublicMethod() {
        return new GetAnyDataPublicMethod();
    }

    /**
     * Create an instance of {@link ValidateMethodResponse }
     * 
     */
    public ValidateMethodResponse createValidateMethodResponse() {
        return new ValidateMethodResponse();
    }

    /**
     * Create an instance of {@link SendEmailResetPasswordResponse }
     * 
     */
    public SendEmailResetPasswordResponse createSendEmailResetPasswordResponse() {
        return new SendEmailResetPasswordResponse();
    }

    /**
     * Create an instance of {@link GetLZBIdAssistant }
     * 
     */
    public GetLZBIdAssistant createGetLZBIdAssistant() {
        return new GetLZBIdAssistant();
    }

    /**
     * Create an instance of {@link CheckCerNumResponse }
     * 
     */
    public CheckCerNumResponse createCheckCerNumResponse() {
        return new CheckCerNumResponse();
    }

    /**
     * Create an instance of {@link GetccLogResponse }
     * 
     */
    public GetccLogResponse createGetccLogResponse() {
        return new GetccLogResponse();
    }

    /**
     * Create an instance of {@link GetOrSetSamlResponseData }
     * 
     */
    public GetOrSetSamlResponseData createGetOrSetSamlResponseData() {
        return new GetOrSetSamlResponseData();
    }

    /**
     * Create an instance of {@link SendEmailResponse }
     * 
     */
    public SendEmailResponse createSendEmailResponse() {
        return new SendEmailResponse();
    }

    /**
     * Create an instance of {@link VerifyEmailResponse }
     * 
     */
    public VerifyEmailResponse createVerifyEmailResponse() {
        return new VerifyEmailResponse();
    }

    /**
     * Create an instance of {@link EresetLZBIdPWResponse }
     * 
     */
    public EresetLZBIdPWResponse createEresetLZBIdPWResponse() {
        return new EresetLZBIdPWResponse();
    }

    /**
     * Create an instance of {@link AccessUserProfileResponse }
     * 
     */
    public AccessUserProfileResponse createAccessUserProfileResponse() {
        return new AccessUserProfileResponse();
    }

    /**
     * Create an instance of {@link FreezeResponse }
     * 
     */
    public FreezeResponse createFreezeResponse() {
        return new FreezeResponse();
    }

    /**
     * Create an instance of {@link LocalValidateSMSOTPResponse }
     * 
     */
    public LocalValidateSMSOTPResponse createLocalValidateSMSOTPResponse() {
        return new LocalValidateSMSOTPResponse();
    }

    /**
     * Create an instance of {@link ChangeLZBIdPW }
     * 
     */
    public ChangeLZBIdPW createChangeLZBIdPW() {
        return new ChangeLZBIdPW();
    }

    /**
     * Create an instance of {@link EchangeLZBIdPW }
     * 
     */
    public EchangeLZBIdPW createEchangeLZBIdPW() {
        return new EchangeLZBIdPW();
    }

    /**
     * Create an instance of {@link EloginResponse }
     * 
     */
    public EloginResponse createEloginResponse() {
        return new EloginResponse();
    }

    /**
     * Create an instance of {@link GetLZBIdAndBindInfo }
     * 
     */
    public GetLZBIdAndBindInfo createGetLZBIdAndBindInfo() {
        return new GetLZBIdAndBindInfo();
    }

    /**
     * Create an instance of {@link MobileMainBindResponse }
     * 
     */
    public MobileMainBindResponse createMobileMainBindResponse() {
        return new MobileMainBindResponse();
    }

    /**
     * Create an instance of {@link ReflashSessionResponse }
     * 
     */
    public ReflashSessionResponse createReflashSessionResponse() {
        return new ReflashSessionResponse();
    }

    /**
     * Create an instance of {@link EMobileLogin }
     * 
     */
    public EMobileLogin createEMobileLogin() {
        return new EMobileLogin();
    }

    /**
     * Create an instance of {@link GetLzbOtherByUserId }
     * 
     */
    public GetLzbOtherByUserId createGetLzbOtherByUserId() {
        return new GetLzbOtherByUserId();
    }

    /**
     * Create an instance of {@link LocalValidateSMSOTP }
     * 
     */
    public LocalValidateSMSOTP createLocalValidateSMSOTP() {
        return new LocalValidateSMSOTP();
    }

    /**
     * Create an instance of {@link ModifyEmailByPhoneForUnactiveResponse }
     * 
     */
    public ModifyEmailByPhoneForUnactiveResponse createModifyEmailByPhoneForUnactiveResponse() {
        return new ModifyEmailByPhoneForUnactiveResponse();
    }

    /**
     * Create an instance of {@link EUserUpdateResponse }
     * 
     */
    public EUserUpdateResponse createEUserUpdateResponse() {
        return new EUserUpdateResponse();
    }

    /**
     * Create an instance of {@link ModifyEmailByPhoneForUnactive }
     * 
     */
    public ModifyEmailByPhoneForUnactive createModifyEmailByPhoneForUnactive() {
        return new ModifyEmailByPhoneForUnactive();
    }

    /**
     * Create an instance of {@link VerifyEmail }
     * 
     */
    public VerifyEmail createVerifyEmail() {
        return new VerifyEmail();
    }

    /**
     * Create an instance of {@link GetElzbid }
     * 
     */
    public GetElzbid createGetElzbid() {
        return new GetElzbid();
    }

    /**
     * Create an instance of {@link ModifyLZBId }
     * 
     */
    public ModifyLZBId createModifyLZBId() {
        return new ModifyLZBId();
    }

    /**
     * Create an instance of {@link UpdateEmailByToken }
     * 
     */
    public UpdateEmailByToken createUpdateEmailByToken() {
        return new UpdateEmailByToken();
    }

    /**
     * Create an instance of {@link EnrollFaceResponse }
     * 
     */
    public EnrollFaceResponse createEnrollFaceResponse() {
        return new EnrollFaceResponse();
    }

    /**
     * Create an instance of {@link EnrollFace }
     * 
     */
    public EnrollFace createEnrollFace() {
        return new EnrollFace();
    }

    /**
     * Create an instance of {@link MobileValidateSMSOTPResponse }
     * 
     */
    public MobileValidateSMSOTPResponse createMobileValidateSMSOTPResponse() {
        return new MobileValidateSMSOTPResponse();
    }

    /**
     * Create an instance of {@link ValidateImage }
     * 
     */
    public ValidateImage createValidateImage() {
        return new ValidateImage();
    }

    /**
     * Create an instance of {@link GetElzbidResponse }
     * 
     */
    public GetElzbidResponse createGetElzbidResponse() {
        return new GetElzbidResponse();
    }

    /**
     * Create an instance of {@link LoginResponse }
     * 
     */
    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    /**
     * Create an instance of {@link GetElzbidForCCWebResponse }
     * 
     */
    public GetElzbidForCCWebResponse createGetElzbidForCCWebResponse() {
        return new GetElzbidForCCWebResponse();
    }

    /**
     * Create an instance of {@link MobileLoginResponse }
     * 
     */
    public MobileLoginResponse createMobileLoginResponse() {
        return new MobileLoginResponse();
    }

    /**
     * Create an instance of {@link SendSMSOTP }
     * 
     */
    public SendSMSOTP createSendSMSOTP() {
        return new SendSMSOTP();
    }

    /**
     * Create an instance of {@link GetUserLoginResponse }
     * 
     */
    public GetUserLoginResponse createGetUserLoginResponse() {
        return new GetUserLoginResponse();
    }

    /**
     * Create an instance of {@link GetUserLogin }
     * 
     */
    public GetUserLogin createGetUserLogin() {
        return new GetUserLogin();
    }

    /**
     * Create an instance of {@link EMobileLoginResponse }
     * 
     */
    public EMobileLoginResponse createEMobileLoginResponse() {
        return new EMobileLoginResponse();
    }

    /**
     * Create an instance of {@link Logout }
     * 
     */
    public Logout createLogout() {
        return new Logout();
    }

    /**
     * Create an instance of {@link BindLZBId }
     * 
     */
    public BindLZBId createBindLZBId() {
        return new BindLZBId();
    }

    /**
     * Create an instance of {@link MoveAssistantUser }
     * 
     */
    public MoveAssistantUser createMoveAssistantUser() {
        return new MoveAssistantUser();
    }

    /**
     * Create an instance of {@link CheckCerNum }
     * 
     */
    public CheckCerNum createCheckCerNum() {
        return new CheckCerNum();
    }

    /**
     * Create an instance of {@link EsendSMSOTPResponse }
     * 
     */
    public EsendSMSOTPResponse createEsendSMSOTPResponse() {
        return new EsendSMSOTPResponse();
    }

    /**
     * Create an instance of {@link BindLZBIdResponse }
     * 
     */
    public BindLZBIdResponse createBindLZBIdResponse() {
        return new BindLZBIdResponse();
    }

    /**
     * Create an instance of {@link ValidateCardInfoResponse }
     * 
     */
    public ValidateCardInfoResponse createValidateCardInfoResponse() {
        return new ValidateCardInfoResponse();
    }

    /**
     * Create an instance of {@link ChangePhoneRequest }
     * 
     */
    public ChangePhoneRequest createChangePhoneRequest() {
        return new ChangePhoneRequest();
    }

    /**
     * Create an instance of {@link EvalidateSMSOTP }
     * 
     */
    public EvalidateSMSOTP createEvalidateSMSOTP() {
        return new EvalidateSMSOTP();
    }

    /**
     * Create an instance of {@link MoveAssistantUserResponse }
     * 
     */
    public MoveAssistantUserResponse createMoveAssistantUserResponse() {
        return new MoveAssistantUserResponse();
    }

    /**
     * Create an instance of {@link GetAllSystemInfo }
     * 
     */
    public GetAllSystemInfo createGetAllSystemInfo() {
        return new GetAllSystemInfo();
    }

    /**
     * Create an instance of {@link Login }
     * 
     */
    public Login createLogin() {
        return new Login();
    }

    /**
     * Create an instance of {@link VerifyHttpAndBackUserInfoResponse }
     * 
     */
    public VerifyHttpAndBackUserInfoResponse createVerifyHttpAndBackUserInfoResponse() {
        return new VerifyHttpAndBackUserInfoResponse();
    }

    /**
     * Create an instance of {@link ValidateInfo }
     * 
     */
    public ValidateInfo createValidateInfo() {
        return new ValidateInfo();
    }

    /**
     * Create an instance of {@link GetElzbidForCCWeb }
     * 
     */
    public GetElzbidForCCWeb createGetElzbidForCCWeb() {
        return new GetElzbidForCCWeb();
    }

    /**
     * Create an instance of {@link ValidateSMSOTPResponse }
     * 
     */
    public ValidateSMSOTPResponse createValidateSMSOTPResponse() {
        return new ValidateSMSOTPResponse();
    }

    /**
     * Create an instance of {@link GetLZBIdResponse }
     * 
     */
    public GetLZBIdResponse createGetLZBIdResponse() {
        return new GetLZBIdResponse();
    }

    /**
     * Create an instance of {@link LogoutResponse }
     * 
     */
    public LogoutResponse createLogoutResponse() {
        return new LogoutResponse();
    }

    /**
     * Create an instance of {@link GetChangePhoneRequest }
     * 
     */
    public GetChangePhoneRequest createGetChangePhoneRequest() {
        return new GetChangePhoneRequest();
    }

    /**
     * Create an instance of {@link GenerateQRCodeResponse }
     * 
     */
    public GenerateQRCodeResponse createGenerateQRCodeResponse() {
        return new GenerateQRCodeResponse();
    }

    /**
     * Create an instance of {@link SendSMSOTPResponse }
     * 
     */
    public SendSMSOTPResponse createSendSMSOTPResponse() {
        return new SendSMSOTPResponse();
    }

    /**
     * Create an instance of {@link MainBindResponse }
     * 
     */
    public MainBindResponse createMainBindResponse() {
        return new MainBindResponse();
    }

    /**
     * Create an instance of {@link EUserRegistResponse }
     * 
     */
    public EUserRegistResponse createEUserRegistResponse() {
        return new EUserRegistResponse();
    }

    /**
     * Create an instance of {@link EresetLZBIdPW }
     * 
     */
    public EresetLZBIdPW createEresetLZBIdPW() {
        return new EresetLZBIdPW();
    }

    /**
     * Create an instance of {@link EsendSMSOTP }
     * 
     */
    public EsendSMSOTP createEsendSMSOTP() {
        return new EsendSMSOTP();
    }

    /**
     * Create an instance of {@link GetLZBIdAssistantResponse }
     * 
     */
    public GetLZBIdAssistantResponse createGetLZBIdAssistantResponse() {
        return new GetLZBIdAssistantResponse();
    }

    /**
     * Create an instance of {@link GetReservedMobilesResponse }
     * 
     */
    public GetReservedMobilesResponse createGetReservedMobilesResponse() {
        return new GetReservedMobilesResponse();
    }

    /**
     * Create an instance of {@link InvokeResponse }
     * 
     */
    public InvokeResponse createInvokeResponse() {
        return new InvokeResponse();
    }

    /**
     * Create an instance of {@link QueryUnbindRequestByYHTUUID }
     * 
     */
    public QueryUnbindRequestByYHTUUID createQueryUnbindRequestByYHTUUID() {
        return new QueryUnbindRequestByYHTUUID();
    }

    /**
     * Create an instance of {@link VerifyHttpAndBackUserInfo }
     * 
     */
    public VerifyHttpAndBackUserInfo createVerifyHttpAndBackUserInfo() {
        return new VerifyHttpAndBackUserInfo();
    }

    /**
     * Create an instance of {@link SendEmail }
     * 
     */
    public SendEmail createSendEmail() {
        return new SendEmail();
    }

    /**
     * Create an instance of {@link ResetLZBIdPW }
     * 
     */
    public ResetLZBIdPW createResetLZBIdPW() {
        return new ResetLZBIdPW();
    }

    /**
     * Create an instance of {@link OpenCustCleaRes }
     * 
     */
    public OpenCustCleaRes createOpenCustCleaRes() {
        return new OpenCustCleaRes();
    }

    /**
     * Create an instance of {@link MainBind }
     * 
     */
    public MainBind createMainBind() {
        return new MainBind();
    }

    /**
     * Create an instance of {@link UnbindLZBId }
     * 
     */
    public UnbindLZBId createUnbindLZBId() {
        return new UnbindLZBId();
    }

    /**
     * Create an instance of {@link QueryCustInfosResponse }
     * 
     */
    public QueryCustInfosResponse createQueryCustInfosResponse() {
        return new QueryCustInfosResponse();
    }

    /**
     * Create an instance of {@link ModifyLZBIdResponse }
     * 
     */
    public ModifyLZBIdResponse createModifyLZBIdResponse() {
        return new ModifyLZBIdResponse();
    }

    /**
     * Create an instance of {@link GetSessionTokenByQrcode }
     * 
     */
    public GetSessionTokenByQrcode createGetSessionTokenByQrcode() {
        return new GetSessionTokenByQrcode();
    }

    /**
     * Create an instance of {@link EnterpriseUpdateResponse }
     * 
     */
    public EnterpriseUpdateResponse createEnterpriseUpdateResponse() {
        return new EnterpriseUpdateResponse();
    }

    /**
     * Create an instance of {@link GetccLog }
     * 
     */
    public GetccLog createGetccLog() {
        return new GetccLog();
    }

    /**
     * Create an instance of {@link GetAllSystemInfoResponse }
     * 
     */
    public GetAllSystemInfoResponse createGetAllSystemInfoResponse() {
        return new GetAllSystemInfoResponse();
    }

    /**
     * Create an instance of {@link ECompanyRegistResponse }
     * 
     */
    public ECompanyRegistResponse createECompanyRegistResponse() {
        return new ECompanyRegistResponse();
    }

    /**
     * Create an instance of {@link GetSystemSSOURLBySystemnameResponse }
     * 
     */
    public GetSystemSSOURLBySystemnameResponse createGetSystemSSOURLBySystemnameResponse() {
        return new GetSystemSSOURLBySystemnameResponse();
    }

    /**
     * Create an instance of {@link Elogin }
     * 
     */
    public Elogin createElogin() {
        return new Elogin();
    }

    /**
     * Create an instance of {@link GetLZBId }
     * 
     */
    public GetLZBId createGetLZBId() {
        return new GetLZBId();
    }

    /**
     * Create an instance of {@link EUserUpdate }
     * 
     */
    public EUserUpdate createEUserUpdate() {
        return new EUserUpdate();
    }

    /**
     * Create an instance of {@link EnterpriseUpdate }
     * 
     */
    public EnterpriseUpdate createEnterpriseUpdate() {
        return new EnterpriseUpdate();
    }

    /**
     * Create an instance of {@link Openepay }
     * 
     */
    public Openepay createOpenepay() {
        return new Openepay();
    }

    /**
     * Create an instance of {@link GetReservedMobiles }
     * 
     */
    public GetReservedMobiles createGetReservedMobiles() {
        return new GetReservedMobiles();
    }

    /**
     * Create an instance of {@link SendEmailResetPassword }
     * 
     */
    public SendEmailResetPassword createSendEmailResetPassword() {
        return new SendEmailResetPassword();
    }

    /**
     * Create an instance of {@link GetChangePhoneRequestResponse }
     * 
     */
    public GetChangePhoneRequestResponse createGetChangePhoneRequestResponse() {
        return new GetChangePhoneRequestResponse();
    }

    /**
     * Create an instance of {@link GetOrSetSamlResponseDataResponse }
     * 
     */
    public GetOrSetSamlResponseDataResponse createGetOrSetSamlResponseDataResponse() {
        return new GetOrSetSamlResponseDataResponse();
    }

    /**
     * Create an instance of {@link ReflashSession }
     * 
     */
    public ReflashSession createReflashSession() {
        return new ReflashSession();
    }

    /**
     * Create an instance of {@link MaintainCustFee }
     * 
     */
    public MaintainCustFee createMaintainCustFee() {
        return new MaintainCustFee();
    }

    /**
     * Create an instance of {@link EvalidateSMSOTPResponse }
     * 
     */
    public EvalidateSMSOTPResponse createEvalidateSMSOTPResponse() {
        return new EvalidateSMSOTPResponse();
    }

    /**
     * Create an instance of {@link MberRealNameAuth }
     * 
     */
    public MberRealNameAuth createMberRealNameAuth() {
        return new MberRealNameAuth();
    }

    /**
     * Create an instance of {@link QueryUnbindRequestByYHTUUIDResponse }
     * 
     */
    public QueryUnbindRequestByYHTUUIDResponse createQueryUnbindRequestByYHTUUIDResponse() {
        return new QueryUnbindRequestByYHTUUIDResponse();
    }

    /**
     * Create an instance of {@link MaintainCustFeeResponse }
     * 
     */
    public MaintainCustFeeResponse createMaintainCustFeeResponse() {
        return new MaintainCustFeeResponse();
    }

    /**
     * Create an instance of {@link ResetLZBIdPWResponse }
     * 
     */
    public ResetLZBIdPWResponse createResetLZBIdPWResponse() {
        return new ResetLZBIdPWResponse();
    }

    /**
     * Create an instance of {@link GenerateQRCode }
     * 
     */
    public GenerateQRCode createGenerateQRCode() {
        return new GenerateQRCode();
    }

    /**
     * Create an instance of {@link GetLZBIdAndBindInfoResponse }
     * 
     */
    public GetLZBIdAndBindInfoResponse createGetLZBIdAndBindInfoResponse() {
        return new GetLZBIdAndBindInfoResponse();
    }

    /**
     * Create an instance of {@link OpenepayResponse }
     * 
     */
    public OpenepayResponse createOpenepayResponse() {
        return new OpenepayResponse();
    }

    /**
     * Create an instance of {@link ValidateMethod }
     * 
     */
    public ValidateMethod createValidateMethod() {
        return new ValidateMethod();
    }

    /**
     * Create an instance of {@link GetSystemSSOURLBySystemname }
     * 
     */
    public GetSystemSSOURLBySystemname createGetSystemSSOURLBySystemname() {
        return new GetSystemSSOURLBySystemname();
    }

    /**
     * Create an instance of {@link GetURLDateResponse }
     * 
     */
    public GetURLDateResponse createGetURLDateResponse() {
        return new GetURLDateResponse();
    }

    /**
     * Create an instance of {@link Freeze }
     * 
     */
    public Freeze createFreeze() {
        return new Freeze();
    }

    /**
     * Create an instance of {@link GetURLDate }
     * 
     */
    public GetURLDate createGetURLDate() {
        return new GetURLDate();
    }

    /**
     * Create an instance of {@link Invoke }
     * 
     */
    public Invoke createInvoke() {
        return new Invoke();
    }

    /**
     * Create an instance of {@link UpdateEmailByTokenResponse }
     * 
     */
    public UpdateEmailByTokenResponse createUpdateEmailByTokenResponse() {
        return new UpdateEmailByTokenResponse();
    }

    /**
     * Create an instance of {@link EchangeLZBIdPWResponse }
     * 
     */
    public EchangeLZBIdPWResponse createEchangeLZBIdPWResponse() {
        return new EchangeLZBIdPWResponse();
    }

    /**
     * Create an instance of {@link RegisterLZBIdResponse }
     * 
     */
    public RegisterLZBIdResponse createRegisterLZBIdResponse() {
        return new RegisterLZBIdResponse();
    }

    /**
     * Create an instance of {@link ValidateCardInfo }
     * 
     */
    public ValidateCardInfo createValidateCardInfo() {
        return new ValidateCardInfo();
    }

    /**
     * Create an instance of {@link GetAnyDataPublicMethodResponse }
     * 
     */
    public GetAnyDataPublicMethodResponse createGetAnyDataPublicMethodResponse() {
        return new GetAnyDataPublicMethodResponse();
    }

    /**
     * Create an instance of {@link AccessUserProfile }
     * 
     */
    public AccessUserProfile createAccessUserProfile() {
        return new AccessUserProfile();
    }

    /**
     * Create an instance of {@link OpenCustCleaResResponse }
     * 
     */
    public OpenCustCleaResResponse createOpenCustCleaResResponse() {
        return new OpenCustCleaResResponse();
    }

    /**
     * Create an instance of {@link GetSessionTokenByQrcodeResponse }
     * 
     */
    public GetSessionTokenByQrcodeResponse createGetSessionTokenByQrcodeResponse() {
        return new GetSessionTokenByQrcodeResponse();
    }

    /**
     * Create an instance of {@link MobileLogin }
     * 
     */
    public MobileLogin createMobileLogin() {
        return new MobileLogin();
    }

    /**
     * Create an instance of {@link ChangeLZBIdPWResponse }
     * 
     */
    public ChangeLZBIdPWResponse createChangeLZBIdPWResponse() {
        return new ChangeLZBIdPWResponse();
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link CcServiceInObj }
     * 
     */
    public CcServiceInObj createCcServiceInObj() {
        return new CcServiceInObj();
    }

    /**
     * Create an instance of {@link CcServiceOutObj.Suuid.Entry }
     * 
     */
    public CcServiceOutObj.Suuid.Entry createCcServiceOutObjSuuidEntry() {
        return new CcServiceOutObj.Suuid.Entry();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ECompanyRegist }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "eCompanyRegist")
    public JAXBElement<ECompanyRegist> createECompanyRegist(ECompanyRegist value) {
        return new JAXBElement<ECompanyRegist>(_ECompanyRegist_QNAME, ECompanyRegist.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MobileValidateSMSOTP }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "mobileValidateSMSOTP")
    public JAXBElement<MobileValidateSMSOTP> createMobileValidateSMSOTP(MobileValidateSMSOTP value) {
        return new JAXBElement<MobileValidateSMSOTP>(_MobileValidateSMSOTP_QNAME, MobileValidateSMSOTP.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLZBIdBySecondIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getLZBIdBySecondIdResponse")
    public JAXBElement<GetLZBIdBySecondIdResponse> createGetLZBIdBySecondIdResponse(GetLZBIdBySecondIdResponse value) {
        return new JAXBElement<GetLZBIdBySecondIdResponse>(_GetLZBIdBySecondIdResponse_QNAME, GetLZBIdBySecondIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EUserRegist }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "eUserRegist")
    public JAXBElement<EUserRegist> createEUserRegist(EUserRegist value) {
        return new JAXBElement<EUserRegist>(_EUserRegist_QNAME, EUserRegist.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateSMSOTP }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "validateSMSOTP")
    public JAXBElement<ValidateSMSOTP> createValidateSMSOTP(ValidateSMSOTP value) {
        return new JAXBElement<ValidateSMSOTP>(_ValidateSMSOTP_QNAME, ValidateSMSOTP.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MobileMainBind }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "mobileMainBind")
    public JAXBElement<MobileMainBind> createMobileMainBind(MobileMainBind value) {
        return new JAXBElement<MobileMainBind>(_MobileMainBind_QNAME, MobileMainBind.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MberRealNameAuthResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "mberRealNameAuthResponse")
    public JAXBElement<MberRealNameAuthResponse> createMberRealNameAuthResponse(MberRealNameAuthResponse value) {
        return new JAXBElement<MberRealNameAuthResponse>(_MberRealNameAuthResponse_QNAME, MberRealNameAuthResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLZBIdBySecondId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getLZBIdBySecondId")
    public JAXBElement<GetLZBIdBySecondId> createGetLZBIdBySecondId(GetLZBIdBySecondId value) {
        return new JAXBElement<GetLZBIdBySecondId>(_GetLZBIdBySecondId_QNAME, GetLZBIdBySecondId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterLZBId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "registerLZBId")
    public JAXBElement<RegisterLZBId> createRegisterLZBId(RegisterLZBId value) {
        return new JAXBElement<RegisterLZBId>(_RegisterLZBId_QNAME, RegisterLZBId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryCustInfos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "queryCustInfos")
    public JAXBElement<QueryCustInfos> createQueryCustInfos(QueryCustInfos value) {
        return new JAXBElement<QueryCustInfos>(_QueryCustInfos_QNAME, QueryCustInfos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLzbOtherByUserIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getLzbOtherByUserIdResponse")
    public JAXBElement<GetLzbOtherByUserIdResponse> createGetLzbOtherByUserIdResponse(GetLzbOtherByUserIdResponse value) {
        return new JAXBElement<GetLzbOtherByUserIdResponse>(_GetLzbOtherByUserIdResponse_QNAME, GetLzbOtherByUserIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateImageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "validateImageResponse")
    public JAXBElement<ValidateImageResponse> createValidateImageResponse(ValidateImageResponse value) {
        return new JAXBElement<ValidateImageResponse>(_ValidateImageResponse_QNAME, ValidateImageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnbindLZBIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "unbindLZBIdResponse")
    public JAXBElement<UnbindLZBIdResponse> createUnbindLZBIdResponse(UnbindLZBIdResponse value) {
        return new JAXBElement<UnbindLZBIdResponse>(_UnbindLZBIdResponse_QNAME, UnbindLZBIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangePhoneRequestResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "changePhoneRequestResponse")
    public JAXBElement<ChangePhoneRequestResponse> createChangePhoneRequestResponse(ChangePhoneRequestResponse value) {
        return new JAXBElement<ChangePhoneRequestResponse>(_ChangePhoneRequestResponse_QNAME, ChangePhoneRequestResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "validateInfoResponse")
    public JAXBElement<ValidateInfoResponse> createValidateInfoResponse(ValidateInfoResponse value) {
        return new JAXBElement<ValidateInfoResponse>(_ValidateInfoResponse_QNAME, ValidateInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAnyDataPublicMethod }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getAnyDataPublicMethod")
    public JAXBElement<GetAnyDataPublicMethod> createGetAnyDataPublicMethod(GetAnyDataPublicMethod value) {
        return new JAXBElement<GetAnyDataPublicMethod>(_GetAnyDataPublicMethod_QNAME, GetAnyDataPublicMethod.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateMethodResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "validateMethodResponse")
    public JAXBElement<ValidateMethodResponse> createValidateMethodResponse(ValidateMethodResponse value) {
        return new JAXBElement<ValidateMethodResponse>(_ValidateMethodResponse_QNAME, ValidateMethodResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendEmailResetPasswordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "sendEmailResetPasswordResponse")
    public JAXBElement<SendEmailResetPasswordResponse> createSendEmailResetPasswordResponse(SendEmailResetPasswordResponse value) {
        return new JAXBElement<SendEmailResetPasswordResponse>(_SendEmailResetPasswordResponse_QNAME, SendEmailResetPasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLZBIdAssistant }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getLZBIdAssistant")
    public JAXBElement<GetLZBIdAssistant> createGetLZBIdAssistant(GetLZBIdAssistant value) {
        return new JAXBElement<GetLZBIdAssistant>(_GetLZBIdAssistant_QNAME, GetLZBIdAssistant.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckCerNumResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "checkCerNumResponse")
    public JAXBElement<CheckCerNumResponse> createCheckCerNumResponse(CheckCerNumResponse value) {
        return new JAXBElement<CheckCerNumResponse>(_CheckCerNumResponse_QNAME, CheckCerNumResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetccLogResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getccLogResponse")
    public JAXBElement<GetccLogResponse> createGetccLogResponse(GetccLogResponse value) {
        return new JAXBElement<GetccLogResponse>(_GetccLogResponse_QNAME, GetccLogResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOrSetSamlResponseData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getOrSetSamlResponseData")
    public JAXBElement<GetOrSetSamlResponseData> createGetOrSetSamlResponseData(GetOrSetSamlResponseData value) {
        return new JAXBElement<GetOrSetSamlResponseData>(_GetOrSetSamlResponseData_QNAME, GetOrSetSamlResponseData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendEmailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "sendEmailResponse")
    public JAXBElement<SendEmailResponse> createSendEmailResponse(SendEmailResponse value) {
        return new JAXBElement<SendEmailResponse>(_SendEmailResponse_QNAME, SendEmailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyEmailResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "verifyEmailResponse")
    public JAXBElement<VerifyEmailResponse> createVerifyEmailResponse(VerifyEmailResponse value) {
        return new JAXBElement<VerifyEmailResponse>(_VerifyEmailResponse_QNAME, VerifyEmailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EresetLZBIdPWResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "eresetLZBIdPWResponse")
    public JAXBElement<EresetLZBIdPWResponse> createEresetLZBIdPWResponse(EresetLZBIdPWResponse value) {
        return new JAXBElement<EresetLZBIdPWResponse>(_EresetLZBIdPWResponse_QNAME, EresetLZBIdPWResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccessUserProfileResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "accessUserProfileResponse")
    public JAXBElement<AccessUserProfileResponse> createAccessUserProfileResponse(AccessUserProfileResponse value) {
        return new JAXBElement<AccessUserProfileResponse>(_AccessUserProfileResponse_QNAME, AccessUserProfileResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FreezeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "freezeResponse")
    public JAXBElement<FreezeResponse> createFreezeResponse(FreezeResponse value) {
        return new JAXBElement<FreezeResponse>(_FreezeResponse_QNAME, FreezeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LocalValidateSMSOTPResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "localValidateSMSOTPResponse")
    public JAXBElement<LocalValidateSMSOTPResponse> createLocalValidateSMSOTPResponse(LocalValidateSMSOTPResponse value) {
        return new JAXBElement<LocalValidateSMSOTPResponse>(_LocalValidateSMSOTPResponse_QNAME, LocalValidateSMSOTPResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeLZBIdPW }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "changeLZBIdPW")
    public JAXBElement<ChangeLZBIdPW> createChangeLZBIdPW(ChangeLZBIdPW value) {
        return new JAXBElement<ChangeLZBIdPW>(_ChangeLZBIdPW_QNAME, ChangeLZBIdPW.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EchangeLZBIdPW }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "echangeLZBIdPW")
    public JAXBElement<EchangeLZBIdPW> createEchangeLZBIdPW(EchangeLZBIdPW value) {
        return new JAXBElement<EchangeLZBIdPW>(_EchangeLZBIdPW_QNAME, EchangeLZBIdPW.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EloginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "eloginResponse")
    public JAXBElement<EloginResponse> createEloginResponse(EloginResponse value) {
        return new JAXBElement<EloginResponse>(_EloginResponse_QNAME, EloginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLZBIdAndBindInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getLZBIdAndBindInfo")
    public JAXBElement<GetLZBIdAndBindInfo> createGetLZBIdAndBindInfo(GetLZBIdAndBindInfo value) {
        return new JAXBElement<GetLZBIdAndBindInfo>(_GetLZBIdAndBindInfo_QNAME, GetLZBIdAndBindInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MobileMainBindResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "mobileMainBindResponse")
    public JAXBElement<MobileMainBindResponse> createMobileMainBindResponse(MobileMainBindResponse value) {
        return new JAXBElement<MobileMainBindResponse>(_MobileMainBindResponse_QNAME, MobileMainBindResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReflashSessionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "reflashSessionResponse")
    public JAXBElement<ReflashSessionResponse> createReflashSessionResponse(ReflashSessionResponse value) {
        return new JAXBElement<ReflashSessionResponse>(_ReflashSessionResponse_QNAME, ReflashSessionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EMobileLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "eMobileLogin")
    public JAXBElement<EMobileLogin> createEMobileLogin(EMobileLogin value) {
        return new JAXBElement<EMobileLogin>(_EMobileLogin_QNAME, EMobileLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLzbOtherByUserId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getLzbOtherByUserId")
    public JAXBElement<GetLzbOtherByUserId> createGetLzbOtherByUserId(GetLzbOtherByUserId value) {
        return new JAXBElement<GetLzbOtherByUserId>(_GetLzbOtherByUserId_QNAME, GetLzbOtherByUserId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LocalValidateSMSOTP }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "localValidateSMSOTP")
    public JAXBElement<LocalValidateSMSOTP> createLocalValidateSMSOTP(LocalValidateSMSOTP value) {
        return new JAXBElement<LocalValidateSMSOTP>(_LocalValidateSMSOTP_QNAME, LocalValidateSMSOTP.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyEmailByPhoneForUnactiveResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "modifyEmailByPhoneForUnactiveResponse")
    public JAXBElement<ModifyEmailByPhoneForUnactiveResponse> createModifyEmailByPhoneForUnactiveResponse(ModifyEmailByPhoneForUnactiveResponse value) {
        return new JAXBElement<ModifyEmailByPhoneForUnactiveResponse>(_ModifyEmailByPhoneForUnactiveResponse_QNAME, ModifyEmailByPhoneForUnactiveResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EUserUpdateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "eUserUpdateResponse")
    public JAXBElement<EUserUpdateResponse> createEUserUpdateResponse(EUserUpdateResponse value) {
        return new JAXBElement<EUserUpdateResponse>(_EUserUpdateResponse_QNAME, EUserUpdateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyEmailByPhoneForUnactive }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "modifyEmailByPhoneForUnactive")
    public JAXBElement<ModifyEmailByPhoneForUnactive> createModifyEmailByPhoneForUnactive(ModifyEmailByPhoneForUnactive value) {
        return new JAXBElement<ModifyEmailByPhoneForUnactive>(_ModifyEmailByPhoneForUnactive_QNAME, ModifyEmailByPhoneForUnactive.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyEmail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "verifyEmail")
    public JAXBElement<VerifyEmail> createVerifyEmail(VerifyEmail value) {
        return new JAXBElement<VerifyEmail>(_VerifyEmail_QNAME, VerifyEmail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetElzbid }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getElzbid")
    public JAXBElement<GetElzbid> createGetElzbid(GetElzbid value) {
        return new JAXBElement<GetElzbid>(_GetElzbid_QNAME, GetElzbid.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyLZBId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "modifyLZBId")
    public JAXBElement<ModifyLZBId> createModifyLZBId(ModifyLZBId value) {
        return new JAXBElement<ModifyLZBId>(_ModifyLZBId_QNAME, ModifyLZBId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateEmailByToken }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "updateEmailByToken")
    public JAXBElement<UpdateEmailByToken> createUpdateEmailByToken(UpdateEmailByToken value) {
        return new JAXBElement<UpdateEmailByToken>(_UpdateEmailByToken_QNAME, UpdateEmailByToken.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnrollFaceResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "enrollFaceResponse")
    public JAXBElement<EnrollFaceResponse> createEnrollFaceResponse(EnrollFaceResponse value) {
        return new JAXBElement<EnrollFaceResponse>(_EnrollFaceResponse_QNAME, EnrollFaceResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnrollFace }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "enrollFace")
    public JAXBElement<EnrollFace> createEnrollFace(EnrollFace value) {
        return new JAXBElement<EnrollFace>(_EnrollFace_QNAME, EnrollFace.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MobileValidateSMSOTPResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "mobileValidateSMSOTPResponse")
    public JAXBElement<MobileValidateSMSOTPResponse> createMobileValidateSMSOTPResponse(MobileValidateSMSOTPResponse value) {
        return new JAXBElement<MobileValidateSMSOTPResponse>(_MobileValidateSMSOTPResponse_QNAME, MobileValidateSMSOTPResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateImage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "validateImage")
    public JAXBElement<ValidateImage> createValidateImage(ValidateImage value) {
        return new JAXBElement<ValidateImage>(_ValidateImage_QNAME, ValidateImage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetElzbidResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getElzbidResponse")
    public JAXBElement<GetElzbidResponse> createGetElzbidResponse(GetElzbidResponse value) {
        return new JAXBElement<GetElzbidResponse>(_GetElzbidResponse_QNAME, GetElzbidResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "loginResponse")
    public JAXBElement<LoginResponse> createLoginResponse(LoginResponse value) {
        return new JAXBElement<LoginResponse>(_LoginResponse_QNAME, LoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetElzbidForCCWebResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getElzbidForCCWebResponse")
    public JAXBElement<GetElzbidForCCWebResponse> createGetElzbidForCCWebResponse(GetElzbidForCCWebResponse value) {
        return new JAXBElement<GetElzbidForCCWebResponse>(_GetElzbidForCCWebResponse_QNAME, GetElzbidForCCWebResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MobileLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "mobileLoginResponse")
    public JAXBElement<MobileLoginResponse> createMobileLoginResponse(MobileLoginResponse value) {
        return new JAXBElement<MobileLoginResponse>(_MobileLoginResponse_QNAME, MobileLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendSMSOTP }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "sendSMSOTP")
    public JAXBElement<SendSMSOTP> createSendSMSOTP(SendSMSOTP value) {
        return new JAXBElement<SendSMSOTP>(_SendSMSOTP_QNAME, SendSMSOTP.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getUserLoginResponse")
    public JAXBElement<GetUserLoginResponse> createGetUserLoginResponse(GetUserLoginResponse value) {
        return new JAXBElement<GetUserLoginResponse>(_GetUserLoginResponse_QNAME, GetUserLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getUserLogin")
    public JAXBElement<GetUserLogin> createGetUserLogin(GetUserLogin value) {
        return new JAXBElement<GetUserLogin>(_GetUserLogin_QNAME, GetUserLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EMobileLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "eMobileLoginResponse")
    public JAXBElement<EMobileLoginResponse> createEMobileLoginResponse(EMobileLoginResponse value) {
        return new JAXBElement<EMobileLoginResponse>(_EMobileLoginResponse_QNAME, EMobileLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Logout }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "logout")
    public JAXBElement<Logout> createLogout(Logout value) {
        return new JAXBElement<Logout>(_Logout_QNAME, Logout.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BindLZBId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "bindLZBId")
    public JAXBElement<BindLZBId> createBindLZBId(BindLZBId value) {
        return new JAXBElement<BindLZBId>(_BindLZBId_QNAME, BindLZBId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoveAssistantUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "moveAssistantUser")
    public JAXBElement<MoveAssistantUser> createMoveAssistantUser(MoveAssistantUser value) {
        return new JAXBElement<MoveAssistantUser>(_MoveAssistantUser_QNAME, MoveAssistantUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckCerNum }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "checkCerNum")
    public JAXBElement<CheckCerNum> createCheckCerNum(CheckCerNum value) {
        return new JAXBElement<CheckCerNum>(_CheckCerNum_QNAME, CheckCerNum.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EsendSMSOTPResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "esendSMSOTPResponse")
    public JAXBElement<EsendSMSOTPResponse> createEsendSMSOTPResponse(EsendSMSOTPResponse value) {
        return new JAXBElement<EsendSMSOTPResponse>(_EsendSMSOTPResponse_QNAME, EsendSMSOTPResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BindLZBIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "bindLZBIdResponse")
    public JAXBElement<BindLZBIdResponse> createBindLZBIdResponse(BindLZBIdResponse value) {
        return new JAXBElement<BindLZBIdResponse>(_BindLZBIdResponse_QNAME, BindLZBIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateCardInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "validateCardInfoResponse")
    public JAXBElement<ValidateCardInfoResponse> createValidateCardInfoResponse(ValidateCardInfoResponse value) {
        return new JAXBElement<ValidateCardInfoResponse>(_ValidateCardInfoResponse_QNAME, ValidateCardInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangePhoneRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "changePhoneRequest")
    public JAXBElement<ChangePhoneRequest> createChangePhoneRequest(ChangePhoneRequest value) {
        return new JAXBElement<ChangePhoneRequest>(_ChangePhoneRequest_QNAME, ChangePhoneRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EvalidateSMSOTP }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "evalidateSMSOTP")
    public JAXBElement<EvalidateSMSOTP> createEvalidateSMSOTP(EvalidateSMSOTP value) {
        return new JAXBElement<EvalidateSMSOTP>(_EvalidateSMSOTP_QNAME, EvalidateSMSOTP.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MoveAssistantUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "moveAssistantUserResponse")
    public JAXBElement<MoveAssistantUserResponse> createMoveAssistantUserResponse(MoveAssistantUserResponse value) {
        return new JAXBElement<MoveAssistantUserResponse>(_MoveAssistantUserResponse_QNAME, MoveAssistantUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllSystemInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getAllSystemInfo")
    public JAXBElement<GetAllSystemInfo> createGetAllSystemInfo(GetAllSystemInfo value) {
        return new JAXBElement<GetAllSystemInfo>(_GetAllSystemInfo_QNAME, GetAllSystemInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Login }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "login")
    public JAXBElement<Login> createLogin(Login value) {
        return new JAXBElement<Login>(_Login_QNAME, Login.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyHttpAndBackUserInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "verifyHttpAndBackUserInfoResponse")
    public JAXBElement<VerifyHttpAndBackUserInfoResponse> createVerifyHttpAndBackUserInfoResponse(VerifyHttpAndBackUserInfoResponse value) {
        return new JAXBElement<VerifyHttpAndBackUserInfoResponse>(_VerifyHttpAndBackUserInfoResponse_QNAME, VerifyHttpAndBackUserInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "validateInfo")
    public JAXBElement<ValidateInfo> createValidateInfo(ValidateInfo value) {
        return new JAXBElement<ValidateInfo>(_ValidateInfo_QNAME, ValidateInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetElzbidForCCWeb }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getElzbidForCCWeb")
    public JAXBElement<GetElzbidForCCWeb> createGetElzbidForCCWeb(GetElzbidForCCWeb value) {
        return new JAXBElement<GetElzbidForCCWeb>(_GetElzbidForCCWeb_QNAME, GetElzbidForCCWeb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateSMSOTPResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "validateSMSOTPResponse")
    public JAXBElement<ValidateSMSOTPResponse> createValidateSMSOTPResponse(ValidateSMSOTPResponse value) {
        return new JAXBElement<ValidateSMSOTPResponse>(_ValidateSMSOTPResponse_QNAME, ValidateSMSOTPResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLZBIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getLZBIdResponse")
    public JAXBElement<GetLZBIdResponse> createGetLZBIdResponse(GetLZBIdResponse value) {
        return new JAXBElement<GetLZBIdResponse>(_GetLZBIdResponse_QNAME, GetLZBIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LogoutResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "logoutResponse")
    public JAXBElement<LogoutResponse> createLogoutResponse(LogoutResponse value) {
        return new JAXBElement<LogoutResponse>(_LogoutResponse_QNAME, LogoutResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetChangePhoneRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getChangePhoneRequest")
    public JAXBElement<GetChangePhoneRequest> createGetChangePhoneRequest(GetChangePhoneRequest value) {
        return new JAXBElement<GetChangePhoneRequest>(_GetChangePhoneRequest_QNAME, GetChangePhoneRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateQRCodeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "generateQRCodeResponse")
    public JAXBElement<GenerateQRCodeResponse> createGenerateQRCodeResponse(GenerateQRCodeResponse value) {
        return new JAXBElement<GenerateQRCodeResponse>(_GenerateQRCodeResponse_QNAME, GenerateQRCodeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendSMSOTPResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "sendSMSOTPResponse")
    public JAXBElement<SendSMSOTPResponse> createSendSMSOTPResponse(SendSMSOTPResponse value) {
        return new JAXBElement<SendSMSOTPResponse>(_SendSMSOTPResponse_QNAME, SendSMSOTPResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MainBindResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "mainBindResponse")
    public JAXBElement<MainBindResponse> createMainBindResponse(MainBindResponse value) {
        return new JAXBElement<MainBindResponse>(_MainBindResponse_QNAME, MainBindResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EUserRegistResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "eUserRegistResponse")
    public JAXBElement<EUserRegistResponse> createEUserRegistResponse(EUserRegistResponse value) {
        return new JAXBElement<EUserRegistResponse>(_EUserRegistResponse_QNAME, EUserRegistResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EresetLZBIdPW }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "eresetLZBIdPW")
    public JAXBElement<EresetLZBIdPW> createEresetLZBIdPW(EresetLZBIdPW value) {
        return new JAXBElement<EresetLZBIdPW>(_EresetLZBIdPW_QNAME, EresetLZBIdPW.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EsendSMSOTP }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "esendSMSOTP")
    public JAXBElement<EsendSMSOTP> createEsendSMSOTP(EsendSMSOTP value) {
        return new JAXBElement<EsendSMSOTP>(_EsendSMSOTP_QNAME, EsendSMSOTP.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLZBIdAssistantResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getLZBIdAssistantResponse")
    public JAXBElement<GetLZBIdAssistantResponse> createGetLZBIdAssistantResponse(GetLZBIdAssistantResponse value) {
        return new JAXBElement<GetLZBIdAssistantResponse>(_GetLZBIdAssistantResponse_QNAME, GetLZBIdAssistantResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetReservedMobilesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getReservedMobilesResponse")
    public JAXBElement<GetReservedMobilesResponse> createGetReservedMobilesResponse(GetReservedMobilesResponse value) {
        return new JAXBElement<GetReservedMobilesResponse>(_GetReservedMobilesResponse_QNAME, GetReservedMobilesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link InvokeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "invokeResponse")
    public JAXBElement<InvokeResponse> createInvokeResponse(InvokeResponse value) {
        return new JAXBElement<InvokeResponse>(_InvokeResponse_QNAME, InvokeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryUnbindRequestByYHTUUID }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "queryUnbindRequestByYHTUUID")
    public JAXBElement<QueryUnbindRequestByYHTUUID> createQueryUnbindRequestByYHTUUID(QueryUnbindRequestByYHTUUID value) {
        return new JAXBElement<QueryUnbindRequestByYHTUUID>(_QueryUnbindRequestByYHTUUID_QNAME, QueryUnbindRequestByYHTUUID.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VerifyHttpAndBackUserInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "verifyHttpAndBackUserInfo")
    public JAXBElement<VerifyHttpAndBackUserInfo> createVerifyHttpAndBackUserInfo(VerifyHttpAndBackUserInfo value) {
        return new JAXBElement<VerifyHttpAndBackUserInfo>(_VerifyHttpAndBackUserInfo_QNAME, VerifyHttpAndBackUserInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendEmail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "sendEmail")
    public JAXBElement<SendEmail> createSendEmail(SendEmail value) {
        return new JAXBElement<SendEmail>(_SendEmail_QNAME, SendEmail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResetLZBIdPW }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "resetLZBIdPW")
    public JAXBElement<ResetLZBIdPW> createResetLZBIdPW(ResetLZBIdPW value) {
        return new JAXBElement<ResetLZBIdPW>(_ResetLZBIdPW_QNAME, ResetLZBIdPW.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenCustCleaRes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "openCustCleaRes")
    public JAXBElement<OpenCustCleaRes> createOpenCustCleaRes(OpenCustCleaRes value) {
        return new JAXBElement<OpenCustCleaRes>(_OpenCustCleaRes_QNAME, OpenCustCleaRes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MainBind }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "mainBind")
    public JAXBElement<MainBind> createMainBind(MainBind value) {
        return new JAXBElement<MainBind>(_MainBind_QNAME, MainBind.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnbindLZBId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "unbindLZBId")
    public JAXBElement<UnbindLZBId> createUnbindLZBId(UnbindLZBId value) {
        return new JAXBElement<UnbindLZBId>(_UnbindLZBId_QNAME, UnbindLZBId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryCustInfosResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "queryCustInfosResponse")
    public JAXBElement<QueryCustInfosResponse> createQueryCustInfosResponse(QueryCustInfosResponse value) {
        return new JAXBElement<QueryCustInfosResponse>(_QueryCustInfosResponse_QNAME, QueryCustInfosResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyLZBIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "modifyLZBIdResponse")
    public JAXBElement<ModifyLZBIdResponse> createModifyLZBIdResponse(ModifyLZBIdResponse value) {
        return new JAXBElement<ModifyLZBIdResponse>(_ModifyLZBIdResponse_QNAME, ModifyLZBIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSessionTokenByQrcode }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getSessionTokenByQrcode")
    public JAXBElement<GetSessionTokenByQrcode> createGetSessionTokenByQrcode(GetSessionTokenByQrcode value) {
        return new JAXBElement<GetSessionTokenByQrcode>(_GetSessionTokenByQrcode_QNAME, GetSessionTokenByQrcode.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnterpriseUpdateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "enterpriseUpdateResponse")
    public JAXBElement<EnterpriseUpdateResponse> createEnterpriseUpdateResponse(EnterpriseUpdateResponse value) {
        return new JAXBElement<EnterpriseUpdateResponse>(_EnterpriseUpdateResponse_QNAME, EnterpriseUpdateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetccLog }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getccLog")
    public JAXBElement<GetccLog> createGetccLog(GetccLog value) {
        return new JAXBElement<GetccLog>(_GetccLog_QNAME, GetccLog.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAllSystemInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getAllSystemInfoResponse")
    public JAXBElement<GetAllSystemInfoResponse> createGetAllSystemInfoResponse(GetAllSystemInfoResponse value) {
        return new JAXBElement<GetAllSystemInfoResponse>(_GetAllSystemInfoResponse_QNAME, GetAllSystemInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ECompanyRegistResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "eCompanyRegistResponse")
    public JAXBElement<ECompanyRegistResponse> createECompanyRegistResponse(ECompanyRegistResponse value) {
        return new JAXBElement<ECompanyRegistResponse>(_ECompanyRegistResponse_QNAME, ECompanyRegistResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSystemSSOURLBySystemnameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getSystemSSOURLBySystemnameResponse")
    public JAXBElement<GetSystemSSOURLBySystemnameResponse> createGetSystemSSOURLBySystemnameResponse(GetSystemSSOURLBySystemnameResponse value) {
        return new JAXBElement<GetSystemSSOURLBySystemnameResponse>(_GetSystemSSOURLBySystemnameResponse_QNAME, GetSystemSSOURLBySystemnameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Elogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "elogin")
    public JAXBElement<Elogin> createElogin(Elogin value) {
        return new JAXBElement<Elogin>(_Elogin_QNAME, Elogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLZBId }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getLZBId")
    public JAXBElement<GetLZBId> createGetLZBId(GetLZBId value) {
        return new JAXBElement<GetLZBId>(_GetLZBId_QNAME, GetLZBId.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EUserUpdate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "eUserUpdate")
    public JAXBElement<EUserUpdate> createEUserUpdate(EUserUpdate value) {
        return new JAXBElement<EUserUpdate>(_EUserUpdate_QNAME, EUserUpdate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnterpriseUpdate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "enterpriseUpdate")
    public JAXBElement<EnterpriseUpdate> createEnterpriseUpdate(EnterpriseUpdate value) {
        return new JAXBElement<EnterpriseUpdate>(_EnterpriseUpdate_QNAME, EnterpriseUpdate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Openepay }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "openepay")
    public JAXBElement<Openepay> createOpenepay(Openepay value) {
        return new JAXBElement<Openepay>(_Openepay_QNAME, Openepay.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetReservedMobiles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getReservedMobiles")
    public JAXBElement<GetReservedMobiles> createGetReservedMobiles(GetReservedMobiles value) {
        return new JAXBElement<GetReservedMobiles>(_GetReservedMobiles_QNAME, GetReservedMobiles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendEmailResetPassword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "sendEmailResetPassword")
    public JAXBElement<SendEmailResetPassword> createSendEmailResetPassword(SendEmailResetPassword value) {
        return new JAXBElement<SendEmailResetPassword>(_SendEmailResetPassword_QNAME, SendEmailResetPassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetChangePhoneRequestResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getChangePhoneRequestResponse")
    public JAXBElement<GetChangePhoneRequestResponse> createGetChangePhoneRequestResponse(GetChangePhoneRequestResponse value) {
        return new JAXBElement<GetChangePhoneRequestResponse>(_GetChangePhoneRequestResponse_QNAME, GetChangePhoneRequestResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOrSetSamlResponseDataResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getOrSetSamlResponseDataResponse")
    public JAXBElement<GetOrSetSamlResponseDataResponse> createGetOrSetSamlResponseDataResponse(GetOrSetSamlResponseDataResponse value) {
        return new JAXBElement<GetOrSetSamlResponseDataResponse>(_GetOrSetSamlResponseDataResponse_QNAME, GetOrSetSamlResponseDataResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReflashSession }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "reflashSession")
    public JAXBElement<ReflashSession> createReflashSession(ReflashSession value) {
        return new JAXBElement<ReflashSession>(_ReflashSession_QNAME, ReflashSession.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MaintainCustFee }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "maintainCustFee")
    public JAXBElement<MaintainCustFee> createMaintainCustFee(MaintainCustFee value) {
        return new JAXBElement<MaintainCustFee>(_MaintainCustFee_QNAME, MaintainCustFee.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EvalidateSMSOTPResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "evalidateSMSOTPResponse")
    public JAXBElement<EvalidateSMSOTPResponse> createEvalidateSMSOTPResponse(EvalidateSMSOTPResponse value) {
        return new JAXBElement<EvalidateSMSOTPResponse>(_EvalidateSMSOTPResponse_QNAME, EvalidateSMSOTPResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MberRealNameAuth }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "mberRealNameAuth")
    public JAXBElement<MberRealNameAuth> createMberRealNameAuth(MberRealNameAuth value) {
        return new JAXBElement<MberRealNameAuth>(_MberRealNameAuth_QNAME, MberRealNameAuth.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QueryUnbindRequestByYHTUUIDResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "queryUnbindRequestByYHTUUIDResponse")
    public JAXBElement<QueryUnbindRequestByYHTUUIDResponse> createQueryUnbindRequestByYHTUUIDResponse(QueryUnbindRequestByYHTUUIDResponse value) {
        return new JAXBElement<QueryUnbindRequestByYHTUUIDResponse>(_QueryUnbindRequestByYHTUUIDResponse_QNAME, QueryUnbindRequestByYHTUUIDResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MaintainCustFeeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "maintainCustFeeResponse")
    public JAXBElement<MaintainCustFeeResponse> createMaintainCustFeeResponse(MaintainCustFeeResponse value) {
        return new JAXBElement<MaintainCustFeeResponse>(_MaintainCustFeeResponse_QNAME, MaintainCustFeeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResetLZBIdPWResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "resetLZBIdPWResponse")
    public JAXBElement<ResetLZBIdPWResponse> createResetLZBIdPWResponse(ResetLZBIdPWResponse value) {
        return new JAXBElement<ResetLZBIdPWResponse>(_ResetLZBIdPWResponse_QNAME, ResetLZBIdPWResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateQRCode }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "generateQRCode")
    public JAXBElement<GenerateQRCode> createGenerateQRCode(GenerateQRCode value) {
        return new JAXBElement<GenerateQRCode>(_GenerateQRCode_QNAME, GenerateQRCode.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLZBIdAndBindInfoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getLZBIdAndBindInfoResponse")
    public JAXBElement<GetLZBIdAndBindInfoResponse> createGetLZBIdAndBindInfoResponse(GetLZBIdAndBindInfoResponse value) {
        return new JAXBElement<GetLZBIdAndBindInfoResponse>(_GetLZBIdAndBindInfoResponse_QNAME, GetLZBIdAndBindInfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenepayResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "openepayResponse")
    public JAXBElement<OpenepayResponse> createOpenepayResponse(OpenepayResponse value) {
        return new JAXBElement<OpenepayResponse>(_OpenepayResponse_QNAME, OpenepayResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateMethod }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "validateMethod")
    public JAXBElement<ValidateMethod> createValidateMethod(ValidateMethod value) {
        return new JAXBElement<ValidateMethod>(_ValidateMethod_QNAME, ValidateMethod.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSystemSSOURLBySystemname }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getSystemSSOURLBySystemname")
    public JAXBElement<GetSystemSSOURLBySystemname> createGetSystemSSOURLBySystemname(GetSystemSSOURLBySystemname value) {
        return new JAXBElement<GetSystemSSOURLBySystemname>(_GetSystemSSOURLBySystemname_QNAME, GetSystemSSOURLBySystemname.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetURLDateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getURLDateResponse")
    public JAXBElement<GetURLDateResponse> createGetURLDateResponse(GetURLDateResponse value) {
        return new JAXBElement<GetURLDateResponse>(_GetURLDateResponse_QNAME, GetURLDateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Freeze }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "freeze")
    public JAXBElement<Freeze> createFreeze(Freeze value) {
        return new JAXBElement<Freeze>(_Freeze_QNAME, Freeze.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetURLDate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getURLDate")
    public JAXBElement<GetURLDate> createGetURLDate(GetURLDate value) {
        return new JAXBElement<GetURLDate>(_GetURLDate_QNAME, GetURLDate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Invoke }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "invoke")
    public JAXBElement<Invoke> createInvoke(Invoke value) {
        return new JAXBElement<Invoke>(_Invoke_QNAME, Invoke.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateEmailByTokenResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "updateEmailByTokenResponse")
    public JAXBElement<UpdateEmailByTokenResponse> createUpdateEmailByTokenResponse(UpdateEmailByTokenResponse value) {
        return new JAXBElement<UpdateEmailByTokenResponse>(_UpdateEmailByTokenResponse_QNAME, UpdateEmailByTokenResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EchangeLZBIdPWResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "echangeLZBIdPWResponse")
    public JAXBElement<EchangeLZBIdPWResponse> createEchangeLZBIdPWResponse(EchangeLZBIdPWResponse value) {
        return new JAXBElement<EchangeLZBIdPWResponse>(_EchangeLZBIdPWResponse_QNAME, EchangeLZBIdPWResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterLZBIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "registerLZBIdResponse")
    public JAXBElement<RegisterLZBIdResponse> createRegisterLZBIdResponse(RegisterLZBIdResponse value) {
        return new JAXBElement<RegisterLZBIdResponse>(_RegisterLZBIdResponse_QNAME, RegisterLZBIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidateCardInfo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "validateCardInfo")
    public JAXBElement<ValidateCardInfo> createValidateCardInfo(ValidateCardInfo value) {
        return new JAXBElement<ValidateCardInfo>(_ValidateCardInfo_QNAME, ValidateCardInfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAnyDataPublicMethodResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getAnyDataPublicMethodResponse")
    public JAXBElement<GetAnyDataPublicMethodResponse> createGetAnyDataPublicMethodResponse(GetAnyDataPublicMethodResponse value) {
        return new JAXBElement<GetAnyDataPublicMethodResponse>(_GetAnyDataPublicMethodResponse_QNAME, GetAnyDataPublicMethodResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccessUserProfile }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "accessUserProfile")
    public JAXBElement<AccessUserProfile> createAccessUserProfile(AccessUserProfile value) {
        return new JAXBElement<AccessUserProfile>(_AccessUserProfile_QNAME, AccessUserProfile.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpenCustCleaResResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "openCustCleaResResponse")
    public JAXBElement<OpenCustCleaResResponse> createOpenCustCleaResResponse(OpenCustCleaResResponse value) {
        return new JAXBElement<OpenCustCleaResResponse>(_OpenCustCleaResResponse_QNAME, OpenCustCleaResResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSessionTokenByQrcodeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "getSessionTokenByQrcodeResponse")
    public JAXBElement<GetSessionTokenByQrcodeResponse> createGetSessionTokenByQrcodeResponse(GetSessionTokenByQrcodeResponse value) {
        return new JAXBElement<GetSessionTokenByQrcodeResponse>(_GetSessionTokenByQrcodeResponse_QNAME, GetSessionTokenByQrcodeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MobileLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "mobileLogin")
    public JAXBElement<MobileLogin> createMobileLogin(MobileLogin value) {
        return new JAXBElement<MobileLogin>(_MobileLogin_QNAME, MobileLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ChangeLZBIdPWResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "changeLZBIdPWResponse")
    public JAXBElement<ChangeLZBIdPWResponse> createChangeLZBIdPWResponse(ChangeLZBIdPWResponse value) {
        return new JAXBElement<ChangeLZBIdPWResponse>(_ChangeLZBIdPWResponse_QNAME, ChangeLZBIdPWResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservices.ccservice.isprint.com/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

}
