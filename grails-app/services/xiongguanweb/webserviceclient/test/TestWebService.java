package xiongguanweb.webserviceclient.test;

import com.alibaba.fastjson.JSONObject;

import xiongguanweb.webserviceclient.CCWebSrv;
import xiongguanweb.webserviceclient.CcServiceInObj;
import xiongguanweb.webserviceclient.CcServiceOutObj;
import xiongguanweb.webserviceclient.ICCWebSrv;

public class TestWebService {

	public static void main(String[] args) {
		CcServiceInObj ccServiceInObj = new CcServiceInObj();
		JSONObject js = new JSONObject();
		js.put("userId", "13939392222");
		js.put("password", "123qwe");
		js.put("validateCode", "123456");
		js.put("IP", "123456");
		ccServiceInObj.setJsonObj(js.toJSONString());
		ccServiceInObj.setSystemName("xgbmk");
		ICCWebSrv serviceImp = null;
		CcServiceOutObj ccServiceOutObj=null;
		CCWebSrv service = new CCWebSrv();
		serviceImp = service.getCCWebSrvPort();
		ccServiceOutObj= serviceImp.login(ccServiceInObj);	
		System.out.println("errorCode:"+ccServiceOutObj.getErrorCode());
		System.out.println("jsonObj:"+ccServiceOutObj.getJsonObj());
		//============= Return message end =============
		System.out.println("========================= testLogin end =========================");

	}

}
