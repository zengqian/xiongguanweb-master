<!doctype html>
<html lang="en" class="no-js">
<head>
    <link rel="shortcut icon" type="text/css" href="../assets/favicon.ico"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    %{--<meta name="viewport" content="width=device-width, initial-scale=1"/>--}%
    <asset:stylesheet src="comm.css"/>
    <asset:stylesheet src="foot.css"/>
    <asset:javascript src="jquery-1.11.1.min.js"/>
    <asset:javascript src="jquery.validate.min.js"/>
    <asset:javascript src="additional-methods.js"/>
    <asset:javascript src="layer/layer.js"/>
    <title>颐瑞互联网综合缴费平台</title>
    <g:layoutHead/>
</head>

<body>
<!--头部-->
<div class="html_header">
    <a href="/" style="color: #333;font-size: 16px; line-height: 84px;height: 40px;display: inline-block;">
        <g:img dir="images" src="favicon.ico" class="header_img"/>
        <label style="margin-left: 15px;">颐瑞互联网综合缴费平台</label>
    </a>
    <ul class="index_ul">
        %{--<li><a href="/about_convenience_card">关于便民卡</a></li>--}%
        %{--<li><a href="/service_guide">服务指南</a></li>--}%
        %{--<li><a href="/convenience_card_application">便民卡应用</a></li>--}%
        %{--<li><a href="/gzh">公众号</a></li>--}%
        %{--<li><a href="/dow">应用下载</a></li>--}%
        %{--<li><a href="/businesslogin">商户中心</a></li>--}%
        <li id="login_false"><a href="/login">个人中心</a></li>
        <li id="login_true" style="display: none;">
            <a class="div_float_left" href="/user/userInfo" style="margin-right: 20px;">个人中心</a>
            <a href="/user/userInfo" class="banner_logo div_float_left">
            <img class="absolute_position" style=""/>
        </a>
        <a class="div_float_left" style="margin-left: 5px;" onclick="loginout()">退出</a>
        </li>
    </ul>
</div>
<!--中心内容-->
<g:layoutBody/>
<!--footer-->
<div class="footer">
    <div class="foot_body">
        <div>
            %{--<g:img dir="images" src="/foot/wechat.png"/>--}%
            <g:img dir="images" src="gzh.png" style="width: 90px;height: 90px;"/>
            <div class="foot_txt">微信公众号</div>
        </div>

        <div>
            <g:img dir="images" src="dowl_scan.png" style="width: 90px;height: 90px;"/>
            <div class="foot_txt">APP应用下载</div>
        </div>

        %{--<p>--}%
            %{--嘉峪关，号称“天下第一雄关”，位于甘肃省嘉峪关市西5千米处最狭窄的山谷中部，城关两侧的城墙横穿沙漠戈壁，北连黑山悬壁长城，南接天下第一墩，是明长城最西端的关口，历史上曾被称为河西咽喉，因地势险要，建筑雄伟，有连陲锁钥之称。嘉峪关是古代“丝绸之路”的交通要塞，中国长城三大奇观之一（东有山海关、中有镇北台、西有嘉峪关）。--}%
        %{--</p>--}%
    </div>

    %{--<div class="foot_body">&copy;2012-2018嘉峪关xxxxx公司甘ICP备18043664号-18</div>--}%
</div>
<asset:javascript src="index.js"/>
<asset:javascript src="islogin.js"/>
<asset:javascript src="readcard.js"/>
</body>
</html>
