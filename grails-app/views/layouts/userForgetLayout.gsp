<!doctype html>
<html lang="en" class="no-js">
<head>
    <link rel="shortcut icon" type="text/css" href="../assets/favicon.ico"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    %{--<meta name="viewport" content="width=device-width, initial-scale=1"/>--}%
    <asset:stylesheet src="comm.css"/>
    <asset:stylesheet src="foot.css"/>
    <asset:stylesheet src="objectStyle.css"/>
    <asset:javascript src="jquery-1.11.1.min.js"/>
    <asset:javascript src="jquery.validate.min.js"/>
    <asset:javascript src="additional-methods.js"/>
    <title>颐瑞互联网综合缴费平台</title>
    <g:layoutHead/>
</head>
<body>
<!--头部-->
<div class="html_header">
    <a href="/" style="color: #333;font-size: 16px; line-height: 84px;height: 40px;display: inline-block;">
        <g:img dir="images" src="favicon.ico" class="header_img"/>
        <label style="margin-left: 15px;">颐瑞互联网综合缴费平台</label>
    </a>
    <label class="gray_color html_header_label">已有云上雄关账号？<a class="main_color" href="/login">去登录</a></label>
</div>
<!--中心内容-->
<g:layoutBody/>
<!--footer-->
<div class="footer">
    <div class="foot_body">
        <div>
            %{--<g:img dir="images" src="/foot/wechat.png"/>--}%
            <g:img dir="images" src="gzh.png" style="width: 90px;height: 90px;"/>
            <div class="foot_txt">微信公众号</div>
        </div>
        <div>
            <g:img dir="images" src="dowl_scan.png" style="width: 90px;height: 90px;"/>
            <div class="foot_txt">APP应用下载</div>
        </div>
        <p>
            嘉峪关，号称“天下第一雄关”，位于甘肃省嘉峪关市西5千米处最狭窄的山谷中部，城关两侧的城墙横穿沙漠戈壁，北连黑山悬壁长城，南接天下第一墩，是明长城最西端的关口，历史上曾被称为河西咽喉，因地势险要，建筑雄伟，有连陲锁钥之称。嘉峪关是古代“丝绸之路”的交通要塞，中国长城三大奇观之一（东有山海关、中有镇北台、西有嘉峪关）。
        </p>
    </div>
    %{--<div class="foot_body">&copy;2012-2018嘉峪关xxxxx公司甘ICP备18043664号-18</div>--}%
</div>
<asset:javascript src="index.js"/>
<asset:javascript src="layer/layer.js"/>
</body>
</html>
