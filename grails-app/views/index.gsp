<!DOCTYPE html>
<html>
<head>
    <link rel="shortcut icon" type="text/css" href="../assets/logo.png"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="renderer" content="ie-comp|webkit|ie-stand">
    <asset:stylesheet src="index.css"/>
    <asset:stylesheet src="comm.css"/>
    <asset:stylesheet src="foot.css"/>
    <title>颐瑞互联网综合缴费平台</title>
</head>

<body>
%{--<OBJECT	id=WebCtl	align="CENTER" WIDTH=0 HEIGHT=0	codeBase=/assets/JygWebActiveX.CAB#version=1,0,0,2 classid=CLSID:0156D719-1F9D-4ED9-99B2-0BA4BD02AB05></OBJECT>--}%

<!-- 顶部导航 -->

<!-- banner -->
<div class="banner">
    <div>
            %{--<a id="a1" class="a1 active"></a>--}%
            %{--<a id="a2" class="a2"></a>--}%
            %{--<a id="a3" class="a3"></a>--}%
            <a id="a1" class="banner_a a1 active"></a>
            <a id="a2" class="banner_a a2"></a>
            <a id="a3" class="banner_a a3"></a>
    </div>

    <div class="index_header">
        <g:img dir="images" src="/index/white_logo.png" class="logo"/>
        <ul class="index_ul">
            %{--<li><a href="/about_convenience_card">关于便民卡</a></li>--}%
            %{--<li><a href="/service_guide">服务指南</a></li>--}%
            %{--<li><a href="/convenience_card_application">便民卡应用</a></li>--}%
            %{--<li><a href="/gzh">公众号</a></li>--}%
            %{--<li><a href="/dow">应用下载</a></li>--}%
            %{--<li><a href="/businesslogin">商户中心</a></li>--}%
            <li id="login_false"><a href="/login">个人中心</a></li>
            <li id="login_true" style="display: none;">
                <a class="div_float_left" href="/user/userInfo" style="margin-right: 20px;">个人中心</a>
                <a href="/user/userInfo" class="banner_logo div_float_left">
                    <img class="absolute_position" style=""/>
                </a>
                <a class="div_float_left" style="margin-left: 5px;" onclick="loginout()">退出</a>
            </li>
        </ul>
    </div>
    <a style="left: 80px;" class="left"></a>
    <a style="right: 80px;" class="right"></a>
</div>

<div class="index_body">
    <!-- 公告 -->
    <div class="gray_color small_fontsize notice_div body">
        <g:img dir="images" src="/index/2.png"/>
        <marquee onMouseOver="this.stop()" onMouseOut="this.start()" style="width: 800px;">
            <a>8月9号，甘肃日报社党委数据、社长
            甘肃日报报业集团董事长王光庆来嘉，深入我市丝绸之路文化博览园、五大项目现场了解我市项目建设进展情况。</a>
            <a>新闻2</a>
            <a>新闻3</a>
        </marquee>
    </div>
    <!--生活缴费-->
    <div class="title_div title_large_fontsize">生活缴费</div>

    <div class="life_money body align_center">
        <a class="water_div not_a" onclick="isReal('/user/getWater')">
            <div class="img_div">
                <g:img dir="images" src="/index/water.png"/>
            </div>

            <div class="hover_div">
                <g:img dir="images" src="/index/6.png" class="jiantou"/>
            </div>

            <div>
                <div class="large_fontsize">水费</div>

                <div class="gray_color">水费查询与缴纳</div>
            </div>
        </a>
        %{--<a class="phone_div not_a" href="/electric_index">--}%
        <a class="phone_div not_a" onclick="toast()">
            <div class="img_div width_small">
                <g:img dir="images" src="/index/3.png"/>
            </div>

            <div class="hover_div">
                <g:img dir="images" src="/index/6.png" class="jiantou"/>
            </div>

            <div>
                <div class="large_fontsize">电费</div>

                <div class="gray_color">电费查询与缴纳</div>
            </div>
        </a>
        <a class="gas_div not_a" onclick="isReal('/gas_first')">
            <div class="img_div width_small">
                <g:img dir="images" src="/index/gas.png"/>
            </div>

            <div class="hover_div">
                <g:img dir="images" src="/index/6.png" class="jiantou"/>
            </div>

            <div>
                <div class="large_fontsize">燃气费</div>

                <div class="gray_color">燃气费查询及缴纳</div>
            </div>
        </a>
    </div>

    <div class="life_money margin_top_middle body align_center">
        <a class="phone_div not_a" onclick="isReal('/recharge_phone')">
            <div class="img_div height_small">
                <g:img dir="images" src="/index/phone.png"/>
            </div>

            <div class="hover_div">
                <g:img dir="images" src="/index/6.png" class="jiantou"/>
            </div>

            <div>
                <div class="large_fontsize">手机充值</div>

                <div class="gray_color">本省话费充值</div>
            </div>

        </a>
        <a class="car_div not_a" onclick="isReal('/fine_query')">
            <div class="img_div height_small">
                <g:img dir="images" src="/index/car.png"/>
            </div>

            <div class="hover_div">
                <g:img dir="images" src="/index/6.png" class="jiantou"/>
            </div>

            <div>
                <div class="large_fontsize">交通违章</div>

                <div class="gray_color">违章缴费</div>
            </div>
        </a>
        %{--<a class="water_div not_a" href="/warm_index">--}%
        <a class="water_div not_a" onclick="toast()">
            <div class="img_div height_small">
                <g:img dir="images" src="/index/4.png"/>
            </div>

            <div class="hover_div">
                <g:img dir="images" src="/index/6.png" class="jiantou"/>
            </div>

            <div>
                <div class="large_fontsize">供暖费</div>

                <div class="gray_color">供暖费查询与缴费</div>
            </div>
        </a>
    </div>


    <!--交通出行-->
    %{--<div class="white_color transport_div margin_top_large ">--}%
        %{--<div class="body small_fontsize align_center animate_active">--}%
            %{--<div class="title_large_fontsize align_left">交通出行</div>--}%

            %{--<div class="margin_top_large align_left">雄关便民卡工程实现了公共出行的"一卡通"和"一机通"服务内容涵盖了公交乘车，公共自行车，出租车和机动车停车领域。</div>--}%

            %{--<div class="margin_top_middle align_left">"一卡通"是指市民可持雄关便民卡刷卡乘坐所有线路的公交卡、刷卡租还公共自行车、刷卡缴纳停车费。</div>--}%

            %{--<div class="margin_top_middle align_left">--}%
                %{--"一机通"是指市民卡可持手机凭雄关APP扫码乘坐所有线路公交车、扫码租还公共自行车、扫码乘坐出租车以及扫码支付停车费。同时可通过APP查询到公共出行领域的便民信息。--}%
            %{--</div>--}%
            %{--<a class="btn">--}%
                %{--<div>开通公共自行车应用</div>--}%
            %{--</a>--}%
        %{--</div>--}%
    %{--</div>--}%
    <!--公共服务-->
    <div class="public_service margin_top_large ">
        <div class="title_div title_large_fontsize ">公共服务</div>

        <div class="animate_active1" style="max-width: 1350px;margin: 0 auto;width: 100%">
            %{--<a><g:img dir="images" src="/index/8.png" class="left_prev"/></a>--}%
            %{--<a><g:img dir="images" src="/index/9.png" class="right_next"/></a>--}%
            <div class="body white_color large_fontsize" id="pic_div">
                <ul class="pic_ul">
                    %{--<li>--}%
                    %{--<a>--}%
                    %{--<g:img dir="images" src="/index/7.png"/>--}%
                    %{--<div>居家养老</div>--}%
                    %{--</a>--}%
                    %{--</li>--}%
                    <li class="active">
                        <a onclick="isReal('/user/queryFund')">
                            <g:img dir="images" src="/index/7-2.png"/>
                            <div>公积金查询</div>
                        </a>
                    </li>
                    <li>
                        <a onclick="toast()">
                            <g:img dir="images" src="/index/7-1.png"/>
                            <div>社保查询</div>
                        </a>
                    </li>
                    <li>
                        <a onclick="isReal('/user/queryCredit')">
                            <g:img dir="images" src="/index/7-3.png"/>
                            <div>雄关信用</div>
                        </a>
                    </li>
                    <li>
                        <a onclick="isReal('/user/getLibrary')">
                            <g:img dir="images" src="/index/7-4.png"/>
                            <div>图书馆</div>
                        </a>
                    </li>
                    %{--<li>--}%
                    %{--<a>--}%
                    %{--<g:img dir="images" src="/index/7-5.png"/>--}%
                    %{--<div>户籍查询</div>--}%
                    %{--</a>--}%
                    %{--</li>--}%
                    %{--<li>--}%
                    %{--<a>--}%
                    %{--<g:img dir="images" src="/index/7-6.png"/>--}%
                    %{--<div>房产登记查询</div>--}%
                    %{--</a>--}%
                    %{--</li>--}%
                    <li>
                        <a class="last" onclick="toast()">
                            <g:img dir="images" src="/index/7-7.png"/>
                            <div>机动车查询</div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!--休闲旅游-->
    <div class="title_div title_large_fontsize ">休闲旅游</div>

    <div class="body travle_div animate_active2">
        <a class="travle_a div_float_left" onclick="toast()">
            <div>
                <g:img dir="images" src="/index/travle.png"/>
            </div>

            <div>
                <p class="large_fontsize margin_top_middlel old_color">旅游年卡</p>

                <p class="margin_top_middle gray_color">办理旅游年卡，优惠多多，本地多处热门景点凭此卡可享受品质服务。</p>
            </div>
        </a>
        <a class="spot_a" onclick="toast()">
            <div>
                <g:img dir="images" src="/index/spot.png"/>
            </div>

            <div>
                <p class="large_fontsize margin_top_middlel old_color">景区介绍</p>

                <p class="margin_top_middle gray_color">万里雄关，长城西端起点，古代丝绸之路的交通要冲，带您领路西北风情。</p>
            </div>
        </a>

        <a class="food_a div_float_right" onclick="toast()">
            <div>
                <g:img dir="images" src="/index/food.png"/>
            </div>

            <div>
                <p class="large_fontsize old_color margin_top_middlel">特色美食</p>

                <p class="margin_top_middle gray_color">浓浓的西北风情，当地的特色美食，让人流连忘返。</p>
            </div>
        </a>
        %{--<a class="face_a last">--}%
        %{--<div>--}%
        %{--<g:img dir="images" src="/index/face.png"/>--}%
        %{--</div>--}%
        %{--<div>--}%
        %{--<p class="large_fontsize old_color margin_top_middlel">人脸采集</p>--}%
        %{--<p class="margin_top_middle gray_color">人脸识别为您提供最安全的保障服务，出入景区方便快捷。</p>--}%
        %{--</div>--}%
        %{--</a>--}%
    </div>
    <!--合作商家-->
    %{--<div class="margin_top_large buiness_div white_color ">--}%
        %{--<div class="title_div title_large_fontsize ">合作商家</div>--}%

        %{--<div class="body">--}%
            %{--<ul class="large_fontsize">--}%
                %{--<li class="active first">--}%
                    %{--<a>商场超市</a>--}%
                %{--</li>--}%
                %{--<li>--}%
                    %{--<a>加油加气</a>--}%
                %{--</li>--}%
                %{--<li>--}%
                    %{--<a>酒店住宿</a>--}%
                %{--</li>--}%
                %{--<li>--}%
                    %{--<a>餐厅饭店</a>--}%
                %{--</li>--}%
                %{--<li class="last">--}%
                    %{--<a>休闲娱乐</a>--}%
                %{--</li>--}%

                %{--<div class="ul_div"></div>--}%
            %{--</ul>--}%

            %{--<div class="buiness_banner_div">--}%
                %{--<div>--}%
                    %{--<div class="buiness_banner_img first">--}%
                        %{--<g:img dir="images" src="/index/11.png"/>--}%
                        %{--<g:img dir="images" src="/index/12.png"/>--}%
                        %{--<g:img dir="images" src="/index/13.png"/>--}%
                    %{--</div>--}%

                    %{--<div class="buiness_banner_img">--}%
                        %{--<g:img dir="images" src="/index/11.png"/>--}%
                    %{--</div>--}%

                    %{--<div class="buiness_banner_img">--}%
                        %{--<g:img dir="images" src="/index/12.png"/>--}%
                    %{--</div>--}%

                    %{--<div class="buiness_banner_img">--}%
                        %{--<g:img dir="images" src="/index/13.png"/>--}%
                    %{--</div>--}%

                    %{--<div class="buiness_banner_img">--}%
                        %{--<g:img dir="images" src="/index/11.png"/>--}%
                        %{--<g:img dir="images" src="/index/13.png"/>--}%
                    %{--</div>--}%
                %{--</div>--}%
            %{--</div>--}%
        %{--</div>--}%
    %{--</div>--}%
    <!--金融服务-->
    <div class="finance_div margin_top_large white_color ">
        <div class="title_div title_large_fontsize">金融服务</div>

        <div class="body animate_active4">
            <div>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida
                dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis
                natoque
                penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra
                vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget.
            </div>
            <a class="btn btn_div" onclick="isReal('/reservation_conveninenceCard')">
                <div>了解更多</div>
            </a>
        </div>

    </div>

    <div class="title_div title_large_fontsize">关于云上雄关APP</div>
    <div class="buiness_div white_color">
        <div class="body animate_active3" style="padding-top: 60px;">
            <ul class="large_fontsize">
                <li class="active first">
                    <a>APP介绍</a>
                </li>
                <li>
                    <a>如何下载</a>
                </li>
                <li>
                    <a>如何注册</a>
                </li>
                <li>
                    <a>如何登录</a>
                </li>
                <li class="last">
                    <a>实名认证</a>
                </li>

                <div class="ul_div"></div>
            </ul>

            <div class="buiness_banner_div align_center">
                <div>
                    <div class="buiness_banner_img first about_app">

                        “云上雄关”APP包含了生活缴费、金融服务、商旅服务、公共服务支付扫码功能，旨在方便用户日常的生活需求。
                    </div>

                    <div class="buiness_banner_img about_app">
                        <p style="line-height: 25px">
                            1、安卓版本手机：(1)点击下方下载按钮进入"云上雄关APP"下载界面，点击"Android版下载"；(2)通过"安智市场"下载，打开安智市场，搜索"云上雄关"，点击下载及安装

                        </p>
                        <p class="margin_top_large" style="line-height: 25px">
                            1、苹果手机：(1)点击下方下载按钮进入"云上雄关APP"下载界面，点击"IPhone版下载"；(2)通过"苹果商店"下载，打开iPhone手机中的"App Store"，搜索"云上雄关"，点击下载及安装

                        </p>

                        <a class="btn_div dow_btn" href="/dow">
                            下载
                        </a>
                    </div>

                    <div class="buiness_banner_img about_app">
                        <p>没有注册兰州银行一户通的个人，需进行注册方可使用，具体操作步骤如下：</p>
                        <p>进入“云上雄关”主界面后，选择“立即注册”，输入手机号码、收到的短信验证码、登录密码，确认密码，点击“注册”——提示注册成功。</p>

                    </div>

                    <div class="buiness_banner_img about_app">
                        <p>已在兰州银行注册“一户通”的用户，直接用注册的手机号码和一户通密码登录即可。没有注册兰州银行一户通的客户，直接使app或网站注册的用户名和密码进行登录即可。</p>
                    </div>

                    <div class="buiness_banner_img about_app">
                        <p>实名认证是享受服务的前提。实名认证的条件：办理并激活了雄关便民卡（雄关便民卡属于嘉峪关政府主导发行的，政府会主导按照计划面向市民发行，市民也可到兰州银行嘉峪关分行柜面自愿申请雄关便民卡），而且预留了能收到短信的手机号，方可进行实名认证。</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--footer-->
<div class="footer">
    <div class="foot_body">
        <div>
            %{--<g:img dir="images" src="/foot/wechat.png"/>--}%
            <g:img dir="images" src="gzh.png" style="width: 90px;height: 90px;"/>
            <div class="foot_txt">微信公众号</div>
        </div>

        <div>
            <g:img dir="images" src="dowl_scan.png" style="width: 90px;height: 90px;"/>
            <div class="foot_txt">APP应用下载</div>
        </div>

        <p>
            嘉峪关，号称“天下第一雄关”，位于甘肃省嘉峪关市西5千米处最狭窄的山谷中部，城关两侧的城墙横穿沙漠戈壁，北连黑山悬壁长城，南接天下第一墩，是明长城最西端的关口，历史上曾被称为河西咽喉，因地势险要，建筑雄伟，有连陲锁钥之称。嘉峪关是古代“丝绸之路”的交通要塞，中国长城三大奇观之一（东有山海关、中有镇北台、西有嘉峪关）。
        </p>
    </div>

    %{--<div class="foot_body">&copy;2012-2018嘉峪关xxxxx公司甘ICP备18043664号-18</div>--}%
</div>
<asset:javascript src="jquery-1.11.1.min.js"/>
<asset:javascript src="layer/layer.js"/>
<asset:javascript src="index.js"/>
<asset:javascript src="islogin.js"/>
<script>
    function banner() {
            if($("#a1").hasClass("active")){
                $("#a1").removeClass("active")
                $("#a2").addClass("active")
                return;
            }

            if($("#a2").hasClass("active")){
                $("#a2").toggleClass("active")
                $("#a3").toggleClass("active")
                return;
            }
            if($("#a3").hasClass("active")){
                $("#a3").toggleClass("active")
                $("#a1").toggleClass("active")
                return;
            }
    }
    setInterval("banner()",7000)
</script>
</body>
</html>