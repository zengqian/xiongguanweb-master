<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>
<body>
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul ul_active">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first" class="active">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            <a o/>
                        </ul>
                    </li>
                    %{--<li>--}%
                        %{--<div class="li"><span>金融服务</span></div>--}%
                        %{--<ul class="in_ul">--}%
                            %{--<a onclick="isReal('/user/queryCardSiteList')">--}%
                                %{--<li>--}%
                                    %{--便民卡预约--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        %{--</ul>--}%
                    %{--</li>--}%
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul">
                            %{--<a onclick="isReal('/user/queryCredit')" >--}%
                                %{--<li>--}%
                                    %{--雄关信用--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/user/queryScore')" >--}%
                                %{--<li>--}%
                                    %{--百合积分--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="toast()">
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="toast()">
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- 水费缴纳content -->
    <div class="life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">品质保证</label>
                <label class="main_color">安全快捷</label>
                <a class="small_fontsize gray_color mar_left" href="/user/getGas">燃气费缴纳</a>
                <g:img dir="images" src="/index/20.png"/>
                <label class="small_fontsize">燃气户号查询</label>
            </label>
        </div>
        <div>
            <form class="add_water water_query" id="gasQueryForm" action="/user/forgetGasQuery" method="post">
                <div class="margin_top_large">
                    <label class="large_fontsize input_title">缴费单位:</label>
                    <select class="form_input btn_div" id="selbranch">
                        <g:each in="${data}">
                            <option value="${it.merchantId}" bankAccNo="${it.bankAccNo}">${it.merchantName}</option>
                        </g:each>
                    </select>
                </div>
                <div class="margin_top_large">
                    <label class="large_fontsize input_title">户主姓名:</label>
                    <input placeholder="请输入户主姓名" name="name" class="form_input btn_div" maxlength="4"/>
                </div>
                %{--<div class="margin_top_large">--}%
                    %{--<label class="large_fontsize input_title">身份证号码:</label>--}%
                    %{--<input placeholder="请输入户主的身份证号码" name="certNo" class="form_input btn_div" maxlength="18"/>--}%
                %{--</div>--}%
                <input type="submit" value="查询" class="form_submit_water margin_top_largemore"/>
                <input type="hidden" id="branchgname" name="branchgname" />
                <input type="hidden" id="branchguid" name="branchguid" />
            </form>
        </div>
    </div>
</div>
<script>
    $("#gasQueryForm").validate({
        rules: {
            name: {
                required: true
            }
        },
        messages: {
            name: {
                required: "请输入户主姓名"
            }
        },
        submitHandler:function(form){
            var branchgname = $("#selbranch").find("option:selected").text();
            $("#branchgname").val(branchgname);
            $("#branchguid").val($("#selbranch").find("option:selected").attr("bankAccNo"));
            // form.action="/user/forgetWaterQuery";
            form.submit();
        }
    });
</script>
</body>
</html>