<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="convenienceCard"/>
</head>

<body>
<!--中心内容-->
<div class="about_card">
    <div>
        <label class="title_middle_fontsize">服务指南</label>

        <div class="title_to">
            <label>当前位置：</label>
            <a href="/service_guide">资讯中心</a>
            <label>></label>
            <a href="/service_guide" class="old_color">服务指南</a>
        </div>

        <div class="img_div"><g:img dir="images" src="/index/16.png"/></div>
    </div>

    <div class="margin_top_large align_center news_detail">
        <label class="title_large_fontsize convenience_title" id="qwer">办卡及服务指南</label><br>

        <p class="large_fontsize lt_gray_color margin_form_middle align_left">
            雄关便民卡是为满足市民个性化服务需要而开发的一种多功能的电子金融工具，同时具备磁条和芯片的银联标准卡，它集定活期储蓄存款于一卡，具有便民缴费、景区入园、存款取款、购物转账、消费结算、自助缴费、委托代理、个人融资等多种功能，是您便民生活的好帮手。雄关便民卡具有安全性高、智能存储、交易快捷等优势，支持非接触式小额快速交易，为持卡人带来全新支付体验。</br>
            办卡渠道：</br>
            （一）银行柜面一站式办理</br>
            市民本人可前往嘉峪关市兰州银行任一网点办理雄关便民卡开卡、挂失、换卡、补卡等一站式金融业务。</br>
            （二）单位渠道办理</br>
            有工作单位的市民可向所在单位提出申请，由所在单位安排专人到兰州银行任一网点进行批量办理。</br>
            （三）社区渠道办理</br>
            无工作单位的社区居民可向社区提出申请并登记，由社区汇总信息至兰州银行任一网点进行办理。</br>
            二、办理雄关便民卡所需资料</br>
            （一）个人柜面渠道办理：本人持本人有效身份证件。</br>
            （二）单位渠道批量办理：单位介绍信，职工花名册等其他基础性资料。</br>
            三．雄关便民卡领卡流程</br>
            （一）个人柜面渠道办理的，可在柜面直接申领雄关便民卡。</br>
            （二）单位或社区批量办理的由兰州银行安排专人前往单位或社区进行卡片发放及激活工作，领卡人需持本人有效身份证件领取。</br>
            （三）行政事业单位工作人员及中核四O四有限公司职工制卡数据已提交，部分卡片已制作完成，兰州银行将安排专人前往单位进行卡片发放及激活工作。</br>
        </p>
        <label class="title_large_fontsize margin_form_middle convenience_title" id="yuio">服务网点</label><br>
        <p class="large_fontsize lt_gray_color margin_form_middle align_left">
            兰州银行嘉峪关分行营业部<br>
            地址：嘉峪关市新华南路2108-B号。<br>
            联系电话：0937-6239823</br>

        </p>
        <p class="large_fontsize lt_gray_color margin_form_middle align_left">
            兰州银行嘉峪关雄关支行</br>
            地址：嘉峪关市新华北路新华城苑1号楼。</br>
            联系电话：0937-6395857</br>

        </p>
        <p class="large_fontsize lt_gray_color margin_form_middle align_left">
            兰州银行嘉峪关长城支行</br>
            地址：嘉峪关市胜利中路宝诚中央公馆西门。</br>
            联系电话：0937-6239867</br>

        </p>
        <p class="large_fontsize lt_gray_color margin_form_middle align_left">
            兰州银行嘉峪关新华支行</br>
            地址：嘉峪关市新华南路229-1号。</br>
            联系电话；0937-6239851</br>

        </p>
        <label class="title_large_fontsize convenience_title" id="tgbn">自助设备服务指南</label><br>
        <p class="large_fontsize lt_gray_color margin_form_middle align_left">

            自助设备包含银行自助终端、小区自助终端、家用便携式读卡器。银行自助终端指布放在兰州银行任一网点的自助设备，可实现雄关便民卡的银行卡功能，包括账户查询、存取款、转账、特色存取款业务等；小区自助终端指布放在小区的自助设备，可实现雄关便民卡的缴费功能，包括水、电、气、暖费的缴费，此外，还可实现银行账户查询、话费缴纳等功能；家用便携式读卡器可方便用户在家通过电脑实现水、电、气、暖费的缴纳。
        </p>
        <label class="title_large_fontsize convenience_title" id="sefc">电话服务指南</label><br>
        <p class="large_fontsize lt_gray_color margin_form_middle align_left">
            政府热线：12345</br>
            嘉峪关市智慧雄关科技有限公司：5976215</br>
            金融服务：0937-6239821</br>
        </p>
        %{--<label class="title_large_fontsize convenience_title" id="ijud">常见问题</label><br>--}%
        %{--<p class="large_fontsize lt_gray_color margin_form_middle align_left">--}%

        %{--1. 关于注册和登录 <br>--}%

        %{--本APP用户为兰州银行一户通统一管理用户，如果在兰州银行一户通已经注册，可以直接用注册的手机号和密码登录本APP。如果没有注册兰州银行一户通，可以在本APP注册用户。<br>--}%

        %{--2. 关于设置密码<br>--}%

        %{--为了密码安全性，密码长度最短不能少于三位，最多不能大于20位，建议6位；密码不允许连续三位都是数字或都是字符。<br>--}%

        %{--3. 关于实名认证<br>--}%

        %{--办理并激活了雄关便民卡，而且预留了能收到短信的手机号，方可进行实名认证。实名认证是享受很多APP服务的前提。<br>--}%

        %{--4. 关于电子便民卡<br>--}%

        %{--电子便民卡是实体便民卡的一种替代方式，在涉及用雄关便民卡身份认证的场合，可以出示电子便民卡，但申领实体便民卡是享有电子便民卡的前提。<br>--}%

        %{--5. 关于支付码<br>--}%

        %{--本APP支付码为银联支付码，其绑定的账户是雄关便民卡的兰州银行账户。凡是支持扫银联二维码的场景均可出示该支付码进行支付，但前提是要办理并激活雄关便民卡，保证账户中有足够的余额进行支付。<br>--}%

        %{--6. 关于扫一扫​<br>--}%

        %{--本APP扫一扫，可以扫描在兰州银行申请了静态收款码商户出示的收款码，并使用百合易付进行支付。<br>--}%

        %{--7. 关于雄关便民卡的办理<br>--}%

        %{--雄关便民卡属于嘉峪关政府主导发行的集金融和城市应用的智能卡，政府会主动按照计划面向市民发行，市民也可到兰州银行嘉峪关分行柜面自愿申请雄关便民卡。<br>--}%

        %{--8. 关于百合易付<br>--}%

        %{--本APP支付主要使用兰州银行百合易付支付工具。百合易付是兰州银行线上统一收银台，支持兰州银行卡账户、银联云闪付、百合积分、微信以及支付宝支付（延后上线）。<br>--}%

        %{--9. 关于水、电、燃气缴费<br>--}%

        %{--本APP水、电、燃气缴费仅限于物联网表和手工抄表用户。此类用户可以用此APP为水、电和燃气的预存账户进行充值。不支持卡表用户，卡表用户需持卡至原缴费渠道、兰州银行柜面、兰州银行自助终端、社区云上雄关自助终端进行充值。<br>--}%
        %{--</p>--}%
    </div>
</div>
</body>
</html>