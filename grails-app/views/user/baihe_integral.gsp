<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>
<body>
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryOils')">
                                <li>
                                    加油卡充值
                                </li>
                            </a>
                            <a onclick="isReal('/queryBill')" >
                                <li>
                                    缴费账单
                                </li>
                            </a>
                        </ul>
                    </li>
                    %{--<li>--}%
                        %{--<div class="li"><span>金融服务</span></div>--}%
                        %{--<ul class="in_ul">--}%
                            %{--<a onclick="isReal('/user/queryCardSiteList')">--}%
                                %{--<li>--}%
                                    %{--便民卡预约--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        %{--</ul>--}%
                    %{--</li>--}%
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul ul_active">
                            <a onclick="isReal('/user/queryCredit')" >
                                <li>
                                    雄关信用
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryScore')" class="active">
                                <li>
                                    百合积分
                                </li>
                            </a>
                            <a  onclick="isReal('/user/getLibrary')" >
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryFund')" >
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- 水费缴纳content -->
    <div class="life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">百合生活</label>
                <label class="main_color">品质保证</label>
                <g:img dir="images" src="/life/2.png" class="fenge"/>
                <label class="small_fontsize">百合积分</label>
            </label>
        </div>
        <div class="custom_letter_div">
            <div class="br_div align_center">
                <div class="cutom_letter_amount br_div baihe_div">
                    <g:if test="${data}">
                    <div class="bigest_size margin_top_largemore">${data.score}</div>
                    </g:if>
                    <div class="more_middle_fontsize">当前积分</div>
                </div>
            </div>
            <div class="div_float_right align_center baihe_right_div">
                <div class="more_middle_fontsize">百合积分▪点滴积累</div>
                <div class="margin_form_middle large_fontsize">
                    <div class="xinyon buy">
                        <div>超值购</div>
                    </div>
                    <div class="xinyon play">
                        <div>趣味玩</div>
                    </div>
                    <div class="xinyon jifen">
                        <div>赚积分</div>
                    </div>
                </div>
                <div class="more_middle_fontsize margin_top_large">百合积分联盟优质商户</div>
                <div class="margin_top_middle bussiness_img">
                    <g:img dir="images" src="/life/baiheyijia.png"/>
                    <g:img dir="images" src="/life/hainan.png" class="center_img"/>
                    <g:img dir="images" src="/life/maidaofood.png"/>
                </div>
                <div class="margin_top_middle bussiness_img">
                    <g:img dir="images" src="/life/juyuan.png"/>
                    <g:img dir="images" src="/life/watch.png" class="center_img"/>
                    <g:img dir="images" src="/life/hengkang.png"/>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>