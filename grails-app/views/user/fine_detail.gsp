<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>
<body>
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul ul_active">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')" class="active">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            %{--<a onclick="isReal('/user/queryOils')">--}%
                                %{--<li>--}%
                                    %{--加油卡充值--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/queryBill')" >--}%
                                %{--<li>--}%
                                    %{--缴费账单--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        </ul>
                    </li>
                    %{--<li>--}%
                        %{--<div class="li"><span>金融服务</span></div>--}%
                        %{--<ul class="in_ul">--}%
                            %{--<a onclick="isReal('/user/queryCardSiteList')">--}%
                                %{--<li>--}%
                                    %{--便民卡预约--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        %{--</ul>--}%
                    %{--</li>--}%
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul">
                            %{--<a onclick="isReal('/user/queryCredit')" >--}%
                                %{--<li>--}%
                                    %{--雄关信用--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/user/queryScore')" >--}%
                                %{--<li>--}%
                                    %{--百合积分--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="toast()" >
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="toast()" >
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- 水费缴纳content -->
    <div class="life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">品质保证</label>
                <label class="main_color">安全快捷</label>
                <g:img dir="images" src="/life/2.png" class="fenge"/>
                <label class="small_fontsize">缴费详情</label>
            </label>
        </div>
        <div>
           %{-- PaySnglNo缴款单号 RltnpNm 当事人 PltNo车牌号 TotAmt罚款总金额 PylAmt罚款金额 LtPymtAmt滞纳金--}%
            <g:if test="${data}">
            <form  id="fine_form" class="gray_color br_div">
                <div class="lt_gray_color fine_main_title">缴款单信息</div>
                <div class="margin_form_middle">
                    <label class="large_fontsize fine_info_title">缴款单号:</label>
                    <label class="large_fontsize div_float_right">${data.PaySnglNo}</label>
                </div>
                <div class="margin_form_middle">
                    <label class="large_fontsize fine_info_title">当事人:</label>
                    <label class="large_fontsize div_float_right">${data.RltnpNm}</label>
                </div>
                <div class="margin_form_middle">
                    <label class="large_fontsize fine_info_title">车牌号:</label>
                    <label class="large_fontsize div_float_right">${data.PltNo}</label>
                </div>
                <div class="margin_form_middle">
                    <label class="large_fontsize fine_info_title">罚款总金额:</label>
                    <label class="large_fontsize div_float_right">${data.TotAmt}</label>
                </div>
                <div class="margin_form_middle">
                    <label class="large_fontsize fine_info_title">罚款金额:</label>
                    <label class="large_fontsize div_float_right">${data.PylAmt}</label>
                </div>
                <div class="margin_form_middle">
                    <label class="large_fontsize fine_info_title">滞纳金:</label>
                    <label class="large_fontsize div_float_right">${data.LtPymtAmt}</label>
                </div>
                <input type="hidden" name="PaySnglNo" id="PaySnglNo" value="${data.PaySnglNo}"/>
                <input type="hidden" name="RltnpNm" id="RltnpNm" value="${data.RltnpNm}"/>
                <input type="hidden" name="PltNo"  id="PltNo" value="${data.PltNo}"/>
                <input type="hidden" name="TotAmt" id="TotAmt" value="${data.TotAmt}"/>
                <input type="hidden" name="PylAmt" id="PylAmt" value="${data.PylAmt}"/>
                <input type="hidden" name="LtPymtAmt" id="LtPymtAmt" value="${data.LtPymtAmt}"/>
                <input type="hidden" name="PaySnglTyp" id="PaySnglTyp" value="${data.PaySnglTyp}"/>
                <input type="hidden" name="DrvsLcnsNo" id="DrvsLcnsNo" value="${data.DrvsLcnsNo}"/>
                <input type="hidden" name="FileNo" id="FileNo" value="${data.FileNo}"/>
                <input type="hidden" name="AtmbLicTp" id="AtmbLicTp" value="${data.AtmbLicTp}"/>
                <input type="hidden" name="DealYr" id="DealYr" value="${data.DealYr}"/>
                <input type="hidden" name="DealTm" id="DealTm" value="${data.DealTm}"/>
                <input type="hidden" name="ExecUnitNm" id="ExecUnitNm" value="${data.ExecUnitNm}"/>
                <input type="hidden" name="ExecInstNm" id="ExecInstNm" value="${data.ExecInstNm}"/>
                <input type="hidden" name="PyeInstCd" id="PyeInstCd" value="${data.PyeInstCd}"/>
                <input type="submit" value="立即缴费" class="form_submit_water margin_top_largemore"/>
            </form>
            </g:if>
            <g:else>
                <h2 class="align_center margin_top_largemorest gray_color">未查询到罚单信息</h2>
            </g:else>
        </div>
    </div>
</div>
<script>
    $("#fine_form").validate({
        submitHandler: function (form) {
            $.post('/user/fineRecharge', $("#fine_form").serialize(),
                function (json) {
                    if(json.code == 0){
                        window.location.href = "http://192.168.2.206:8879/pay/gateWay?orderId="+json.orderno
                        // window.location.href = "https://etest.lzbank.com:20028/ccweb/samlsso?ChangeVal=mainPageSAML&spid=p3depay&method="+json.method
                    }else {
                        layer.msg(json.errormsg)
                    }
                })
        }
    });
</script>
</body>
</html>