<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>
<body>
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul ul_active">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            <a onclick="toast()">
                                <li>
                                    电费缴纳
                                </li>
                            </a>
                            <a onclick="toast()" class="active">
                                <li>
                                    取暖费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryOils')">
                                <li>
                                    加油卡充值
                                </li>
                            </a>
                            <a onclick="isReal('/queryBill')" >
                                <li>
                                    缴费账单
                                </li>
                            </a>
                        </ul>
                    </li>
                    %{--<li>--}%
                        %{--<div class="li"><span>金融服务</span></div>--}%
                        %{--<ul class="in_ul">--}%
                            %{--<a onclick="isReal('/user/queryCardSiteList')">--}%
                                %{--<li>--}%
                                    %{--便民卡预约--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        %{--</ul>--}%
                    %{--</li>--}%
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul">
                            <a onclick="isReal('/user/queryCredit')" >
                                <li>
                                    雄关信用
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryScore')" >
                                <li>
                                    百合积分
                                </li>
                            </a>
                            <a  onclick="isReal('/user/getLibrary')" >
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryFund')" >
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- 水费缴纳content -->
    <div class="life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">品质保证</label>
                <label class="main_color">安全快捷</label>
                <label class="small_fontsize gray_color mar_left">暖气费缴纳</label>
                <g:img dir="images" src="/index/20.png"/>
                <label class="small_fontsize">添加暖气费账户</label>
            </label>
        </div>
        <div>
            <form class="add_water">
                <div class="margin_top_large">
                    <label class="large_fontsize input_title">缴费单位:</label>
                    <select class="form_input btn_div">
                        <option>缴费单位</option>
                        <option>缴费单位</option>
                        <option>缴费单位</option>
                    </select>
                </div>
                <div class="margin_top_large">
                    <label class="large_fontsize input_title">户号:</label>
                    <input placeholder="请输入户号" class="form_input btn_div"/>
                    <a class="main_color add_water_forget_txt" href="/warm_query">忘记户号？</a>
                </div>
                <div class="margin_top_large">
                    <label class="large_fontsize input_title">户主名字:</label>
                    <input placeholder="请输入户主姓名" class="form_input btn_div"/>
                </div>
                <div class="margin_top_large">
                    <label class="large_fontsize input_title">账户别名:</label>
                    <input placeholder="给账户起个名称，方便记忆" class="form_input btn_div"/>
                </div>
                <input type="submit" value="添加" class="form_submit_water margin_top_largemore"/>
            </form>
        </div>
    </div>
</div>
</body>
</html>