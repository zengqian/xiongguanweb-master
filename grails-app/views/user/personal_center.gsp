<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>

<body>
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span>生活缴费</span></div>
                        <ul class="in_ul">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            %{--<a onclick="isReal('/user/queryOils')">--}%
                                %{--<li>--}%
                                    %{--加油卡充值--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/queryBill')">--}%
                                %{--<li>--}%
                                    %{--缴费账单--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        </ul>
                    </li>
                    %{--<li>--}%
                        %{--<div class="li"><span>金融服务</span></div>--}%
                        %{--<ul class="in_ul">--}%
                            %{--<a onclick="isReal('/user/queryCardSiteList')">--}%
                                %{--<li>--}%
                                    %{--便民卡预约--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        %{--</ul>--}%
                    %{--</li>--}%
                    <li>
                        <div class="li"><span>公共服务</span></div>
                        <ul class="in_ul">
                            %{--<a onclick="isReal('/user/queryCredit')">--}%
                                %{--<li>--}%
                                    %{--雄关信用--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/user/queryScore')">--}%
                                %{--<li>--}%
                                    %{--百合积分--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="toast()">
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="toast()">
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- 个人中心content -->
    <div class="">
        <div class="personal_body_top height_lose relative_position">
            <div class="body_top1">
                <div class="div_float_left body_top_img br_div relative_position">
                    <g:if test="${data.realnameinfo}">
                        <img src="${data.userinfo.imageUrl == "" ? '/assets/index/21.png' : data.userinfo.imageUrl}"
                             class="body_top_img update_logo"/>
                        <g:img dir="images" src="/life/39.png" class="realname_logo"/>
                        <form id='upload_form' action='/user/upload' enctype = 'multipart/form-data' method='post'>
                            <input type="file" name="upload_logo" onchange="uploads()" style="position: absolute;top: 0;left: 0;width: 100%;height: 100%;opacity: 0;cursor: pointer;"/>
                        </form>
                    </g:if>
                    <g:else>
                        <img src="${data.userinfo.imageUrl == "" ? '/assets/index/21.png' : data.userinfo.imageUrl}"
                             class="body_top_img update_logo"/>
                    </g:else>
                </div>
            <!--已实名-->
                <g:if test="${data.userinfo.userName}">
                    <div class="div_float_left margin_top_middle white_color">
                        <div class="more_middle_fontsize">${data.userinfo.userName}</div>
                        <g:if test="${data.realnameinfo}">
                            <div class="margin_form_middle large_fontsize">${data.realnameinfo.name}</div>
                        </g:if>
                        <g:else>
                            <div class="margin_form_middle large_fontsize">未实名认证</div>
                        </g:else>
                    </div>
                    <a class="div_float_right lt_gray_color personal_info_a" href="/user/getRealInfo">个人信息设置</a>
                    %{--<a class="div_float_right lt_gray_color personal_info_a" onclick="toast()">个人信息设置</a>--}%
                </g:if>
                <g:else>
                    <div class="div_float_left margin_top_middle white_color">
                        <div class="more_middle_fontsize">用户名</div>

                        <div class="margin_form_middle large_fontsize">未实名认证</div>
                    </div>
                    '<a class="div_float_right lt_gray_color personal_info_a" onclick="toast('请先进行实名认证')">个人信息设置</a>
                </g:else>
            </div>

            <form>
                <div class="body_top2 absolute_position">
                    <a onclick="getPath('/user/queryOrderInfo')">
                        <g:img dir="images" src="/index/22.png" class="order_img"/>
                        全部订单
                    </a>
                    <a onclick="getPath('/user/queryUnPayOrder')">
                        <g:img dir="images" src="/index/23.png" class="order_img"/>
                        待付款
                    </a>
                    <a onclick="getPath('/user/queryPayOrder')">
                        <g:img dir="images" src="/index/24.png" class="order_img"/>
                        已完成
                    </a>
                    <a class="last" onclick="getPath('/user/queryCancelOrder')">
                        <g:img dir="images" src="/index/25.png" class="order_img"/>
                        已取消
                    </a>
                </div>
                <input id="total" type="hidden" name="total" />
                <input id="page" type="hidden" name="page" />
            </form>
        </div>

        <div class="personal_body_bottom aaa">
            <table style="width: 100%;padding: 0 13px;" class="order_table" id="order_table">
                %{--<tr>--}%
                    %{--<td>汇能燃气充值</td>--}%
                    %{--<td>201812342343241234123421</td>--}%
                    %{--<td>汇能燃气缴费</td>--}%
                    %{--<td>处理中</td>--}%
                %{--</tr>--}%
                %{--<tr>--}%
                    %{--<td>汇能燃气充值</td>--}%
                    %{--<td>201812342343241234123421</td>--}%
                    %{--<td>汇能燃气缴费</td>--}%
                    %{--<td>处理中</td>--}%
                %{--</tr>--}%
                %{--<tr>--}%
                    %{--<td>汇能燃气充值</td>--}%
                    %{--<td>201812342343241234123421</td>--}%
                    %{--<td>汇能燃气缴费</td>--}%
                    %{--<td>处理中</td>--}%
                %{--</tr>--}%
                %{--<tr>--}%
                    %{--<td>汇能燃气充值</td>--}%
                    %{--<td>201812342343241234123421</td>--}%
                    %{--<td>汇能燃气缴费</td>--}%
                    %{--<td>处理中</td>--}%
                %{--</tr>--}%
                %{--<tr>--}%
                    %{--<td>汇能燃气充值</td>--}%
                    %{--<td>201812342343241234123421</td>--}%
                    %{--<td>汇能燃气缴费</td>--}%
                    %{--<td>处理中</td>--}%
                %{--</tr>--}%
                %{--<tr>--}%
                    %{--<td>汇能燃气充值</td>--}%
                    %{--<td>201812342343241234123421</td>--}%
                    %{--<td>汇能燃气缴费</td>--}%
                    %{--<td>处理中</td>--}%
                %{--</tr>--}%
                %{--<tr>--}%
                    %{--<td>汇能燃气充值</td>--}%
                    %{--<td>201812342343241234123421</td>--}%
                    %{--<td>汇能燃气缴费</td>--}%
                    %{--<td>处理中</td>--}%
                %{--</tr>--}%
            </table>
        </div>
        <!-- 分页按钮 -->
        <div class="pagination">
            <button class="prev_page">上一页</button>

            <div class="number" id="number">
            </div>
            <button class="next_page">下一页</button>
            <button class="last_page">尾页</button>
        </div>
    </div>
</div>
<asset:javascript src="pagination.js"/>
<script>
    var path="";
    //进入页面之后局部加载获得数据
    $().ready(function () {
        //订单查询
        path ='/user/queryOrderInfo';
        getOrder(path);
    })
    function getPath(path) {
        path = path;
        $("#page").val("1");//重置页码
        getOrder(path);//请求数据并渲染
    }
    function getOrder(path) {
        $.post(path,{page:function () {
                    return $("#page").val();
                }},
            function (json) {
                if(json.errorcode == 0){
                    $(".order_table").html("");//查询之前先清空table
                    createtd("订单类型","订单号","订单描述","订单状态");
                    list  = json.data.list;
                    console.log(list);
                    for(var i =0;i<list.length;i++){
                       switch (list[i].orderType) {
                            case "10001":
                                list[i].orderType = "交通罚没";
                                break;
                            case "11001":
                                list[i].orderType = "移动缴费";
                                break;
                            case "11002":
                                list[i].orderType = "联通缴费";
                                break;
                            case "11003":
                               list[i].orderType = "电信缴费";
                               break;
                           case "56001":
                               list[i].orderType = "汇能燃气充值";
                               break;
                           case "31001":
                               list[i].orderType = "水投缴费";
                               break;
                           case "55001":
                               list[i].orderType = "昆仑燃气缴费";
                               break;
                           case "21001":
                               list[i].orderType = "图书馆消费";
                               break;
                           case "77777":
                               list[i].orderType = "个人到个人扫码付-付款";
                               break;
                           case "21002":
                               list[i].orderType = "图书馆退款";
                               break;
                           case "41001":
                               list[i].orderType = "中国石化加油卡充值";
                               break;
                           case "41002":
                               list[i].orderType = "中国石油加油卡充值";
                               break;
                        }
                        switch (list[i].status) {
                            case "01":
                                list[i].status = "待支付";
                                break;
                            case "02":
                                list[i].status = "处理中";
                                break;
                            case "03":
                                list[i].status = "退款中";
                                break;
                            case "04":
                                list[i].status = "退款完成";
                                break;
                            case "05":
                                list[i].status = "交易完成";
                                break;
                            case "06":
                                list[i].status = "订单取消";
                                break;
                        }
                        createtd(list[i].orderType,list[i].orderId,list[i].orderDesc,list[i].status);
                    }
                    $("#total").val(json.data.totalRow);//设值总条数
                    $("#page").val(json.page);//设值页码
                    $.firstinit(json.data.totalRow, json.page, function () { //默认每页十条数据
                        console.log(path);
                        getOrder(path);
                    })
                }else {
                    layer.msg("请求失败");
                }
            });
        //获取到数据list 循环调用 type为查询的种类，全部订单或者待付款的这种
        // $.post(function () {
        //
        // });
        //createtd("汇能燃气充值","订单号","订单描述","订单状态")
    }
    function createtd(type,orderno,orderdesc,state) {
        var tr = document.createElement("tr")
        var td1 = document.createElement("td")
        var td2 = document.createElement("td")
        var td3 = document.createElement("td")
        var td4 = document.createElement("td")
        td1.innerHTML = type
        td2.innerHTML = orderno
        td3.innerHTML = orderdesc
        td4.innerHTML = state
        tr.appendChild(td1)
        tr.appendChild(td2)
        tr.appendChild(td3)
        tr.appendChild(td4)
        $("#order_table").append(tr)
    }

    function uploads() {
        $("#upload_form").submit();
    }
</script>
</body>
</html>