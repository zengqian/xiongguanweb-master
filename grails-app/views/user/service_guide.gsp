<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="convenienceCard"/>
</head>
<body>
<!--中心内容-->
<div class="about_card">
    <div>
        <label class="title_middle_fontsize">资讯中心</label>
        <div class="title_to">
            <label>当前位置：</label>
            <a>资讯中心</a>
            <label>></label>
            <a class="old_color">服务指南</a>
        </div>
        <div class="img_div"><g:img dir="images" src="/index/16.png"/></div>
    </div>
    <div class="margin_top_middle content" style="overflow: hidden">
        <div class="menu" id="convenience_menu">
            <ul>
                <li>
                    <div class="li">
                        <g:img dir="images" src="/index/17.png"/>
                        <a href="/about_convenience_card" class="convenience_menu">关于便民卡</a>
                    </div>
                </li>
                <li>
                    <div class="li">
                        <g:img dir="images" src="/index/19.png"/>
                        <a href="/service_guide" class="active convenience_menu">服务指南</a>
                    </div>
                </li>
                <li>
                    <div class="li">
                        <g:img dir="images" src="/index/19.png"/>
                        <a href="/convenience_card_application" class="convenience_menu">便民卡应用</a>
                    </div>
                </li>
            </ul>
        </div>
        <ul class="news_content" style="overflow: auto;height: 100%;">
            <li>
                <a class="lt_gray_color large_fontsize" href="/serviceguide_info#qwer">办卡及服务指南</a>
                <div class="gray_color">
                    <label>雄关便民卡是为满足市民个性化服务需要而开发的一种多功能的电子金融工具</label>
                    %{--<label class="div_float_right">2018年9月4号</label>--}%
                </div>
            </li>
            <li>
                <a class="lt_gray_color large_fontsize" href="/serviceguide_info#yuio">服务网点</a>
                <div class="gray_color">
                    <label>兰州银行嘉峪关分行营业部 地址：嘉峪关市新华南路2108-B号</label>
                    <!--<label class="div_float_right">2018年9月4号</label>-->
                </div>
            </li>
            <li>
                <a class="lt_gray_color large_fontsize" href="/serviceguide_info#tgbn">自助设备服务指南</a>
                <div class="gray_color">
                    <label>自助设备包含银行自助终端、小区自助终端、家用便携式读卡器。</label>
                    <!--<label class="div_float_right">2018年9月4号</label>-->
                </div>
            </li>
            <li>
                <a class="lt_gray_color large_fontsize" href="/serviceguide_info#sefc">电话服务指南</a>
                <div class="gray_color">
                    <label>政府热线: 12345 雄关智慧科技有限公司</label>
                    <!--<label class="div_float_right">2018年9月4号</label>-->
                </div>
            </li>
            %{--<li>--}%
                %{--<a class="lt_gray_color large_fontsize" href="/serviceguide_info#ijud">常见问题</a>--}%
                %{--<div class="gray_color">--}%
                %{--</div>--}%
            %{--</li>--}%
        </ul>
    </div>
</div>
</body>
</html>