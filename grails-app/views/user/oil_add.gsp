<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>
<body>
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul ul_active">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryOils')" class="active">
                                <li>
                                    加油卡充值
                                </li>
                            </a>
                            <a onclick="isReal('/queryBill')">
                                <li>
                                    缴费账单
                                </li>
                            </a>
                        </ul>
                    </li>
                    %{--<li>--}%
                        %{--<div class="li"><span>金融服务</span></div>--}%
                        %{--<ul class="in_ul">--}%
                            %{--<a onclick="isReal('/user/queryCardSiteList')">--}%
                                %{--<li>--}%
                                    %{--便民卡预约--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        %{--</ul>--}%
                    %{--</li>--}%
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul">
                            %{--<a onclick="isReal('/user/queryCredit')">--}%
                                %{--<li>--}%
                                    %{--雄关信用--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/user/queryScore')">--}%
                                %{--<li>--}%
                                    %{--百合积分--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="toast()">
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="toast()">
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- 水费缴纳content -->
    <div class="life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">品质保证</label>
                <label class="main_color">安全快捷</label>
                <a class="small_fontsize gray_color mar_left" onclick="isReal('/user/queryOils')">加油卡充值</a>
                <g:img dir="images" src="/index/20.png"/>
                <label class="small_fontsize">添加加油卡</label>
            </label>
        </div>
        <form class="margin_top_large oil_add" id="recharge_form">
            <div class="large_fontsize">加油卡类型：</div>
            <div class="margin_top_middlel">
                <div class="oil_radio">
                    <g:img dir="images" src="/life/43.png" class="vertical_align" style="height: 30px"/>
                    <label style="">中国石化</label>
                    <input type="radio" name="oilRadio" value="41001" id="cn_sh"/>
                </div>
                <div class="oil_radio div_float_right">
                    <g:img dir="images" src="/life/44.png" class="vertical_align" style="height: 33px"/>
                    <label>中国石油</label>
                    <input type="radio" name="oilRadio" value="41002" id="cn_sy"/>
                </div>
            </div>
                <label class="water_error_label absolute_position"></label>
            <div class="large_fontsize margin_top_large">加油卡号</div>
            <div class="margin_top_middlel">
                <input placeholder="请输入加油卡卡号" type="text" class="form_input btn_div" name="cardNo"/>
            </div>
            <label class="water_error_label absolute_position"></label>
            <input type="submit" value="确&nbsp;认" class="form_submit_water margin_top_largemore"/>
        </form>
    </div>
</div>
<script>
    $("#recharge_form").validate({
        // onkeyup: false,
        errorPlacement: function (error, element) {
            element.parent().parent().next("label.water_error_label").append(error);
            element.parent().next("label.water_error_label").append(error);
        },
        rules: {
            cardNo: {
                required: true,
                rangelength: [16,19]
            },
            oilRadio: {
                required: true
            }
        },
        messages: {
            cardNo: {
                required: "请填写卡号",
                rangelength: "石油卡为19位，石化卡16位"
            },
            oilRadio: {
                required: "请选择卡类型"
            }
        },
        submitHandler: function (form) {
            $.post('/user/addOil', $("#recharge_form").serialize(),
                function (json) {
                    if(json.errorcode == 0){
                        layer.msg('添加成功');
                        isReal('/user/queryOils');
                    }else {
                        layer.msg(json.errormsg)
                    }
                })
            // validator.resetForm();
        }
    });
</script>
</body>
</html>