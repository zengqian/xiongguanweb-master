<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userForgetLayout"/>
    <asset:javascript src="jquery-1.11.1.min.js"/>
    <asset:javascript src="writeObject.js"/>
    <asset:stylesheet src="objectStyle.css"/>
</head>

<body>
<!--中心内容-->
<div class="user_forget_div">
    <!--重置密码-->
    <div class="content_div">
        <div class="title_large_fontsize lt_gray_color">登录密码找回</div>
        <!--step1-->
        <div class="margin_top_large br_div gray_color align_center step1">
            <div class="jiantou_div">
                <g:img dir="images" src="/index/true_jiantou.png"/>
                <label class="jiantou_txt white_color">1.&nbsp;身份验证</label>
            </div>

            <div class="jiantou_div">
                <g:img dir="images" src="/index/false_jiantou.png" class="left_img"/>
                <label class="jiantou_txt left2">2.&nbsp;设置新密码</label>
            </div>

            <div class="jiantou_div">
                <g:img dir="images" src="/index/false_jiantou_zhi.png" class="left_img"/>
                <label class="jiantou_txt left3">3.&nbsp;完成</label>
            </div>
            <!--警告语句-->
            <div class="gray_color warn_txt align_center">
                <g:img dir="images" src="/index/warn.png"/>
                为了你的账户安全请先进行收集短信验证
            </div>

            <form class="br_div register_form" id="forget_01_form">
                <div class="margin_top_first">
                    <input class="btn_div form_input " placeholder="输入手机号码" name="phone" id="phone"/>
                </div>

                <div class="margin_form_middle relative_position">
                    <input class="btn_div form_input" placeholder="输入短信验证码" maxlength="6" name="tellCode"
                           id="tellCode"/>

                    <div class="get_mess_btn get_tell_code">获取短信验证码</div>
                </div>

                <div class="margin_form_middle relative_position get_mess_div">
                    <input class="btn_div form_input imgCode" placeholder="输入图片验证码" maxlength="4" name="imgCode"/>
                    <div class="get_mess_btn">
                        <img src="/user/verifyCode" id="verifyCodeImg"/></div>
                </div>
                <!-- 获取验证码接口参数 1002-注册 -->
                <input type="hidden" value="1012" name="interfaceCode">
                <input type="submit" class="form_submit_btn margin_top_large " value="下一步"/>
            </form>
        </div>
        <!--step2-->
        <div style="display: none;" class="margin_top_large br_div gray_color align_center step2">
            <div class="jiantou_div">
                <g:img dir="images" src="/index/false_jiantou1.png"/>
                <label class="jiantou_txt">1.&nbsp;身份验证</label>
            </div>

            <div class="jiantou_div">
                <g:img dir="images" src="/index/true_jiantou2.png" class="left_img"/>
                <label class="jiantou_txt left2 white_color">2.&nbsp;设置新密码</label>
            </div>

            <div class="jiantou_div">
                <g:img dir="images" src="/index/false_jiantou_zhi.png" class="left_img"/>
                <label class="jiantou_txt left3">3.&nbsp;完成</label>
            </div>
            <!--警告语句-->
            <div class="gray_color warn_txt align_center">
                <g:img dir="images" src="/index/warn.png"/>
                您的验证已成功通过，请设置新的登录密码
            </div>

            <form class="br_div register_form" id="forget_02_form">
                <div class="margin_top_first relative_position input_div">
                    <input class="btn_div form_input" placeholder="设置新的登录密码" name="newpass" id="newpass" type="hidden"/>
                    <script type="text/javascript">writePassObject("pass1", {
                        "width": 270,
                        "height": 22,
                        "accepts": "[0-9a-zA-Z]+"
                    });</script>
                    <label class="passcontrol_prompt_right" id="reset_pass1"></label>
                </div>

                <div class="margin_form_middle relative_position input_div">
                    <input class="btn_div form_input" placeholder="再次输入登录密码" name="newpass_copy" type="hidden"/>
                    <script type="text/javascript">writePassObject("pass2", {
                        "width": 270,
                        "height": 22,
                        "accepts": "[0-9a-zA-Z]+"
                    });</script>
                    <label class="passcontrol_prompt_right" id="reset_pass2"></label>
                </div>
                <input type="submit" class="form_submit_btn margin_top_large" value="下一步"/>
            </form>
        </div>
        <!--step3-->
        <div style="display: none;" class="margin_top_large br_div gray_color align_center step3">
            <div class="jiantou_div">
                <g:img dir="images" src="/index/false_jiantou1.png"/>
                <label class="jiantou_txt">1.&nbsp;身份验证</label>
            </div>

            <div class="jiantou_div">
                <g:img dir="images" src="/index/false_jiantou.png" class="left_img"/>
                <label class="jiantou_txt left2 ">2.&nbsp;设置新密码</label>
            </div>

            <div class="jiantou_div">
                <g:img dir="images" src="/index/true_jiantou_zhi.png" class="left_img"/>
                <label class="jiantou_txt left3 white_color">3.&nbsp;完成</label>
            </div>
            <!--警告语句-->
            <div class="gray_color warn_txt align_center">
                <g:img dir="images" src="/index/succ_warn.png"/>
                您的登录密码已重新设置，请记录并妥善保管
            </div>
            <a class="succ_btn not_a" href="/login">完成</a>
        </div>
    </div>
</div>
<script>
    var ts = "<%=System.currentTimeMillis()%>";
    $("#forget_01_form").validate({
        rules: {
            phone: {
                required: true
            },
            tellCode: {
                required: true
            },
            imgCode: {
                required: true,
                rangelength: [4, 4],
                remote: {
                    url: "/user/checkVerifyCode",
                    type: "post",
                    data: {                     //要传递的数据
                        verifyCode: function () {
                            return $(".imgCode").val();
                        }
                    },
                    complete: function (data) {
                        //最后这里做返回处理，是否验证成功等
                        if (data.responseText == "false") {
                            layer.msg("验证码错误！");
                            $(".imgCode").val("");
                            $("#verifyCodeImg").attr("src", "user/verifyCode?number=" + Math.random());
                        }
                    }
                }
            }
        },
        messages: {
            phone: {
                required: "请输入手机号码"
            },
            tellCode: {
                required: "请输入手机验证码"
            },
            imgCode: {
                required: "请输入图片验证码",
                rangelength: "请输入四位验证码",
                remote: "验证码错误"
            }
        },
        submitHandler: function (form) {
            $.post("/msg/checkPhoneCode", $("#forget_01_form").serialize(), function (json) {
                if (json.code == 0) {
                    show2()
                } else {
                    $("#verifyCodeImg").attr("src", "user/verifyCode?number=" + Math.random());
                    layer.msg(json.errormsg)
                }
            });
            // validator.resetForm();
        }
    });

    $("#forget_02_form").validate({
        // rules: {
        //     newpass: {
        //         required: true,
        //         rangelength: [6, 18],
        //         stringCheckPassword: true
        //     }
        // },
        // messages: {
        //     newpass: {
        //         required: "请输入手机验证码",
        //         rangelength: "密码长度因为6-18"
        //     }
        // },
        submitHandler: function (form) {
            var password = getPassInput("pass1", ts, "reset_pass1", "密码输入错误：");
            var password2 = getPassInput("pass2", ts, "reset_pass2", "密码输入错误：");
            if (password != null && password2 != null) {
                $.post("/user/resetLoginpass", {
                    "password": password,
                    "passwordConfirm": password2,
                    "mobile": $("#phone").val()
                }, function (json) {
                    console.log(json)
                    // if (json.code == "5000") {
                    //     show3()
                    // } else {
                        switch (json.code) {
                            case "5000":
                                show3();
                                break
                            case "5001":
                                layer.msg("重置失败，密码格式错误");
                                break
                            case "5002":
                                layer.msg("重置失败，密码不能为空。");
                                break
                            case "5003":
                                layer.msg("重置失败，用户ID格式不正确");
                                break
                            case "5004":
                                layer.msg("重置失败，用户ID不能为空");
                                break
                            case "5005":
                                layer.msg("重置失败，确认密码不能为空");
                                break
                            case "5006":
                                layer.msg("重置失败，两个密码不匹配");
                                break
                            case "5007":
                                layer.msg("密码质量不符合要求");
                                break
                            case "5099":
                                layer.msg("系统异常，请联系管理员");
                                break
                        }

                    // }
                });
            }
        }
    });

    /**
     * to step2
     */
    function show2() {
        $(".step1").toggle()
        $(".step2").toggle()
    }

    /**
     * to step3
     */
    function show3() {
        $(".step2").toggle()
        $(".step3").toggle()
    }
</script>
</body>
</html>