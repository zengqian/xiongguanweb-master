<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>

<body>
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul ul_active">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')" class="active">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            %{--<a onclick="isReal('/user/queryOils')">--}%
                                %{--<li>--}%
                                    %{--加油卡充值--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/queryBill')" >--}%
                                %{--<li>--}%
                                    %{--缴费账单--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        </ul>
                    </li>
                    %{--<li>--}%
                        %{--<div class="li"><span>金融服务</span></div>--}%
                        %{--<ul class="in_ul">--}%
                            %{--<a onclick="isReal('/user/queryCardSiteList')">--}%
                                %{--<li>--}%
                                    %{--便民卡预约--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        %{--</ul>--}%
                    %{--</li>--}%
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul">
                            %{--<a onclick="isReal('/user/queryCredit')" >--}%
                                %{--<li>--}%
                                    %{--雄关信用--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/user/queryScore')" >--}%
                                %{--<li>--}%
                                    %{--百合积分--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="toast()">
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="toast()">
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- 水费缴纳content -->
    <div class="life_content not_life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">品质保证</label>
                <label class="main_color">安全快捷</label>
                <g:img dir="images" src="/life/2.png" class="fenge"/>
                <label class="small_fontsize">水费缴纳</label>
            </label>
            <a class="large_fontsize div_float_right main_color" href="/user/addWater">
                <label class="vertical_align">添加我的水费账户</label>
                <g:img dir="images" src="/life/3.png" class="vertical_align"/>
            </a>
        </div>

        <div>
            <g:if test="${data}">
                <ul class="water_ul height_lose aaa life_index_info">
                    <g:each in="${data}" var="item">
                        <li class="div_float_left life_index_li">
                            <a onclick="delWater(${item})" class="del_logo" title="删除"></a>
                            <div><label class="water_card">账户别名：</label><label
                                    class="gray_color">${item.alisName}</label></div>

                            <div><label class="water_card">户主：</label><label class="gray_color">${item.name}</label>
                            </div>

                            <div><label class="water_card">户号：</label><label class="gray_color">${item.feesNo}</label>
                            </div>

                            <form id="accountInfo" action="/user/queryWater" method="post">
                                <input type="hidden" value="${item.feesNo}" name="userNo"/>
                                <input type="hidden" value="${item.branchGname}" name="branchGname"/>
                                <input type="hidden" value="${item.name}" name="name"/>
                                <input type="hidden" value="${item.address}" name="address"/>
                                <input type="hidden" value="${item.branchGuid}" name="branchGuid"/>
                                <input type="hidden" value="${item.oderType}" name="odertype"/>
                                <input type="submit" class="see_account" value="查看账户"/>
                            </form>
                        </li>
                    </g:each>
                </ul>
            </g:if>
            <g:else>
                <h2 class="align_center margin_top_largemorest gray_color">您尚未绑定任何水费账户</h2>
            </g:else>
        </div>
    </div>
</div>
<script>
    function delWater(item){
        console.info(item);
        layer.confirm('确认删除该账户?', {
            btn: ['确认','取消'] //按钮
        }, function(index){
            layer.close(index);
            submitDel(item);
        }, function(){
        });

    }
    function  submitDel(item) {
        $.ajax({
            url: '/user/delWater',
            type: 'post',
            dataType: 'json',
            data: {
                'userNp': item.feesNo,
                'name': item.name
            },
            success: function (data) {
                console.info(data);
                if(data.errorcode==0){
                    layer.msg("删除成功");
                    isReal('/user/getWater');
                }else{
                    layer.msg(data.errormsg);
                }
            }
        })
    }
</script>

</body>
</html>