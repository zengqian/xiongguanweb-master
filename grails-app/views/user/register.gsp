<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userForgetLayout"/>
    <asset:javascript src="writeObject.js"/>
</head>
<body>
<!--中心内容-->
<div class="user_register_div">
    <div class="content_div">
        <div class="title_large_fontsize lt_gray_color">欢迎注册云上雄关</div>
        <form class="br_div register_form" id="register_form" action="">
            <div class="relative_position margin_register_top_first">
                <input class="btn_div form_input" placeholder="设置用户名" name="username"/>
                <div class="form_mess">
                    <ul>
                    <li>用户名长度为3-20个字符</li>
                    <li>支持字母（区分大小写）数字及下划线</li>
                    </ul>
                </div>
                <div class="form_mess">
                </div>
            </div>
            <div class="margin_form_middle relative_position input_div">
                <input class="btn_div" placeholder="设置您的登录密码" name="password1" id="password1" type="hidden"/>
                <script type="text/javascript">writePassObject("pass1",{"width":270,"height":22,"accepts":"[0-9a-zA-Z]+"});
                </script>
                <!--<span class="span_error" style="position: absolute;display: inline-table;line-height: 36px;color: red;margin-left: 10px"></span>-->
                <div class="form_mess">
                    <ul>
                        <li>密码长度为6-18个字符</li>
                        <li>支持字母（区分大小写）数字及下划线及特殊字符</li>
                    </ul>
                </div>
            </div>
            <label class="passcontrol_prompt" id="reset_pass1"></label>
            <div class="margin_form_middle relative_position input_div">
                <input class="btn_div form_input" placeholder="再次输入您的密码" name="password2" type="hidden" id="password2"/>
                <script type="text/javascript">writePassObject("pass2",{"width":270,"height":22,"accepts":"[0-9a-zA-Z-]+"});</script>
                <div class="form_mess">
                    <ul>
                        <li>请再次输入您的密码</li>
                    </ul>
                </div>
            </div>
            <label class="passcontrol_prompt" id="reset_pass2"></label>
            <div class="margin_form_middle">
                <input class="btn_div form_input" placeholder="输入手机号码" name="phone"/>
            </div>
            <div class="margin_form_middle relative_position">
                <input class="btn_div form_input" placeholder="输入短信验证码" maxlength="6" name="tellCode"/>
                <div class="get_mess_btn get_tell_code">获取短信验证码</div>
            </div>
            <div class="margin_form_middle relative_position">
                <input class="btn_div form_input imgCode" placeholder="输入图片验证码" maxlength="4" name="imgCode"/>
                <div class="get_mess_btn">
                    <img src="/user/verifyCode" id="verifyCodeImg"/>
                    %{--<g:img dir="images" src="/index/yanzm.png"/>--}%
                </div>
            </div>
            <div class="margin_form_middle height_lose checkbox_parent_div relative_position">
                <input type="checkbox" class="div_float_left checkbox_div" value="" name="check"/>
                <div class="checkbox_dailog ac">请勾选</div>
                <!--<div class="checkbox_div div_float_left"></div>-->
                <label class="div_float_left gray_color">我已阅读并同意"用户协议和隐私条款"</label>
            </div>
            <!-- 获取验证码接口参数 1002-注册 -->
            <input type="hidden" value="1002" name="interfaceCode">
            <!-- end -->
            <input type="submit" class="form_submit_btn margin_form_middle" value="注册"/>
        </form>
    </div>
</div>
</div>
<script>
    var ts = "<%=System.currentTimeMillis()%>";
    $("#register_form").validate({
        rules: {
            username: {
                required: true,
                rangelength: [3,20]
            },
            password2: {
                required: true,
                equalTo: "#password1"
            },
            phone: {
                required: true,
                isMobile: true
            },
            tellCode: {
                required: true
            },
            imgCode: {
                required: true,
                rangelength:[4,4],
                remote: {
                    url: "/user/checkVerifyCode",
                    type: "post",
                    data: {                     //要传递的数据
                        verifyCode: function () {
                            return $(".imgCode").val();
                        }
                    },
                    complete: function (data) {
                        //最后这里做返回处理，是否验证成功等
                        if (data.responseText == "false") {
                            layer.msg("验证码错误！");
                            $(".imgCode").val("");
                            $("#verifyCodeImg").attr("src", "user/verifyCode?number=" + Math.random());
                        }
                    }
                }
            },
            password1: {
                required: true,
                rangelength: [6,18],
                stringCheckPassword: true
            },
            check: {
                // required: true,
                isCheckk: true
            }
        },
        messages: {
            username: {
                required: "请输入用户名",
                rangelength: "用户名长度为3-20"
            },
            imgCode: {
                required: "请输入图片验证码",
                rangelength: "请输入四位验证码",
                remote: "验证码错误"
            },
            password2: {
                required: "请再次输入登录密码",
                equalTo: "两次密码不一致"
            },
            phone: {
                required: "请输入注册电话号码"
            },
            tellCode: {
                required: "请输入手机验证码"
            },
            password1: {
                required: "请输入登录密码",
                rangelength: "密码长度因为6-18"
            }
        },
        submitHandler: function (form) {
            //验证手机验证码
                    var password = getPassInput("pass1", ts, "reset_pass1", "密码输入错误：");
                    var password2 = getPassInput("pass2", ts, "reset_pass2", "密码输入错误：");
                    $("#password1").val(password)
                    $("#password2").val(password2)
                    console.log($("#password1").val())
            if($("#password1").val() && $("#password2").val() && $("#password1").val() == $("#password2").val()) {
                $.post("/msg/checkRegisterPhoneCode", {
                    "interfaceCode": $("input[name='interfaceCode']").val(),
                    "phone": $("input[name='phone']").val(),
                    "tellCode": $("input[name='tellCode']").val()
                }, function (json) {
                    if (json.code == 0) {
                        //注册
                        $.post("/user/userRegister", $("#register_form").serialize(), function (json) {
                            if (json.code == "1605") {
                                console.log(json.data)
                                saveuserinfo($("input[name='phone']").val(), password, json.data.userUuid)
                            } else {
                                $("#verifyCodeImg").attr("src", "user/verifyCode?number=" + Math.random());
                                switch (json.code) {
                                    case "1614":
                                        layer.msg("该账户已存在，请重新输入");
                                        break
                                    default:
                                        layer.msg("一户通错误");
                                        break
                                }
                            }
                        });
                    } else {
                        layer.msg(json.errormsg)
                    }
                });
            }else {
                        layer.msg("两次密码不一致")
            }
        }
    });

    function saveuserinfo(username,pwd,uuid) {
        $.post("/user/userRegisterSaveInfo", {loginName:username,pwd: pwd,uuid: uuid}, function (json) {
            if (json.errorcode == 0) {
                layer.msg("注册成功")
                setTimeout("$(location).attr('href','/login')", 3000 )
            } else {
                layer.msg("注册成功,信息保存失败")
            }
        });
    }
</script>
</body>
</html>