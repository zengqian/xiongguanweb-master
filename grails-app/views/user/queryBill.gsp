<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>
<body>
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul ul_active">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryOils')">
                                <li>
                                    加油卡充值
                                </li>
                            </a>
                            <a onclick="isReal('/queryBill')" class="active">
                                <li>
                                    缴费账单
                                </li>
                            </a>
                        </ul>
                    </li>
                    %{--<li>--}%
                        %{--<div class="li"><span>金融服务</span></div>--}%
                        %{--<ul class="in_ul">--}%
                            %{--<a onclick="isReal('/user/queryCardSiteList')">--}%
                                %{--<li>--}%
                                    %{--便民卡预约--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        %{--</ul>--}%
                    %{--</li>--}%
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul">
                            %{--<a onclick="isReal('/user/queryCredit')">--}%
                                %{--<li>--}%
                                    %{--雄关信用--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/user/queryScore')">--}%
                                %{--<li>--}%
                                    %{--百合积分--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="toast()">
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="toast()">
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- 水费缴纳content -->
    <div class="life_content relative_position">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">品质保证</label>
                <label class="main_color">安全快捷</label>
                <a class="small_fontsize gray_color mar_left">缴费账单</a>
            </label>
        </div>
        <form class="querybill_checkbox margin_top_large" id="query_form">
            <div>
                <label>缴费类型：</label>
                <label class="checkbox_label relative_position">
                    <input type="checkbox" id="checkedAlltype"/>
                    <label>全部</label>
                </label>
                <label class="checkbox_label relative_position">
                    <input type="checkbox" name="orderType" value="31001"/>
                    <label>水费</label>
                </label>
                %{--<label class="checkbox_label relative_position">--}%
                    %{--<input type="checkbox" name="orderType" value="电费"/>--}%
                    %{--<label>电费</label>--}%
                %{--</label>--}%
                <label class="checkbox_label relative_position">
                    <input type="checkbox" name="orderType" value="55001|55002|55003"/>
                    <label>燃气费</label>
                </label>
                %{--<label class="checkbox_label relative_position">--}%
                    %{--<input type="checkbox" name="orderType" value="取暖费"/>--}%
                    %{--<label>取暖费</label>--}%
                %{--</label>--}%
            </div>
            <div class="margin_form_middle">
                <label>缴费渠道：</label>
                <label class="checkbox_label relative_position">
                    <input type="checkbox" id="checkedAllsource"/>
                    <label>全部</label>
                </label>
                <label class="checkbox_label relative_position">
                    <input type="checkbox" name="orderSource" value="REG-02|REG-03"/>
                    <label>APP</label>
                </label>
                %{--<label class="checkbox_label relative_position">--}%
                    %{--<input type="checkbox" name="orderSource" value="REG-04"/>--}%
                    %{--<label>微信</label>--}%
                %{--</label>--}%
                <label class="checkbox_label relative_position">
                    <input type="checkbox" name="orderSource" value="REG-01"/>
                    <label>网站</label>
                </label>
            </div>
            <input type="submit" value="确认" class="form_submit_water br_div"/>
            <input id="total" type="hidden" name="total" />
            <input id="page" type="hidden" name="page" />
        </form>
        <div class="aaa" style="overflow: auto; height: 330px;">
            <table class="bill_table">

            </table>
        </div>
        <!-- 分页按钮 -->
        <div class="pagination">
            <button class="prev_page">上一页</button>

            <div class="number" id="number">
            </div>
            <button class="next_page">下一页</button>
            <button class="last_page">尾页</button>
        </div>
    </div>
</div>

<asset:javascript src="pagination.js"/>
<script>
    $(function () {
        //默认每页十条数据
        var total = $("#total").val(); //总条数
        var pagenumber = $("#page").val(); //页码
        $.firstinit(total, pagenumber, function () {
            console.log("点击之后执行方法");
            $.post('/user/queryBill', $("#query_form").serialize(),
                function (json) {
                    if(json.errorcode == 0){
                        $(".bill_table").html("");//查询之前先清空table
                        list  = json.data.list;
                        for(var i =0;i<list.length;i++){
                            switch (list[i].orderSource) {
                                case "REG-01":
                                    list[i].orderSource = "网站";
                                    break;
                                case "REG-02":
                                    list[i].orderSource = "APP";
                                    break;
                                case "REG-03":
                                    list[i].orderSource = "APP";
                                    break;
                                // case "REG-04":
                                //     list[i].orderSource = "微信";
                                //     break;
                            }
                            createtable(list[i].paymentTime,list[i].orderType,list[i].orderSource,list[i].bindInfo,list[i].payment);
                        }
                        $("#total").val(json.total);//设值总条数
                        $("#page").val(json.page);//设值页码
                        //alert("total:"+json.total+"page:"+json.page);
                        $.firstinit(json.total, json.page, function () { //默认每页十条数据
                            isSubmitBtnClick=false;//分页按钮点击设置值false
                            $("#query_form").submit();// 触发调用submitHandler
                        })
                    }else {
                        layer.msg(json.errormsg);
                    }
                });
        })
    });
</script>
<script>
    var list;
    var isSubmitBtnClick = true; //判断是否是提交按钮的点击
    $("#query_form").validate({
        submitHandler: function (form) {
            console.log($(this));
            /*if(isSubmitBtnClick){//提交按钮的点击，重置page为1
                $("#page").val(json.page);
            }*/
            $.post('/user/queryBill', $("#query_form").serialize(),
                function (json) {
                    if(json.errorcode == 0){
                        $(".bill_table").html("");//查询之前先清空table
                        list  = json.data.list;
                        for(var i =0;i<list.length;i++){
                            switch (list[i].orderSource) {
                                case "REG-01":
                                    list[i].orderSource = "网站";
                                    break;
                                case "REG-02":
                                    list[i].orderSource = "APP";
                                    break;
                                case "REG-03":
                                    list[i].orderSource = "APP";
                                    break;
                                // case "REG-04":
                                //     list[i].orderSource = "微信";
                                //     break;
                            }
                            createtable(list[i].paymentTime,list[i].orderType,list[i].orderSource,list[i].bindInfo,list[i].payment);
                        }
                        $("#total").val(json.total);//设值总条数
                        $("#page").val(json.page);//设值页码
                        //alert("total:"+json.total+"page:"+json.page);
                        $.firstinit(json.total, json.page, function () { //默认每页十条数据
                            isSubmitBtnClick=false;//分页按钮点击设置值false
                            $("#query_form").submit();// 触发调用submitHandler
                        })
                    }else {
                        layer.msg(json.errormsg);
                    }
                });



        }
    });
    //payTime支付时间
    // payType类型(燃气，水费。。)
    // payWay 支付渠道
    // myName 账户 (我的家|张三)
    // money 订单金额
    function createtable(payTime,payType,payWay,myName,money) {
        if($(".date_tr").last().text() !== payTime.split(" ")[0]){
            var tr = document.createElement("tr")
            tr.setAttribute("class", "date_tr")
            var tdd = document.createElement("td")
            tdd.setAttribute("class","date_tr_td")
            tdd.innerHTML = payTime.split(" ")[0]
            tr.appendChild(tdd)
            $(".bill_table").append(tr)
        }
        var tr1 = document.createElement("tr")
        var td1 = document.createElement("td")
        var td2 = document.createElement("td")
        var td3 = document.createElement("td")
        var td4 = document.createElement("td")
        var td5 = document.createElement("td")
        switch (payType) {
            case "31001":
                td1.innerHTML = "水费"
                tr1.setAttribute("class", "water_tr tr_info")
                break
            case "55001":
                td1.innerHTML = "燃气费"
                tr1.setAttribute("class", "gas_tr tr_info")
                break
            case "55002":
                td1.innerHTML = "燃气费"
                tr1.setAttribute("class", "gas_tr tr_info")
                break
            case "55003":
                td1.innerHTML = "燃气费"
                tr1.setAttribute("class", "gas_tr tr_info")
                break
            case 2:
                td1.innerHTML = "暖气费"
                tr1.setAttribute("class", "electric_tr tr_info")
                break
        }
        td2.setAttribute("class", "td_myhome")
        td2.innerHTML = myName
        td3.setAttribute("class", "td_payway")
        td3.innerHTML = payWay
        td4.setAttribute("class", "td_time")
        td4.innerHTML = payTime
        td5.innerHTML =money
        tr1.appendChild(td1)
        tr1.appendChild(td2)
        tr1.appendChild(td3)
        tr1.appendChild(td4)
        tr1.appendChild(td5)
        $(".bill_table").append(tr1)
    }

    $("#checkedAlltype").click(function() {
        if (this.checked) {
            $("[name=orderType]:checkbox").prop("checked", true);
        }else {
            $("[name=orderType]:checkbox").prop("checked", false);
        }
    });

    $("#checkedAllsource").click(function() {
        if (this.checked) {
            $("[name=orderSource]:checkbox").prop("checked", true);
        }else {
            $("[name=orderSource]:checkbox").prop("checked", false);
        }
    });
    $("[name=orderSource]:checkbox").click(function() {
        var checkedSum=0;
        //未选择，取消勾选“全部”
        if (!this.checked) {
            $("#checkedAllsource").prop("checked", false);
        }
        //遍历所有checkbox，若全选择择勾选"全部"
        $("input[name=orderSource]:checkbox").each(function(){
            if(this.checked)
            {
                checkedSum = checkedSum+1;
            }
        });
        if(checkedSum==$("input[name=orderSource]:checkbox").length){
            $("#checkedAllsource").prop("checked", true);
        }
    });
    $("[name=orderType]:checkbox").click(function() {
        var checkedSum=0;
        //未选择，取消勾选“全部”
        if (!this.checked) {
            $("#checkedAlltype").prop("checked", false);
        }
        //遍历所有checkbox，若全选择择勾选"全部"
        $("input[name=orderType]:checkbox").each(function(){
            if(this.checked)
            {
                checkedSum = checkedSum+1;
            }
        });
        if(checkedSum==$("input[name=orderType]:checkbox").length){
            $("#checkedAlltype").prop("checked", true);
        }
    });
    //监听复选框的变化，重置页码等参数
    $("input[type=checkbox]").change(function() {
        $("#page").val("");
        $("#total").val("");
    });

</script>
</body>
</html>
