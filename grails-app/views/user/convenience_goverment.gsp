<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta name="layout" content="convenienceCard"/>
</head>
<body>
<!--中心内容-->
<div class="about_card">
    <div>
        <label class="title_middle_fontsize">便民卡应用</label>
        <div class="title_to">
            <label>当前位置：</label>
            <a href="/convenience_card_application">资讯中心</a>
            <label>></label>
            <a href="/convenience_card_application" class="old_color">便民卡应用</a>
        </div>
        <div class="img_div"><g:img dir="images" src="/index/16.png"/></div>
    </div>
    <div class="margin_top_large align_center news_detail">
        <label class="title_large_fontsize convenience_title" id="qwer">政府公共服务应用</label>
        <p class="large_fontsize lt_gray_color margin_form_middle align_left">
            1、社会保障：全面实现养老、医疗、失业、工伤、生育等社会保险业务管理的信息交换、信息服务。</br>
        2、公积金：实现公积金查询，并能与其它相关部门的软件系统建立链接，查询企业信息数据。</br>
        3、残联：系统整合后便民卡具有有残疾人的身份认证功能。</br>
        </p>
        <label class="title_large_fontsize margin_form_middle convenience_title" id="yuio">公共事业应用介绍</label>
        <p class="large_fontsize lt_gray_color margin_form_middle align_left">
            1、水:<br>
            互联网卡表用户：采用系统对接模式，实现手机端缴费。<br>
            预充值卡表用户：采用系统对接，在社区布置自助终端的方式，实现IC卡充值业务。</br>
            2、电:</br>
            3、燃气:</br>
            互联网卡表用户：采用系统对接模式，实现手机端缴费。</br>
            预充值卡表用户：采用系统对接，在社区布置自助终端的方式，实现IC卡充值业务。</br>
            4、供热:采用系统对接模式，实现手机端缴费。</br>
            5、电话费:实现手机APP话费缴纳。</br>
            6、交通罚没:全市私人汽车保有量约55000辆，全年个人违章总数超过10万次。实现手机APP在线缴罚,不当停车，可通过APP、微信、短信、电话等方式联络和提醒车主通过与12123平台对接，实现在线处理违章、在线缴罚业务。</br>
            7、图书馆:采用便民实体卡+电子借书证，避免重复发卡。实现手机端查询借阅情况、书本分布情况、借阅到期提醒、逾期自动扣款。</br>
            8、博物馆:实现便民卡刷卡进馆，手机app扫码进馆。</br>
        </p>
        <label class="title_large_fontsize convenience_title" id="tgbn">金融应用介绍</label>
        <p class="large_fontsize lt_gray_color margin_form_middle align_left">
            1、实现便民卡在超市、药店、便利店、餐饮店、加油（气）站等小额支付网点进行金融消费。</br>
            一、2019年1月7日8:30兰州银行发售一款保本浮动收益型家计划系列理财产品，理财期限为91天,预期年化收益率4.15%,预期收益103.46元，募集规模1亿元，起购金额1万元，全渠道均可销售。</br>
            二、2019年1月14日8:30兰州银行发售一款保本浮动收益型家计划系列理财产品，理财期限为182天,预期年化收益率4.25%,预期收益211.91元，募集规模1亿元，起购金额1万元，全渠道均可销售。</br>
            三、2019年1月11日8:30兰州银行发售一款非保本浮动收益型稳健系列理财产品，理财期限为180天,预期年化收益率4.7%,预期收益231.78元，募集规模1亿元，起购金额1万元，全渠道均可销售。</br>
        </p>
        <label class="title_large_fontsize convenience_title" id="sefc">公共出行介绍</label>
        <p class="large_fontsize lt_gray_color margin_form_middle align_left">
            1、公交车：所有公交机具更换为多合一车载机，支持银联闪付、手机APP二维码支付、原公交卡刷卡支付，预留交通部全国互联互通接口，预留信用乘车接口，APP可支持线路查询，站点导航。</br>
            2、公共自行车：通过对公共自行车桩的改造及系统对接，实现刷卡支付、扫码支付、信用租车等多种支付手段，APP支持自行车站点查询、站点车辆查询、站点导航、保证金缴纳等功能。
            出租车：扫码支付、信用租车。</br>
        </p>
        <label class="title_large_fontsize convenience_title" id="ijud">旅游休闲应用介绍</label>
        <p class="large_fontsize lt_gray_color margin_form_middle align_left">
           1、市民可持便民卡到城楼、第一墩、悬臂长城等服务网点开通景区年卡功能。支持多种支付手段、手机APP端个人数据采集、银行电商平台合作。</br>
            2、全市统一的旅游年卡、支持多种支付手段、旅游宣传与活动推广、景点及周边介绍与定向导航 、手机APP端个人数据采集、景区人脸识别进入、银行电商平台合作。</br>
        </p>
        <label class="title_large_fontsize convenience_title" id="miyd">便民消费应用介绍</label>
        <p class="large_fontsize lt_gray_color margin_form_middle align_left">
            1、超市：实现pos机刷卡支付，手机APP扫码支付。</br>
            2、加油站和加气站：实现pos机刷便民卡支付，手机APP扫码支付。</br>
        </p>
        <label class="title_large_fontsize convenience_title" id="ygdb">企业一卡通应用介绍：重点介绍酒钢</label>
        <p class="large_fontsize lt_gray_color margin_form_middle align_left">
            1、门禁：酒钢集团门禁读卡器需要能够识别便民卡和虚拟卡（视当前读卡器支持程度和酒钢集团改造意向），门禁的管理采用原门禁系统。</br>
            2、考勤：酒钢集团考勤机需要能够识别便民卡和虚拟卡（视当前考勤机支持程度和酒钢集团改造意向），考勤的管理采用原考勤系统。</br>
            3、员工福利：酒钢集团每年为每位员工发放固定的生活补贴，定点消费可使用便民卡刷卡支付，手机APP扫码支付。</br>
        </p>
        <label class="title_large_fontsize convenience_title" id="yshb">雄关信用介绍</label>
        <p class="large_fontsize lt_gray_color margin_form_middle align_left">
            “雄关信用”是一个面向嘉峪关市民的社会信用服务综合应用体系。该体系主要通过社保公积金、房产、车辆、工资收入、运营商信息、银行资产负债等多维度数据信息，运用大数据、云计算及机器学习方法对个人进行全方位价值评价和信用测量，逐步构建嘉峪关市社会信用评估模型体系，为嘉峪关市民提供多种信用服务。
        </p>
    </div>
</div>
</body>
</html>