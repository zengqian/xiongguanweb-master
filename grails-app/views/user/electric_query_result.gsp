<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>
<body>
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul ul_active">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()" class="active">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryOils')">
                                <li>
                                    加油卡充值
                                </li>
                            </a>
                            <a onclick="isReal('/queryBill')" >
                                <li>
                                    缴费账单
                                </li>
                            </a>
                        </ul>
                    </li>
                    %{--<li>--}%
                        %{--<div class="li"><span>金融服务</span></div>--}%
                        %{--<ul class="in_ul">--}%
                            %{--<a onclick="isReal('/user/queryCardSiteList')">--}%
                                %{--<li>--}%
                                    %{--便民卡预约--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        %{--</ul>--}%
                    %{--</li>--}%
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul">
                            %{--<a onclick="isReal('/user/queryCredit')" >--}%
                                %{--<li>--}%
                                    %{--雄关信用--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/user/queryScore')" >--}%
                                %{--<li>--}%
                                    %{--百合积分--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a  onclick="isReal('/user/getLibrary')" >
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryFund')" >
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- 水费缴纳content -->
    <div class="life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">品质保证</label>
                <label class="main_color">安全快捷</label>
                <label class="small_fontsize gray_color mar_left">电费缴纳</label>
                <g:img dir="images" src="/index/20.png"/>
                <label class="small_fontsize">户号查询结果</label>
            </label>
        </div>
        <div>
            <div style="color:red;" class="margin_form_middle">* <label class="lt_gray_color">共计查到2条户号记录</label></div>
            <div class="height_lose aaa query_result_height">
                <div class="br_div water_query_result_div life_index_li">
                    <div class="water_query_result">
                        <div class="margin_top_middle"><label class="water_card water_query_result_title">缴费单位：</label><label class="gray_color div_float_right water_query_result_content">电力公司1</label></div>
                        <div class="margin_form_middle"><label class="water_card water_query_result_title">户号：</label><label class="gray_color div_float_right water_query_result_content">230230230230</label></div>
                        <div class="margin_form_middle"><label class="water_card water_query_result_title">户主姓名：</label><label class="gray_color div_float_right water_query_result_content">吴彦祖</label></div>
                        <div class="margin_form_middle"><label class="water_card water_query_result_title">户主身份证号码：</label><label
                                class="gray_color div_float_right water_query_result_content">230227199205132816</label></div>
                        <div class="margin_form_middle"><label class="water_card water_query_result_title">地址：</label><label
                                class="gray_color div_float_right water_query_result_content">嘉峪关市xxx小区xxx栋xxx单元xxxx室</label></div>
                    </div>
                    <a class="water_query_result_btn" href="#">绑定此户号</a>
                </div>
                <div class="br_div water_query_result_div life_index_li">
                    <div class="water_query_result">
                        <div class="margin_top_middle"><label class="water_card water_query_result_title">缴费单位：</label><label class="gray_color div_float_right water_query_result_content">电力公司1</label></div>
                        <div class="margin_form_middle"><label class="water_card water_query_result_title">户号：</label><label class="gray_color div_float_right water_query_result_content">230230230230</label></div>
                        <div class="margin_form_middle"><label class="water_card water_query_result_title">户主姓名：</label><label class="gray_color div_float_right water_query_result_content">吴彦祖</label></div>
                        <div class="margin_form_middle"><label class="water_card water_query_result_title">户主身份证号码：</label><label
                                class="gray_color div_float_right water_query_result_content">230227199205132816</label></div>
                        <div class="margin_form_middle"><label class="water_card water_query_result_title">地址：</label><label
                                class="gray_color div_float_right water_query_result_content">嘉峪关市xxx小区xxx栋xxx单元xxxx室</label></div>
                    </div>
                    <a class="water_query_result_btn" href="#">绑定此户号</a>
                </div>
                <div class="br_div water_query_result_div life_index_li">
                    <div class="water_query_result">
                        <div class="margin_top_middle"><label class="water_card water_query_result_title">缴费单位：</label><label class="gray_color div_float_right water_query_result_content">电力公司1</label></div>
                        <div class="margin_form_middle"><label class="water_card water_query_result_title">户号：</label><label class="gray_color div_float_right water_query_result_content">230230230230</label></div>
                        <div class="margin_form_middle"><label class="water_card water_query_result_title">户主姓名：</label><label class="gray_color div_float_right water_query_result_content">吴彦祖</label></div>
                        <div class="margin_form_middle"><label class="water_card water_query_result_title">户主身份证号码：</label><label
                                class="gray_color div_float_right water_query_result_content">230227199205132816</label></div>
                        <div class="margin_form_middle"><label class="water_card water_query_result_title">地址：</label><label
                                class="gray_color div_float_right water_query_result_content">嘉峪关市xxx小区xxx栋xxx单元xxxx室</label></div>
                    </div>
                    <a class="water_query_result_btn" href="#">绑定此户号</a>
                </div>
                <div class="br_div water_query_result_div life_index_li">
                    <div class="water_query_result">
                        <div class="margin_top_middle"><label class="water_card water_query_result_title">缴费单位：</label><label class="gray_color div_float_right water_query_result_content">电力公司1</label></div>
                        <div class="margin_form_middle"><label class="water_card water_query_result_title">户号：</label><label class="gray_color div_float_right water_query_result_content">230230230230</label></div>
                        <div class="margin_form_middle"><label class="water_card water_query_result_title">户主姓名：</label><label class="gray_color div_float_right water_query_result_content">吴彦祖</label></div>
                        <div class="margin_form_middle"><label class="water_card water_query_result_title">户主身份证号码：</label><label
                                class="gray_color div_float_right water_query_result_content">230227199205132816</label></div>
                        <div class="margin_form_middle"><label class="water_card water_query_result_title">地址：</label><label
                                class="gray_color div_float_right water_query_result_content">嘉峪关市xxx小区xxx栋xxx单元xxxx室</label></div>
                    </div>
                    <a class="water_query_result_btn" href="#">绑定此户号</a>
                </div>
            </div>

        </div>
    </div>
</div>
</body>
</html>