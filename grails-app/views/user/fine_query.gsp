<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>
<body>
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul ul_active">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')" class="active">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            %{--<a onclick="isReal('/user/queryOils')">--}%
                                %{--<li>--}%
                                    %{--加油卡充值--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/queryBill')" >--}%
                                %{--<li>--}%
                                    %{--缴费账单--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        </ul>
                    </li>
                    %{--<li>--}%
                        %{--<div class="li"><span>金融服务</span></div>--}%
                        %{--<ul class="in_ul">--}%
                            %{--<a onclick="isReal('/user/queryCardSiteList')">--}%
                                %{--<li>--}%
                                    %{--便民卡预约--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        %{--</ul>--}%
                    %{--</li>--}%
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul">
                            <a onclick="toast()" >
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="toast()" >
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- 水费缴纳content -->
    <div class="life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">品质保证</label>
                <label class="main_color">安全快捷</label>
                <g:img dir="images" src="/life/2.png" class="fenge"/>
                <label class="small_fontsize">交通违章</label>
            </label>
        </div>
        <div>
            <form id="fine_query_form" class="add_water" method="post" action="/user/toFineInfo">
                <div class="margin_top_large">
                    <label class="large_fontsize input_title">罚款单编号:</label>
                    <input placeholder="请输入罚款单编号" name="fineNo" class="form_input btn_div"/>
                </div>
                <input type="submit" value="查询" class="form_submit_water margin_top_largemore"/>
                %{-- PaySnglNo缴款单号 RltnpNm 当事人 PltNo车牌号 TotAmt罚款总金额 PylAmt罚款金额 LtPymtAmt滞纳金--}%
                <input type="hidden" name="PaySnglNo" id="PaySnglNo" />
                <input type="hidden" name="RltnpNm" id="RltnpNm"/>
                <input type="hidden" name="PltNo"  id="PltNo" />
                <input type="hidden" name="TotAmt" id="TotAmt"/>
                <input type="hidden" name="PylAmt" id="PylAmt"/>
                <input type="hidden" name="LtPymtAmt" id="LtPymtAmt"/>
                <input type="hidden" name="PaySnglTyp" id="PaySnglTyp"/>
                <input type="hidden" name="DrvsLcnsNo" id="DrvsLcnsNo"/>
                <input type="hidden" name="FileNo" id="FileNo"/>
                <input type="hidden" name="AtmbLicTp" id="AtmbLicTp"/>
                <input type="hidden" name="DealYr" id="DealYr"/>
                <input type="hidden" name="DealTm" id="DealTm"/>
                <input type="hidden" name="ExecUnitNm" id="ExecUnitNm"/>
                <input type="hidden" name="ExecInstNm" id="ExecInstNm"/>
                <input type="hidden" name="PyeInstCd" id="PyeInstCd"/>
            </form>
        </div>
    </div>
</div>
<script>
    $("#fine_query_form").validate({
        // onkeyup: false,
        rules: {
            fineNo: {
                required: true,
                maxlength: 16,
                minlength: 16,
                Number: true
            }
        },
        messages: {
            fineNo: {
                required: "请输入罚单编号",
                maxlength: "罚单编号长度16位,请正确输入",
                minlength: "罚单编号长度16位,请正确输入"
            }
        },
        submitHandler: function (form) {
            $.post('/user/queryFine', $("#fine_query_form").serialize(),
                function (json) {
                    if(json.errorcode == 0){
                        $("#PaySnglNo").val(json.data.PaySnglNo);
                        $("#RltnpNm").val(json.data.RltnpNm);
                        $("#PltNo").val(json.data.PltNo);
                        $("#TotAmt").val(json.data.TotAmt);
                        $("#PylAmt").val(json.data.PylAmt);
                        $("#LtPymtAmt").val(json.data.LtPymtAmt);
                        $("#PaySnglTyp").val(json.data.PaySnglTyp);
                        $("#DrvsLcnsNo").val(json.data.DrvsLcnsNo);
                        $("#FileNo").val(json.data.FileNo);
                        $("#AtmbLicTp").val(json.data.AtmbLicTp);
                        $("#DealYr").val(json.data.DealYr);
                        $("#DealTm").val(json.data.DealTm);
                        $("#ExecUnitNm").val(json.data.ExecUnitNm);
                        $("#ExecInstNm").val(json.data.ExecInstNm);
                        $("#PyeInstCd").val(json.data.PyeInstCd);
                        form.submit();
                    }else {
                        layer.msg(json.errormsg);
                    }
                });
        }
    });
</script>
</body>
</html>