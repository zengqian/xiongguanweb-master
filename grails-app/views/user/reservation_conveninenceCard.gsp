<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>

<body>
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryOils')">
                                <li>
                                    加油卡充值
                                </li>
                            </a>
                            <a onclick="isReal('/queryBill')">
                                <li>
                                    缴费账单
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span>金融服务</span></div>
                        <ul class="in_ul ul_active">
                            <a onclick="isReal('/user/queryCardSiteList')" class="active">
                                <li>
                                    便民卡预约
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul">
                            <a onclick="isReal('/user/queryCredit')">
                                <li>
                                    雄关信用
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryScore')">
                                <li>
                                    百合积分
                                </li>
                            </a>
                            <a onclick="isReal('/user/getLibrary')">
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryFund')">
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- 水费缴纳content -->
    <div class="life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">品质保证</label>
                <label class="main_color">安全快捷</label>
                <a class="small_fontsize gray_color mar_left">便民卡预约</a>
            </label>
            <a class="div_float_right lt_gray_color" onclick="isReal('/user/reservationRecord')">预约记录</a>
        </div>

        <div>
            <form class="add_water br_div" id="reservarion_form">
                <div class="margin_top_large">
                    <label class="large_fontsize input_title">姓名:</label>
                    <input placeholder="请输入真实姓名" class="form_input btn_div" name="username"/>
                </div>

                <div class="margin_top_large">
                    <label class="large_fontsize input_title">身份证号:</label>
                    <input placeholder="请输入身份证号码" class="form_input btn_div" name="certno"/>
                </div>

                <div class="margin_top_large">
                    <label class="large_fontsize input_title">办理网点:</label>
                    <select class="form_input btn_div" name="branchid">
                        <g:each in="${data}">
                            <option value="${it.branchId}">${it.branchName}</option>
                        </g:each>
                    </select>
                </div>

                <div class="margin_top_large">
                    <label class="large_fontsize input_title">手机号码:</label>
                    <input placeholder="请输入手机号码" class="form_input btn_div" name="phone" id="mobile"/>
                </div>

                <div class="margin_top_large relative_position reservation_conveninence_div">
                    <label class="large_fontsize input_title">短信验证码:</label>

                    <div class="br_div relative_position">
                        <input placeholder="请输入短信验证码" class="form_input btn_div" name="code" id="code" maxlength="6"/>

                        <div class="get_mess_btn get_tell_code">获取短信验证码</div>
                    </div>
                </div>
                <input type="hidden" value="2044" name="interfaceCode"/>
                <input type="submit" value="确认预约" class="form_submit_water margin_top_largemore"/>
            </form>
        </div>
    </div>
</div>

<script>
    $("#reservarion_form").validate({
        rules: {
            username: {
                required: true,
                chinese: true,
                rangelength: [2, 4]
            },
            phone: {
                required: true,
                isMobile: true
            },
            certno: {
                required: true,
                isIdCardNo: true
            },
            branchid: {
                required: true
            },
            code: {
                required: true
            }
        },
        messages: {
            username: {
                required: "请输入您的姓名",
                rangelength: "姓名长度应该为2-4位"
            },
            certno: {
                required: "请输入您的身份证号码"
            },
            branchid: {
                required: "请选择您的办理网点"
            },
            code: {
                required: "请输入验证码"
            },
            phone: {
                required: "请输入正确的手机号码"
            }
        },
        submitHandler: function (form) {
            $.post('/user/reservationCard', $("form").serialize(),
                function (json) {
                    if (json.errorcode == 0) {//
                        layer.msg("预约成功");
                        isReal("/user/reservationRecord");
                    } else {
                        layer.msg(json.errormsg)
                    }
                })
        }
    });
    //获取短信验证码
    // function getAuthCode() {
    //     var mobile = $("#mobile").val();
    //     $.post('/user/getAuthCode', {mobile:mobile},
    //         function (json) {
    //             if(json.errorcode == 0){
    //
    //             }else {
    //                 layer.msg("验证码获取异常")
    //             }
    //         })
    // }
</script>
</body>
</html>