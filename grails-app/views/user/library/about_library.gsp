<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="convenienceCard"/>
</head>
<body class="fff_body">
<!--中心内容-->
<div class="about_card">
    <div style="padding-bottom: 6px;">
        <a class="small_fontsize main_color" onclick="isReal('/user/getLibrary')"><g:img dir="images" src="/life/23.png" class="vertical_align" style="margin-right: 6px"/>返回</a>
        <div class="title_to">
            <label>当前位置：</label>
            <a onclick="isReal('/user/getLibrary')">嘉峪关市图书馆</a>
            <label>></label>
            <a class="old_color">关于嘉图</a>
        </div>
    </div>
    <div class="margin_top_large align_center news_detail about_library">
        <h2 class="main_title">厉害了！带你去看最全面的嘉峪关市图书馆</h2>
        <g:img dir="images" src="/life/18.png" class="margin_top_largemore"/>
        <p class="margin_top_large align_left">嘉峪关市图书馆建筑面积10858平方米，2015年9月正式对外开放，成为面向全社会的大型综合性公共图书馆。新馆总藏书规模约34万册，阅览休闲坐席1200余个，实现无线有线网络全覆盖。以数字图书馆为基础，采用藏、借、阅、查、展一体的新型服务模式，收集了7000余册具有地方特色的珍贵的历史文化典籍，形成了较为浓厚的地方藏书特色，开设红色主题馆、无障碍阅览室、文史资料专馆和方志馆等特色阅览室。全馆分布11个对外服务窗口，实施文献开架、借阅一体化的开放型管理模式，为读者提供读者服务、参考咨询、电子文献检索、网上预约续借等基本功能。除此之外，数字图书馆还会将15万册超星电子图书、CNKI中国知网期刊报纸以及少儿“爱不释书”3D动画课堂等特色数字图书资源在网上推荐阅读。</p>
        <g:img dir="images" src="/life/20.png" class="margin_top_largemore"/>
        <h2 class="margin_top_large">中央检索大厅</h2>
        <p class="align_left">大厅中央设立人工服务台，为到馆读者提供综合业务办理工作，解答读者的一般咨询，辅导查找馆藏，同时提供读者打印、复印等服务。大厅四周设置了触摸屏一体式电子检索设备、在线读报机、期刊阅览机和一台自助办证查询机，读者到馆可以利用免费Wi-Fi，下载手机客户端，在线查阅最新的报纸信息，享受“掌上图书馆”悦读体验。</p>
        <h2 class="margin_top_large">主题摄影展&nbsp;展厅</h2>
        <p class="align_left">为迎接2018年嘉峪关市“全民读书月”的到来，弘扬尊重知识、崇尚文明的阅读理念，由嘉峪关市文广新局主办，市图书馆和市摄影家协会联合承办的“阅读•让生活更美好”主题摄影展在这里展出。活动自今年3月中旬启动，短短一个多月的时间内，共征集原创摄影作品近300幅，经组委会反复甄别，共评选90件优秀作品展出。这些作品通过摄影家独特的视角，生动形象地展现我市广大群众日常和谐的文化生活，折射出丰富多彩的阅读活动中市民良好的精神风貌。</p>
        <h2 class="margin_top_large">文化艺术展览馆</h2>
        <g:img dir="images" src="/life/19.png" class="margin_top_largemore"/>
        <p class="align_left">本馆主要展示优秀绘画、书法、摄影等艺术作品。本期展品以嘉峪关市新城魏晋墓中出土的大量的画像砖为素材，将这些画像砖以绘画的形式呈现在市民面前，充分表达魏晋时期艺术风格的豪放雄健。作品真实广泛地反映了当时生活在这里古代民族的现实生活，内容有农桑、畜牧、酿造、驿传、营垒、狩猎等，涉及政治、经济、军事、文化和阶级关系、民族关系等诸多方面。</p>
        <h2 class="margin_top_large">红色主题馆</h2>
        <g:img dir="images" src="/life/21.png" class="margin_top_largemore"/>
        <p class="align_left">收藏800余册红色经典书籍，自主题馆建成开放以来，开展各类红色经典阅读推广活动近30场次。馆内分为党的光辉历程、党的主题教育实践活动导览、社会主义核心价值体系、嘉峪关图书馆悦分享文学经典阅读书目展和图书馆党支部风采展示五个模块,通过专架集中陈列经典红色书目和图片展示的形式,让读者全面了解中国共产党的发展历程和领导中国的建设成就,学习先辈们的光辉业绩,继承和发扬光荣传统,坚定理想信念。</p>
        <h2 class="margin_top_large">图书捐赠交换中心</h2>
        <p class="align_left">图书捐赠交换中心旨在推动民间的图书流动，凝聚图书馆、媒体与社会力量，共同搭建全民阅读资源公共服务平台，倡导图书捐赠、分享的文化交流理念，面向全体市民免费提供捐赠及换书服务，营造“全民阅读”的良好氛围。</p>
        <h2 class="margin_top_large">报刊阅览室</h2>
        <g:img dir="images" src="/life/22.png" class="margin_top_largemore"/>
        <p class="align_left">划分为报纸阅览区和期刊阅览区两部分。期刊报纸阅览范围随着图书馆的发展而不断扩大，订有报纸100余种，杂志400余种，涉及政治、文学、经济、法律、科技等各门学科，种类齐全，内容广泛。室内卫生整洁、光线充足，设有68个阅览坐席，为读者提供温馨、舒适的阅读空间。</p>
        <h2 class="margin_top_large">珍善本书库</h2>
        <p class="align_left">1980年，在市委、市政府的支持下，购买了这套《四库全书》影印本，收藏时间近40年，已成为我馆的“镇馆之宝”。同时，图书馆还陆续购置与之配套的《四库全书荟要》、《四库全书总目》等书籍，共有1850册</p>
    </div>
</div>
</body>
</html>