<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>
<body>
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryOils')">
                                <li>
                                    加油卡充值
                                </li>
                            </a>
                            <a onclick="isReal('/queryBill')" >
                                <li>
                                    缴费账单
                                </li>
                            </a>
                        </ul>
                    </li>
                    %{--<li>--}%
                        %{--<div class="li"><span>金融服务</span></div>--}%
                        %{--<ul class="in_ul">--}%
                            %{--<a onclick="isReal('/user/queryCardSiteList')">--}%
                                %{--<li>--}%
                                    %{--便民卡预约--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        %{--</ul>--}%
                    %{--</li>--}%
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul ul_active">
                            %{--<a onclick="isReal('/user/queryCredit')" >--}%
                                %{--<li>--}%
                                    %{--雄关信用--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/user/queryScore')" >--}%
                                %{--<li>--}%
                                    %{--百合积分--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="toast()" class="active">
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryFund')" >
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- 水费缴纳content -->
    <div class="life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">嘉图书馆</label>
                <label class="main_color">博览众长</label>
                <a class="small_fontsize gray_color mar_left"  onclick="isReal('/user/getLibrary')" >嘉峪关市图书馆</a>
                <g:img dir="images" src="/index/20.png"/>
                <label class="small_fontsize">欠费信息</label>
            </label>
        </div>
        <g:if test="${data}">
            <g:each in="${data}">
                <g:if test="${it.type == '001'}">
                    <div class="br_div margin_top_large arrearage_info_div">
                        <div class="arrearage_div3">欠费金额：${it.amt}元</div>
                        <a onclick="pay()" class="arrearage_a form_submit_water">去缴纳</a>
                    </div>
                    <form id="payForm">
                        <input type="hidden" name="type" value="${it.type}"/>
                        <input type="hidden" name="amt" value="${it.amt}"/>
                    </form>
                </g:if>
                <g:elseif test="${it.type == '002'}">
                    <div class="br_div margin_top_large arrearage_info_div">
                        <div class="arrearage_div1">欠费金额：${it.amt}元</div>
                        <a onclick="pay()" class="arrearage_a form_submit_water">去缴纳</a>
                    </div>
                    <form id="payForm">
                        <input type="hidden" name="type" value="${it.type}"/>
                        <input type="hidden" name="amt" value="${it.amt}"/>
                    </form>
                </g:elseif>
                <g:elseif test="${it.type == '003'}">
                    <div class="br_div margin_top_large arrearage_info_div">
                        <div class="arrearage_div2">欠费金额：${it.amt}元</div>
                        <a onclick="pay()" class="arrearage_a form_submit_water">去缴纳</a>
                    </div>
                    <form id="payForm">
                        <input type="hidden" name="type" value="${it.type}"/>
                        <input type="hidden" name="amt" value="${it.amt}"/>
                    </form>
                </g:elseif>
                <g:else test="${it.type == '004'}">
                    <div class="br_div margin_top_large arrearage_info_div">
                        <div class="arrearage_div4">欠费金额：${it.amt}元</div>
                        <a onclick="pay()" class="arrearage_a form_submit_water">去缴纳</a>
                    </div>
                    <form id="payForm">
                        <input type="hidden" name="type" value="${it.type}"/>
                        <input type="hidden" name="amt" value="${it.amt}"/>
                    </form>
                </g:else>
            </g:each>
        </g:if>
        <g:else>
            <h2 class="align_center margin_top_largemorest gray_color">您没有欠费记录</h2>
        </g:else>
    </div>
</div>
<script>
    //缴纳押金
    function pay() {
        $.post('/user/payArrearage',$("form").serialize(),
            function (json) {
                if (json.errorcode == 0) {
                    window.location.href = "https://etest.lzbank.com:20028/ccweb/samlsso?ChangeVal=mainPageSAML&spid=p3depay&method="+json.method;
                } else {
                    layer.msg(json.errormsg);
                }
            });
    }
</script>
</body>
</html>