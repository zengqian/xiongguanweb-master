<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>
<body>
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryOils')">
                                <li>
                                    加油卡充值
                                </li>
                            </a>
                            <a onclick="isReal('/queryBill')" >
                                <li>
                                    缴费账单
                                </li>
                            </a>
                        </ul>
                    </li>
                    %{--<li>--}%
                        %{--<div class="li"><span>金融服务</span></div>--}%
                        %{--<ul class="in_ul">--}%
                            %{--<a onclick="isReal('/user/queryCardSiteList')">--}%
                                %{--<li>--}%
                                    %{--便民卡预约--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        %{--</ul>--}%
                    %{--</li>--}%
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul ul_active">
                            <a onclick="isReal('/user/queryCredit')" >
                                <li>
                                    雄关信用
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryScore')" >
                                <li>
                                    百合积分
                                </li>
                            </a>
                            <a onclick="isReal('/user/getLibrary')" class="active">
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryFund')" >
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- 个人中心content -->
    <div class="">
        <div class="library_div height_lose relative_position">
            <div class="library_top_div">
                <a class="first" onclick="isReal('/user/queryMyLibCard')">
                    <g:img dir="images" src="/life/library_card.png"/>
                    <label>我的借阅证</label>
                </a>
                <a onclick="active()">
                    <g:img dir="images" src="/life/library_jihuo.png"/>
                    <label>激活</label>
                </a>
                <!--已缴纳-->
                <a onclick="pay()">
                    <g:img dir="images" src="/life/library_jiao.png"/>
                    <label>缴押金</label>
                </a>
                <!--end-->
                <!--未缴纳-->
                %{--<a href="/deposit">--}%
                    %{--<g:img dir="images" src="/life/library_jiao.png"/>--}%
                    %{--<label>缴押金</label>--}%
                %{--</a>--}%
                <!--end-->
                <a onclick="refund()">
                    <g:img dir="images" src="/life/library_tui.png"/>
                    <label>退押金</label>
                </a>
            </div>
        </div>
        <div class="library_bottm_div height_lose">
            <a onclick="isReal('/user/querylibReadInfo')">
                <div>
                    借阅信息
                </div>
                <div>
                    轻松查看借阅书籍状态
                </div>
            </a>
            <a onclick="arrearage()">
                <div>
                    欠费信息
                </div>
                <div>
                    欠费详情快速查阅
                </div>
            </a>
            <a href="/about_library">
                <div>
                    关于嘉图
                </div>
                <div>
                    更多了解嘉图相关规定
                </div>
            </a>
            <a href="/need_know">
                <div>
                    读者须知
                </div>
                <div>
                    更多了解嘉图相关规定
                </div>
            </a>
            <a class="last" href="/open_time">
                <div>
                    开馆时间
                </div>
                <div>
                    嘉峪关市图书时间表
                </div>
            </a>
        </div>
    </div>
    <g:if test="${data}">
        <form id="lib_form">
            <input type="hidden" name="isActive" id="isActive" value="${data.isActive}"/>
            <input type="hidden" name="hasPayment" value="${data.hasPayment}"/>
            <input type="hidden" name="hasOrder" value="${data.hasOrder}"/>
            <input type="hidden" name="orderId" value="${data.order_id}"/>
            <input type="hidden" name="amt" value="${data.amt}"/>
        </form>
    </g:if>
</div>
<script>
    //押金退款
    function refund() {
        $.post('/user/judgeLibrary',{},
            function (json) {
                if (json.errorcode == 0) {
                    if(json.isActive!=1){//0未激活1激活
                        //layer.msg("您尚未激活图书管应用");
                        isReal("/card_activate");//直接跳转至激活页面
                        return;
                    }
                    if(json.hasPayment!=1){//0未激活1激活
                        layer.msg("您尚未缴纳过押金");
                        return;
                    }
                    layer.confirm('该操作将为您退还图书馆押金', {
                        btn: ['确认','取消'] //按钮
                    }, function(index){
                        layer.close(index);
                        $.post('/user/refundLibrary',$("form").serialize(),
                            function (json) {
                                if (json.code == 0) {
                                   //跳转百合易付
                                    window.location.href = "https://etest.lzbank.com:20028/ccweb/samlsso?ChangeVal=mainPageSAML&spid=p3depay&method="+json.method
                                } else {
                                    layer.msg(json.errormsg);
                                }
                            });
                    });
                } else {
                    layer.msg(json.errormsg);
                }
            });
    }
    //缴纳押金
    function pay() {
        $.post('/user/judgeLibrary',{},
            function (json) {
                if (json.errorcode == 0) {
                    if(json.isActive!=1){//0未激活1激活
                        //layer.msg("您尚未激活图书管应用");
                        isReal("/card_activate");//直接跳转至激活页面
                        return;
                    }
                    if(json.hasPayment==1){
                        layer.msg("您已缴纳过押金");
                    }else{
                        isReal("/deposit");//跳转至缴费页面
                    }
                } else {
                    layer.msg(json.errormsg);
                }
            });
    }
    function  active() {
        var isActive = $("#isActive").val();
        if(isActive==1){
            layer.msg("您已激活图书馆应用，不能重复激活");
            return;
        }
        isReal("/card_activate");
    }
    function arrearage() {
        var isActive = $("#isActive").val();
        if(isActive==0){
            isReal("/card_activate");
            return;
        }
        isReal("/user/arrearageLibrary");
    }
</script>
</body>
</html>