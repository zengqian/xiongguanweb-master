<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>
<body>
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryOils')">
                                <li>
                                    加油卡充值
                                </li>
                            </a>
                            <a onclick="isReal('/queryBill')" >
                                <li>
                                    缴费账单
                                </li>
                            </a>
                        </ul>
                    </li>
                    %{--<li>--}%
                        %{--<div class="li"><span>金融服务</span></div>--}%
                        %{--<ul class="in_ul">--}%
                            %{--<a onclick="isReal('/user/queryCardSiteList')">--}%
                                %{--<li>--}%
                                    %{--便民卡预约--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        %{--</ul>--}%
                    %{--</li>--}%
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul ul_active">
                            <a onclick="isReal('/user/queryCredit')" >
                                <li>
                                    雄关信用
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryScore')" >
                                <li>
                                    百合积分
                                </li>
                            </a>
                            <a  onclick="isReal('/user/getLibrary')"  class="active">
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryFund')" >
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- 水费缴纳content -->
    <div class="life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">嘉图书馆</label>
                <label class="main_color">博览众长</label>
                <a class="small_fontsize gray_color mar_left"  onclick="isReal('/user/getLibrary')" >嘉峪关市图书馆</a>
                <g:img dir="images" src="/index/20.png"/>
                <label class="small_fontsize">读者须知</label>
            </label>
        </div>
        <div class="small_fontsize margin_form_middle">
            <div>
                一、书目查询
            </div>
            <div class="gray_color margin_top_middle">读者可到图书馆主馆检索大厅及各阅览室查询公共图书馆的书目信息，检索所需文献。</div>
        </div>
        <div class="small_fontsize margin_form_middle">
            <div>
                二、外借租金
            </div>
            <div class="gray_color margin_top_middle">100元读者卡押金-可借中文图书1-3册，借期30天 续借15/次，可续借两次。</div>
        </div>
        <div class="small_fontsize margin_form_middle">
            <div>
                三、借还服务
            </div>
            <div class="gray_color margin_top_middle">读者已开通外借服务功能的读者卡，可在图书馆南市区主馆各阅览室自主借阅机借还图书。</div>
        </div>
        <div class="small_fontsize margin_form_middle">
            <div>
                四、逾期滞还费及缴纳方式
            </div>
            <div class="gray_color margin_top_middle">逾期归还所借图书，每册收取0.1/天滞还费。</div>
        </div>
        <div class="small_fontsize margin_form_middle" style="line-height: 24px;">
            <div>
                五、污损及遗失图书的赔偿
            </div>
            <div class="gray_color margin_top_middle">污损图书由工作人员视损坏情况酌情核定赔偿金额，污损严重者，按遗失文献处理。</div>
            <div class="gray_color">遗失图书，读者需进行赔偿，遗失普通文献按原价2倍赔偿，遗失整套文献的任一分册按遗失文献原价3-5倍赔偿。</div>
            <div class="gray_color">读者亦可自购相同版本的正版图书归还图书馆，并缴纳文献加工费5元/册。</div>
            <div class="gray_color">遗失图书累计达5册将被暂停外借文献权限180天。</div>
        </div>
    </div>
</div>
</body>
</html>