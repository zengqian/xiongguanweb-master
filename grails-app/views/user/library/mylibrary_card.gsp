<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>

<body>
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryOils')">
                                <li>
                                    加油卡充值
                                </li>
                            </a>
                            <a onclick="isReal('/queryBill')">
                                <li>
                                    缴费账单
                                </li>
                            </a>
                        </ul>
                    </li>
                    %{--<li>--}%
                        %{--<div class="li"><span>金融服务</span></div>--}%
                        %{--<ul class="in_ul">--}%
                            %{--<a onclick="isReal('/user/queryCardSiteList')">--}%
                                %{--<li>--}%
                                    %{--便民卡预约--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        %{--</ul>--}%
                    %{--</li>--}%
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul ul_active">
                            <a onclick="isReal('/user/queryCredit')">
                                <li>
                                    雄关信用
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryScore')">
                                <li>
                                    百合积分
                                </li>
                            </a>
                            <a onclick="isReal('/user/getLibrary')" class="active">
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryFund')">
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- content -->
    <div class="life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">嘉图书馆</label>
                <label class="main_color">博览众长</label>
                <a class="small_fontsize gray_color mar_left" onclick="isReal('/user/getLibrary')">嘉峪关市图书馆</a>
                <g:img dir="images" src="/index/20.png"/>
                <label class="small_fontsize">借阅证</label>
            </label>
        </div>
        <g:if test="${data.user}">
            <div class="library_card_info br_div">
                <label class="library_card_title"><g:img dir="images" src="/life/12.png" class="div_float_left"
                                                         style="padding-right: 10px"/>借阅证信息</label>

                <div class="margin_top_middle">
                    <label>激活状态</label>
                    <label class="div_float_right">${data.user.status}</label>
                </div>

                <div class="margin_top_middle">
                    <label>用户编号</label>
                    <label class="div_float_right">${String.valueOf(data.user.userId).substring(0, 4)}******${String.valueOf(data.user.userId).substring(String.valueOf(data.user.userId).length() - 4)}</label>
                </div>

                <div class="margin_top_middle">
                    <label>用户类型</label>
                    <g:if test="${data.user.userType == '001'}">
                        <label class="div_float_right">
                            身份证
                        </label>
                    </g:if>
                    <g:if test="${data.user.userType == '003'}">
                        <label class="div_float_right">
                            便民卡
                        </label>
                    </g:if>
                </div>

                <div class="margin_top_middle">
                    <label>用户名称</label>
                    <label class="div_float_right">${data.user.userName}</label>
                </div>

                <div class="margin_top_middle">
                    <label>用户等级</label>
                    <label class="div_float_right">${data.user.userLevel}</label>
                </div>
                <a class="absolute_position library_card_info_button" onclick="active()"><g:img dir="images"
                                                                                                src="/life/library_jihuo.png"/>激活</a>
            </div>

            <div class="library_deposit_info br_div">
                <label class="library_card_title"><g:img dir="images" src="/life/14.png" class="div_float_left"
                                                         style="padding-right: 10px"/>押金信息</label>

                <div class="margin_top_middle">
                    <label>是否缴纳</label>
                    <label class="div_float_right">${data.user.hasPayment}</label>
                </div>

                <div class="margin_top_middle">
                    <label>缴费金额</label>
                    <label class="div_float_right">${data.amt}</label>
                </div>

                <div class="margin_top_middle">
                    <label>订单编号</label>
                    <label class="div_float_right">${data.user.order_id}</label>
                </div>

                <div class="margin_top_middle">
                    <label>支付时间</label>
                    <label class="div_float_right">${data.user.payment_time}</label>
                </div>

                <div class="margin_top_middle">
                    <label>支付方式</label>
                    <label class="div_float_right">${data.user.payment_type}</label>
                </div>

                <div class="absolute_position library_deposit_a">
                    <a class="library_card_info_button" onclick="pay()"><g:img dir="images" src="/life/15.png"/>缴押金</a>
                    <a class="library_card_info_button div_float_right" onclick="refund()"><g:img dir="images"
                                                                                                  src="/life/16.png"/>退押金</a>
                </div>
            </div>
        </g:if>
    </div>
    <g:if test="${data}">
        <form id="lib_form">
            <input type="hidden" name="isActive" id="isActive" value="${data.isActive}"/>
            <input type="hidden" name="hasPayment" value="${data.hasPayment}"/>
            <input type="hidden" name="hasOrder" value="${data.hasOrder}"/>
            <input type="hidden" name="orderId" value="${data.order_id}"/>
            <input type="hidden" name="amt" value="${data.amt}"/>
        </form>
    </g:if>
</div>
<script>
    //押金退款
    function refund() {
        $.post('/user/judgeLibrary', {},
            function (json) {
                if (json.errorcode == 0) {
                    if (json.isActive != 1) {//0未激活1激活
                        //layer.msg("您尚未激活图书管应用");
                        isReal("/card_activate");//直接跳转至激活页面
                        return;
                    }
                    if (json.hasPayment != 1) {//0未激活1激活
                        layer.msg("您尚未缴纳过押金");
                        return;
                    }
                    layer.confirm('该操作将为您退还图书馆押金', {
                        btn: ['确认', '取消'] //按钮
                    }, function (index) {
                        layer.close(index);
                        $.post('/user/refundLibrary', $("form").serialize(),
                            function (json) {
                                if (json.code == 0) {
                                    //跳转百合易付
                                    window.location.href = "https://etest.lzbank.com:20028/ccweb/samlsso?ChangeVal=mainPageSAML&spid=p3depay&method=" + json.method
                                } else {
                                    layer.msg(json.errormsg);
                                }
                            });
                    });
                } else {
                    layer.msg(json.errormsg);
                }
            });
    }

    //缴纳押金
    function pay() {
        $.post('/user/judgeLibrary', {},
            function (json) {
                if (json.errorcode == 0) {
                    if (json.isActive != 1) {//0未激活1激活
                        //layer.msg("您尚未激活图书管应用");
                        isReal("/card_activate");//直接跳转至激活页面
                        return;
                    }
                    if (json.hasPayment == 1) {
                        layer.msg("您已缴纳过押金");
                    } else {
                        isReal("/deposit");//跳转至缴费页面
                    }
                } else {
                    layer.msg(json.errormsg);
                }
            });
    }

    function active() {
        var isActive = $("#isActive").val();
        if (isActive == 1) {
            layer.msg("您已激活图书馆应用，不能重复激活");
            return;
        }
        isReal("/card_activate");
    }
</script>
</body>
</html>