<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>
<body>
<!--中心内容-->
<div class="body height_lose personal_body relative_position">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryOils')">
                                <li>
                                    加油卡充值
                                </li>
                            </a>
                            <a onclick="isReal('/queryBill')" >
                                <li>
                                    缴费账单
                                </li>
                            </a>
                        </ul>
                    </li>
                    %{--<li>--}%
                        %{--<div class="li"><span>金融服务</span></div>--}%
                        %{--<ul class="in_ul">--}%
                            %{--<a onclick="isReal('/user/queryCardSiteList')">--}%
                                %{--<li>--}%
                                    %{--便民卡预约--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        %{--</ul>--}%
                    %{--</li>--}%
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul ul_active">
                            <a onclick="isReal('/user/queryCredit')" >
                                <li>
                                    雄关信用
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryScore')" >
                                <li>
                                    百合积分
                                </li>
                            </a>
                            <a  onclick="isReal('/user/getLibrary')"  class="active">
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryFund')" >
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- 水费缴纳content -->
    <div class="life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">嘉图书馆</label>
                <label class="main_color">博览众长</label>
                <a class="small_fontsize gray_color mar_left"  onclick="isReal('/user/getLibrary')" >嘉峪关市图书馆</a>
                <g:img dir="images" src="/index/20.png"/>
                <label class="small_fontsize">借阅证信息</label>
            </label>
        </div>
        <div class="librarycard_maincontent height_lose aaa">
            <g:if test="${data.reads}">
            <g:each in="${data.reads}">
            <div class="div_float_left library_card_info_div br_div">
                <div>
                    <label>书名</label>
                    <label class="div_float_right">借阅状态</label>
                </div>
                <div class="height_lose">
                    <div class="default_img br_div">
                        <g:img dir="images" src="/life/19.png"/>
                    </div>
                    <div class="br_div div_float_right">
                        <div>图书编号：${it.bookId}</div>
                        <div class="margin_top_middle">借书时间：${it.startDate}</div>
                        <div class="margin_top_middle">续借时间：${it.renewDate}</div>
                        <div class="margin_top_middle">结束时间：${it.endDate}</div>
                    </div>
                </div>
            </div>
            </g:each>
            </g:if>
            <g:else>
                <h2 class="align_center margin_top_largemorest gray_color">您没有借书记录</h2>
            </g:else>
        </div>
    <form id="query_form" action="/user/querylibReadInfo" method="post">
        <!--接口获取到的页面数赋值在此处 剩下把请求的方法写在下面js里面就可以了-->
        <input id="total" type="hidden" name="total" value="${data.total}"/>
        <input id="page" type="hidden" name="page" value="${data.page}">
    </form>
    </div>

    <!-- 分页按钮 -->
    <div class="pagination">
        <button class="prev_page">上一页</button>

        <div class="number" id="number">
        </div>
        <button class="next_page">下一页</button>
        <button class="last_page">尾页</button>
    </div>
</div>
<asset:javascript src="pagination.js"/>
<script>
    $(function () {
        //默认每页十条数据
        var total = $("#total").val(); //总条数
        var pagenumber = $("#page").val(); //页码
        $.firstinit(total, pagenumber, function () {
            console.log("点击之后执行方法");
            $("#query_form").submit();
        })
    });
</script>
</body>
</html>