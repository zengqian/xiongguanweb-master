<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="convenienceCard"/>
</head>
<body>
<!--中心内容-->
<div class="about_card">
    <div>
        <label class="title_middle_fontsize">资讯中心</label>
        <div class="title_to">
            <label>当前位置：</label>
            <a>资讯中心</a>
            <label>></label>
            <a class="old_color">关于便民卡</a>
        </div>
        <div class="img_div"><g:img dir="images" src="/index/16.png"/></div>
    </div>
    <div class="margin_top_middle content">
        <div class="menu" id="convenience_menu">
            <ul>
                <li>
                    <div class="li">
                        <g:img dir="images" src="/index/17.png"/>
                        <a class="active convenience_menu" href="/about_convenience_card">关于便民卡</a>
                    </div>
                </li>
                <li>
                    <div class="li">
                        <g:img dir="images" src="/index/19.png"/>
                        <a href="/service_guide" class="convenience_menu">服务指南</a>
                    </div>
                </li>
                <li>
                    <div class="li">
                        <g:img dir="images" src="/index/26.png"/>
                        <a href="/convenience_card_application" class="convenience_menu">便民卡应用</a>
                    </div>
                </li>
            </ul>
        </div>
        <ul class="news_content">
            <li>
                <a class="lt_gray_color large_fontsize" href="/about_convenience_newsinfo">便民卡简介</a>
                <div class="gray_color">
                    <label>"雄关便民卡"是城市现代化文明的重要标志，以政府主导、市...</label>
                    <label class="div_float_right">2018年9月4号</label>
                </div>
            </li>
            <li>
                <a class="lt_gray_color large_fontsize" href="/about_convenience_newsinfo">嘉峪关市智慧雄关科技有限责任公司简介</a>
                <div class="gray_color">
                    <label>嘉峪关市智慧雄关信息科技有限责任公司成立于2018年5月30...</label>
                    <label class="div_float_right">2018年9月4号</label>
                </div>
            </li>
        </ul>
    </div>
</div>
</body>
</html>