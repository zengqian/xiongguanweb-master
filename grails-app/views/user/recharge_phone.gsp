<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>
<body>
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul ul_active">
                            <a onclick="isReal('/recharge_phone')" class="active">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            %{--<a onclick="isReal('/user/queryOils')">--}%
                                %{--<li>--}%
                                    %{--加油卡充值--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/queryBill')" >--}%
                                %{--<li>--}%
                                    %{--缴费账单--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        </ul>
                    </li>
                    %{--<li>--}%
                        %{--<div class="li"><span>金融服务</span></div>--}%
                        %{--<ul class="in_ul">--}%
                            %{--<a onclick="isReal('/user/queryCardSiteList')">--}%
                                %{--<li>--}%
                                    %{--便民卡预约--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        %{--</ul>--}%
                    %{--</li>--}%
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul">
                            %{--<a onclick="isReal('/user/queryCredit')" >--}%
                                %{--<li>--}%
                                    %{--雄关信用--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/user/queryScore')" >--}%
                                %{--<li>--}%
                                    %{--百合积分--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="toast()">
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="toast()">
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- 水费缴纳content -->
    <div class="life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">品质保证</label>
                <label class="main_color">安全快捷</label>
                <g:img dir="images" src="/life/2.png" class="fenge"/>
                <label class="small_fontsize">话费充值</label>
            </label>
        </div>
        <div>
            <form class="add_water" class="form" id="recharge_form" action="/user/phoneRecharge">
                <div class="margin_top_large btn_div">
                    <label class="large_fontsize input_title div_float_left">运营商:</label>
                    <div class="form_input btn_div br_div select_outdiv">
                        <div class="select_div"><label class="gray_color">请选择运营商</label></div>
                        <ul class="select_ul">
                            <li>
                                <g:img dir="images" src="/life/mobile.png" class="vertical_align"/>
                                <label class="unicom_option">中国移动</label>
                                <input type="radio" name="mobile_company" class="unicom_option_radio" value="1"/>
                            </li>
                            <li>
                                <g:img dir="images" src="/life/unicom.png" class="vertical_align"/>
                                <label class="unicom_option">中国联通</label>
                                <input type="radio" name="mobile_company" class="unicom_option_radio" value="2"/>
                            </li>
                            <li>
                                <g:img dir="images" src="/life/netmoibl.png" class="vertical_align"/>
                                <label class="unicom_option">中国电信</label>
                                <input type="radio" name="mobile_company" class="unicom_option_radio" value="3"/>
                            </li>
                        </ul>
                    </div>
                    <label class="absolute_position water_error_label"></label>
                </div>
                <div class="margin_top_large btn_div">
                    <label class="large_fontsize input_title div_float_left">手机号:</label>
                    <input placeholder="请输入手机号码" class="form_input btn_div" name="phone"/>
                    <label class="water_error_label"></label>
                </div>
                <div class="margin_top_large">
                    <label class="large_fontsize input_title div_float_left">充值面值:</label>
                    <div class="form_input btn_div br_div recharge_phone_form_input" style="position: relative">
                        <div class="height_lose">
                            <div class="recharge_phone_div">500元</div>
                            <div class="recharge_phone_div div">200元</div>
                            <div class="recharge_phone_div">100元</div>
                        </div>
                        <div class="margin_top_middlel height_lose">
                            <div class="recharge_phone_div">50元</div>
                            <div class="recharge_phone_div div">30元</div>
                            <div class="recharge_phone_div">20元</div>
                        </div>
                    </div>
                    <!--<label></label>-->
                </div>
                <div class="margin_top_large btn_div">
                    <label class="large_fontsize input_title div_float_left">金额:</label>
                    <input placeholder="请输入充值金额或选择面值" class="form_input btn_div recharge_amount" name="amount"/>
                    <label class="water_error_label"></label>
                </div>
                <input type="submit" value="立即充值" class="form_submit_water margin_top_largemore"/>
            </form>
        </div>
    </div>
</div>
<script>
        $("#recharge_form").validate({
            // onkeyup: false,
            errorPlacement: function (error, element) {
                element.next("label.water_error_label").append(error);
                element.parent().parent(".select_ul").parent().next("label.water_error_label").append(error);
            },
            rules: {
                mobile_company: {
                    required: true,
                    minlength: 1
                },
                phone: {
                    required: true,
                    isMobile: true
                },
                amount: {
                    required: true,
                    number: true
                }
            },
            messages: {
                mobile_company: {
                    required: "请选择充值公司"
                },
                phone: {
                    required: "请填写手机号"
                },
                amount: {
                    required: "请输入充值面值或选择面值",
                    number: "请输入数值"
                }
            },
            submitHandler: function (form) {
                $.post('/user/phoneRecharge', $("#recharge_form").serialize(),
                    function (json) {
                    console.log(json)
                        if(json.errorcode == 0){
                            // window.location.href = "https://etest.lzbank.com:20028/ccweb/samlsso?ChangeVal=mainPageSAML&spid=p3depay&method="+json.orderno
                            window.location.href = "http://192.168.2.206:8879/pay/gateWay?orderId="+json.orderno
                        }else {
                            layer.msg(json.errormsg);
                        }
                    });
            }
        });
</script>
</body>

</html>