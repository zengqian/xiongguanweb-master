<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>
<body>
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span>生活缴费</span></div>
                        <ul class="in_ul">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            <a onclick="toast()">
                                <li>
                                    电费缴纳
                                </li>
                            </a>
                            <a onclick="toast()">
                                <li>
                                    取暖费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryOils')">
                                <li>
                                    加油卡充值
                                </li>
                            </a>
                            <a onclick="isReal('/queryBill')" >
                                <li>
                                    缴费账单
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span>金融服务</span></div>
                        <ul class="in_ul">
                            <a onclick="isReal('/user/queryCardSiteList')">
                                <li>
                                    便民卡预约
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul">
                            <a onclick="isReal('/user/queryCredit')" >
                                <li>
                                    雄关信用
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryScore')" >
                                <li>
                                    百合积分
                                </li>
                            </a>
                            <a  onclick="isReal('/user/getLibrary')" >
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryFund')" >
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- 水费缴纳content -->
    <div class="life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">确保安全</label>
                <label class="main_color">信息认证</label>
                <label class="small_fontsize gray_color mar_left">个人信息设置</label>
                <g:img dir="images" src="/index/20.png"/>
                <a class="small_fontsize gray_color" href="/torealname">实名认证</a>
                <g:img dir="images" src="/index/20.png"/>
                <label class="small_fontsize">支付密码</label>
            </label>
        </div>
        <div class="div_float_left">
            <div class="large_fontsize margin_top_middlel"><div class="realname_txt">*</div>请输入支付密码</div>
        <form class="add_water br_div">
            <div class="margin_form_middle">
                <label class="large_fontsize input_title">支付密码:</label>
                <input class="btn_div form_input" placeholder="请输入支付密码" maxlength="6" name="phone"/>
            </div>
            <input type="submit" value="完成" class="form_submit_water margin_top_largemore"/>
        </form>
        </div>
    </div>
</div>
<script>
</script>
</body>
</html>