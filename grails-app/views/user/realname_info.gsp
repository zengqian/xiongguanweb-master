<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>

<body>
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span>生活缴费</span></div>
                        <ul class="in_ul">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            %{--<a onclick="isReal('/user/queryOils')">--}%
                                %{--<li>--}%
                                    %{--加油卡充值--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/queryBill')">--}%
                                %{--<li>--}%
                                    %{--缴费账单--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        </ul>
                    </li>
                    %{--<li>--}%
                        %{--<div class="li"><span>金融服务</span></div>--}%
                        %{--<ul class="in_ul">--}%
                            %{--<a onclick="isReal('/user/queryCardSiteList')">--}%
                                %{--<li>--}%
                                    %{--便民卡预约--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        %{--</ul>--}%
                    %{--</li>--}%
                    <li>
                        <div class="li"><span>公共服务</span></div>
                        <ul class="in_ul">
                            %{--<a onclick="isReal('/user/queryCredit')">--}%
                                %{--<li>--}%
                                    %{--雄关信用--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/user/queryScore')">--}%
                                %{--<li>--}%
                                    %{--百合积分--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getLibrary')">
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryFund')">
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- content -->
    <div class="life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">确保安全</label>
                <label class="main_color">信息认证</label>
                <g:img dir="images" src="/life/2.png" class="fenge"/>
                <label class="small_fontsize">个人信息</label>
            </label>
        </div>

        <div>
            <form class="add_water" id="real_form">
                <div class="margin_top_large">
                    <label class="large_fontsize input_title">手机号码:</label>
                    <input placeholder="请输入手机号码" class="form_input btn_div" value="${data.realnameinfo.mobile}"
                           disabled/>
                </div>

                <div class="margin_top_large">
                    <label class="large_fontsize input_title">姓名:</label>
                    <input placeholder="请输入真实姓名" class="form_input btn_div" value="${data.realnameinfo.name}" disabled/>
                </div>

                <div class="margin_top_large">
                    <label class="large_fontsize input_title">身份证:</label>
                    <input placeholder="请输入身份证号码" class="form_input btn_div" value="${data.realnameinfo.certNo}"
                           disabled/>
                </div>

                <div class="margin_top_large">
                    <label class="large_fontsize input_title">性别:</label>
                    <select class="form_input btn_div" value="${data.userinfo.gender}" disabled>
                        <option value="01">男</option>
                        <option value="02">女</option>
                        <option>其他</option>
                    </select>
                </div>

                <div class="margin_top_large">
                    <label class="large_fontsize input_title">民族:</label>
                    <select class="form_input btn_div" disabled>
                        <option>汉族</option>
                    </select>
                </div>
                <input type="hidden" value="${data.realnameinfo.cardNo}" name="cardNo">
                <input type="button" value="解除实名认证" class="form_submit_water margin_top_largemore" onclick="lost()"/>
            </form>
        </div>
    </div>
</div>
<script>
    function lost() {
        $.post('/user/lostRealInfo', $("#real_form").serialize(),
            function (json) {
                if (json.errorcode == 0) {
                    layer.msg("解绑成功")
                } else {
                    layer.msg(json.errormsg)
                }
            })
    }
</script>
</body>
</html>