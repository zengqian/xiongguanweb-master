<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="convenienceCard"/>
</head>
<body class="fff_body">
<!--中心内容-->
<div class="about_card">
    <div style="padding-bottom: 6px;">
        <a class="small_fontsize" href="/user/userInfo" ><g:img dir="images" src="/life/23.png" class="vertical_align" style="margin-right: 6px"/>返回</a>
        <div class="title_to">
            <label>当前位置：</label>
            <a class="old_color">嘉峪关市博物馆</a>
        </div>
    </div>
    <div class="margin_top_large align_center news_detail about_library">
        <h2 class="main_title">你真的了解嘉峪关博物馆吗？</h2>
        <g:img dir="images" src="/life/37.png" class="margin_top_largemore"/>
        <h2 class="margin_top_large">嘉峪关城市博物馆</h2>
        <p>周二至周日9:00-17:00（16:30停止入馆）</p>
        <p>周一闭馆（法定节假日除外）</p>
        <p>五一南路1379号</p>
        <p class="align_left" style="text-indent: 20px;">嘉峪关城市博物馆于2008年8月开馆，免费向公众开放。该馆占地面积1.9万平方米，建筑面积1.1万平方米，展厅面积6520平方米。博物馆展示内容分为“三篇四厅”，即辉煌篇、历史篇、展望篇三个篇章，序厅、展示厅、公示厅、临展厅四个展厅。布展设计运用宏大的场景、翔实的资料、艺术的构思、高科技的手段，生动形象地展现了嘉峪关市50年来的发展历程、发展成就和发展前景。通过大量的珍贵图片、实物陈列、场景复原、多媒体演示、影视互动等表现形式，充分体现了“城市、人文、环境、发展”的展览主题，是一座集宣传、教育、观光、休闲等功能于一体的现代化博物馆。</p>
        <h2 class="margin_top_large">嘉峪关城市博物馆</h2>
        <h3 class="margin_form_middle">辉煌篇</h3>
        <p class="align_left" style="text-indent: 20px;">嘉峪关市“因关得名，因企设市”，于1958年开始建设，1965年设市。市辖总面积为3000平方公里，城区规划面积为260平方公里。嘉峪关地处西北内陆，干旱少雨。缺水少绿成为建市初期最大的难题。近年来，我市坚持将水库建设同园林绿化、城市规划建设结合起来，先后建成了东湖生态旅游景区、迎宾湖旅游园区、森林公园、英雄广场等一批高标准的城市公园，有效调节了我市的小气候，大大改善了人居生活环境。先后被授予了“全国园林绿化先进城市”、“甘肃省绿化模范城市等荣誉称号”。</p>
        <p class="align_left" style="text-indent: 20px;"> 嘉峪关市工业基础雄厚，工业产值占生产总值的75%以上。现已形成以冶金工业为主导，商贸旅游为支柱，城郊型农业为特色的经济发展格局。酒泉钢铁集团公司建于1958年，是我国西北地区建设较早、规模较大的钢铁联合企业，经过50多年的发展，现已发展成为以碳钢、不锈钢为主，兼以葡萄酿造、高科技种植养殖等多元产业并举的大型企业。</p>
           <p class="align_left" style="text-indent: 20px;"> 嘉峪关市旅游资源丰富，现拥有世界文化遗产地、国家首批AAAAA级旅游景区——嘉峪关关城文物景区，拥有 “西部八达岭”之称的悬壁长城、明代万里长城沿线第一座烽火台——万里长城第一墩；世界第一大地下画廊之称的“魏晋墓地下画廊”；亚洲距离城市最近的冰川——七一冰川，国家AAAA级旅游景区——东湖生态旅游景区、紫轩葡萄酒庄园；国家AAA级旅游景区嘉峪关长城博物馆等一批自然景观、人文景观</p>
        <h3 class="margin_form_middle">展望篇</h3>
        <p class="align_left" style="text-indent: 20px;">甘肃省“十一五”发展规划中提出将重点打造以兰州为中心、嘉酒和天水为两翼的新型城市格局。为抢抓机遇，加快发展，市委、市政府开发建设了南市区讨赖河两岸70平方公里用地。南市区新区的开发建设，有利于推进嘉酒区域经济一体化发展；有利于拓展城市发展空间，增加城市用地；有利于实现城乡一体化发展及改善城市生态环境。</p>
            <p class="align_left" style="text-indent: 20px;">为了进一步加强镇区、中心村建设。我市将按照城市标准、遵循农村特点改造农村，目前正在对我市文殊镇、新城镇、峪泉镇进行改造建设。通过推进供排水、供热、供气、电力、交通、环保、通信等基础设施向农村的延伸，不断加快农村居民住房改造、村容环境整治、垃圾收集清运处理和污水处理等项目的建设，来真正实现农村城市化、农民市民化、农业工业化。</p>
        <h3 class="margin_form_middle">历史篇</h3>
        <p class="align_left" style="text-indent: 20px;">嘉峪关南依祁连山，北靠黑山，两山之间仅有15公里，是古丝绸之路上的交通要冲，历代为兵家必争之地，有“河西第一隘口”之称。明代的征西大将军冯胜正是以一个战略家的眼光，发现了这一地理优势，在这里选址建关。正是如此，嘉峪关才真正发挥了它边陲锁钥的作用。</p>
            <p class="align_left" style="text-indent: 20px;">嘉峪关是丝绸之路的一个重要门户，很多名人都曾来过嘉峪关。如张骞、霍去病、玄奘、马可波罗、林则徐、左宗棠等。1842年林则徐被贬新疆伊犁，途经嘉峪关，写下了名篇《出嘉峪关感赋诗四首》。</p>
           <p class="align_left" style="text-indent: 20px;"> 五十年前的嘉峪关除了拥有一座古人留下的关城以外，完全是一张白纸，所以它的建设、发展与酒钢的建设息息相关。曾经正是一位名叫柴昂阿莱什登的藏族同胞发现了一块“又黑、又硬、又重的石头”为镜铁山矿的发现指明了方向。但是，这里的气候条件恶劣，工作环境艰苦，在短短两年的时间里，先后有11名地质队员献出了自己宝贵的生命。他们被誉为钢城的开路先锋。正是在这些探矿工作者的努力下，在嘉峪关境内共发现储量近5亿吨的铁矿。1958年，酒钢成立建厂，在一年的时间里，全国各地4万多名建设者加入了酒钢的建设队伍。酒钢初期的建设可谓一波三折，面对三年大跃进、自然灾害、文化大革命等特殊的历史时期。酒钢人以顽强的意志，度过重重难关，于1970年终于炼成了第一桶铁水。酒钢的建设和发展为设立城市奠定了基础。</p>
            <p class="align_left" style="text-indent: 20px;">近几年来，嘉峪关人民紧抓“西部大开发”的历史机遇，加快改革开放和现代化建设的步伐，全市经济社会发展取得了前所未有的成就。正是在雄关儿女的不懈努力下，嘉峪关市在城市建设、经济建设、环境保护、社会事业等各方面取得了骄人的成绩。先后被评为全国卫生城市，全国环保模范城市，中国优秀旅游城市，全国园林绿化先进城市，等20多项殊荣。</p>
            <p class="align_left">相信在不久的将来，嘉峪关一定会建设成为经济发展、社会和谐、生态宜人的区域中心城市。
    </div>
</div>
</body>
</html>