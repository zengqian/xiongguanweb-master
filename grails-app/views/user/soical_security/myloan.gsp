<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>
<body>
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryOils')">
                                <li>
                                    加油卡充值
                                </li>
                            </a>
                            <a onclick="isReal('/queryBill')" >
                                <li>
                                    缴费账单
                                </li>
                            </a>
                        </ul>
                    </li>
                    %{--<li>--}%
                        %{--<div class="li"><span>金融服务</span></div>--}%
                        %{--<ul class="in_ul">--}%
                            %{--<a onclick="isReal('/user/queryCardSiteList')">--}%
                                %{--<li>--}%
                                    %{--便民卡预约--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        %{--</ul>--}%
                    %{--</li>--}%
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul ul_active">
                            <a onclick="isReal('/user/queryCredit')" >
                                <li>
                                    雄关信用
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryScore')" >
                                <li>
                                    百合积分
                                </li>
                            </a>
                            <a  onclick="isReal('/user/getLibrary')" >
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryFund')"  class="active">
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- 水费缴纳content -->
    <div class="life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">品质保证</label>
                <label class="main_color">安全快捷</label>
                <a class="small_fontsize gray_color mar_left" onclick="isReal('/user/queryFund')" >公积金</a>
                <g:img dir="images" src="/index/20.png"/>
                <label class="small_fontsize">我的贷款</label>
            </label>
        </div>
        <div class="myloan_maincontent height_lose aaa">
            <g:if test="${data.pvdLoanInfos}">
            <g:each in="${data.pvdLoanInfos}" var="item">
            <a class="div_float_left myloan_div_content br_div" onclick="queryDetail(${item.LoanNo})">
                <div>货款编号：${item.LoanNo}</div>
                <div class="myloan_div_content_div">
                    <label>公积金账号</label>
                    <label class="div_float_right">${item.PvdFndAcctNo2}</label>
                </div>
                <div class="myloan_div_content_div">
                    <label>公积金贷款金额（元)</label>
                    <label class="div_float_right">${item.PvdFndLoanAmt}</label>
                </div>
            </a>
            <form id="${item.LoanNo}" method="post" action="/user/queryFundLoanDetail">
                <input type="hidden" name ="PvdFndAcctNo2" value=" ${item.PvdFndAcctNo2}"/>
                <input type="hidden" name ="LoanNo" value=" ${item.LoanNo}"/>
                <input type="hidden" name ="PvdFndLoanAmt" value=" ${item.PvdFndLoanAmt}"/>
                <input type="hidden" name ="PvdFndLoanTerm" value=" ${item.PvdFndLoanTerm}"/>
                <input type="hidden" name ="PvdFndLoanRpyMth" value=" ${item.PvdFndLoanRpyMth}"/>
                <input type="hidden" name ="PvdFndLoanMoRpyAmt" value=" ${item.PvdFndLoanMoRpyAmt}"/>
                <input type="hidden" name ="PvdFndLoanStrtDt" value=" ${item.PvdFndLoanStrtDt}"/>
                <input type="hidden" name ="PvdFndLoanExpDt" value=" ${item.PvdFndLoanExpDt}"/>
                <input type="hidden" name ="PvdFndLoanClsDt" value=" ${item.PvdFndLoanClsDt}"/>
                <input type="hidden" name ="PvdFndLoanRpyBal" value=" ${item.PvdFndLoanRpyBal}"/>
                <input type="hidden" name ="ComLndrNm" value=" ${item.ComLndrNm}"/>
                <input type="hidden" name ="ComLndrIdCrdNo" value=" ${item.ComLndrIdCrdNo}"/>
                <input type="hidden" name ="ComLndEstbPvdFndFlg" value=" ${item.ComLndEstbPvdFndFlg}"/>
                <input type="hidden" name ="CtcAdr1" value=" ${item.CtcAdr1}"/>
                <input type="hidden" name ="LoanSt" value=" ${item.LoanSt}"/>
                <input type="hidden" name ="PrdRpySt" value=" ${item.PrdRpySt}"/>
                <input type="hidden" name ="OdueAmt" value=" ${item.OdueAmt}"/>
                <input type="hidden" name ="OdueTms" value=" ${item.OdueTms}"/>
                <input type="hidden" name ="MxCtnsOdueTms" value=" ${item.MxCtnsOdueTms}"/>
            </form>
            </g:each>
            </g:if>
            <g:else>
                <h2 class="align_center margin_top_largemorest gray_color">您没有贷款记录</h2>
            </g:else>
        </div>
    </div>
</div>
<script>
    //提交表单
    function queryDetail(formid){
        var str = "#"+formid;
        $(str).submit();
    }
</script>
</body>
</html>