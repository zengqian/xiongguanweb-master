<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>

<body>
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryOils')">
                                <li>
                                    加油卡充值
                                </li>
                            </a>
                            <a onclick="isReal('/queryBill')">
                                <li>
                                    缴费账单
                                </li>
                            </a>
                        </ul>
                    </li>
                    %{--<li>--}%
                        %{--<div class="li"><span>金融服务</span></div>--}%
                        %{--<ul class="in_ul">--}%
                            %{--<a onclick="isReal('/user/queryCardSiteList')">--}%
                                %{--<li>--}%
                                    %{--便民卡预约--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        %{--</ul>--}%
                    %{--</li>--}%
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul ul_active">
                            <a onclick="isReal('/user/queryCredit')">
                                <li>
                                    雄关信用
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryScore')">
                                <li>
                                    百合积分
                                </li>
                            </a>
                            <a onclick="isReal('/user/getLibrary')">
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryFund')" class="active">
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- 水费缴纳content -->
    <div class="life_content not_life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">雄关公积金</label>
                <label class="main_color">品质保证</label>
                <g:img dir="images" src="/life/2.png" class="fenge"/>
                <label class="small_fontsize">雄关公积金</label>
            </label>
        </div>

        <div class="height_lose relative_position">
                <div class="social_security_content margin_top_large height_lose">
                    <div class="account_money_div">
                        <div class="money_hidden_div">
                            <g:img dir="images" src="/life/35.png" class="hidden_money"/><label
                                class="hidden_money">隐藏</label>
                        </div>

                        <div class="margin_top_middle">账户余额（元）</div>

                        <div class="small_fontsize margin_top_middle money_div">*****</div>

                        <div class="small_fontsize margin_top_middle money_div" style="display: none"></div>

                        <div class="small_fontsize margin_top_middle">${data.CstNm?:''}&nbsp;|&nbsp;${data.IdvNo?:''}</div>
                        <a class="btn" onclick="isReal('/user/queryFundAcc')">查看账户信息</a>
                    </div>
                    <a class="mydeposit_div" onclick="isReal('/user/queryFundDeposit')">
                        <g:img dir="images" src="/life/28.png" class="margin_top_large"/>
                        <div class="margin_top_middlel">我的缴存</div>

                        <div class="small_fontsize margin_top_middlel">品质保证、安全快捷</div>
                    </a>
                </div>

                <div class="social_security_content margin_form_middle absolute_position height_lose">
                    <a class="myextract_div" onclick="isReal('/user/queryFundExtract')">
                        <g:img dir="images" src="/life/29.png" class="margin_top_large"/>
                        <div class="margin_top_middlel">我的提取</div>

                        <div class="small_fontsize margin_top_middlel">品质保证、安全快捷</div>
                    </a>
                    <a class="myloan_div" onclick="isReal('/user/queryFundLoan')">
                        <g:img dir="images" src="/life/30.png" class="margin_top_large"/>
                        <div class="margin_top_middlel">我的贷款</div>

                        <div class="small_fontsize margin_top_middlel">品质保证、安全快捷</div>
                    </a>
                </div>
        </div>
    </div>
</div>
<script>
    if ("${data.errormsg}") {
        layer.msg("${data.errormsg}")
    }
</script>
</body>
</html>