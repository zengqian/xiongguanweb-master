<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>
<body>
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul ul_active">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            <a onclick="toast()">
                                <li>
                                    电费缴纳
                                </li>
                            </a>
                            <a onclick="toast()" class="active">
                                <li>
                                    取暖费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryOils')">
                                <li>
                                    加油卡充值
                                </li>
                            </a>
                            <a onclick="isReal('/queryBill')" >
                                <li>
                                    缴费账单
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span>金融服务</span></div>
                        <ul class="in_ul">
                            <a onclick="isReal('/user/queryCardSiteList')">
                                <li>
                                    便民卡预约
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul">
                            %{--<a onclick="isReal('/user/queryCredit')" >--}%
                                %{--<li>--}%
                                    %{--雄关信用--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/user/queryScore')" >--}%
                                %{--<li>--}%
                                    %{--百合积分--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a  onclick="isReal('/user/getLibrary')" >
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryFund')" >
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- 水费缴纳content -->
    <div class="life_content not_life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">品质保证</label>
                <label class="main_color">安全快捷</label>
                <g:img dir="images" src="/life/2.png" class="fenge"/>
                <label class="small_fontsize">暖气费缴纳</label>
            </label>
            <a class="large_fontsize div_float_right main_color" href="/warm_add">
                <label class="vertical_align">添加我的暖气费账户</label>
                <g:img dir="images" src="/life/3.png" class="vertical_align"/>
            </a>
        </div>
        <div>
            <ul class="water_ul warm_ul height_lose aaa life_index_info">
                <li class="life_index_li div_float_left">
                <a href="" class="del_logo" title="删除"></a>
                    <div><label class="water_card">账户名称：</label><label class="gray_color">我的家</label></div>
                    <div><label class="water_card">户主：</label><label class="gray_color">吴彦祖</label></div>
                    <div><label class="water_card">户号：</label><label class="gray_color">230230230230</label></div>
                    <input type="submit" class="see_account" value="查看账户"/>
                </li>
                <li class="life_index_li div_float_left">
                    <div><label class="water_card">账户名称：</label><label class="gray_color">我的家</label></div>
                    <div><label class="water_card">户主：</label><label class="gray_color">吴彦祖</label></div>
                    <div><label class="water_card">户号：</label><label class="gray_color">230230230230</label></div>
                    <input type="submit" class="see_account" value="查看账户"/>
                </li>
                <li class="life_index_li div_float_left">
                    <div><label class="water_card">账户名称：</label><label class="gray_color">我的家</label></div>
                    <div><label class="water_card">户主：</label><label class="gray_color">吴彦祖</label></div>
                    <div><label class="water_card">户号：</label><label class="gray_color">230230230230</label></div>
                    <input type="submit" class="see_account" value="查看账户"/>
                </li>
                <li class="life_index_li div_float_left">
                    <div><label class="water_card">账户名称：</label><label class="gray_color">我的家</label></div>
                    <div><label class="water_card">户主：</label><label class="gray_color">吴彦祖</label></div>
                    <div><label class="water_card">户号：</label><label class="gray_color">230230230230</label></div>
                    <input type="submit" class="see_account" value="查看账户"/>
                </li>
            </ul>
        </div>
    </div>
</div>
</body>
</html>