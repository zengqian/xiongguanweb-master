<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>
<body>
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul ul_active">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryOils')" class="active">
                                <li>
                                    加油卡充值
                                </li>
                            </a>
                            <a onclick="isReal('/queryBill')">
                                <li>
                                    缴费账单
                                </li>
                            </a>
                        </ul>
                    </li>
                    %{--<li>--}%
                        %{--<div class="li"><span>金融服务</span></div>--}%
                        %{--<ul class="in_ul">--}%
                            %{--<a onclick="isReal('/user/queryCardSiteList')">--}%
                                %{--<li>--}%
                                    %{--便民卡预约--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        %{--</ul>--}%
                    %{--</li>--}%
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul">
                            %{--<a onclick="isReal('/user/queryCredit')">--}%
                                %{--<li>--}%
                                    %{--雄关信用--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/user/queryScore')">--}%
                                %{--<li>--}%
                                    %{--百合积分--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="toast()">
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="toast()">
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- 水费缴纳content -->
    <div class="life_content not_life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">品质保证</label>
                <label class="main_color">安全快捷</label>
                <g:img dir="images" src="/life/2.png" class="fenge"/>
                <label class="small_fontsize">加油卡充值</label>
            </label>
            <div class="div_float_right">
                <a class="small_fontsize lt_gray_color div_float_left" href="/oil_recharge_explain">
                    <label class="vertical_align">充值说明</label>
                </a>
                <g:img dir="images" src="/life/2.png" class="oil_fenge div_float_left"/>
                <a class="small_fontsize lt_gray_color div_float_left" href="/oil_question">
                    <label class="vertical_align">常见问题</label>
                </a>
            </div>
        </div>
        <div class="margin_top_large large_fontsize br_div">
            <div class="height_lose br_div">
                <div class="oil_border_div div_float_left relative_position">
                    <a class="absolute_position white_color more_middle_fontsize" style="left: 15px;z-index: 2;" onclick="prevcard()"><</a>
                    <a class="absolute_position white_color more_middle_fontsize" style="right: 40px;z-index: 2;"
                       onclick="nextcard()">></a>
                    <g:if test="${data==null}">
                        <div class="oil_card_div default_oil_card">
                        您尚未添加油卡
                        </div>
                    </g:if>

                    <div class="card_oilnext">
                        <g:each in="${data}">
                            <g:if test="${it.oilType=='41002'}">
                            <div class="petrochina_card oil_card_div white_color">${it.oilCardNo}
                            </div>
                            </g:if>
                            <g:if test="${it.oilType=='41001'}">
                            <div class="cnpc_card oil_card_div white_color">${it.oilCardNo}
                            </div>
                            </g:if>
                        </g:each>
                    </div>
                </div>
                <a class="add_oil_a div_float_left" title="" href="/oil_add">
                    <g:img dir="images" src="/life/oil_back.png"/>
                    <div class="margin_top_middle">
                    <g:img dir="images" src="/life/oil1.png" class="vertical_align"/>
                        添加油卡
                    </div>
                </a>
            </div>
            <div class="oil_recharge_amount">
                充值金额：<a class="div_float_right main_color" href="/oil_recharge_record">充值记录</a>
            </div>
            <form id="recharge_form">
                <input type="hidden" name="cardNo" id="cardno">
                <div class="margin_top_large">
                    <div class="height_lose">
                        <div class="recharge_phone_div">200元
                            <input type="radio" class="oil_recharge_radio" value="100" name="paymoney"/>
                        </div>
                        <div class="recharge_phone_div div">500元
                            <input type="radio" class="oil_recharge_radio" value="200" name="paymoney"/>
                        </div>
                    </div>
                    <div class="margin_top_middlel height_lose">
                        <div class="recharge_phone_div">1000元
                            <input type="radio" class="oil_recharge_radio" value="2000" name="paymoney"/>
                        </div>
                        <div class="recharge_phone_div div">2000元
                            <input type="radio" class="oil_recharge_radio" value="300" name="paymoney"/>
                        </div>
                    </div>
                </div>
                <input type="submit" class="form_submit_water margin_top_largemore" value="立即充值"/>
            </form>
        </div>
    </div>
</div>
<script>
    $(".oil_card_div:first-child").addClass("active")
    $("#cardno").val($(".oil_card_div:first-child").text().replace(/[\r\n]/g,""))
    $("#recharge_form").validate({
        submitHandler: function (form) {
            $.post('/user/oilRecharge', $("#recharge_form").serialize(),
                function (json) {
                    if(json.code == 0){
                        window.location.href = "https://etest.lzbank.com:20028/ccweb/samlsso?ChangeVal=mainPageSAML&spid=p3depay&method="+json.method
                    }else {
                        layer.msg(json.errormsg)
                    }
                })
        }
    });

    // 当前卡号 alert($(".oil_card_div.active").text())
    function nextcard() {
        if ($(".oil_card_div.active").next("div").attr("class") != undefined) {
            var acitve = $(".oil_card_div.active")
            $(".oil_card_div.active").removeClass("active")
            acitve.next("div").addClass("active")
            $(".card_oilnext").offset({left: $(".card_oilnext").offset().left - 311})
            $("#cardno").val($(".oil_card_div.active").text().replace(/[\r\n]/g,""))
        }
    }

    function prevcard() {
        if ($(".oil_card_div.active").prev("div").attr("class") != undefined) {
            var acitve = $(".oil_card_div.active")
            $(".oil_card_div.active").removeClass("active")
            acitve.prev("div").addClass("active")
            $(".card_oilnext").offset({left: $(".card_oilnext").offset().left + 311})
            $("#cardno").val($(".oil_card_div.active").text().replace(/[\r\n]/g,""))
        }
    }
</script>
</body>
</html>