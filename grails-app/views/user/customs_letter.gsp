<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>
<body>
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryOils')">
                                <li>
                                    加油卡充值
                                </li>
                            </a>
                            <a onclick="isReal('/queryBill')" >
                                <li>
                                    缴费账单
                                </li>
                            </a>
                        </ul>
                    </li>
                    %{--<li>--}%
                        %{--<div class="li"><span>金融服务</span></div>--}%
                        %{--<ul class="in_ul">--}%
                            %{--<a onclick="isReal('/user/queryCardSiteList')">--}%
                                %{--<li>--}%
                                    %{--便民卡预约--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        %{--</ul>--}%
                    %{--</li>--}%
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul ul_active">
                            <a onclick="isReal('/user/queryCredit')" class="active">
                                <li>
                                    雄关信用
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryScore')" >
                                <li>
                                    百合积分
                                </li>
                            </a>
                            <a  onclick="isReal('/user/getLibrary')" >
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryFund')" >
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- 水费缴纳content -->
    <div class="life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">雄关信用</label>
                <label class="main_color">品质保证</label>
                <g:img dir="images" src="/life/2.png" class="fenge"/>
                <label class="small_fontsize">雄关信用</label>
            </label>
        </div>
        <div class="custom_letter_div">
            <canvas id="myCanvas" width="296" height="296"></canvas>
            <div class="br_div align_center">
            <div class="cutom_letter_amount main_color br_div">
                    <g:if test="${data}">
                    <div id="fico_score">${data.score}</div>
                    <div class="more_middle_fontsize">${data.level}</div>
                    </g:if>
            </div>
                <div class="lt_gray_color large_fontsize">信用贷款额度</div>
                <g:if test="${data}">
                <div class="main_color margin_top_middle large_fontsize">￥${data.amt}</div>
                </g:if>
            </div>

            <div class="div_float_right align_center right_div">
                <div class="more_middle_fontsize">信用生活▪点滴积累</div>
                <div class="margin_form_middle large_fontsize">
                    <div class="xinyon daikuan"><div>信用贷款</div></div>
                    <div class="xinyon book"><div>免押借书</div></div>
                    <div class="xinyon car"><div>免押借车</div></div>
                </div>
                <div class="more_middle_fontsize margin_top_large">关于雄关信用</div>
                <div class="align_left gray_color margin_form_middle xinyon_txt">
                    1.同意授权雄关信用通过云上雄关信息平台获取您的个人信用记录信息，并用对于您的个人信用评价。
                </div>
                <div class="align_left gray_color margin_form_middle xinyon_txt">
                    2.同意授权云上雄关获取您的雄关信用及其其他信用信息，并输出政府部门在行政事务管理中使用。
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    // var score = parseInt($("#fico_score").innerText)/1000
    console.log($("#fico_score").text())
    var speed = 0
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    var ctx1 = c.getContext("2d");
    var rad = Math.PI*1.5/100
    ctx1.beginPath();
    ctx1.lineWidth = 15;
    ctx1.strokeStyle = "#d8f0f6";
    ctx1.lineCap = "round";
    ctx1.arc(148, 148, 140, 0.75 * Math.PI, 0.25 * Math.PI);
    ctx.restore();
    ctx1.stroke();

    drawFrame()
    function drawFrame() {
        // ctx.clearRect(0,0,296,296)
        ctx.lineWidth = 15;
        ctx.strokeStyle = "#aae0ee";
        ctx.lineCap = "round";
        ctx.beginPath();
        ctx.arc(148, 148, 140,  0.75*Math.PI, 0.75*Math.PI+rad*speed,false);
        ctx.stroke();
        // // ctx.closePath();
        // ctx.restore();
        if(speed >= 30) return; //满70
        speed++
        window.requestAnimationFrame(drawFrame);

    }


</script>
</body>
</html>