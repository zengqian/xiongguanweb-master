<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="convenienceCard"/>
</head>
<body>
<!--中心内容-->
<div class="about_card">
    <div>
        <label class="title_middle_fontsize">资讯中心</label>
        <div class="title_to">
            <label>当前位置：</label>
            <a>资讯中心</a>
            <label>></label>
            <a class="old_color">便民卡应用</a>
        </div>
        <div class="img_div"><g:img dir="images" src="/index/16.png"/></div>
    </div>
    <div class="margin_top_middle content" style="overflow: hidden">
        <div class="menu" id="convenience_menu">
            <ul>
                <li>
                    <div class="li">
                        <g:img dir="images" src="/index/17.png"/>
                        <a href="/about_convenience_card" class="convenience_menu">关于便民卡</a>
                    </div>
                </li>
                <li>
                    <div class="li">
                        <g:img dir="images" src="/index/19.png"/>
                        <a href="/service_guide" class="convenience_menu">服务指南</a>
                    </div>
                </li>
                <li>
                    <div class="li">
                        <g:img dir="images" src="/index/19.png"/>
                        <a class="active convenience_menu" href="/convenience_card_application">便民卡应用</a>
                    </div>
                </li>
            </ul>
        </div>
        <ul class="news_content aaa" style="overflow: auto;height: 100%;">
            <li>
                <a class="lt_gray_color large_fontsize" href="/convenience_goverment#qwer">政府公共服务应用</a>
                <div class="gray_color">
                    <label>社会保障：全面实现养老、医疗、失业、工伤、生育等社会保险业务管理的信息交换、信息服务...</label>
                    <!--<label class="div_float_right">2018年9月4号</label>-->
                </div>
            </li>
            <li>
                <a class="lt_gray_color large_fontsize" href="/convenience_goverment#yuio">公共事业应用介绍</a>
                <div class="gray_color">
                    <label>互联网卡表用户：采用系统对接模式，实现手机端缴费...</label>
                    <!--<label class="div_float_right">2018年9月4号</label>-->
                </div>
            </li>
            <li>
                <a class="lt_gray_color large_fontsize" href="/convenience_goverment#tgbn">金融应用介绍</a>
                <div class="gray_color">
                    <label>实现便民卡在超市、药店、便利店、餐饮店、加油（气）站等小额支付网点进行金融消费...</label>
                    <!--<label class="div_float_right">2018年9月4号</label>-->
                </div>
            </li>
            <li>
                <a class="lt_gray_color large_fontsize" href="/convenience_goverment#sefc">公共出行介绍</a>
                <div class="gray_color">
                    <label>公交车：所有公交机具更换为多合一车载机，支持银联闪付、手机APP二维码支付...</label>
                    <!--<label class="div_float_right">2018年9月4号</label>-->
                </div>
            </li>
            <li>
                <a class="lt_gray_color large_fontsize" href="/convenience_goverment#ijud">旅游休闲应用介绍</a>
                <div class="gray_color">
                    <label>市民可持便民卡到城楼、第一墩、悬臂长城等服务网点开通景区年卡功能...</label>
                    <!--<label class="div_float_right">2018年9月4号</label>-->
                </div>
            </li>
            <li>
                <a class="lt_gray_color large_fontsize" href="/convenience_goverment#miyd">便民消费应用介绍</a>
                <div class="gray_color">
                    <label>超市：实现pos机刷卡支付，手机APP扫码支付...</label>
                    <!--<label class="div_float_right">2018年9月4号</label>-->
                </div>
            </li>
            <li>
                <a class="lt_gray_color large_fontsize" href="/convenience_goverment#ygdb">企业一卡通应用介绍：重点介绍酒钢</a>
                <div class="gray_color">
                    <label>门禁：酒钢集团门禁读卡器需要能够识别便民卡和虚拟卡（视当前读卡器支持程度和酒钢...</label>
                    <!--<label class="div_float_right">2018年9月4号</label>-->
                </div>
            </li>
            <li>
                <a class="lt_gray_color large_fontsize" href="/convenience_goverment#yshb">雄关信用介绍</a>
                <div class="gray_color">
                    <label>“雄关信用”是一个面向嘉峪关市民的社会信用服务综合应用体系...</label>
                </div>
            </li>
        </ul>
    </div>
</div>
</body>
</html>