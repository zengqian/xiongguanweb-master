<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>
<body>
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul ul_active">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first" class="active">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            %{--<a onclick="isReal('/user/queryOils')">--}%
                                %{--<li>--}%
                                    %{--加油卡充值--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/queryBill')" >--}%
                                %{--<li>--}%
                                    %{--缴费账单--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        </ul>
                    </li>
                    %{--<li>--}%
                        %{--<div class="li"><span>金融服务</span></div>--}%
                        %{--<ul class="in_ul">--}%
                            %{--<a onclick="isReal('/user/queryCardSiteList')">--}%
                                %{--<li>--}%
                                    %{--便民卡预约--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        %{--</ul>--}%
                    %{--</li>--}%
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul">
                            %{--<a onclick="isReal('/user/queryCredit')" >--}%
                                %{--<li>--}%
                                    %{--雄关信用--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/user/queryScore')" >--}%
                                %{--<li>--}%
                                    %{--百合积分--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="toast()" >
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="toast()">
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- 水费缴纳content -->
    <div class="life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">品质保证</label>
                <label class="main_color">安全快捷</label>
                <a class="small_fontsize gray_color mar_left" href="/user/getGas">燃气费缴纳</a>
                <g:img dir="images" src="/index/20.png"/>
                <label class="small_fontsize">添加燃气费账户</label>
            </label>
        </div>
        <div>
            <form class="add_water" id="add_gas_form">
                <div class="margin_top_large">
                    <label class="large_fontsize input_title">缴费单位:</label>
                    <select class="form_input btn_div" id="selbranch">
                        <g:each in="${data}">
                            <option value="${it.merchantId}" bankAccNo="${it.bankAccNo}">${it.merchantName}</option>
                        </g:each>
                    </select>
                </div>
                <div class="margin_top_large">
                    <label class="large_fontsize input_title">户号:</label>
                    <input placeholder="请输入户号" name="userNp" id="userNp" class="form_input btn_div"/>
                    <a class="main_color add_water_forget_txt"onclick="isReal('/user/forgetGas')">忘记户号？</a>
                </div>
                <div class="margin_top_large">
                    <label class="large_fontsize input_title">户主姓名:</label>
                    <input placeholder="请输入户主姓名" name="name" id="name" class="form_input btn_div"/>
                </div>
                <div class="margin_top_large">
                    <label class="large_fontsize input_title">账户别名:</label>
                    <input placeholder="给账户起个名称，方便记忆" name="alisName" id="alisName" class="form_input btn_div"/>
                </div>
                <input type="submit" value="添加" class="form_submit_water margin_top_largemore"/>
            </form>
        </div>
    </div>
</div>

<script>
    $("#add_gas_form").validate({
        rules: {
            userNp: {
                required: true,
                maxlength: 30,
                stringCheck: true
            },
            name: {
                required: true,
                rangelength: [2,4],
                chinese: true
            },
            alisName: {
                required: true,
                maxlength: 10,
                stringCheck: true
            }
        },
        messages: {
            userNp: {
                required: "请填写户号",
                maxlength: "户号不能超过30位"
            },
            name: {
                required: "请填写户主名字",
                rangelength: "户主名字长度应该为2-4位"
            },
            alisName: {
                required: "请填写账户别名",
                maxlength: "账户别名长度不能超过10"
            }
        },
        submitHandler: function (form) {
            console.info($("#userNp").val());
            $.ajax({
                url: '/user/bindGas',
                type: 'post',
                dataType: 'json',
                data: {
                    'userNp': $("#userNp").val(),
                    'name': $("#name").val(),
                    'alisName': $("#alisName").val(),
                    'branchguid': $("#selbranch").find("option:selected").attr("bankAccNo"),
                    'branchgname':$("#selbranch").find("option:selected").text()
                },
                success: function (data) {
                    console.info(data);
                    if(data.errorcode==0){
                        layer.msg('添加成功');
                        isReal('/user/getGas');
                    }else{
                        layer.msg(data.errormsg);
                    }
                }
            })
        }
    });
</script>
</body>
</html>