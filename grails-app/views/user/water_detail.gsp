<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>
<body>
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul ul_active">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')" class="active">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            %{--<a onclick="isReal('/user/queryOils')">--}%
                                %{--<li>--}%
                                    %{--加油卡充值--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/queryBill')" >--}%
                                %{--<li>--}%
                                    %{--缴费账单--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        </ul>
                    </li>
                    %{--<li>--}%
                        %{--<div class="li"><span>金融服务</span></div>--}%
                        %{--<ul class="in_ul">--}%
                            %{--<a onclick="isReal('/user/queryCardSiteList')">--}%
                                %{--<li>--}%
                                    %{--便民卡预约--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        %{--</ul>--}%
                    %{--</li>--}%
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul">
                            %{--<a onclick="isReal('/user/queryCredit')" >--}%
                                %{--<li>--}%
                                    %{--雄关信用--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/user/queryScore')" >--}%
                                %{--<li>--}%
                                    %{--百合积分--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="toast()">
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="toast()">
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- 水费缴纳content -->
    <div class="life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">品质保证</label>
                <label class="main_color">安全快捷</label>
                <a class="small_fontsize gray_color mar_left" href="/user/getWater">水费缴纳</a>
                <g:img dir="images" src="/index/20.png"/>
                <label class="small_fontsize">账户信息</label>
            </label>
        </div>
        <div>
            <g:if test="${data}">
            <form class="gray_color br_div" id="water_form">
                <div class="margin_top_large">
                    <label class="large_fontsize water_info_title">缴费单位:</label>
                    <label class="large_fontsize div_float_right">${data.branchGname}</label>
                </div>
                <div class="margin_top_middlel">
                    <label class="large_fontsize water_info_title">户号:</label>
                    <label class="large_fontsize div_float_right">${data.userNo}</label>
                </div>
                <div class="margin_top_middlel">
                    <label class="large_fontsize water_info_title">户主姓名:</label>
                    <label class="large_fontsize div_float_right">${data.name}</label>
                </div>
                <div class="margin_top_middlel">
                    <label class="large_fontsize water_info_title">户主身份证号码:</label>
                    <label class="large_fontsize div_float_right">${data.certNo.split("")[0]}***********${data.certNo.split("").last()}</label>
                </div>
                <div class="margin_top_middlel">
                    <label class="large_fontsize water_info_title">地址:</label>
                    <label class="large_fontsize div_float_right">${data.address}</label>
                </div>
                %{--<div class="margin_top_middlel">--}%
                    %{--<label class="large_fontsize water_info_title">可用余额:</label>--}%
                    %{--<label class="large_fontsize div_float_right">${data.watervalue}</label>--}%
                %{--</div>--}%
                <div class="margin_top_middlel">
                    <label class="large_fontsize input_title div_float_left">充值面值:</label>
                    <div class="form_input btn_div br_div recharge_phone_form_input" style="position: relative;margin-left: 30px;">
                        <div class="height_lose">
                            <div class="recharge_phone_div">500元</div>
                            <div class="recharge_phone_div div">200元</div>
                            <div class="recharge_phone_div">100元</div>
                        </div>
                        <div class="margin_top_middlel height_lose">
                            <div class="recharge_phone_div">50元</div>
                            <div class="recharge_phone_div div">30元</div>
                            <div class="recharge_phone_div">20元</div>
                        </div>
                    </div>
                    <!--<label></label>-->
                </div>
                <input type="hidden" value="${data.userNo}" name="userno"/>
                <input type="hidden" value="${data.branchGuid}" name="branchguid"/>
                <input type="hidden" value="${data.odertype}" name="odertype"/>
                <div class="margin_top_middlel water_info_div_amount relative_position">
                    <label class="large_fontsize main_color">金额：</label>
                    <input placeholder="请输入充值金额" class="water_info_input recharge_amount" name="payment" maxlength="8"/>
                </div>
                <input type="submit" value="立即缴费" class="form_submit_water margin_top_large"/>
            </form>
            </g:if>
        </div>
    </div>
</div>
<script>
    $("#water_form").validate({
        rules: {
            payment: {
                required: true,
                dotNumber: true
            }
        },
        messages: {
            payment: {
                required: "请输入缴费金额",
            }
        },
        submitHandler: function (form) {
                $.post('/user/waterRecharge', $("#water_form").serialize(),
                    function (json) {
                    if(json.code == 0){
                        // window.location.href = "https://etest.lzbank.com:20028/ccweb/samlsso?ChangeVal=mainPageSAML&spid=p3depay&method="+json.method
                        window.location.href = "http://192.168.2.206:8879/pay/gateWay?orderId="+json.orderno
                    }else {
                        layer.msg(json.errormsg)
                    }
                    })
            }
    });
</script>
</body>
</html>