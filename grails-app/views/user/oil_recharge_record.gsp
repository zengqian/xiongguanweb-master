<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>
<body>
<!--中心内容-->
<div class="body height_lose personal_body relative_position">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul ul_active">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryOils')" class="active">
                                <li>
                                    加油卡充值
                                </li>
                            </a>
                            <a onclick="isReal('/queryBill')">
                                <li>
                                    缴费账单
                                </li>
                            </a>
                        </ul>
                    </li>
                    %{--<li>--}%
                        %{--<div class="li"><span>金融服务</span></div>--}%
                        %{--<ul class="in_ul">--}%
                            %{--<a onclick="isReal('/user/queryCardSiteList')">--}%
                                %{--<li>--}%
                                    %{--便民卡预约--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        %{--</ul>--}%
                    %{--</li>--}%
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul">
                            %{--<a onclick="isReal('/user/queryCredit')">--}%
                                %{--<li>--}%
                                    %{--雄关信用--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/user/queryScore')">--}%
                                %{--<li>--}%
                                    %{--百合积分--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="toast()">
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="toast()">
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- 水费缴纳content -->
    <div class="life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">品质保证</label>
                <label class="main_color">安全快捷</label>
                <a class="small_fontsize gray_color mar_left" onclick="isReal('/user/queryOils')">加油卡充值</a>
                <g:img dir="images" src="/index/20.png"/>
                <label class="small_fontsize">充值记录</label>
            </label>
        </div>
        <ul class="oil_recharge_record aaa" id="records_ul">
            %{--<li class="gray_color">
                <div class="br_div align_center">
                    <g:img dir="images" src="/life/43.png"/>
                    <div>中国石化</div>
                </div>
                <label class="br_div oil_record_txt">
                    <label class="lt_gray_color" style="line-height: 55px;">卡号：123123123123123123</label>
                    <p>订单编号：1231231231231231231231231</p>
                </label>
                <div class="oil_record_info">
                    <label class="">充值日期：2018年11月30日</label>
                    <label class="main_color">金额：200元</label>
                    <label class="main_color">充值成功</label>
                </div>
            </li>--}%
        </ul>
        <form>
            <input id="total" type="hidden" name="total" />
            <input id="page" type="hidden" name="page" />
        </form>
    </div>
    <!-- 分页按钮 -->
    <div class="pagination">
        <button class="prev_page">上一页</button>

        <div class="number" id="number">
        </div>
        <button class="next_page">下一页</button>
        <button class="last_page">尾页</button>
    </div>
</div>
<asset:javascript src="pagination.js"/>
<script>
    var path="";
    //进入页面之后局部加载获得数据
    $().ready(function () {
        //订单查询
        path ='/user/findOilsRecords';
        getRecords(path);
       //createtd('中石油','1000111100008304493','2018120617325300000005701','2018-12-06 17:31:45','200','充值成功');
    })
    function getRecords(path) {
        $.post(path,{page:function () {
                    return $("#page").val();
                }},
            function (json) {
                if(json.errorcode == 0){
                    $("#records_ul").html("");//查询之前先清空table
                    list  = json.data.list;
                    for(var i =0;i<list.length;i++){
                            switch (list[i].orderType) {
                                case "41001":
                                    list[i].orderType = "中石化";
                                    break;
                                case "41002":
                                    list[i].orderType = "中石油";
                                    break;
                                default:
                                    list[i].orderType = "其他";
                                    break;
                            }
                            switch (list[i].status) {
                                case "01":
                                    list[i].status = "待支付";
                                    break;
                                case "02":
                                    list[i].status = "处理中";
                                    break;
                                case "05":
                                    list[i].status = "充值成功";
                                    break;
                                case "06":
                                    list[i].status = "订单取消";
                                    break;
                                default:
                                    list[i].status = "其他";
                                    break;
                            }
                            //type类型，cardno卡号，orderno订单编号，recdate充值时间，amt充值金额，state充值状态（默认查出来都是成功的）
                            createtd(list[i].orderType,list[i].userNp,list[i].orderId,list[i].paymentTime,list[i].payment,'充值成功');
                    }
                    $("#total").val(json.data.totalRow);//设值总条数
                    $("#page").val(json.page);//设值页码
                    $.firstinit(json.data.totalRow, json.page, function () { //默认每页十条数据
                        getRecords(path);
                    })
                }else {
                    layer.msg("请求失败");
                }
            });
    }
    //type类型，cardno卡号，orderno订单编号，recdate充值时间，amt充值金额，state充值状态
    function createtd(type,cardno,orderno,recdate,amt,state) {
        var li = document.createElement("li");li.setAttribute('class','gray_color');
        var div1 = document.createElement("div");div1.setAttribute('class','br_div align_center');
        var img = document.createElement("img");img.setAttribute('src','../assets/life/43.png');
        var div2 = document.createElement("div");div2.innerHTML = type;
        var label1 = document.createElement("label");label1.setAttribute('class','br_div oil_record_txt');
        var label2 = document.createElement("label");label2.setAttribute('class','lt_gray_color');label2.setAttribute('style','line-height: 55px;');label2.innerHTML='卡号:'+cardno;
        var p = document.createElement("p");p.innerHTML ='订单编号：'+orderno;
        var div3 = document.createElement("div");div3.setAttribute('class','oil_record_info');
        var label3 = document.createElement("label");label3.innerHTML ='充值日期：'+recdate;
        var label4 = document.createElement("label");label4.setAttribute('class','main_color');label4.innerHTML ='金额：'+amt;
        var label5 = document.createElement("label");label5.setAttribute('class','main_color');label5.innerHTML =state;
        div1.append(img);div1.append(div2);
        label1.append(label2);label1.append(p);
        div3.append(label3);div3.append(label4);div3.append(label5);
        li.append(div1);li.append(label1);li.append(div3);
        $("#records_ul").append(li);
    }
</script>
</body>
</html>