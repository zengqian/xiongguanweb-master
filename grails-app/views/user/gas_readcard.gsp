<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>

<body>
<OBJECT id=WebCtl align="CENTER" WIDTH=0 HEIGHT=0 codeBase=/assets/JygWebActiveX.CAB#version=1,0,0,3
        classid=CLSID:0156D719-1F9D-4ED9-99B2-0BA4BD02AB05></OBJECT>

<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul ul_active">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first" class="active">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            %{--<a onclick="isReal('/user/queryOils')">--}%
                                %{--<li>--}%
                                    %{--加油卡充值--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/queryBill')">--}%
                                %{--<li>--}%
                                    %{--缴费账单--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        </ul>
                    </li>
                    %{--<li>--}%
                        %{--<div class="li"><span>金融服务</span></div>--}%
                        %{--<ul class="in_ul">--}%
                            %{--<a onclick="isReal('/user/queryCardSiteList')">--}%
                                %{--<li>--}%
                                    %{--便民卡预约--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        %{--</ul>--}%
                    %{--</li>--}%
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul">
                            %{--<a onclick="isReal('/user/queryCredit')">--}%
                                %{--<li>--}%
                                    %{--雄关信用--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/user/queryScore')">--}%
                                %{--<li>--}%
                                    %{--百合积分--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="toast()">
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="toast()">
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- 水费缴纳content -->
    <div class="life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">品质保证</label>
                <label class="main_color">安全快捷</label>
                <label class="small_fontsize gray_color mar_left">燃气费缴纳</label>
            </label>
        </div>

        <div class="margin_top_large relative_position">
            <div class="reacard_animate align_center">
                <g:img dir="images" src="/life/redacard.png" style="width: 90px;height: 130px;margin-top: 20px;"/>
                <div>
                <g:img dir="images" src="/life/card.png" class="relative_position card_annimate"/>
                </div>
            </div>
            <div class="read_card_div">
                <p class="align_left margin_form_middle">
                    1.请插入燃气卡,并点击“读卡”按钮进行读卡
                </p>

                <p class="align_left">
                    2.请不要在充值过程中拔出卡片,以免造成写卡错误
                </p>
                <g:if test="${data.cardType == '1'}">
                    <a class="read_card_a" onclick="Read_GHRQ_CQSCCard()">读卡</a>
                </g:if>
                <g:if test="${data.cardType == '2'}">
                    <a class="read_card_a" onclick="Read_GHRQ_DLXCard()">读卡</a>
                </g:if>
                <g:if test="${data.cardType == '3'}">
                    <a class="read_card_a" onclick="Read_GHRQ_NJYHCard()">读卡</a>
                </g:if>
            </div>
            <!--读卡-->
            <div class="gas_card_info form1">
                <form id="gas_info_form" class="br_div">
                    <div class="gas_info_top_div">
                        <div class="">
                            <label class="large_fontsize input_title gas_info_label">燃气卡号:</label>
                            <input placeholder="" class="form_input btn_div" name="cardno" disabled/>
                            <input type="hidden" class="form_input btn_div" name="cardno" maxlength="20"/>
                        </div>
                        <g:if test="${data.cardType == '1'|| data.cardType == '3'}">
                            <div class="margin_top_middlel">
                                <label class="large_fontsize input_title gas_info_label">上次购气日期:</label>
                                <input placeholder="" class="form_input btn_div" name="bugdate" disabled/>
                                <input type="hidden" class="form_input btn_div" name="bugdate"/>
                            </div>
                        </g:if>
                        <g:if test="${data.cardType == '2'}">
                            <div class="margin_top_middlel">
                                <label class="large_fontsize input_title gas_info_label">表号:</label>
                                <input placeholder="" class="form_input btn_div" name="CustomerId" disabled/>
                                <input type="hidden" class="form_input btn_div" name="CustomerId"/>
                            </div>
                        </g:if>
                        <div class="margin_top_middlel">
                            <label class="large_fontsize input_title gas_info_label">卡内燃气余量:</label>
                            <input class="form_input btn_div" name="gasvalue" disabled/>
                            <input type="hidden" class="form_input btn_div" name="gasvalue"/>
                        </div>

                        <div class="margin_top_middlel">
                            <label class="large_fontsize input_title gas_info_label">购气次数:</label>
                            <input placeholder="" class="form_input btn_div" name="bugtimes" disabled/>
                            <input type="hidden" class="form_input btn_div" name="bugtimes"/>
                        </div>

                    </div>
                    <input type="hidden" name="salepointId"/>
                    <input type="hidden" name="operateId"/>
                    %{--<div class="margin_top_middlel relative_position">--}%
                    %{--<label class="large_fontsize input_title gas_info_label rechare_radio_label">--}%
                    %{--<input type="radio" name="rechargetype" class="rechare_radio"/>--}%
                    %{--<label>按金额充值:</label>--}%
                    %{--</label>--}%
                    %{--<input placeholder="" class="form_input btn_div" disabled id="gas_money"/>--}%
                    %{--<label class="gas_recharge_unit">元</label>--}%
                    %{--</div>--}%
                    <div class="margin_top_middlel">
                        <label class="large_fontsize input_title gas_info_label div_float_left">充值气量:</label>
                        <div class="form_input br_div recharge_phone_form_input" style="position: relative;margin-left: 6px;">
                            <div class="height_lose">
                                <div class="recharge_phone_div">500</div>
                                <div class="recharge_phone_div div">200</div>
                                <div class="recharge_phone_div">100</div>
                            </div>
                            <div class="margin_top_middlel height_lose">
                                <div class="recharge_phone_div">50</div>
                                <div class="recharge_phone_div div">30</div>
                                <div class="recharge_phone_div">20</div>
                            </div>
                        </div>
                        <!--<label></label>-->
                    </div>
                    <div class="margin_top_middlel relative_position gas_error">
                        <label class="large_fontsize input_title gas_info_label">
                            %{--<input type="radio" name="rechargetype" class="rechare_radio" id="gas_value"/>--}%
                            <label>按气量充值:</label>
                        </label>
                        <input placeholder="" class="form_input btn_div recharge_amount" name="gasamount" id="gasamount" maxlength="7"/>
                        <label class="gas_recharge_unit">立方米</label>
                    </div>
                    <a class="form_submit_water margin_top_largemore br_div prev_btn"
                       href="/user/getHuiNengGasList">上一步</a>
                    <input type="submit" value="下一步"
                           class="form_submit_water margin_top_largemore div_float_right next_btn"/>
                </form>
            </div>
            <!--卡信息-->

            <div class="gas_card_info form2" style="display: none;">
                <form id="gas_info_form2" class="gray_color">
                    <div>
                        <label class="large_fontsize water_info_title align_right">用户编号:</label>
                        <label class="large_fontsize gas_recharge_info_label" id="useno"></label>
                    </div>

                    <div class="margin_form_middle height_lose">
                        <label class="large_fontsize water_info_title align_right">用户地址:</label>
                        <label class="large_fontsize gas_recharge_info_label" id="address"></label>
                    </div>

                    <div class="margin_form_middle">
                        <label class="large_fontsize water_info_title align_right">本次购气量:</label>
                        <label class="large_fontsize gas_recharge_info_label" id="gasvalue"></label>
                    </div>

                    <div class="margin_form_middle">
                        <label class="large_fontsize water_info_title align_right">费用合计:</label>
                        <label class="large_fontsize gas_recharge_info_label" id="totBal"></label>
                    </div>

                    <div class="margin_form_middle gas_discount_div">
                        <label class="large_fontsize water_info_title align_right">优惠:</label>
                        <label class="large_fontsize gas_recharge_info_label" id="PrefAmt"></label>
                    </div>

                    <div class="margin_form_middle">
                        <label class="large_fontsize water_info_title align_right">应缴:</label>
                        <label class="more_middle_fontsize gas_recharge_info_label main_color" id="AssAmt"></label>
                    </div>

                    <div>
                        <a class="form_submit_water margin_top_largemore br_div prev_btn" onclick="show1()">上一步</a>
                        <input type="submit" value="下一步"
                               class="form_submit_water margin_top_largemore div_float_right next_btn"/>
                    </div>
                    <input type="hidden" name="cardType" id="cardType" value="${data.cardType}"/>
                    <input type="hidden" name="cardno"/>
                    <input type="hidden" name="userno"/>
                </form>
            </div>
        </div>
    </div>
</div>
<script>


    $("#gas_info_form").validate({
        rules: {
            gasamount: {
                required: true,
                dotNumber: true
            },
            cardno: {
                required: true,
                number: true
            },
            gasvalue: {
                required: true
            },
            bugtimes: {
                required: true
            }
        },
        messages: {
            gasamount: {
                required: "请输入需要充值的气量",
            }
        },
        submitHandler: function (form) {
            // if()
            $.post('/user/queryGasInfo', $("#gas_info_form").serialize(),
                function (json) {
                    console.log(json)
                    if (json.errorcode == 0) {
                        $("#useno").text(json.data.UserNo)
                        $("#address").text(json.data.Address)
                        $("#gasvalue").text($("#gasamount").val())
                        $("#totBal").text(json.data.TotBal)
                        $("#PrefAmt").text(json.data.PrefAmt)
                        $("#AssAmt").text(json.data.AssAmt)
                        $(".form1").css("display","none")
                        $(".form2").show()
                        $(".read_card_a").toggle()
                    } else {
                        layer.msg(json.errormsg)
                    }
                })
        }
    });

    $("#gas_info_form2").validate({
        submitHandler: function (form) {
            $.post('/user/gasorder', {"gasamount": $("#gasamount").val(),"cardType": $("#cardType").val()},
                function (json) {
                    if (json.code == 0) {
                        // window.open("https://etest.lzbank.com:20028/ccweb/samlsso?ChangeVal=mainPageSAML&spid=p3depay&method=" + json.method)
                        window.location.href = "http://192.168.2.206:8879/pay/gateWay?orderId="+json.orderno
                    } else {
                        layer.msg(json.errormsg)
                    }
                })
        }
    });

    function show1() {
        $(".form1").css("display","inline-block")
        $(".form2").hide()
        $(".read_card_a").toggle()
    }
</script>
</body>
</html>