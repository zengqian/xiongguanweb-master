<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="convenienceCard"/>
</head>
<body>
<!--中心内容-->
<div class="about_card">
    <div>
        <label class="title_middle_fontsize">关于便民卡</label>
        <div class="title_to">
            <label>当前位置：</label>
            <a href="/about_convenience_card">资讯中心</a>
            <label>></label>
            <a href="/about_convenience_card" class="old_color">关于便民卡</a>
        </div>
        <div class="img_div"><g:img dir="images" src="/index/16.png"/></div>
    </div>
    <div class="margin_top_large align_center news_detail">

        <label class="title_large_fontsize convenience_title" id="yhgb">便民卡简介</label>
        <p class="large_fontsize lt_gray_color margin_form_middle align_left">

            “雄关便民卡”是城市现代化文明的重要标志，以政府主导、市场化运作的“民心工程”。优化城市社会公共服务流程，实现跨地区、跨部门的资源整合与共享，促进与百姓生活息息相关的信息消费。融合城市交通、金融、商业、教育、社保、卫生、旅游、社区等领域的社会资源，提升城市整体品位，推动数字化宜居的新型智慧城市发展，树立让“城市生活更便捷”的城市新形象。
        </p>

        <label class="title_large_fontsize convenience_title" id="bvun">嘉峪关市智慧雄关科技有限责任公司简介</label>
        <p class="large_fontsize lt_gray_color margin_form_middle align_left">
            嘉峪关市智慧雄关信息科技有限责任公司成立于2018年5月30日，地址：嘉峪关市新华南路2108C栋二层,注册资金2000万元，隶属于嘉峪关市政府国资委的国有控股混合制企业。主要从事计算机网络领域内的项目运营、技术开发、服务、推广及信息系统集成；智能化系统设备的运行维护服务；通信工程;智能化工程;安防工程；电子商务；文化艺术交流策划；旅游产品开发、批发零售。是一家以大数据民生服务开发应用为主业的互联网创新型企业。致力于推进大数据与用户日常生活、社会公共服务等深度融合，运用大数据促进保障和改善民生，实现“智慧雄关”建设目标。<br>
            公司联系方式：5976215
        </p>
    </div>
</div>
</body>
</html>