<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>

<body>
<OBJECT id=WebCtl align="CENTER" WIDTH=0 HEIGHT=0 codeBase=http://172.30.215.44:5432/JygWebActiveX.CAB#version=1,0,0,2
        classid=CLSID:0156D719-1F9D-4ED9-99B2-0BA4BD02AB05></OBJECT>
<!--中心内容-->
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul ul_active">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first" class="active">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            %{--<a onclick="isReal('/user/queryOils')">--}%
                                %{--<li>--}%
                                    %{--加油卡充值--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/queryBill')">--}%
                                %{--<li>--}%
                                    %{--缴费账单--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span>金融服务</span></div>
                        <ul class="in_ul">
                            <a onclick="isReal('/user/queryCardSiteList')">
                                <li>
                                    便民卡预约
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul">
                            %{--<a onclick="isReal('/user/queryCredit')">--}%
                                %{--<li>--}%
                                    %{--雄关信用--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/user/queryScore')">--}%
                                %{--<li>--}%
                                    %{--百合积分--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="toast()">
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="toast()">
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- 水费缴纳content -->
    <div class="life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">品质保证</label>
                <label class="main_color">安全快捷</label>
                <label class="small_fontsize gray_color mar_left">燃气费缴纳</label>
            </label>
        </div>

        <div class="margin_top_large recharging relative_position">
            <div class="reacard_animate align_center">
                <g:img dir="images" src="/life/redacard.png" style="width: 90px;height: 130px;margin-top: 20px;"/>
                <div>
                    <g:img dir="images" src="/life/card.png" class="relative_position card_annimate"/>
                </div>
            </div>
            <div class="read_card_div">
                <p class="align_left margin_form_middle">
                    1.请插入燃气卡,并点击“读卡”按钮进行读卡
                </p>

                <p class="align_left">
                    2.请不要在充值过程中拔出卡片,以免造成写卡错误
                </p>
            </div>

            <!--充值中-->
            <div class="gas_card_info gas_working_div">
                <g:img dir="images" src="/life/41.png" class="animate_image"/>
                <div class="yellow_color more_middle_fontsize margin_top_large">充值处理中</div>

                <div class="yellow_color more_middle_fontsize margin_form_middle">请勿拔卡或关闭页面</div>
            </div>
        </div>

        <div class="relative_position">
            <!--充值完成-->
            <div class="gas_card_info gas_working_div recharge_finish" style="display: none;">
                <g:img dir="images" src="/life/42.png" class=""/>
                <div class="lt_gray_color more_middle_fontsize margin_top_large">充值成功</div>

                <div class="lt_gray_color margin_form_middle small_fontsize">当前卡内燃气余量：
                    <label class="main_color more_middle_fontsize current_gasvalue"></label>
                </div>
                <a class="form_submit_water br_div margin_top_large" href="/gas_first">完成</a>
            </div>
            <!--充值失败-->
            <div class="gas_card_info gas_working_div recharge_fail" style="display: none;">
                <g:img dir="images" src="/life/47.png" class=""/>
                <div class="lt_gray_color more_middle_fontsize margin_top_large">充值失败</div>

                <div class="lt_gray_color margin_form_middle small_fontsize">当前卡内燃气余量：
                    <label class="main_color more_middle_fontsize current_gasvalue"></label>
                </div>
                <a class="form_submit_water br_div margin_top_large" href="/gas_first">完成</a>
            </div>

        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        setTimeout(function () {
            $.post('/user/getGasToken',
                function (json) {
                    if (json.errorcode == 0) {
                        switch (json.cardType) {
                            case "1":
                                Write_GHRQ_CQSCOrders(json.token, json.cardno, json.buytimes, json.gasvalue, json.orderid)
                                break;
                            case "2":
                                Write_GHRQ_DLXOrders(json.token, json.cardno, json.buytimes, json.gasvalue, json.orderid,json.CustomerId)
                                break;
                            case "3":
                                Write_GHRQ_NJYHOrders(json.token, json.cardno, json.gasvalue, json.orderid,json.operateId,json.salepointId)
                                break;
                        }
                    } else {
                        layer.msg(json.errormsg)
                        $(".recharging").toggle()
                        $(".recharge_fail").toggle()
                    }
                })
        }, 3000);
    })
</script>
</body>
</html>