<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>
<body>
<!--中心内容-->
<div class="body height_lose personal_body">
    <!-- 个人中心menu -->
    <div class="menu div_float_left personal_menu">
        <ul>
            <li>
                <div class="li"><span class="jiantou_active">个人中心</span></div>
                <ul class="ul_active">
                    <li>
                        <div class="li"><span>我的订单</span></div>
                        <ul class="in_ul">
                            <a href="#">
                                <li>
                                    全部订单
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    待付款
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已完成
                                </li>
                            </a>
                            <a href="#">
                                <li>
                                    已取消
                                </li>
                            </a>
                        </ul>
                    </li>
                    <li>
                        <div class="li"><span class="jiantou_active">生活缴费</span></div>
                        <ul class="in_ul ul_active">
                            <a onclick="isReal('/recharge_phone')">
                                <li>
                                    话费充值
                                </li>
                            </a>
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--电费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="toast()">--}%
                                %{--<li>--}%
                                    %{--取暖费缴纳--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="isReal('/user/getWater')">
                                <li>
                                    水费缴纳
                                </li>
                            </a>
                            <a href="/gas_first">
                                <li>
                                    燃气费缴纳
                                </li>
                            </a>
                            <a onclick="isReal('/fine_query')">
                                <li>
                                    违章罚款
                                </li>
                            </a>
                            <a onclick="isReal('/user/queryOils')" class="active">
                                <li>
                                    加油卡充值
                                </li>
                            </a>
                            <a onclick="isReal('/queryBill')">
                                <li>
                                    缴费账单
                                </li>
                            </a>
                        </ul>
                    </li>
                    %{--<li>--}%
                        %{--<div class="li"><span>金融服务</span></div>--}%
                        %{--<ul class="in_ul">--}%
                            %{--<a onclick="isReal('/user/queryCardSiteList')">--}%
                                %{--<li>--}%
                                    %{--便民卡预约--}%
                                %{--</li>--}%
                            %{--</a>--}%
                        %{--</ul>--}%
                    %{--</li>--}%
                    <li>
                        <div class="li"><span class="jiantou_active">公共服务</span></div>
                        <ul class="in_ul">
                            %{--<a onclick="isReal('/user/queryCredit')">--}%
                                %{--<li>--}%
                                    %{--雄关信用--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            %{--<a onclick="isReal('/user/queryScore')">--}%
                                %{--<li>--}%
                                    %{--百合积分--}%
                                %{--</li>--}%
                            %{--</a>--}%
                            <a onclick="toast()">
                                <li>
                                    图书馆
                                </li>
                            </a>
                            <a onclick="toast()">
                                <li>
                                    公积金
                                </li>
                            </a>
                            <a href="/about_museum">
                                <li>
                                    博物馆
                                </li>
                            </a>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
    <!-- 水费缴纳content -->
    <div class="life_content">
        <div class="life_header">
            <label class="life_title lt_gray_color large_fontsize">
                <g:img dir="images" src="/life/1.png" class="promise"/>
                <label class="main_color">品质保证</label>
                <label class="main_color">安全快捷</label>
                <a class="small_fontsize gray_color mar_left" onclick="isReal('/user/queryOils')">加油卡充值</a>
                <g:img dir="images" src="/index/20.png"/>
                <label class="small_fontsize">常见问题</label>
            </label>
        </div>
        <div class="aaa" style="height: 440px;overflow: auto;">
        <div class="small_fontsize margin_form_middle">
            <div>
                一、如何申请加油卡？
            </div>
            <div class="gray_color margin_top_middle">用户需要携带身份证到任一加油站发卡充值网点申请加油卡。</div>
        </div>
        <div class="small_fontsize margin_form_middle">
            <div>
                二、充值说明？
            </div>
            <div class="gray_color margin_top_middle">中石化加油卡仅支持状态正常的主卡充值，中石油加油卡仅支持个人记名卡充值：副卡、过期卡、损坏卡、挂失卡、司机卡及充值超限卡登均不支持代充服务。</div>
        </div>
        <div class="small_fontsize margin_form_middle">
            <div>
                三、充值成功后，还需要哪些操作？
            </div>
            <div class="gray_color margin_top_middle">充值成功后，需要到当地的中石油/中石化加油站自助机或柜台上办理圈存后方可使用。</div>
        </div>
        <div class="small_fontsize margin_form_middle">
            <div>
                四、什么是圈存？
            </div>
            <div class="gray_color margin_top_middle">通过线上充值的资金,先进入了系统后台的备用金账户，还未存入加油卡中。圈存后，备用金账户
                中的资金就存入了加油卡，才可以使用加油卡加油。</div>
        </div>
        <div class="small_fontsize margin_form_middle" style="line-height: 24px;">
            <div>
                五、发票说明
            </div>
            <div class="gray_color margin_top_middle">本服务为全国加油卡代充服务，代充服务不提供充值发票。</div>
        </div>
        <div class="small_fontsize margin_form_middle" style="line-height: 24px;">
            <div>
                六、延时到账说明
            </div>
            <div class="gray_color margin_top_middle">每日22:50至次日凌晨00:50为中石化夜间维护时间，充值将会延迟到账。中石油暂无固定维护时间。</div>
            <div class="gray_color">充值交易通常10分钟内会返回最终结果，如遇充值高峰时段，有可能会延长至30分钟</div>
        </div>
        <div class="small_fontsize margin_form_middle" style="line-height: 24px;">
            <div>
                七、其他说明
            </div>
            <div class="gray_color margin_top_middle">充值成功，不支持退款服务。</div>
            <div class="gray_color">如您对交易有疑问，请拨打江苏欧飞电子商务有限公司。</div>
        </div>
        </div>
    </div>
</div>
<script>
    $("#recharge_form").validate({
        // onkeyup: false,
        errorPlacement: function (error, element) {
            element.next("label.water_error_label").append(error);
            element.parent().parent(".select_ul").parent().next("label.water_error_label").append(error);
        },
        rules: {
            mobile_company: {
                required: true,
                minlength: 1
            },
            phone: {
                required: true
            },
            amount: {
                required: true
            }
        },
        messages: {
            mobile_company: {
                required: "请选择充值公司"
            },
            phone: {
                required: "请填写手机号"
            },
            amount: {
                required: "请输入充值面值或选择面值"
            }
        },
        submitHandler: function (form) {
            form.submit();
            validator.resetForm();
        }
    });
</script>
</body>
</html>