<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="userCenter"/>
</head>
<body>
<!--中心内容-->
<div class="dowl_content">
    <div class="dowl_div white_color">
        <g:img dir="images" src="phone.png"/>
        <div class="div_float_right margin_top_large">
            <label>智慧城市，云上雄关</label>
            <div class="margin_top_middle">生活缴费、交通出行、酒钢专用、旅游休闲、公共服务、打造全新生活</div>
            <div class="dowl_bottom">
                <div class="div_float_left">
                    <a class="android white_color" href="http://60.164.190.154:90/app/preview">
                        <span>Android版下载</span>
                    </a>
                    <div class="android_text margin_top_middle">
                        适合于Android 4.0及以上平台
                    </div>
                    <a class="ios margin_top_middlel white_color" href="http://60.164.190.154:90/app/preview">
                        <span>iPhone版下载</span>
                    </a>
                    <div class="margin_top_middle">
                        适用于IOS 9.0及以上平台
                    </div>
                </div>
                <g:img dir="images" src="dowl_scan.png" class="div_float_right"/>
            </div>
        </div>
    </div>
</div>
</body>
</html>