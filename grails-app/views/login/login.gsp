<!DOCTYPE html>
<html style="background-color: inherit">
<head>
    <link rel="shortcut icon" type="text/css" href="../assets/favicon.ico"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <asset:stylesheet src="comm.css"/>
    <asset:stylesheet src="foot.css"/>
    <asset:stylesheet src="objectStyle.css"/>
    <asset:javascript src="jquery-1.11.1.min.js"/>
    <asset:javascript src="layer/layer.js"/>
    <asset:javascript src="writeObject.js"/>
    <title>颐瑞互联网综合缴费平台</title>
</head>

<body class="root_body_login">
<div class="login_outdiv">
    <!--<img src="assets/images/index/login_back.png" style="width: 100%"/>-->
    <!--头部-->
    <div class="html_header">
        <a href="/">
            %{--<g:img dir="images" src="/index/white_logo.png" class="header_img"/>--}%
            <g:img dir="images" src="favicon.ico" class="header_img"/>
            <label style="margin-left: 15px;color: white;">颐瑞互联网综合缴费平台</label>
        </a>
        <ul class="index_ul">
            %{--<li><a href="/about_convenience_card">关于便民卡</a></li>--}%
            %{--<li><a href="/service_guide">服务指南</a></li>--}%
            %{--<li><a href="/convenience_card_application">便民卡应用</a></li>--}%
            %{--<li><a>公众号</a></li>--}%
            %{--<li><a href="/dow">应用下载</a></li>--}%
            %{--<li><a href="/businesslogin">商户中心</a></li>--}%
            <li><a href="/login">个人中心</a></li>
        </ul>
    </div>
    <!--登录框-->
    <div class="body height_lose">
        <div class="login_body">
            <div class="login_content">
                <!-- 账号登录/APP扫码登录-->
                <div>
                    <label class="title">账号登录</label>
                    <label class="div_float_right title">APP扫码登录
                    <g:img dir="images" src="/index/15.png"/>
                    </label>
                </div>
                <!--登录表单-->
                <form class="login_form" id="login_form" action="">
                    <div class="margin_top_login relative_position">
                        <input class="btn_div form_input" id="username" placeholder="请输入用户名/手机号码" name="username" maxlength="16"/>
                        %{--<script> writeEditObject("poweredit",{"entertype":"edit","width":250,"height":36,"accepts":"[:graph:]+"});</script>--}%
                        <div class="login_user"><g:img dir="images" src="/index/user.png"/></div>
                    </div>

                    <div class="margin_form_middle relative_position">
                        <input class="btn_div form_input" id="password" placeholder="请输入密码" name="password" type="password">
                        %{--<script type="text/javascript">writePassObject("powerpass", {--}%
                            %{--"width": 230,--}%
                            %{--"height": 22,--}%
                            %{--"accepts": "[0-9a-zA-Z]+"--}%
                        %{--});</script>--}%

                        <div class="login_user"><g:img dir="images" src="/index/pass.png"/></div>
                    </div>
                    <label class="passcontrol_prompt" id="pass"></label>

                    %{--<div class="margin_form_middle relative_position">--}%
                        %{--<input class="btn_div login_code" id="code" placeholder="请输入验证码" name="code" maxlength="4"/>--}%
                        %{--<img src="https://etest.lzbank.com:20028/ccweb/Page/ValidateCodetoXgbmk.jsp?d=' + Math.random();"--}%
                             %{--class="iframe_img" id="code_img"--}%
                             %{--style="display: inline-block;width: 100px;height: 36px;border: 1px solid #e9e9e9;position: absolute;right: 0;border-radius: 5px;cursor: pointer;"--}%
                             %{--onclick="this.src = 'https://etest.lzbank.com:20028/ccweb/Page/ValidateCodetoXgbmk.jsp?d=' + Math.random();">--}%
                    %{--</div>--}%

                    <div class="height_lose">
                        <a href=""
                           class="margin_top_middlel lt_gray_color div_float_left">忘记密码？</a>
                        <a href="" class="margin_top_middlel div_float_right main_color">没有账号？&nbsp;立即注册</a>
                    </div>
                    <div class="relative_position align_center">
                        <g:img dir="images" src="loading.png" class="loading"/>
                        <input type="submit" class="login_sumbit margin_top_large " value="登录"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--footer-->
    <div class="footer login_footer">
        <div class="foot_body">
            <div>
                %{--<g:img dir="images" src="/foot/wechat.png"/>--}%
                <g:img dir="images" src="gzh.png" style="width: 90px;height: 90px;"/>

                <div class="foot_txt">微信公众号</div>
            </div>

            <div>
                <g:img dir="images" src="/foot/app.png"/>

                <div class="foot_txt">APP应用下载</div>
            </div>

            %{--<p>--}%
                %{--嘉峪关，号称“天下第一雄关”，位于甘肃省嘉峪关市西5千米处最狭窄的山谷中部，城关两侧的城墙横穿沙漠戈壁，北连黑山悬壁长城，南接天下第一墩，是明长城最西端的关口，历史上曾被称为河西咽喉，因地势险要，建筑雄伟，有连陲锁钥之称。嘉峪关是古代“丝绸之路”的交通要塞，中国长城三大奇观之一（东有山海关、中有镇北台、西有嘉峪关）。--}%
            %{--</p>--}%
        </div>

        %{--<div class="foot_body">&copy;2012-2018嘉峪关xxxxx公司甘ICP备18043664号-18</div>--}%
    </div>
</div>
<asset:javascript src="jquery.validate.min.js"/>
<script>
    var ts = "<%=System.currentTimeMillis()%>";
    /*个人注册第一步*/
    $("#login_form").validate({
        rules: {
            username: {
                required: true
            },
            password: {
                required: true
            }
        },
        messages: {
            username: {
                required: "请输入用户名或手机号"
            },
            password: {
                required: "请输入用户名密码"
            }
        },
        submitHandler: function (form) {
            logindis();
            login();
        }
    });
    function updatecode() {
        $("#code_img").attr("src", "https://etest.lzbank.com:20028/ccweb/Page/ValidateCodetoXgbmk.jsp?d=" + Math.random());
    }

    function login() {
        $.post("/login/authToken",$("#login_form").serialize(), function (json) {
            if (json.errorcode == 0) {
                $(location).attr('href', '/user/userInfo');
            } else {
                loginactive();
                layer.msg(json.errormsg);
            }
        });
    }

    function logindis() {
        $(".loading").toggle()
        $("#username").attr("readonly", true)
        $("input[type=submit]").attr("disabled", true)
    }

    function loginactive() {
        $(".loading").toggle()
        $("#username").attr("readonly", false)
        $("input[type=submit]").attr("disabled", false)
    }
</script>
</body>
</html>
