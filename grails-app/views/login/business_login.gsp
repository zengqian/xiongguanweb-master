<!DOCTYPE html>
<html style="background-color: inherit">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <asset:stylesheet src="comm.css"/>
    <asset:stylesheet src="foot.css"/>
</head>
<body class="root_body_businesslogin">
<div style="">
    <!--<img src="assets/images/index/login_back.png" style="width: 100%"/>-->
    <!--头部-->
    <div class="html_header">
        <a href="/">
            <g:img dir="images" src="/index/logo_white.png" class="header_img"/>
        </a>
        <a href="/" class="white_color div_float_right a_to_index">网站首页</a>
    </div>
    <!--登录框-->
    <div class="body height_lose">
        <div class="login_body">
        <div class="login_content">
            <!-- 账号登录/APP扫码登录-->
            <div>
                <label class="title main_color">商家登录</label>
                </label>
            </div>
            <div class="lt_gray_color margin_form_middle">请使用分配的商家编号及登录密码进行登录</div>
            <!--登录表单-->
            <form class="login_form" id="login_form">
                <div class="margin_top_login relative_position">
                    <input class="btn_div form_input" placeholder="请输入商家编号" name="username"/>
                    <div class="login_user"><g:img dir="images" src="/index/user.png"/></div>
                </div>
                <div class="margin_top_login relative_position">
                    <input class="btn_div form_input" placeholder="请输入密码" name="pass"/>
                    <div class="login_user"><g:img dir="images" src="/index/pass.png"/></div>
                </div>
                <input type="submit" class="login_sumbit margin_top_large" value="登录"/>
                <div>
                    <a href="" class="margin_top_middlel lt_gray_color div_float_left">忘记商家编号或密码</a>
                <a href="register.html" class="margin_top_middlel div_float_right main_color">新商家注册</a>
                </div>
            </form>
        </div>
        </div>
    </div>
    <!--footer-->
    <div class="footer login_footer">
        <div class="foot_body">
            <div>
                <g:img dir="images" src="/foot/wechat.png"/>
                <div class="foot_txt">微信公众号</div>
            </div>
            <div>
                <g:img dir="images" src="/foot/app.png"/>
                <div class="foot_txt">APP应用下载</div>
            </div>
            <p>
                嘉峪关，号称“天下第一雄关”，位于甘肃省嘉峪关市西5千米处最狭窄的山谷中部，城关两侧的城墙横穿沙漠戈壁，北连黑山悬壁长城，南接天下第一墩，是明长城最西端的关口，历史上曾被称为河西咽喉，因地势险要，建筑雄伟，有连陲锁钥之称。嘉峪关是古代“丝绸之路”的交通要塞，中国长城三大奇观之一（东有山海关、中有镇北台、西有嘉峪关）。
            </p>
        </div>
        <div class="foot_body">&copy;2012-2018嘉峪关xxxxx公司甘ICP备18043664号-18</div>
    </div>
</div>
<asset:javascript src="jquery-1.11.1.min.js"/>
<asset:javascript src="jquery.validate.min.js"/>
<script>
    /*个人注册第一步*/
    $("#login_form").validate({
        rules: {
            username: {
                required: true
            },
            pass: {
                required: true
            }
        },
        messages: {
            username: {
                required: "请输入商家编号"
            },
            pass: {
                required: "请输入商户密码"
            }
        },
        submitHandler: function (form) {
            form.submit();
            validator.resetForm();
        }
    });
</script>

</body>
</html>